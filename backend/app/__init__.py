# Modulos de Python por defecto
from os import path,environ
from collections import defaultdict

# Modulos de Flask
from flask import Flask, g
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base
from flask_restful import Resource, Api

# Modulos de config general
from config import config
from app.db import db
from sqlalchemy import event
from app.createDefaultRows import createDefault

# Modelos de la DB
from app.models.paciente import Paciente as PacienteModel
from app.models.estudio import Estudio as EstudioModel
from app.models.estudio_disincronia import EstudioDisincronia as EstudioDisincroniaModel
from app.models.estudio_2d_doppler import EstudioDoppler as EstudioDopplerModel
from app.models.estudio_congenitas import EstudioCongenitas as EstudioCongenitasModel
from app.models.estudio_salina_agitada import EstudioSalinaAgitada as EstudioSalinaAgitadaModel


# Resources para la API
from app.resources import user as userAPI
from app.resources import paciente as pacienteAPI
from app.resources import obraSocial as obraSocialAPI
from app.resources import estudio as estudioAPI
from app.resources import conclusion_predefinida as conclusionPredefinidaAPI
#from app.resources import desc_predefinida as descripcionPredefinidaAPI
from app.resources import diagnostico as diagnosticoAPI
from app.resources import especialista as especialistaAPI
from app.resources import motivo as motivoAPI
from app.resources import tipo_estudio as tipoEstudioAPI
from app.resources import estadisticas as statsAPI

# CORS
from flask_cors import CORS

# Waitress
from waitress import serve


def create_app(environment="development"):

    #Configuración inicial de la app
    app = Flask(__name__)
    api = Api(app)

    #CORS
    cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

    #Carga de la configuración
    env = environ.get("FLASK_ENV", environment)
    app.config.from_object(config[env])

    #Configuración DB
    db.init_app(app)

    

    @app.route('/updatedb')
    def update_db():
        db.create_all()
        return 'Database Updated'

    @app.route('/create_default')
    def create_default():
        createDefault()
        return 'Default rows created'


    @event.listens_for(EstudioModel, 'mapper_configured')
    def receive_mapper_configured(mapper, class_):    
        mapper.polymorphic_map = defaultdict(lambda: mapper, mapper.polymorphic_map)

        # to prevent 'incompatible polymorphic identity' warning, not mandatory
        mapper._validate_polymorphic_identity = None
       

    # Rutas API

    # User
    api.add_resource(userAPI.Login, '/api/user/login')

    # Paciente
    api.add_resource(pacienteAPI.Paciente, '/api/paciente')
    api.add_resource(pacienteAPI.AllPacientes, '/api/paciente/all')
    api.add_resource(pacienteAPI.SearchPacientes, '/api/paciente/all/search')
    api.add_resource(pacienteAPI.AllEstudios, '/api/paciente/<id_paciente>/estudios')

    # Estudio
    api.add_resource(estudioAPI.Estudio, '/api/estudio')

    # Obra Social
    api.add_resource(obraSocialAPI.ObraSocial, '/api/obra_social')
    api.add_resource(obraSocialAPI.AllPacientesObra, '/api/obra_social/<id_obra_social>/pacientes')
    
    # Conclusion
    api.add_resource(conclusionPredefinidaAPI.Conclusion, '/api/conclusion')

    # Descripcion
    #api.add_resource(descripcionPredefinidaAPI.DescripcionPredefinida, '/api/descripcion')

    # Diagnostico
    api.add_resource(diagnosticoAPI.Diagnostico, '/api/diagnostico')

    # Especialista
    api.add_resource(especialistaAPI.Especialista, '/api/especialista')

    # Motivo
    api.add_resource(motivoAPI.Motivo, '/api/motivo')

    # Tipo Estudio
    api.add_resource(tipoEstudioAPI.TipoEstudio, '/api/tipo_estudio')

    # Estadisticas
    api.add_resource(statsAPI.Estadisticas, '/api/estadisticas')
    #return app

    # En el momento de hacer deploy, comentar la linea 121 (return app) y
    # descomentar la linea 124 (serve(...))

    serve(app,host='0.0.0.0', port=5000, threads=4)