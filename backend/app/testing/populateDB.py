import requests, random
import datetime


# Aca tenes que reemplazar con tu token

headers={"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTA1MDY5MDEsImlhdCI6MTYxMDQ3ODEwMSwic3ViIjoiYWxlam8ifQ.EOTj92uZ6UETUlkxcR6U2jIEX_H3_tld5QKiyBZFy7k"}

def randomDate(start_date=datetime.date(1930,1,1),end_date=datetime.date(2005,1,1)):
    time_between_dates = end_date - start_date
    days_between_dates = time_between_dates.days
    random_number_of_days = random.randrange(days_between_dates)
    return start_date + datetime.timedelta(days=random_number_of_days)

def createRandomPatients(cant=10):

    names=[
        "Alejo",
        "Martin",
        "Tadeo",
        "Fernando",
        "Pablo",
        "Gonzalo",
        "Tomas",
        "Enzo",
        "Soledad",
        "Solange",
        "Florencia",
        "Martina",
        "Claudio",
        "Cecilia"
    ]

    lastNames=[
        "Fernandez",
        "Velis",
        "Campagner",
        "Stupia",
        "Vanrell",
        "Benitez",
        "Gomez",
        "Lopez",
        "Delgadino",
        "Delgadillo",
        "Martinez",
        "De Dominicis",
        "Costello",
        "Moreno",
        "Pisoni"
    ]
    for i in range(0,cant):
        data={
            'nombre':random.choice(names),
            'apellido':random.choice(lastNames),
            'dni':random.randint(1,99999999),
            'sexo':random.choice(["Masculino","Femenino"]),
            'estado':random.choice(["Ambulatorio","Internado"]),
            'localidad':random.choice(["La Plata", "Otro"]),
            'fecha_nac':randomDate(),
            'obra_social_id':1,
        }
        requests.post("http://localhost:5000/api/paciente", data=data, headers=headers)
    print("Ready! "+str(cant)+" patients were created.")

def createRandomStudies(cant=10):
    especialistas=[i for i in range(1,7)]
    motivos=[i for i in range(1,21)]
    diagnosticos=[i for i in range(1,34)]
    descripciones=[i for i in range(1,12)]
    conclusiones=[i for i in range(1,5)]
    for i in range(1,cant):
        print("Paciente actual: "+ str(i))

        # Creo estudio basico
        dataBasico={
            "nombre_tipo":"basico",
            "fecha":randomDate(datetime.date(2020,1,1),datetime.date(2022,1,1)),
            "conclusion":"Una conclusión",
            "id_especialista":random.choice(especialistas),
            "id_motivo":random.choice(motivos),
            "id_diagnostico_obligatorio":random.choice(diagnosticos),
            "id_diagnostico_opcional_1":random.choice(diagnosticos),
            "id_diagnostico_opcional_2":random.choice(diagnosticos),
            "id_paciente":i,
        }
        requests.post("http://localhost:5000/api/estudio", data=dataBasico, headers=headers)

        # Creo estudio disincronia
        dataDisincronia={
            "nombre_tipo":"disincronia",
            "fecha":randomDate(datetime.date(2020,1,1),datetime.date(2022,1,1)),
            "conclusion":"Una conclusión",
            "id_especialista":random.choice(especialistas),
            "id_motivo":random.choice(motivos),
            "id_diagnostico_obligatorio":random.choice(diagnosticos),
            "id_diagnostico_opcional_1":random.choice(diagnosticos),
            "id_diagnostico_opcional_2":random.choice(diagnosticos),
            "id_paciente":i,
            "retraso_septal":round(random.uniform(1,50), 4),
            "dpt":round(random.uniform(1,50), 4),
            "str":round(random.uniform(1,50), 4),
            "llenado_distolico":round(random.uniform(1,50), 4),
            "diferencia_periodos":round(random.uniform(1,50), 4)            
        }
        requests.post("http://localhost:5000/api/estudio", data=dataDisincronia, headers=headers)

        # Creo estudio salina agitada
        dataSalinaAgitada={
            "nombre_tipo":"salina_agitada",
            "fecha":randomDate(datetime.date(2020,1,1),datetime.date(2022,1,1)),
            "conclusion":"Una conclusión",
            "id_especialista":random.choice(especialistas),
            "id_motivo":random.choice(motivos),
            "id_diagnostico_obligatorio":random.choice(diagnosticos),
            "id_diagnostico_opcional_1":random.choice(diagnosticos),
            "id_diagnostico_opcional_2":random.choice(diagnosticos),
            "id_paciente":i,
            "id_conclusion_predefinida":random.choice(conclusiones)            
        }
        requests.post("http://localhost:5000/api/estudio", data=dataSalinaAgitada, headers=headers)

        # Creo estudio 2D Doppler
        data2D_Doppler={
            "nombre_tipo":"2d_doppler",
            "fecha":randomDate(datetime.date(2020,1,1),datetime.date(2022,1,1)),
            "conclusion":"Una conclusión",
            "id_especialista":random.choice(especialistas),
            "id_motivo":random.choice(motivos),
            "id_diagnostico_obligatorio":random.choice(diagnosticos),
            "id_diagnostico_opcional_1":random.choice(diagnosticos),
            "id_diagnostico_opcional_2":random.choice(diagnosticos),
            "id_paciente":i,
            "id_descripcion_predefinida":random.choice(descripciones),
            "vb_SIV": round(random.uniform(1,50), 4),
            "vb_DSVI": round(random.uniform(1,50), 4),
            "vb_DDVI": round(random.uniform(1,50), 4),
            "vb_PP": round(random.uniform(1,50), 4),
            "vb_FEY": round(random.uniform(1,50), 4),
            "vb_Al_Area":round(random.uniform(1,50), 4),
            "vb_Al_Vol_SC":round(random.uniform(1,50), 4),
            "vb_Aorta":round(random.uniform(1,50), 4),
            "vb_Ap_Vao":round(random.uniform(1,50), 4),
            "vb_TSVI":round(random.uniform(1,50), 4),
            "dp_Onda_S_Septal":round(random.uniform(1,50), 4),  
            "dp_Onda_S_Lateral":round(random.uniform(1,50), 4),  
            "dp_Onda_e":round(random.uniform(1,50), 4),  
            "dp_Relacion_E_e":round(random.uniform(1,50), 4),  
            "dp_Onda_S_Vd":round(random.uniform(1,50), 4),  
            "vd_Pad":round(random.uniform(1,50), 4),  
            "vd_PmAP":round(random.uniform(1,50), 4),  
            "vd_PAP":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Pico_TT":round(random.uniform(1,50), 4),  
            "vd_velocidad_Regurgitante_TT":round(random.uniform(1,50), 4),  
            "vd_Insuficiencia_Tricuspidea":round(random.uniform(1,50), 4),  
            "vd_Velocidad_Maxima_A_Pulmonar":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Maximo":round(random.uniform(1,50), 4),  
            "vd_Tiempo_Al_Pico":round(random.uniform(1,50), 4),  
            "vd_Insuficiencia_Pulmonar":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Protosistolico":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Telesistolico":round(random.uniform(1,50), 4),  
            "vd_QP_QS":round(random.uniform(1,50), 4),  
            "vd_Velocidad_Onda_E":round(random.uniform(1,50), 4),  
            "vd_Velocidad_Onda_A":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Medio_Trasmitral":round(random.uniform(1,50), 4),  
            "vd_Insuficiencia_Mitral":round(random.uniform(1,50), 4),  
            "vd_ORE":round(random.uniform(1,50), 4),  
            "vd_Volumen_Regurgitante":round(random.uniform(1,50), 4),  
            "vd_Area_VM_THP":round(random.uniform(1,50), 4),  
            "vd_Dp_dt":round(random.uniform(1,50), 4),  
            "vd_Flujo_Reverso_AA":round(random.uniform(1,50), 4),  
            "vd_Flujo_Reverso_ADT":round(random.uniform(1,50), 4),  
            "vd_THP":round(random.uniform(1,50), 4),  
            "vd_Insuficiencia_Aortica":round(random.uniform(1,50), 4), 
            "vd_Gradiente_Medio_Ao":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Maximo_Ao":round(random.uniform(1,50), 4),  
            "vd_Velocidad_Maxima":round(random.uniform(1,50), 4),                                     
        }
        requests.post("http://localhost:5000/api/estudio",data=data2D_Doppler, headers=headers)

        # Creo estudio congenitas
        dataCongenitas={
            "nombre_tipo":"congenitas",
            "fecha":randomDate(datetime.date(2020,1,1),datetime.date(2022,1,1)),
            "descripcion":"Una descripción",
            "conclusion":"Una conclusión",
            "id_especialista":random.choice(especialistas),
            "id_motivo":random.choice(motivos),
            "id_diagnostico_obligatorio":random.choice(diagnosticos),
            "id_diagnostico_opcional_1":random.choice(diagnosticos),
            "id_diagnostico_opcional_2":random.choice(diagnosticos),
            "id_paciente":i,
            "situs":random.choice(["SOLITUS","INVERSUS","AMBIGUO"]),
            "conexion_ventriculo_arterial":random.choice(["Concordante","Discordante"]),
            "conexion_auriculo_ventricular":random.choice(["Concordante","Discordante"]),
            "vb_SIV": round(random.uniform(1,50), 4),
            "vb_DSVI": round(random.uniform(1,50), 4),
            "vb_DDVI": round(random.uniform(1,50), 4),
            "vb_PP": round(random.uniform(1,50), 4),
            "vb_FEY": round(random.uniform(1,50), 4),
            "vb_Al_Area":round(random.uniform(1,50), 4),
            "vb_Al_Vol_SC":round(random.uniform(1,50), 4),
            "vb_Aorta":round(random.uniform(1,50), 4),
            "vb_Ap_Vao":round(random.uniform(1,50), 4),
            "vb_TSVI":round(random.uniform(1,50), 4),
            "dp_Onda_S_Septal":round(random.uniform(1,50), 4),  
            "dp_Onda_S_Lateral":round(random.uniform(1,50), 4),  
            "dp_Onda_e":round(random.uniform(1,50), 4),  
            "dp_Relacion_E_e":round(random.uniform(1,50), 4),  
            "dp_Onda_S_Vd":round(random.uniform(1,50), 4),  
            "vd_Pad":round(random.uniform(1,50), 4),  
            "vd_PmAP":round(random.uniform(1,50), 4),  
            "vd_PAP":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Pico_TT":round(random.uniform(1,50), 4),  
            "vd_velocidad_Regurgitante_TT":round(random.uniform(1,50), 4),  
            "vd_Insuficiencia_Tricuspidea":round(random.uniform(1,50), 4),  
            "vd_Velocidad_Maxima_A_Pulmonar":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Maximo":round(random.uniform(1,50), 4),  
            "vd_Tiempo_Al_Pico":round(random.uniform(1,50), 4),  
            "vd_Insuficiencia_Pulmonar":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Protosistolico":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Telesistolico":round(random.uniform(1,50), 4),  
            "vd_QP_QS":round(random.uniform(1,50), 4),  
            "vd_Velocidad_Onda_E":round(random.uniform(1,50), 4),  
            "vd_Velocidad_Onda_A":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Medio_Trasmitral":round(random.uniform(1,50), 4),  
            "vd_Insuficiencia_Mitral":round(random.uniform(1,50), 4),  
            "vd_ORE":round(random.uniform(1,50), 4),  
            "vd_Volumen_Regurgitante":round(random.uniform(1,50), 4),  
            "vd_Area_VM_THP":round(random.uniform(1,50), 4),  
            "vd_Dp_dt":round(random.uniform(1,50), 4),  
            "vd_Flujo_Reverso_AA":round(random.uniform(1,50), 4),  
            "vd_Flujo_Reverso_ADT":round(random.uniform(1,50), 4),  
            "vd_THP":round(random.uniform(1,50), 4),  
            "vd_Insuficiencia_Aortica":round(random.uniform(1,50), 4), 
            "vd_Gradiente_Medio_Ao":round(random.uniform(1,50), 4),  
            "vd_Gradiente_Maximo_Ao":round(random.uniform(1,50), 4),  
            "vd_Velocidad_Maxima":round(random.uniform(1,50), 4),                                     
        }
        requests.post("http://localhost:5000/api/estudio",data=dataCongenitas, headers=headers)

        # Creo estudio transesofagico
        dataTransesofagico={
            "nombre_tipo":"transesofagico",
            "fecha":randomDate(datetime.date(2020,1,1),datetime.date(2022,1,1)),
            "conclusion":"Una conclusión",
            "id_especialista":random.choice(especialistas),
            "id_motivo":random.choice(motivos),
            "id_diagnostico_obligatorio":random.choice(diagnosticos),
            "id_diagnostico_opcional_1":random.choice(diagnosticos),
            "id_diagnostico_opcional_2":random.choice(diagnosticos),
            "id_paciente":i,
            "id_descripcion_predefinida":random.choice(descripciones),
        }
        requests.post("http://localhost:5000/api/estudio",data=dataTransesofagico, headers=headers)





createRandomPatients(cant=5) # Aca podes cambiar la cantidad de pacientes
createRandomStudies()

# PD: Este algoritmo tarda. Realmente tarda. Asi que dejalo correr nomas

