from flask_wtf import FlaskForm

from wtforms import StringField, IntegerField, DateField
from wtforms.validators import DataRequired, Length, NumberRange

class formEstudioBasico(FlaskForm):

    class Meta:
        csrf = False

    fecha = DateField(
        "Fecha Estudio",
        validators=[DataRequired()]
    )
    conclusion = StringField(
        "Conclusión del Estudio",
        validators=[DataRequired(),Length(max=1000)]
    )
    id_especialista = IntegerField(
        "ID del Especialista",
        validators=[DataRequired()]
    )
    nombre_tipo = StringField(
        "Nombre Único del Tipo de Estudio",
        validators=[DataRequired(),Length(max=50)]
    )
    id_motivo = IntegerField(
        "ID del Motivo",
        validators=[DataRequired()]
    )
    id_diagnostico_obligatorio = IntegerField(
        "ID del Diagnostico Obligatorio",
        validators=[DataRequired()]
    )
    id_diagnostico_opcional_1 = IntegerField(
        "ID del Diagnostico Opcional 1"
    )
    id_diagnostico_opcional_2 = IntegerField(
        "ID del Diagnostico Opcional 2"
    )
    id_paciente = IntegerField(
        "ID del Paciente",
        validators=[DataRequired()]
    )