from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms.validators import DataRequired, Length

class formObraSocial(FlaskForm):

    class Meta:
        csrf = False

    nombre = StringField("Nombre Obra Social", validators=[
        DataRequired(), Length(max=50)
    ])     