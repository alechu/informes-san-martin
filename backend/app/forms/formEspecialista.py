from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms.validators import DataRequired, Length

class formEspecialista(FlaskForm):

    class Meta:
        csrf = False

    nombre = StringField("Nombre del Especialista", validators=[
        DataRequired(), Length(max=50)
    ])     