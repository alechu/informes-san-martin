from app.forms import formEstudioBasico

from wtforms import DecimalField, IntegerField, StringField
from wtforms.validators import DataRequired, Length

class formEstudioDoppler(formEstudioBasico.formEstudioBasico):


    # Descripcion

    descripcion = StringField("Descripcion", validators=[
        DataRequired(), Length(max=8000)]
        )

    # Valores VB

    vb_SIV = DecimalField("SIV", places=2)
    vb_DSVI = DecimalField("DSVI", places=2)
    vb_DDVI = DecimalField("DDVI", places=2)
    vb_PP = DecimalField("PP", places=2)
    vb_FEY = DecimalField("FEY", places=2)
    vb_Al_Area = DecimalField("Al Área", places=2)
    vb_Al_Vol_SC = DecimalField("Al Vol SC", places=2)
    vb_Aorta = DecimalField("Aorta", places=2)
    vb_Ap_Vao = DecimalField("AP Vao", places=2)
    vb_TSVI = DecimalField("TSVI", places=2)

    # Valores DP

    dp_Onda_S_Septal = DecimalField("Onda S Septal", places=2)
    dp_Onda_S_Lateral = DecimalField("Onda S Lateral", places=2)
    dp_Onda_e = DecimalField("Onda e", places=2)
    dp_Relacion_E_e = DecimalField("Relación E/e", places=2)
    dp_Onda_S_Vd = DecimalField("Onda S/Vd", places=2)

    # <== Valores Doppler ==>

    # Tricuspide

    vd_Pad = DecimalField("Pad", places=2)
    vd_PmAP = DecimalField("PmAP", places=2)
    vd_PAP = DecimalField("PAP", places=2)
    vd_Gradiente_Pico_TT = DecimalField("Gradiente Pico TT", places=2)
    vd_velocidad_Regurgitante_TT = DecimalField("Velocidad Regurgitante TT", places=2)
    vd_Insuficiencia_Tricuspidea = DecimalField("Insuficiencia Tricuspidea", places=2)

    # Pulmonar

    vd_Velocidad_Maxima_A_Pulmonar = DecimalField("Velocidad Maxima P", places=2)
    vd_Gradiente_Maximo = DecimalField("Gradiente Maximo", places=2)
    vd_Tiempo_Al_Pico = DecimalField("Tiempo Al Pico", places=2)
    vd_Insuficiencia_Pulmonar = DecimalField("Insuficiencia Pulmonar", places=2)
    vd_Gradiente_Protosistolico = DecimalField("Gradiente Protosistólico", places=2)
    vd_Gradiente_Telesistolico = DecimalField("Gradiente Telesistólico", places=2)
    vd_QP_QS = DecimalField("QP QS", places=2)

    # Mitral 

    vd_Velocidad_Onda_E = DecimalField("Velocidad Onda E", places=2)
    vd_Velocidad_Onda_A = DecimalField("Velocidad Onda A", places=2)
    vd_Gradiente_Medio_Trasmitral = DecimalField("Gradiente Medio Trasmitral", places=2)
    vd_Insuficiencia_Mitral = DecimalField("Insuficiencia Mitral", places=2)
    vd_ORE = DecimalField("ORE", places=2)
    vd_Volumen_Regurgitante = DecimalField("Volumen Regurgitante", places=2)
    vd_Area_VM_THP = DecimalField("Area VM THP", places=2)
    vd_Dp_dt = DecimalField("Dp/dt", places=2)

    # Aortica

    vd_Flujo_Reverso_AA = DecimalField("Flujo Reverso AA", places=2)
    vd_Flujo_Reverso_ADT = DecimalField("Flujo Reverso ADT", places=2)
    vd_THP = DecimalField("THP", places=2)
    vd_Insuficiencia_Aortica = DecimalField("Insuficiencia Aortica", places=2)
    vd_Gradiente_Medio_Ao = DecimalField("Gradiente Medio Ao", places=2)
    vd_Gradiente_Maximo_Ao = DecimalField("Gradiente Maximo Ao", places=2)
    vd_Velocidad_Maxima = DecimalField("Velocidad Maxima", places=2)
