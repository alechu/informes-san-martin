from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms.validators import DataRequired, Length

class formTipoEstudio(FlaskForm):

    class Meta:
        csrf = False

    nombre = StringField("Identificador del Tipo Estudio", validators=[
        DataRequired(), Length(max=200)
    ])

    nombre_mostrado = StringField("Nombre del Tipo Estudio", validators=[
        DataRequired(), Length(max=200)
    ])
         