from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms.validators import DataRequired, Length

class formDescripcionPredefinida(FlaskForm):

    class Meta:
        csrf = False

    nombre = StringField("Contenido de la Descripción", validators=[
        DataRequired(), Length(max=1000)
    ])     