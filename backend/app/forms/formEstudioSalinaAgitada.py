from app.forms import formEstudioBasico

from wtforms import IntegerField
from wtforms.validators import DataRequired

class formEstudioSalinaAgitada(formEstudioBasico.formEstudioBasico):

    id_conclusion_predefinida = IntegerField(
        "ID de la Conclusión Predefinida",
        validators=[DataRequired()]
    )