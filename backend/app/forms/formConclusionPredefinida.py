from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms.validators import DataRequired, Length

class formConclusionPredefinida(FlaskForm):

    class Meta:
        csrf = False

    escenario = StringField("Escenario", validators=[
        DataRequired(), Length(max=1000)
    ])

    conclusion = StringField("Conclusion", validators=[
        DataRequired(), Length(max=400)
    ])     