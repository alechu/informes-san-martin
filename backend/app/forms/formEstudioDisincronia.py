from app.forms import formEstudioBasico

from wtforms import DecimalField

class formEstudioDisincronia(formEstudioBasico.formEstudioBasico):

    retraso_septal = DecimalField(
        "Retraso Septal",
        places=2
    )
    dpt = DecimalField(
        "DPT",
        places=2
    )
    str = DecimalField(
        "STR",
        places=2
    )
    llenado_distolico = DecimalField(
        "Llenado Distolico",
        places=2
    )
    diferencia_periodos = DecimalField(
        "Diferencia de Periodos",
        places=2
    )