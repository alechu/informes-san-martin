from app.forms import formEstudioDoppler

from wtforms import DecimalField, StringField
from wtforms.validators import Length, DataRequired

class formEstudioCongenitas(formEstudioDoppler.formEstudioDoppler):

    situs = StringField("Situs", validators=[
        Length(max=50)
    ])
    conexion_ventriculo_arterial = StringField("Conexion Ventriculo Arterial", validators=[
        Length(max=50)
    ])
    conexion_auriculo_ventricular = StringField("Conexion Auriculo Ventricular", validators=[
        Length(max=50)
    ])
    descripcion = StringField("Descripción", validators=[
        DataRequired(), Length(max=1000)
    ])
    