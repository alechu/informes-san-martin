from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms.validators import DataRequired, Length

class formMotivo(FlaskForm):

    class Meta:
        csrf = False

    nombre = StringField("Nombre del Motivo", validators=[
        DataRequired(), Length(max=200)
    ])     