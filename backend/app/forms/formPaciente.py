from flask_wtf import FlaskForm

from wtforms import StringField, IntegerField, DateField
from wtforms.validators import DataRequired, Length, NumberRange

class formPaciente(FlaskForm):

    class Meta:
        csrf = False

    nombre = StringField("Nombre Paciente", validators=[
        DataRequired(), Length(max=50)
    ])
    apellido = StringField("Apellido Paciente", validators=[
        DataRequired(), Length(max=50)
    ])    
    dni = StringField("DNI Paciente", validators=[
        Length(max=10)
    ])
    fecha_nac = DateField("Fecha Nacimiento Paciente", validators=[
        DataRequired()
    ])
    sexo = StringField("Sexo Paciente", validators=[
        DataRequired(), Length(max=10)
    ])    
    estado = StringField("Estado Paciente", validators=[
        DataRequired(), Length(max=25)
    ])
    localidad = StringField("Localidad Paciente", validators=[
        DataRequired(), Length(max=30)
    ])
    obra_social_id = IntegerField("ID Obra Social", validators=[
        DataRequired()
    ])        