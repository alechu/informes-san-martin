from app.forms import formEstudioBasico

from wtforms import StringField
from wtforms.validators import DataRequired, Length

class formEstudioTransesofagico(formEstudioBasico.formEstudioBasico):

    descripcion = StringField("Descripcion", validators=[DataRequired(), Length(max=8000)])