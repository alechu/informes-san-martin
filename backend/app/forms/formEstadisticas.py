from flask_wtf import FlaskForm

from wtforms import StringField, IntegerField, DateField
from wtforms.validators import DataRequired, Length

class formEstadisticas(FlaskForm):

    class Meta:
        csrf = False

    agrupado_por = StringField("Resultados agrupados por", validators=[
        DataRequired()
    ])

    sexo = StringField("Sexo Paciente", validators=[
        Length(max=10)
    ])

    nacido_en = IntegerField("Nacido en el año")

    localidad = StringField("Localidad Paciente", validators=[
        Length(max=30)
    ])

    estado = StringField("Estado Paciente", validators=[
        Length(max=25)
    ])

    tipo = StringField("Tipo Estudio", validators=[
        Length(max=50)
    ])

    diag_obligatorio = IntegerField("Diagnostico Obligatorio")

    diag_opcional_1 = IntegerField("Diagnostico Opcional 1")

    diag_opcional_2 = IntegerField("Diagnostico Opcional 2")

    fecha_inicio = DateField("Fecha Inicio")

    fecha_fin = DateField("Fecha Fin")