from flask_wtf import FlaskForm

from wtforms import StringField
from wtforms.validators import DataRequired, Length

class formDiagnostico(FlaskForm):

    class Meta:
        csrf = False

    nombre = StringField("Contenido del Diagnóstico", validators=[
        DataRequired(), Length(max=1000)
    ])     