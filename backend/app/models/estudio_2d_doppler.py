from sqlalchemy import inspect

from app import db

from app.models.estudio import Estudio
from app.models.especialista import Especialista
from app.models.tipo_estudio import TipoEstudio
from app.models.motivo import Motivo 
from app.models.diagnostico import Diagnostico
from app.models.paciente import Paciente

from app.helpers.exceptions import NoneType

class EstudioDoppler(Estudio):
    __tablename__ = "estudio_2d_doppler"
    id = db.Column(db.Integer, db.ForeignKey('estudio.id'), primary_key=True)

    descripcion = db.Column(db.String(8000), nullable=False)     

    # Valores VB
    vb_SIV = db.Column(db.Float(precision=2), nullable=True)
    vb_DSVI = db.Column(db.Float(precision=2), nullable=True)
    vb_DDVI = db.Column(db.Float(precision=2), nullable=True)
    vb_PP = db.Column(db.Float(precision=2), nullable=True)
    vb_FEY = db.Column(db.Float(precision=2), nullable=True)
    vb_Al_Area = db.Column(db.Float(precision=2), nullable=True)
    vb_Al_Vol_SC = db.Column(db.Float(precision=2), nullable=True)
    vb_Aorta = db.Column(db.Float(precision=2), nullable=True)
    vb_Ap_Vao = db.Column(db.Float(precision=2), nullable=True)
    vb_TSVI = db.Column(db.Float(precision=2), nullable=True)

    # Valores DP

    dp_Onda_S_Septal = db.Column(db.Float(precision=2), nullable=True)
    dp_Onda_S_Lateral = db.Column(db.Float(precision=2), nullable=True)
    dp_Onda_e = db.Column(db.Float(precision=2), nullable=True)
    dp_Relacion_E_e = db.Column(db.Float(precision=2), nullable=True)
    dp_Onda_S_Vd = db.Column(db.Float(precision=2), nullable=True)

    # Valores Doppler

    # Tricuspide

    vd_Pad = db.Column(db.Float(precision=2), nullable=True)
    vd_PmAP = db.Column(db.Float(precision=2), nullable=True)
    vd_PAP = db.Column(db.Float(precision=2), nullable=True)
    vd_Gradiente_Pico_TT = db.Column(db.Float(precision=2), nullable=True)
    vd_velocidad_Regurgitante_TT = db.Column(db.Float(precision=2), nullable=True)
    vd_Insuficiencia_Tricuspidea = db.Column(db.Float(precision=2), nullable=True)

    # Pulmonar

    vd_Velocidad_Maxima_A_Pulmonar = db.Column(db.Float(precision=2), nullable=True)
    vd_Gradiente_Maximo = db.Column(db.Float(precision=2), nullable=True)
    vd_Tiempo_Al_Pico = db.Column(db.Float(precision=2), nullable=True)
    vd_Insuficiencia_Pulmonar = db.Column(db.Float(precision=2), nullable=True)
    vd_Gradiente_Protosistolico = db.Column(db.Float(precision=2), nullable=True)
    vd_Gradiente_Telesistolico = db.Column(db.Float(precision=2), nullable=True)
    vd_QP_QS = db.Column(db.Float(precision=2), nullable=True)

    # Mitral

    vd_Velocidad_Onda_E = db.Column(db.Float(precision=2), nullable=True)
    vd_Velocidad_Onda_A = db.Column(db.Float(precision=2), nullable=True)
    vd_Gradiente_Medio_Trasmitral = db.Column(db.Float(precision=2), nullable=True)
    vd_Insuficiencia_Mitral = db.Column(db.Float(precision=2), nullable=True)
    vd_ORE = db.Column(db.Float(precision=2), nullable=True)
    vd_Volumen_Regurgitante = db.Column(db.Float(precision=2), nullable=True)
    vd_Area_VM_THP = db.Column(db.Float(precision=2), nullable=True)
    vd_Dp_dt = db.Column(db.Float(precision=2), nullable=True)

    # Aortica

    vd_Flujo_Reverso_AA = db.Column(db.Float(precision=2), nullable=True)
    vd_Flujo_Reverso_ADT = db.Column(db.Float(precision=2), nullable=True)
    vd_THP = db.Column(db.Float(precision=2), nullable=True)
    vd_Insuficiencia_Aortica = db.Column(db.Float(precision=2), nullable=True)
    vd_Gradiente_Medio_Ao = db.Column(db.Float(precision=2), nullable=True)
    vd_Gradiente_Maximo_Ao = db.Column(db.Float(precision=2), nullable=True)
    vd_Velocidad_Maxima = db.Column(db.Float(precision=2), nullable=True)


    __mapper_args__ = {
        'polymorphic_identity': '2d_doppler',
    } 


    def create(data_estudio):
        nuevo_estudio = EstudioDoppler(
            fecha = data_estudio['fecha'],
            conclusion = data_estudio['conclusion'],
            descripcion = data_estudio['descripcion'],

            # Valores VB

            vb_SIV = data_estudio['vb_SIV'],
            vb_DSVI = data_estudio['vb_DSVI'],
            vb_DDVI = data_estudio['vb_DDVI'],
            vb_PP = data_estudio['vb_PP'],
            vb_FEY = data_estudio['vb_FEY'],
            vb_Al_Area = data_estudio['vb_Al_Area'],
            vb_Al_Vol_SC = data_estudio['vb_Al_Vol_SC'],
            vb_Aorta = data_estudio['vb_Aorta'],
            vb_Ap_Vao = data_estudio['vb_Ap_Vao'],
            vb_TSVI = data_estudio['vb_TSVI'],

            # Valores DP

            dp_Onda_S_Septal = data_estudio['dp_Onda_S_Septal'],
            dp_Onda_S_Lateral = data_estudio['dp_Onda_S_Lateral'],
            dp_Onda_e = data_estudio['dp_Onda_e'],
            dp_Relacion_E_e = data_estudio['dp_Relacion_E_e'],
            dp_Onda_S_Vd = data_estudio['dp_Onda_S_Vd'],

            # Valores Doppler

            # Tricuspide 

            vd_Pad = data_estudio['vd_Pad'],
            vd_PmAP = data_estudio['vd_PmAP'],
            vd_PAP = data_estudio['vd_PAP'],
            vd_Gradiente_Pico_TT = data_estudio['vd_Gradiente_Pico_TT'],
            vd_velocidad_Regurgitante_TT = data_estudio['vd_velocidad_Regurgitante_TT'],
            vd_Insuficiencia_Tricuspidea = data_estudio['vd_Insuficiencia_Tricuspidea'], 

            # Pulmonar

            vd_Velocidad_Maxima_A_Pulmonar = data_estudio['vd_Velocidad_Maxima_A_Pulmonar'],
            vd_Gradiente_Maximo = data_estudio['vd_Gradiente_Maximo'],
            vd_Tiempo_Al_Pico = data_estudio['vd_Tiempo_Al_Pico'],
            vd_Insuficiencia_Pulmonar = data_estudio['vd_Insuficiencia_Pulmonar'],
            vd_Gradiente_Protosistolico = data_estudio['vd_Gradiente_Protosistolico'],
            vd_Gradiente_Telesistolico = data_estudio['vd_Gradiente_Telesistolico'],
            vd_QP_QS = data_estudio['vd_QP_QS'],

            # Mitral 

            vd_Velocidad_Onda_E = data_estudio['vd_Velocidad_Onda_E'],
            vd_Velocidad_Onda_A = data_estudio['vd_Velocidad_Onda_A'],
            vd_Gradiente_Medio_Trasmitral = data_estudio['vd_Gradiente_Medio_Trasmitral'],
            vd_Insuficiencia_Mitral = data_estudio['vd_Insuficiencia_Mitral'],
            vd_ORE = data_estudio['vd_ORE'],
            vd_Volumen_Regurgitante = data_estudio['vd_Volumen_Regurgitante'],
            vd_Area_VM_THP = data_estudio['vd_Area_VM_THP'],
            vd_Dp_dt = data_estudio['vd_Dp_dt'],

            # Aortica

            vd_Flujo_Reverso_AA = data_estudio['vd_Flujo_Reverso_AA'],
            vd_Flujo_Reverso_ADT = data_estudio['vd_Flujo_Reverso_ADT'],
            vd_THP = data_estudio['vd_THP'],
            vd_Insuficiencia_Aortica = data_estudio['vd_Insuficiencia_Aortica'],
            vd_Gradiente_Medio_Ao = data_estudio['vd_Gradiente_Medio_Ao'],
            vd_Gradiente_Maximo_Ao = data_estudio['vd_Gradiente_Maximo_Ao'],
            vd_Velocidad_Maxima = data_estudio['vd_Velocidad_Maxima'],                                                                              
        )
        try:
            especialista = Especialista.get_by_id(data_estudio['id_especialista'])
            tipo = TipoEstudio.get_by_name(data_estudio['nombre_tipo'])
            motivo = Motivo.get_by_id(data_estudio['id_motivo'])
            diagnostico_obligatorio = Diagnostico.get_by_id(data_estudio['id_diagnostico_obligatorio'])

            if(data_estudio['id_diagnostico_opcional_1'] != None):
                diagnostico_opcional_1 = Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_1'])
                nuevo_estudio.diagnostico_opcional_1 = diagnostico_opcional_1

            if(data_estudio['id_diagnostico_opcional_2'] != None):
                diagnostico_opcional_2 = Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_2'])
                nuevo_estudio.diagnostico_opcional_2 = diagnostico_opcional_2
            
            paciente = Paciente.get_by_id(data_estudio['id_paciente'])

            # Asigno los Foreign

            nuevo_estudio.llevado_a_cabo_por = especialista
            nuevo_estudio.es_de_tipo = tipo
            nuevo_estudio.tiene_motivo = motivo
            nuevo_estudio.tiene_paciente = paciente
            nuevo_estudio.diagnostico_obligatorio = diagnostico_obligatorio

            # Lo inserto en la DB

            db.session.add(nuevo_estudio)
            db.session.commit()
            return nuevo_estudio
        except:
            db.session.rollback()
            raise
            return False

    def update(id_estudio,data_estudio):
        estudio_a_actualizar = Estudio.get_by_id(id_estudio)
        if estudio_a_actualizar == None:
            raise NoneType("Ese estudio no existe")
        if not estudio_a_actualizar.tipo == data_estudio['nombre_tipo']:
            raise NoneType("Expected: "+estudio_a_actualizar.tipo+". Got: "+data_estudio['nombre_tipo'])                    
        campos_no_deseados = [
            'id',
            'descripcion',
            'conclusion',
            'diagnostico_obligatorio',
            'diagnostico_opcional_1', 
            'diagnostico_opcional_2',
            'diagnostico_obligatorio_id',
            'diagnostico_opcional_1_id',
            'diagnostico_opcional_2_id',
            'especialista',
            'paciente',
            'tipo',
            'motivo',
            'tiene_paciente',
            'llevado_a_cabo_por',
            'es_de_tipo',
            'tiene_motivo',
        ]
        try:
            # Actualizo todos los campos que no son Foreign
            mapper = inspect(estudio_a_actualizar)
            for column in mapper.attrs:
                if str(column.key) not in campos_no_deseados:
                    if(data_estudio[str(column.key)] != None):
                        query = "estudio_a_actualizar."+str(column.key)+" = '"+str(data_estudio[str(column.key)])+"'"
                        exec(query)
            # Excepcion a la regla porque puede contener saltos de linea
            estudio_a_actualizar.descripcion = str(data_estudio["descripcion"])
            estudio_a_actualizar.conclusion = str(data_estudio["conclusion"])
        except:
            raise
        try:

            # Actualizo todos los campos que si son Foreign
            campos_deseados = {
                'id_especialista':("llevado_a_cabo_por","Especialista"),
                'id_motivo':("tiene_motivo","Motivo"),
                'id_diagnostico_obligatorio':("diagnostico_obligatorio","Diagnostico"),
                'id_paciente':("tiene_paciente","Paciente"),
            }
            for key in campos_deseados:
                if (data_estudio[key] != None):
                    query_object = campos_deseados[key][1]+".get_by_id("+str(data_estudio[key])+")"
                    db_object = eval(query_object)
                    if db_object == None:
                        db.session.rollback()
                        raise NoneType("La ID del campo "+campos_deseados[key][1]+" no es válida.")
                    query_update = "estudio_a_actualizar."+campos_deseados[key][0]+" = db_object"
                    exec(query_update)

            # Actualizo los diagnosticos opcionales
            if(data_estudio['id_diagnostico_opcional_1'] != None):
                if(data_estudio['id_diagnostico_opcional_1'] == 0):
                    estudio_a_actualizar.diagnostico_opcional_1_id = None
                else:
                    if(Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_1']) == None):
                        raise NoneType("La ID del campo id_diagnostico_opcional_1 no es válida.")                
                    estudio_a_actualizar.diagnostico_opcional_1_id = data_estudio['id_diagnostico_opcional_1']

            if(data_estudio['id_diagnostico_opcional_2'] != None):
                if(data_estudio['id_diagnostico_opcional_2'] == 0):
                    estudio_a_actualizar.diagnostico_opcional_2_id = None
                else:
                    if(Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_2']) == None):
                        raise NoneType("La ID del campo id_diagnostico_opcional_2 no es válida.")
                    estudio_a_actualizar.diagnostico_opcional_2_id = data_estudio['id_diagnostico_opcional_2']            
            
            db.session.commit()
            return estudio_a_actualizar
        except Exception as e:
            db.session.rollback()
            raise e
            return False            