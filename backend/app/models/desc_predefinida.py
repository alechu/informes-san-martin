from app import db

from app.helpers.exceptions import NoneType

class DescripcionPredefinida(db.Model):
    __tablename__ = "descripcion_predefinida"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(1000), nullable=False)
    estudios_T = db.relationship(
        'EstudioTransesofagico',
        backref="tiene_descripcion",
        lazy="dynamic"
    )
    estudios_D = db.relationship(
        'EstudioDoppler',
        backref="tiene_descripcion",
        lazy="dynamic"
    )    


    def get_all():
        try:
            return DescripcionPredefinida.query.all()
        except:
            raise
            return False

    def get_by_id(id_DescripcionPredefinida):
        try:
            desc = DescripcionPredefinida.query.filter_by(id=id_DescripcionPredefinida).first()
            if desc == None:
                raise NoneType("Esa Descripción predefinida no existe")
            return desc
        except:
            raise
            return False

    def get_estudios(id_DescripcionPredefinida,page,per_page):
        DescripcionPredefinida = DescripcionPredefinida.query.filter_by(id=id_DescripcionPredefinida).first()
        if(DescripcionPredefinida == None):
            return False
        totales = DescripcionPredefinida.estudios.count()
        datos = DescripcionPredefinida.estudios.paginate(page,per_page,False).items
        numPaginas = DescripcionPredefinida.estudios.paginate(page,per_page,False).pages
        return [totales,datos,numPaginas]

    def create(data):
        nuevo_DescripcionPredefinida = DescripcionPredefinida(
            nombre = data['nombre']
        )
        try:
            db.session.add(nuevo_DescripcionPredefinida)
            db.session.commit()
            return nuevo_DescripcionPredefinida
        except Exception as e:
            db.session.rollback()
            raise
            return False     

    def update(id_DescripcionPredefinida,data):
        try:
            desc = DescripcionPredefinida.query.filter_by(id=id_DescripcionPredefinida).first()
            if desc == None:
                raise NoneType("Esa Descripción predefinida no existe")
            desc.nombre = data['nombre']
            db.session.commit()
            return desc
        except:
            db.session.rollback()
            raise
            return False        

    def delete(id_DescripcionPredefinida):
        try:
            DescripcionPredefinida_a_borrar = DescripcionPredefinida.query.filter_by(id=id_DescripcionPredefinida).first()
            if DescripcionPredefinida_a_borrar == None:
                raise NoneType("Esa Descripción predefinida no existe")
            if((DescripcionPredefinida_a_borrar.estudios_T.count() != 0) or (DescripcionPredefinida_a_borrar.estudios_D.count())): 
                raise NoneType('tiene_estudios_asociados')
            db.session.delete(DescripcionPredefinida_a_borrar)
            db.session.commit()
            return True
        except:
            db.session.rollback()
            raise
            return False

    