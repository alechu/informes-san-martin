import datetime

from sqlalchemy import inspect, extract, func

from app import db

from app.models.especialista import Especialista
from app.models.tipo_estudio import TipoEstudio
from app.models.motivo import Motivo 
from app.models.diagnostico import Diagnostico
from app.models.paciente import Paciente

from app.helpers.exceptions import NoneType

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Estudio(db.Model, Base):
    __tablename__ = "estudio"
    id = db.Column(db.Integer, primary_key=True)
    fecha = db.Column(db.Date, nullable=False)
    conclusion = db.Column(db.String(1000), nullable=False)

    especialista = db.Column(
        db.Integer, 
        db.ForeignKey('especialista.id'),
        nullable=False
    )

    tipo = db.Column(
        db.String(50),
        db.ForeignKey('tipo_estudio.nombre'),
        nullable=False
    )

    motivo = db.Column(
        db.Integer,
        db.ForeignKey('motivo.id'),
        nullable=False
    )

    diagnostico_obligatorio_id = db.Column(
        db.Integer,
        db.ForeignKey('diagnostico.id'),
        nullable=False
    )
    
    diagnostico_opcional_1_id = db.Column(
        db.Integer,
        db.ForeignKey('diagnostico.id'),
        nullable=True
    )

    diagnostico_opcional_2_id = db.Column(
        db.Integer,
        db.ForeignKey('diagnostico.id'),
        nullable=True
    )

    diagnostico_obligatorio = db.relationship(
        "Diagnostico",
        foreign_keys=[diagnostico_obligatorio_id]
    )

    diagnostico_opcional_1 = db.relationship(
        "Diagnostico",
        foreign_keys=[diagnostico_opcional_1_id]
    )

    diagnostico_opcional_2 = db.relationship(
        "Diagnostico",
        foreign_keys=[diagnostico_opcional_2_id]
    )                   
    
    paciente = db.Column(
        db.Integer,
        db.ForeignKey('paciente.id'),
        nullable=False
    )

    
    __mapper_args__ = {
        'polymorphic_identity': 'basico',
        'polymorphic_on': tipo
    }
       

    def get_all(page,per_page):
        try:
            totales = db.session.query(Estudio).count()
            datos = Paciente.query.paginate(page,per_page,False).items
            numPaginas = Paciente.query.paginate(page,per_page,False).pages
            res = [totales,datos,numPaginas]
            return res
        except:
            raise
            return False

    def get_all_stats(data):

        # Armo la query

        if data['agrupado_por'] == 'dia':
            query = db.session.query(Estudio.fecha,func.count(Estudio.fecha))
        elif data['agrupado_por'] == 'mes':
            query = db.session.query(func.month(Estudio.fecha),func.count(Estudio.fecha))
        else:
            query = db.session.query(func.year(Estudio.fecha),func.count(Estudio.fecha))

        # Apendo atributos de estudio



        if data['tipo'] != '':
            query = query.filter(Estudio.tipo==data['tipo'])
        if data['diag_obligatorio'] != None:
            query = query.filter(Estudio.diagnostico_obligatorio_id==data['diag_obligatorio'])
        if data['diag_opcional_1'] != None:
            query = query.filter(Estudio.diagnostico_opcional_1_id==data['diag_opcional_1'])
        if data['diag_opcional_2'] != None:
            query = query.filter(Estudio.diagnostico_opcional_2_id==data['diag_opcional_2'])
        if data['fecha_inicio'] != None:
            fi = data['fecha_inicio']
        else:
            fi = '2019-01-01'
        if data['fecha_fin'] != None:
            ff = data['fecha_fin']
        else:
            ff = '9999-12-31'
        query = query.filter(Estudio.fecha.between(fi,ff))

        # Apendo atributos de paciente

        query = query.join(Paciente)

        if data['sexo'] != '':
            query = query.filter(Paciente.sexo==data['sexo'])
        if data['localidad'] != '':
            query = query.filter(Paciente.localidad==data['localidad'])
        if data['estado']  != '':
            query = query.filter(Paciente.estado==data['estado'])
        if data['nacido_en'] != None:
            ano_nac = data['nacido_en']
            fi_paciente = str(ano_nac)+'-01-01'
            ff_paciente = str(ano_nac)+'-12-31'
            query = query.filter(Paciente.fecha_nac.between(fi_paciente,ff_paciente))
            
        
        # Agrupo la query

        if data['agrupado_por'] == 'dia':
            query = query.group_by(Estudio.fecha)
        elif data['agrupado_por'] == 'mes':
            query = query.group_by(func.year(Estudio.fecha), func.month(Estudio.fecha))
        else:
            query = query.group_by(func.year(Estudio.fecha))



        # Ejecuto la query

        query = query.all()

        return query



        

    def get_by_id(id_estudio):
        try:
            return Estudio.query.filter_by(id=id_estudio).first()
        except:
            raise
            return False

    def get_all_diags(id_diagnostico):
        obligatorios = Estudio.query.filter_by(
            diagnostico_obligatorio_id=id_diagnostico
            ).all()
        opcionales_1 = Estudio.query.filter_by(
            diagnostico_opcional_1_id=id_diagnostico
            ).all()
        opcionales_2 = Estudio.query.filter_by(
            diagnostico_opcional_2_id=id_diagnostico
            ).all()

        if obligatorios != [] or opcionales_1 != [] or opcionales_2 != []:
            return True
        else:
            return False 

    def create(data_estudio):
        nuevo_estudio = Estudio(
            fecha = data_estudio['fecha'],
            conclusion = data_estudio['conclusion']
        )

        try:
            # Me traigo todos los Foreign

            especialista = Especialista.get_by_id(data_estudio['id_especialista'])
            tipo = TipoEstudio.get_by_name(data_estudio['nombre_tipo'])
            if tipo == None:
                raise NoneType("cannot be null")
                return False
            motivo = Motivo.get_by_id(data_estudio['id_motivo'])
            diagnostico_obligatorio = Diagnostico.get_by_id(data_estudio['id_diagnostico_obligatorio'])
            if(data_estudio['id_diagnostico_opcional_1'] != None):
                diagnostico_opcional_1 = Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_1'])
                nuevo_estudio.diagnostico_opcional_1 = diagnostico_opcional_1

            if(data_estudio['id_diagnostico_opcional_2'] != None):
                diagnostico_opcional_2 = Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_2'])
                nuevo_estudio.diagnostico_opcional_2 = diagnostico_opcional_2
            
            paciente = Paciente.get_by_id(data_estudio['id_paciente'])

            # Asigno los Foreign

            nuevo_estudio.llevado_a_cabo_por = especialista
            nuevo_estudio.es_de_tipo = tipo
            nuevo_estudio.tiene_motivo = motivo
            nuevo_estudio.tiene_paciente = paciente
            nuevo_estudio.diagnostico_obligatorio = diagnostico_obligatorio
            # Lo inserto en la DB
            db.session.add(nuevo_estudio)
            db.session.commit()
            return nuevo_estudio
        except:
            db.session.rollback()
            raise 
            return False
    
    def update(id_estudio,data_estudio):
        if (TipoEstudio.get_by_name(data_estudio['nombre_tipo']) == None):
            raise NoneType("Ese tipo de estudio no existe")
        estudio_a_actualizar = Estudio.get_by_id(id_estudio)
        if estudio_a_actualizar == None:
            raise NoneType("Ese estudio no existe")
        if not estudio_a_actualizar.tipo == data_estudio['nombre_tipo']:
            raise NoneType("Expected: "+estudio_a_actualizar.tipo+". Got: "+data_estudio['nombre_tipo'])            
        campos_no_deseados = [
            'id',
            'diagnostico_obligatorio',
            'diagnostico_opcional_1', 
            'diagnostico_opcional_2',
            'diagnostico_obligatorio_id',
            'diagnostico_opcional_1_id',
            'diagnostico_opcional_2_id',
            'especialista',
            'paciente',
            'tipo',
            'motivo',
            'tiene_paciente',
            'llevado_a_cabo_por',
            'es_de_tipo',
            'tiene_motivo',
        ]
        try:
            # Actualizo todos los campos que no son Foreign
            mapper = inspect(estudio_a_actualizar)
            for column in mapper.attrs:
                if str(column.key) not in campos_no_deseados:
                    if(data_estudio[str(column.key)] != None):
                        query = "estudio_a_actualizar."+str(column.key)+" = '"+str(data_estudio[str(column.key)])+"'"
                        exec(query)
        except:
            raise
        try:

            # Actualizo todos los campos que si son Foreign
            campos_deseados = {
                'id_especialista':("llevado_a_cabo_por","Especialista"),
                'id_motivo':("tiene_motivo","Motivo"),
                'id_diagnostico_obligatorio':("diagnostico_obligatorio","Diagnostico"),
                'id_paciente':("tiene_paciente","Paciente")
            }
            for key in campos_deseados:
                if (data_estudio[key] != None):
                    query_object = campos_deseados[key][1]+".get_by_id("+str(data_estudio[key])+")"
                    db_object = eval(query_object)
                    if db_object == None:
                        db.session.rollback()
                        raise NoneType("La ID del campo "+campos_deseados[key][1]+" no es válida.")
                    query_update = "estudio_a_actualizar."+campos_deseados[key][0]+" = db_object"
                    exec(query_update)

            

            # Actualizo los diagnosticos opcionales
            if(data_estudio['id_diagnostico_opcional_1'] != None):
                if(data_estudio['id_diagnostico_opcional_1'] == 0):
                    estudio_a_actualizar.diagnostico_opcional_1_id = None
                else:
                    if(Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_1']) == None):
                        raise NoneType("La ID del campo id_diagnostico_opcional_1 no es válida.")                
                    estudio_a_actualizar.diagnostico_opcional_1_id = data_estudio['id_diagnostico_opcional_1']

            if(data_estudio['id_diagnostico_opcional_2'] != None):
                if(data_estudio['id_diagnostico_opcional_2'] == 0):
                    estudio_a_actualizar.diagnostico_opcional_2_id = None
                else:                   
                    if(Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_2']) == None):
                        raise NoneType("La ID del campo id_diagnostico_opcional_2 no es válida.")
                    estudio_a_actualizar.diagnostico_opcional_2_id = data_estudio['id_diagnostico_opcional_2']            
            
            db.session.commit()
            return estudio_a_actualizar
        except Exception as e:
            db.session.rollback()
            raise e
            return False

    def delete(id_estudio):
        try:
            estudio_a_borrar = Estudio.query.filter_by(id=id_estudio).first()
            db.session.delete(estudio_a_borrar)
            db.session.commit()
            return True
        except:
            db.session.rollback()
            raise
            return False