from app import db

from app.helpers.exceptions import NoneType, NotEmptyRelationships

class TipoEstudio(db.Model):
    __tablename__ = "tipo_estudio"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(191), nullable=False, unique=True)
    nombre_mostrado = db.Column(db.String(200), nullable=False)
    estudios = db.relationship(
        'Estudio',
        backref="es_de_tipo",
        lazy="dynamic"
    )


    def get_all():
        try:
            return TipoEstudio.query.all()
        except:
            raise
            return False

    def get_by_id(id_TipoEstudio):
        try:
            tipo = TipoEstudio.query.filter_by(id=id_TipoEstudio).first()
            if tipo == None:
                raise NoneType("Ese Tipo de Estudio no existe")
            return tipo
        except:
            raise

    def get_by_name(name_TipoEstudio):
        try:
            tipo = TipoEstudio.query.filter_by(nombre=name_TipoEstudio).first() 
            if tipo == None:
                raise NoneType("Ese Tipo de Estudio no existe")
            return tipo
        except:
            raise        
        return        

    def get_estudios(id_TipoEstudio,page,per_page):
        TipoEstudio = TipoEstudio.query.filter_by(id=id_TipoEstudio).first()
        if(TipoEstudio == None):
            return False
        totales = TipoEstudio.estudios.count()
        datos = TipoEstudio.estudios.paginate(page,per_page,False).items
        numPaginas = TipoEstudio.estudios.paginate(page,per_page,False).pages
        return [totales,datos,numPaginas]

    def create(data):
        nombre = data['nombre'].replace(" ","_").lower()
        nuevo_TipoEstudio = TipoEstudio(
            nombre = nombre,
            nombre_mostrado = data['nombre_mostrado']
        )
        try:
            db.session.add(nuevo_TipoEstudio)
            db.session.commit()
            return nuevo_TipoEstudio
        except Exception as e:
            db.session.rollback()
            raise
            return False     

    def update(id_TipoEstudio,data):
        try:
            TipoEstudio_a_actualizar = TipoEstudio.get_by_id(id_TipoEstudio)
            if TipoEstudio_a_actualizar == None:
                raise NoneType("Ese Tipo de Estudio no existe")
            TipoEstudio_a_actualizar.nombre_mostrado = data['nombre_mostrado']
            db.session.commit()
            return TipoEstudio_a_actualizar
        except:
            raise
            return False        

    def delete(id_TipoEstudio):
        try:
            TipoEstudio_a_borrar = TipoEstudio.query.filter_by(id=id_TipoEstudio).first()
            if TipoEstudio_a_borrar == None:
                raise NoneType("Ese Tipo de Estudio no existe")
            if(TipoEstudio_a_borrar.estudios.count() != 0):
                raise NotEmptyRelationships('tiene_estudios_asociados') 
            db.session.delete(TipoEstudio_a_borrar)
            db.session.commit()
            return True
        except:
            db.session.rollback()
            raise
            return False