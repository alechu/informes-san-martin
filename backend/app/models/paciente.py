from app import db

from sqlalchemy import desc

from app.models.obraSocial import ObraSocial 

from app.helpers.exceptions import NoneObraSocial, NotEmptyRelationships

class  Paciente(db.Model):
    __tablename__ = "paciente"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), nullable=False)
    apellido = db.Column(db.String(50), nullable=False)
    dni = db.Column(db.String(10), unique=True, nullable=True)
    fecha_nac = db.Column(db.Date, nullable=True)
    sexo = db.Column(db.String(10), nullable=False)
    estado = db.Column(db.String(25), nullable=False)
    localidad = db.Column(db.String(30), nullable=False)
    obra_social_id = db.Column(db.Integer, db.ForeignKey('obra_social.id'), nullable=False)
    estudios = db.relationship("Estudio", backref='tiene_paciente', lazy="dynamic")

    def get_all(page,per_page):
        try:
            totales = db.session.query(Paciente).count()
            datos = Paciente.query.order_by(Paciente.apellido.asc()).paginate(page,per_page,False).items
            numPaginas = Paciente.query.paginate(page,per_page,False).pages
            res = [totales,datos,numPaginas]
            return res
        except:
            raise
            return False

    def get_by_id(id_paciente):
        try:
            return Paciente.query.filter_by(id=id_paciente).first()
        except Exception as e:
            raise
    
    def get_by_dni(dni_paciente):
        try:
            return Paciente.query.filter_by(dni=dni_paciente).first()
        except Exception as e:
            raise

    def get_by_lastname_and_bd(page,per_page,apellido,fecha_nac):
        try:
            query = Paciente.query
            if apellido != None:
                query = query.filter(Paciente.apellido==apellido)
            if fecha_nac != None:
                query = query.filter(Paciente.fecha_nac==fecha_nac)
            #query = query.all()
            totales = query.count()
            datos = query.paginate(page,per_page,False).items
            numPaginas = query.paginate(page,per_page,False).pages
            res = [totales,datos,numPaginas]
            return res
        except:
            raise
            return False
        

    def get_estudios(id_paciente,page,per_page):
        try:
            paciente = Paciente.get_by_id(id_paciente)
            totales = paciente.estudios.count()
            datos = paciente.estudios.order_by(desc("fecha")).paginate(page,per_page,False).items
            numPaginas = paciente.estudios.paginate(page,per_page,False).pages
            res = [totales,datos,numPaginas]
            return res
        except:
            raise
            return False
    
    def create(data_paciente):
        nuevo_paciente = Paciente(
            nombre = data_paciente['nombre'],
            apellido = data_paciente['apellido'],
            dni = data_paciente['dni'],
            fecha_nac = data_paciente['fecha_nac'],
            sexo = data_paciente['sexo'],
            estado = data_paciente['estado'],
            localidad = data_paciente['localidad']
        )
        try:
            obra = ObraSocial.get_by_id(data_paciente['obra_social_id'])
            nuevo_paciente.asociado_a = obra
            db.session.add(nuevo_paciente)
            db.session.commit()
            return nuevo_paciente
        except Exception as e:
            db.session.rollback()
            raise
            return False
    
    def delete(id_paciente):
        try:
            paciente_a_borrar = Paciente.get_by_id(id_paciente)
            if paciente_a_borrar.estudios.all() != []:
                raise NotEmptyRelationships("Tiene estudios asociados")
            db.session.delete(paciente_a_borrar)
            db.session.commit()
            return True
        except Exception as e:
            db.session.rollback()
            raise
            return False
    
    def update(paciente,data):
        try:
            # Chequeo para saber que campos actualizar
            if data['nombre'] != '': paciente.nombre = data['nombre']
            if data['apellido'] != '': paciente.apellido = data['apellido']
            if data['dni'] != '': paciente.dni = data['dni']
            if data['sexo'] != '': paciente.sexo = data['sexo']
            if data['localidad'] != '': paciente.localidad = data['localidad']
            if data['estado'] != '': paciente.estado = data['estado']
            if data['fecha_nac'] != None: paciente.fecha_nac = data['fecha_nac']
            if(data['obra_social_id'] != None):
                obra = ObraSocial.get_by_id(data['obra_social_id'])

                if(obra == None):
                    db.session.rollback()
                    raise NoneObraSocial("La Obra Social no existe")     

                paciente.asociado_a = obra
            db.session.commit()
            return paciente
        except:
            db.session.rollback()
            raise
        


     

