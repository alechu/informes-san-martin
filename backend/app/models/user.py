from app import db
from app.helpers import jwt_utils 

class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(8000), nullable=False)

    def login(username, password):

       user = User.query.filter_by(username=username, password=password).first()
       if user == None:
           return False
       else:
           token = jwt_utils.encode_auth_token(user.username)
           return [user.username,token]

    def auth(token):
        return jwt_utils.decode_auth_token(token)

