from app import db


class ObraSocial(db.Model):
    __tablename__="obra_social"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), nullable=False)
    pacientes = db.relationship('Paciente',backref='asociado_a', lazy='dynamic')


    def get_all():
        return ObraSocial.query.all()

    def get_by_id(id_obra_social):
        return ObraSocial.query.filter_by(id=id_obra_social).first()
    
    def get_pacientes(id_obra_social,page,per_page):
        obra = ObraSocial.query.filter_by(id=id_obra_social).first()
        if(obra == None):
            return False
        totales = obra.pacientes.count()
        datos = obra.pacientes.paginate(page,per_page,False).items
        numPaginas = obra.pacientes.paginate(page,per_page,False).pages
        return [totales,datos,numPaginas]

    def create(data):
        nueva_obra = ObraSocial(
            nombre = data['nombre']
        )
        try:
            db.session.add(nueva_obra)
            db.session.commit()
            return nueva_obra
        except Exception as e:
            db.session.rollback()
            raise
            return False

    def update(obra_a_actualizar,data):
        try:
            obra_a_actualizar.nombre = data['nombre']
            db.session.commit()
            return obra_a_actualizar
        except:
            raise
            return False
    
    def delete(id_obra_social):
        try:
            obra_a_borrar = ObraSocial.query.filter_by(id=id_obra_social).first()
            if(obra_a_borrar.pacientes.count() != 0):
                return 'tiene_pacientes'
            db.session.delete(obra_a_borrar)
            db.session.commit()
            return True
        except:
            db.session.rollback()
            raise
            return False