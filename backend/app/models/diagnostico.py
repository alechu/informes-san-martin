from app import db

from app.helpers.exceptions import NoneType

class Diagnostico(db.Model):
    __tablename__ = "diagnostico"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(1000), nullable=False)
    
    def get_all():
        try:
            return Diagnostico.query.order_by(Diagnostico.nombre.asc()).all()
        except:
            raise
            return False

    def get_by_id(id_Diagnostico):
        diag = Diagnostico.query.filter_by(id=id_Diagnostico).first()
        if diag == None:
            raise NoneType("Ese Diagnóstico no existe")
        else:
            return diag

    def create(data):
        nuevo_Diagnostico = Diagnostico(
            nombre = data['nombre']
        )
        try:
            db.session.add(nuevo_Diagnostico)
            db.session.commit()
            return nuevo_Diagnostico
        except Exception as e:
            db.session.rollback()
            raise
            return False     

    def update(id_Diagnostico,data):
        try:
            Diagnostico_a_actualizar = Diagnostico.query.filter_by(id=id_Diagnostico).first()
            if Diagnostico_a_actualizar == None:
                raise NoneType("Ese Diagnóstico no existe")
            Diagnostico_a_actualizar.nombre = data['nombre']
            db.session.commit()
            return Diagnostico_a_actualizar
        except:
            raise
            return False        

    def delete(id_Diagnostico):
        try:
            Diagnostico_a_borrar = Diagnostico.query.filter_by(id=id_Diagnostico).first()
            if Diagnostico_a_borrar == None:
                raise NoneType("Ese Diagnóstico no existe")           
            db.session.delete(Diagnostico_a_borrar)
            db.session.commit()
            return True
        except:
            db.session.rollback()
            raise
            return False
    