from app import db

from app.helpers.exceptions import NoneType, NotEmptyRelationships

class Especialista(db.Model):
    __tablename__ = "especialista"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), nullable=False)
    estudios_realizados = db.relationship(
        'Estudio', 
        backref="llevado_a_cabo_por", 
        lazy="dynamic"
    )

    def get_all():
        try:
            return Especialista.query.all()
        except:
            raise
            return False

    def get_by_id(id_especialista):
        try:
            especialista = Especialista.query.filter_by(id=id_especialista).first()
            if especialista == None:
                raise NoneType("Ese Especialista no existe")
            return especialista
        except:
            raise
            return False 

    def get_estudios(id_especialista,page,per_page):
        especialista = Especialista.query.filter_by(id=id_especialista).first()
        if(especialista == None):
            return False
        totales = especialista.estudios_realizados.count()
        datos = especialista.estudios_realizados.paginate(page,per_page,False).items
        numPaginas = especialista.estudios_realizados.paginate(page,per_page,False).pages
        return [totales,datos,numPaginas]

    def create(data):
        nuevo_especialista = Especialista(
            nombre = data['nombre']
        )
        try:
            db.session.add(nuevo_especialista)
            db.session.commit()
            return nuevo_especialista
        except Exception as e:
            db.session.rollback()
            raise
            return False     

    def update(id_especialista,data):
        try:
            especialista_a_actualizar = Especialista.query.filter_by(id=id_especialista).first()
            if especialista_a_actualizar == None:
                raise NoneType("Ese Especialista no existe")
            especialista_a_actualizar.nombre = data['nombre']
            db.session.commit()
            return especialista_a_actualizar
        except:
            raise
            return False        

    def delete(id_especialista):
        try:
            especialista_a_borrar = Especialista.query.filter_by(id=id_especialista).first()
            if especialista_a_borrar == None:
                raise NoneType("Ese Especialista no existe")
            if(especialista_a_borrar.estudios_realizados.count() != 0):
                raise NotEmptyRelationships("Ese Especialista tiene estudios asociados")
            db.session.delete(especialista_a_borrar)
            db.session.commit()
            return True
        except:
            db.session.rollback()
            raise
            return False
