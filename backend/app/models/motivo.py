from app import db

from app.helpers.exceptions import NoneType, NotEmptyRelationships

class Motivo(db.Model):
    __tablename__ = "motivo"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(191), nullable=False, unique=True)
    estudios = db.relationship(
        'Estudio',
        backref="tiene_motivo",
        lazy="dynamic"
    )

    def get_all():
        try:
            return Motivo.query.all()
        except:
            raise
            return False

    def get_by_id(id_Motivo):
        try:
            motivo = Motivo.query.filter_by(id=id_Motivo).first()
            if motivo == None:
                raise NoneType("Ese Motivo no existe")
            return motivo
        except:
            raise 
            return False 

    def get_estudios(id_Motivo,page,per_page):
        Motivo = Motivo.query.filter_by(id=id_Motivo).first()
        if(Motivo == None):
            return False
        totales = Motivo.estudios.count()
        datos = Motivo.estudios.paginate(page,per_page,False).items
        numPaginas = Motivo.estudios.paginate(page,per_page,False).pages
        return [totales,datos,numPaginas]

    def create(data):
        nuevo_Motivo = Motivo(
            nombre = data['nombre']
        )
        try:
            db.session.add(nuevo_Motivo)
            db.session.commit()
            return nuevo_Motivo
        except Exception as e:
            db.session.rollback()
            raise
            return False     

    def update(id_motivo,data):
        try:
            Motivo_a_actualizar = Motivo.get_by_id(id_motivo)
            if Motivo_a_actualizar == None:
                raise NoneType("Ese Motivo no existe")
            Motivo_a_actualizar.nombre = data['nombre']
            db.session.commit()
            return Motivo_a_actualizar
        except:
            raise
            return False        

    def delete(id_Motivo):
        try:
            Motivo_a_borrar = Motivo.query.filter_by(id=id_Motivo).first()
            if Motivo_a_borrar == None:
                raise NoneType("Ese Motivo no existe")
            if(Motivo_a_borrar.estudios.count() != 0):
                raise NotEmptyRelationships("Ese Motivo tiene estudios asociados")
            db.session.delete(Motivo_a_borrar)
            db.session.commit()
            return True
        except:
            db.session.rollback()
            raise
            return False    

    