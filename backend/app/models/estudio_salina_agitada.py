from sqlalchemy import inspect

from app import db

from app.models.estudio import Estudio
from app.models.especialista import Especialista
from app.models.tipo_estudio import TipoEstudio
from app.models.motivo import Motivo 
from app.models.diagnostico import Diagnostico
from app.models.paciente import Paciente
from app.models.conclusion_predefinida import ConclusionPredefinida

from app.helpers.exceptions import NoneType

class EstudioSalinaAgitada(Estudio):
    __tablename__ = "estudio_salina_agitada"
    id = db.Column(db.Integer, db.ForeignKey('estudio.id'), primary_key=True)
    conclusion_predefinida = db.Column(
        db.Integer,
        db.ForeignKey('conclusion_predefinida.id'),
        nullable=False
    )

    __mapper_args__ = {
        'polymorphic_identity': 'salina_agitada',
    }

    def create(data_estudio):
        nuevo_estudio = EstudioSalinaAgitada(
            fecha = data_estudio['fecha'],
            conclusion = data_estudio['conclusion'],
        )
        try:
            especialista = Especialista.get_by_id(data_estudio['id_especialista'])
            tipo = TipoEstudio.get_by_name(data_estudio['nombre_tipo'])
            motivo = Motivo.get_by_id(data_estudio['id_motivo'])
            diagnostico_obligatorio = Diagnostico.get_by_id(data_estudio['id_diagnostico_obligatorio'])

            if(data_estudio['id_diagnostico_opcional_1'] != None):
                diagnostico_opcional_1 = Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_1'])
                nuevo_estudio.diagnostico_opcional_1 = diagnostico_opcional_1

            if(data_estudio['id_diagnostico_opcional_2'] != None):
                diagnostico_opcional_2 = Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_2'])
                nuevo_estudio.diagnostico_opcional_2 = diagnostico_opcional_2
            
            paciente = Paciente.get_by_id(data_estudio['id_paciente'])
            conclusion_predefinida = ConclusionPredefinida.get_by_id(data_estudio['id_conclusion_predefinida'])

            # Asigno los Foreign

            nuevo_estudio.llevado_a_cabo_por = especialista
            nuevo_estudio.es_de_tipo = tipo
            nuevo_estudio.tiene_motivo = motivo
            nuevo_estudio.tiene_paciente = paciente
            nuevo_estudio.diagnostico_obligatorio = diagnostico_obligatorio
            nuevo_estudio.tiene_conclusion_predefinida = conclusion_predefinida

            # Lo inserto en la DB

            db.session.add(nuevo_estudio)
            db.session.commit()
            return nuevo_estudio
        except:
            db.session.rollback()
            raise
            return False


    def update(id_estudio,data_estudio):
        estudio_a_actualizar = Estudio.get_by_id(id_estudio)
        if estudio_a_actualizar == None:
            raise NoneType("Ese estudio no existe")
        if not estudio_a_actualizar.tipo == data_estudio['nombre_tipo']:
            raise NoneType("Expected: "+estudio_a_actualizar.tipo+". Got: "+data_estudio['nombre_tipo'])     
        campos_no_deseados = [
            'id',
            'diagnostico_obligatorio',
            'diagnostico_opcional_1', 
            'diagnostico_opcional_2',
            'diagnostico_obligatorio_id',
            'diagnostico_opcional_1_id',
            'diagnostico_opcional_2_id',
            'especialista',
            'paciente',
            'tipo',
            'motivo',
            'tiene_paciente',
            'llevado_a_cabo_por',
            'es_de_tipo',
            'tiene_motivo',
            'tiene_conclusion_predefinida',
            'conclusion_predefinida',
        ]
        try:

            # Actualizo todos los campos que no son Foreign
            mapper = inspect(estudio_a_actualizar)
            for column in mapper.attrs:
                if str(column.key) not in campos_no_deseados:
                    if(data_estudio[str(column.key)] != None):
                        query = "estudio_a_actualizar."+str(column.key)+" = '"+str(data_estudio[str(column.key)])+"'"
                        exec(query)
            
        except Exception as e:
            raise e

        try:

            # Actualizo todos los campos que si son Foreign
            campos_deseados = {
                'id_especialista':("llevado_a_cabo_por","Especialista"),
                'id_motivo':("tiene_motivo","Motivo"),
                'id_diagnostico_obligatorio':("diagnostico_obligatorio","Diagnostico"),
                'id_paciente':("tiene_paciente","Paciente"),
                'id_conclusion_predefinida':("tiene_conclusion_predefinida","ConclusionPredefinida")
            }            

            for key in campos_deseados:
                if (data_estudio[key] != None):
                    query_object = campos_deseados[key][1]+".get_by_id("+str(data_estudio[key])+")"
                    db_object = eval(query_object)
                    if db_object == None:
                        db.session.rollback()
                        raise NoneType("La ID del campo "+campos_deseados[key][1]+" no es válida.")
                    query_update = "estudio_a_actualizar."+campos_deseados[key][0]+" = db_object"
                    exec(query_update)
            
            # Actualizo los diagnosticos opcionales
            if(data_estudio['id_diagnostico_opcional_1'] != None):
                if(data_estudio['id_diagnostico_opcional_1'] == 0):
                    estudio_a_actualizar.diagnostico_opcional_1_id = None
                else:
                    if(Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_1']) == None):
                        raise NoneType("La ID del campo id_diagnostico_opcional_1 no es válida.")                
                    estudio_a_actualizar.diagnostico_opcional_1_id = data_estudio['id_diagnostico_opcional_1']
            if(data_estudio['id_diagnostico_opcional_2'] != None):
                if(data_estudio['id_diagnostico_opcional_2'] == 0):
                    estudio_a_actualizar.diagnostico_opcional_2_id = None
                else:
                    if(Diagnostico.get_by_id(data_estudio['id_diagnostico_opcional_2']) == None):
                        raise NoneType("La ID del campo id_diagnostico_opcional_2 no es válida.")
                    estudio_a_actualizar.diagnostico_opcional_2_id = data_estudio['id_diagnostico_opcional_2']            

            db.session.commit()
            return estudio_a_actualizar

        except Exception as e:
            db.session.rollback()
            raise e
            return False   
