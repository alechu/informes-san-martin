from app import db

from app.helpers.exceptions import NoneType,NotEmptyRelationships

class ConclusionPredefinida(db.Model):
    __tablename__ = "conclusion_predefinida"
    id = db.Column(db.Integer, primary_key=True)
    escenario = db.Column(db.String(1000), nullable=False)
    conclusion = db.Column(db.String(400), nullable=False)
    estudios = db.relationship(
        'EstudioSalinaAgitada',
        backref="tiene_conclusion_predefinida",
        lazy="dynamic"
    )

    def get_all():
        try:
            return ConclusionPredefinida.query.all()
        except:
            raise
            return False

    def get_by_id(id_conclusion):
        try:
            conclusion = ConclusionPredefinida.query.filter_by(id=id_conclusion).first()
            if conclusion == None:
                raise NoneType("Esa conclusión no existe")
            else:
                return conclusion
        except:
            raise
            return False

    def create(data_conclusion):
        nueva_conclusion = ConclusionPredefinida(
            escenario = data_conclusion['escenario'],
            conclusion = data_conclusion['conclusion']
        )
        try:
            db.session.add(nueva_conclusion)
            db.session.commit()
            return nueva_conclusion
        except Exception as e:
            db.session.rollback()
            raise e
            return False

    def update(id_conclusion,data_conclusion):
        conclusion_a_actualizar = ConclusionPredefinida.query.filter_by(id=id_conclusion).first()
        if conclusion_a_actualizar == None:
            raise NoneType("Esa conclusión predefinida no existe")

        try:
            if data_conclusion['escenario'] != '': 
                conclusion_a_actualizar.escenario = data_conclusion['escenario']
            if data_conclusion['conclusion'] != '':
                conclusion_a_actualizar.conclusion = data_conclusion['conclusion']

            db.session.commit()
            return conclusion_a_actualizar
        except Exception as e:
            db.session.rollback()
            raise e
            return False
    
    def delete(id_conclusion):
        conclusion_a_borrar = ConclusionPredefinida.query.filter_by(id=id_conclusion).first()
        if conclusion_a_borrar == None: 
            raise NoneType("Esa conclusión predefinida no existe")
        
        try:
            if (conclusion_a_borrar.estudios.count() != 0):
                raise NotEmptyRelationships("Esa conclusión tiene estudios asociados")
            else:
                db.session.delete(conclusion_a_borrar)
                db.session.commit()
                return True
        except:
            db.session.rollback()
            raise
            return False


