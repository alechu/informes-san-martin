import json

from flask import Response

from app.helpers.jwt_utils import decode_auth_token

def authToken(header):
    if header:
        try:
            _, token = header.split()
            decoded_token = decode_auth_token(token)
            if (("inválido" in str(decoded_token)) or ("expirado" in str(decoded_token))) :
                datos = {
                    'status': 401, 
                    'body': decoded_token
                    }
                return Response(
                    json.dumps(datos), 
                    status=401,
                    mimetype='application/json'
                    )                    
        except ValueError:
            datos = {
                'status': 401, 
                'body': 'JWT Token not found'
                }
            return Response(
                json.dumps(datos), 
                status=401,
                mimetype='application/json'
                )                
    else:
        datos = {
            'status': 401, 
            'body': 'JWT Token not found'
            }
        return Response(
            json.dumps(datos), 
            status=401,
            mimetype='application/json'
            ) 


