import json
import datetime

from flask import jsonify
from sqlalchemy import inspect

def serializeSQLAlchemyInheritance(sqlresult,campos_no_deseados=[]):
    parsed_sql_object = json.loads("{}")
    mapper = inspect(sqlresult)
    for column in mapper.attrs:
        if (isinstance(getattr(sqlresult, str(column.key)),datetime.date)):
            if str(column.key) in campos_no_deseados:
                continue            
            objectToAppend = {str(column.key):(getattr(sqlresult, str(column.key))).strftime("%Y-%m-%d")}
        else:
            if str(column.key) in campos_no_deseados:
                continue
            else:
                objectToAppend = {str(column.key):getattr(sqlresult, str(column.key))}   
        parsed_sql_object.update(objectToAppend)
    return (parsed_sql_object)
