class NoneType(Exception):
    pass

class InvalidForm(Exception):
    pass

class NoneObraSocial(Exception):
    pass

class NotEmptyRelationships(Exception):
    pass