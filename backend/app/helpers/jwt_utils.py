import jwt
import datetime
from os import environ
from flask import current_app

def encode_auth_token(user_username):
    """
    Generates the Auth Token
    :return: string
    """

    conf = current_app.config

    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, hours=12, minutes=0, seconds=0),
            'iat': datetime.datetime.utcnow(),
            'sub': user_username
        }
        return jwt.encode(
            payload,
            str(conf["SECRET_KEY"]),
            algorithm='HS256'
        )
    except Exception as e:
        return e

def decode_auth_token(auth_token):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    conf = current_app.config

    try:
        payload = jwt.decode(auth_token, str(conf["SECRET_KEY"]), algorithms=["HS256"])
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return 'JWT Token expirado.'
    except jwt.InvalidTokenError:
        return 'JWT Token inválido.'

