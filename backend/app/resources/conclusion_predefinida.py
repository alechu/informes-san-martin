import json

from flask import Response, request
from flask_restful import Resource

from app.models.conclusion_predefinida import ConclusionPredefinida
from app.forms.formConclusionPredefinida import formConclusionPredefinida
from app.helpers.serialize import serializeSQLAlchemy
from app.helpers.auth_helper import authToken

class Conclusion(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response          
        id_conclusion = request.args.get("id")
        all_conclusiones = request.args.get("all")
        if(id_conclusion == None and all_conclusiones == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )           
        if(id_conclusion != None and all_conclusiones != None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            if(all_conclusiones == 'True'):
                conclusiones = ConclusionPredefinida.get_all()
                serialized_data = []
                for conclusion in conclusiones:
                    serialized_data.append(serializeSQLAlchemy(conclusion))
                datos = {
                    'status': 200, 
                    'body': serialized_data
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )               
            else:
                if(id_conclusion != None):
                    conclusion = ConclusionPredefinida.get_by_id(id_conclusion)
                    datos = {
                        'status': 200, 
                        'body': serializeSQLAlchemy(conclusion)
                        }
                    return Response(
                        json.dumps(datos), 
                        status=200,
                        mimetype='application/json'
                        )
                else:
                    datos = {
                        'status': 400, 
                        'body': 'Bad Request'
                        }
                    return Response(
                        json.dumps(datos), 
                        status=400,
                        mimetype='application/json'
                        )                    

        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Esa Conclusión predefinida no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )                                
            else:
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        try:
            form = formConclusionPredefinida(request.form)
            if(not form.validate()):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                
            nueva_conclsion = ConclusionPredefinida.create(form.data)
            datos = {
                'status':201, 
                'body': 'Conclusión predefinida creada exitosamente',
                'conclusion_predefinida': serializeSQLAlchemy(nueva_conclsion)
                }
            return Response(
                json.dumps(datos), 
                status=201,
                mimetype='application/json'
                )
        except Exception as e:
            print(str(e))
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )        

    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_conclusion = request.args.get("id")
        if(id_conclusion == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        nuevos_datos = formConclusionPredefinida(request.form)
        try:
            conclusion_actualizada = ConclusionPredefinida.update(id_conclusion,nuevos_datos.data)              
            datos = {
                'status': 200, 
                'body': 'Conclusión predefinida actualizada exitosamente',
                'conclusion_predefinida': serializeSQLAlchemy(conclusion_actualizada)
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        except Exception as e:
            if ("no existe" in str(e)):
                datos = {
                    'status': 404,
                    'body': 'Not Found', 
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )
            else:           
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_conclusion = request.args.get("id")
        if(id_conclusion == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            result = ConclusionPredefinida.delete(id_conclusion)
            if(result == True):
                datos = {
                    'status': 200, 
                    'body': 'Conclusión predefinida eliminada exitosamente'
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )                          
        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Esa Conclusión predefinida no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            elif ('asociados' in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Esa Conclusión predefinida tiene estudios asociados'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            else:                                  
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )