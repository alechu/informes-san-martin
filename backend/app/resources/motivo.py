import json

from flask import Response, request
from flask_restful import Resource


from app.models.motivo import Motivo as MotivoModel

from app.forms.formMotivo import formMotivo

from app.helpers.serialize import serializeSQLAlchemy

from app.helpers.auth_helper import authToken

class Motivo(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_motivo = request.args.get("id")
        all_motivos = request.args.get("all")
        if(id_motivo == None and all_motivos == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )           
        if(id_motivo != None and all_motivos != None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )

        try:
            if(all_motivos == 'True'):
                motivos = MotivoModel.get_all()
                serialized_data = []
                for motivo in motivos:
                    serialized_data.append(serializeSQLAlchemy(motivo))
                datos = {
                    'status': 200, 
                    'body': serialized_data
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )               
            else:
                if(id_motivo != None):
                    motivo = MotivoModel.get_by_id(id_motivo)
                    datos = {
                        'status': 200, 
                        'body': serializeSQLAlchemy(motivo)
                        }
                    return Response(
                        json.dumps(datos), 
                        status=200,
                        mimetype='application/json'
                        )
                else:
                    datos = {
                        'status': 400, 
                        'body': 'Bad Request'
                        }
                    return Response(
                        json.dumps(datos), 
                        status=400,
                        mimetype='application/json'
                        )                    

        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese Motivo no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )                              
            else:
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        try:
            form = formMotivo(request.form)
            if(not form.validate()):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                
            nuevo_motivo = MotivoModel.create(form.data)
            datos = {
                'status': 201, 
                'body': 'Motivo creado exitosamente',
                'motivo': serializeSQLAlchemy(nuevo_motivo)
                }
            return Response(
                json.dumps(datos), 
                status=201,
                mimetype='application/json'
                )
        except Exception as e:
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )

    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_motivo = request.args.get("id")
        nuevos_datos = formMotivo(request.form)
        if(id_motivo == None or not nuevos_datos.validate()):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            motivo_actualizado = MotivoModel.update(
                id_motivo,
                nuevos_datos.data
                )              
            datos = {
                'status': 200, 
                'body': 'Motivo actualizado exitosamente',
                'motivo': serializeSQLAlchemy(motivo_actualizado)
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        except Exception as e:
            if ("no existe" in str(e)):
                datos = {
                    'status': 404,
                    'body': 'Not Found', 
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            else:
                print(str(e))           
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_motivo = request.args.get("id")
        if(id_motivo == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            result = MotivoModel.delete(id_motivo)
            if(result == True):
                datos = {
                    'status': 200, 
                    'body': 'Motivo eliminado exitosamente'
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )                          
        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese Motivo no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            elif ('asociados' in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Ese Motivo tiene estudios asociados'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            else:                                  
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )                                             