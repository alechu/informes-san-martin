import json

from flask import Response, request
from flask_restful import Resource


from app.models.tipo_estudio import TipoEstudio as TipoEstudioModel

from app.forms.formTipoEstudio import formTipoEstudio

from app.helpers.serialize import serializeSQLAlchemy

from app.helpers.auth_helper import authToken

class TipoEstudio(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_tipo_estudio = request.args.get("id")
        all_tipos = request.args.get("all")
        if(id_tipo_estudio == None and all_tipos == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )           
        if(id_tipo_estudio != None and all_tipos != None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )

        try:
            if(all_tipos == 'True'):
                tipos = TipoEstudioModel.get_all()
                serialized_data = []
                for tipo in tipos:
                    serialized_data.append(serializeSQLAlchemy(tipo))
                datos = {
                    'status': 200, 
                    'body': serialized_data
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )               
            else:
                if(id_tipo_estudio != None):
                    tipo = TipoEstudioModel.get_by_id(id_tipo_estudio)
                    datos = {
                        'status': 200, 
                        'body': serializeSQLAlchemy(tipo)
                        }
                    return Response(
                        json.dumps(datos), 
                        status=200,
                        mimetype='application/json'
                        )
                else:
                    datos = {
                        'status': 400, 
                        'body': 'Bad Request'
                        }
                    return Response(
                        json.dumps(datos), 
                        status=400,
                        mimetype='application/json'
                        )                    

        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese Tipo de Estudio no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )                              
            else:
                print(str(e))
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        try:
            form = formTipoEstudio(request.form)
            if(not form.validate()):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                
            nuevo_tipo = TipoEstudioModel.create(form.data)
            datos = {
                'status': 201, 
                'body': 'Tipo de Estudio creado exitosamente',
                'tipo_estudio': serializeSQLAlchemy(nuevo_tipo)
                }
            return Response(
                json.dumps(datos), 
                status=201,
                mimetype='application/json'
                )
        except Exception as e:
            if('Duplicate' in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request',
                    'details': 'Ese Tipo de Estudio ya existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )
            else:
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_tipo = request.args.get("id")
        nuevos_datos = formTipoEstudio(request.form)
        if(id_tipo == None or nuevos_datos.data['nombre_mostrado'] == ''):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            tipo_actualizado = TipoEstudioModel.update(
                id_tipo,
                nuevos_datos.data
                )              
            datos = {
                'status': 200, 
                'body': 'Tipo actualizado exitosamente',
                'tipo_estudio': serializeSQLAlchemy(tipo_actualizado)
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        except Exception as e:
            if ("no existe" in str(e)):
                datos = {
                    'status': 404,
                    'body': 'Not Found', 
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            elif ('Duplicate' in str(e)):
                datos = {
                    'status': 400,
                    'body': 'Bad Request', 
                    'details': 'Ese Tipo de Estudio ya existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )                
            else:           
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_tipo = request.args.get("id")
        if(id_tipo == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            result = TipoEstudioModel.delete(id_tipo)
            if(result == True):
                datos = {
                    'status': 200, 
                    'body': 'Tipo de Estudio eliminado exitosamente'
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )                          
        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese Tipo de Estudio no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            elif ('asociados' in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Ese Tipo de Estudio tiene estudios asociados'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            else:                                  
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )                                             