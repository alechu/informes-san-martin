import json

from flask import Response, request
from flask_restful import Resource

from app.models.desc_predefinida import DescripcionPredefinida as DescripcionPredefinidaModel
from app.forms.formDescripcionPredefinida import formDescripcionPredefinida
from app.helpers.serialize import serializeSQLAlchemy
from app.helpers.auth_helper import authToken

class DescripcionPredefinida(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_descripcion = request.args.get("id")
        all_descripciones = request.args.get("all")
        if(id_descripcion == None and all_descripciones == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )           
        if(id_descripcion != None and all_descripciones != None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )

        try:
            if(all_descripciones == 'True'):
                descripciones = DescripcionPredefinidaModel.get_all()
                serialized_data = []
                for desc in descripciones:
                    serialized_data.append(serializeSQLAlchemy(desc))
                datos = {
                    'status': 200, 
                    'body': serialized_data
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )               
            else:
                if(id_descripcion != None):
                    desc = DescripcionPredefinidaModel.get_by_id(id_descripcion)
                    datos = {
                        'status': 200, 
                        'body': serializeSQLAlchemy(desc)
                        }
                    return Response(
                        json.dumps(datos), 
                        status=200,
                        mimetype='application/json'
                        )
                else:
                    datos = {
                        'status': 400, 
                        'body': 'Bad Request'
                        }
                    return Response(
                        json.dumps(datos), 
                        status=400,
                        mimetype='application/json'
                        )                    

        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Esa Descripción predefinida no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )                              
            else:
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        try:
            form = formDescripcionPredefinida(request.form)
            if(not form.validate()):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                
            nueva_descripcion = DescripcionPredefinidaModel.create(form.data)
            datos = {
                'status': 201, 
                'body': 'Descripción predefinida creada exitosamente',
                'descripcion_predefinida': serializeSQLAlchemy(nueva_descripcion)
                }
            return Response(
                json.dumps(datos), 
                status=201,
                mimetype='application/json'
                )
        except Exception as e:
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )

    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_descripcion = request.args.get("id")
        nuevos_datos = formDescripcionPredefinida(request.form)
        if(id_descripcion == None or not nuevos_datos.validate()):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            desc_actualizada = DescripcionPredefinidaModel.update(
                id_descripcion,
                nuevos_datos.data
                )              
            datos = {
                'status': 200, 
                'body': 'Descripción predefinida actualizada exitosamente',
                'descripcion_predefinida': serializeSQLAlchemy(desc_actualizada)
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        except Exception as e:
            if ("no existe" in str(e)):
                datos = {
                    'status': 404,
                    'body': 'Not Found', 
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            else:
                print(str(e))           
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_descripcion = request.args.get("id")
        if(id_descripcion == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            result = DescripcionPredefinidaModel.delete(id_descripcion)
            if(result == True):
                datos = {
                    'status': 200, 
                    'body': 'Descripción predefinida eliminada exitosamente'
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )                          
        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Esa Descripción predefinida no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            elif ('asociados' in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Esa Descripción predefinida tiene estudios asociados'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            else:     
                print(str(e))                             
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )                                             