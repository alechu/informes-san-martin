import json 

from flask import Response, request
from flask_restful import Resource


# Importo los modelos

from app.models.estudio import Estudio as EstudioBasicoModel
from app.models.estudio_2d_doppler import EstudioDoppler as EstudioDopplerModel
from app.models.estudio_congenitas import EstudioCongenitas as EstudioCongenitasModel
from app.models.estudio_disincronia import EstudioDisincronia as EstudioDisincroniaModel
from app.models.estudio_salina_agitada import EstudioSalinaAgitada as EstudioSalinaAgitadaModel
from app.models.estudio_transesofagico import EstudioTransesofagico as EstudioTransesofagicoModel

# Importo los forms

from app.forms.formEstudioBasico import formEstudioBasico
from app.forms.formEstudioDoppler import formEstudioDoppler
from app.forms.formEstudioCongenitas import formEstudioCongenitas
from app.forms.formEstudioDisincronia import formEstudioDisincronia
from app.forms.formEstudioSalinaAgitada import formEstudioSalinaAgitada
from app.forms.formEstudioTransesofagico import formEstudioTransesofagico

# Utilidades

from app.helpers.serialize import serializeSQLAlchemy
from app.helpers.serializeInheritance import serializeSQLAlchemyInheritance
from app.helpers.exceptions import InvalidForm
from app.helpers.auth_helper import authToken

class Estudio(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_estudio = request.args.get('id')
        if id_estudio == None:
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            estudio = EstudioBasicoModel.get_by_id(id_estudio)
            if estudio == None:
                datos = {
                    'status': 404, 
                    'body': 'Ese Estudio no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )

            # La lógica de esto se puede leer en el comentario
            # del método POST más abajo.
            campos_no_deseados = [
                'id',
                'fecha',
                'especialista',
                'tipo',
                'motivo',
                'diagnostico_obligatorio_id',
                'diagnostico_obligatorio',
                'diagnostico_opcional_1_id',
                'diagnostico_opcional_1',
                'diagnostico_opcional_2',
                'diagnostico_opcional_2_id',
                'paciente',
                'tiene_motivo',
                'es_de_tipo',
                'llevado_a_cabo_por',
                'tiene_paciente',
                ]
            if estudio.diagnostico_opcional_1 == None:
                diagnostico_opcional_1_json = None
            else:
                diagnostico_opcional_1_json = {
                    'id':estudio.diagnostico_opcional_1.id,
                    'nombre':estudio.diagnostico_opcional_1.nombre
                }
            if estudio.diagnostico_opcional_2 == None:
                diagnostico_opcional_2_json = None
            else:
                diagnostico_opcional_2_json = {
                    'id':estudio.diagnostico_opcional_2.id,
                    'nombre':estudio.diagnostico_opcional_2.nombre
                }
            if estudio.tipo == 'salina_agitada':
                atributos = serializeSQLAlchemy(estudio.tiene_conclusion_predefinida)
            elif estudio.tipo == 'transesofagico':
                atributos = {
                    'descripcion':estudio.descripcion,
                    'conclusion':estudio.conclusion
                }
            elif estudio.tipo == '2d_doppler':
                #campos_no_deseados.append("descripcion_predefinida")
                atributos = serializeSQLAlchemy(estudio, campos_no_deseados)  
                #desc_predefinida =  serializeSQLAlchemy(estudio.tiene_descripcion) 
                #atributos["descripcion_predefinida"] = desc_predefinida
                atributos["conclusion"] = estudio.conclusion  
            else:
                atributos = serializeSQLAlchemyInheritance(estudio,campos_no_deseados)                            
            datos = {
                'status':200,
                'body':{
                    'estudio':{
                        'informacion_general':{
                            'id':estudio.id,
                            'fecha':str(estudio.fecha),
                            'especialista':{
                                'id':estudio.llevado_a_cabo_por.id,
                                'nombre':estudio.llevado_a_cabo_por.nombre
                            },
                            'tipo':{
                                'id':estudio.es_de_tipo.id,
                                'nombre_id':estudio.es_de_tipo.nombre,
                                'nombre_mostrado':estudio.es_de_tipo.nombre_mostrado
                            },
                            'motivo':{
                                'id':estudio.tiene_motivo.id,
                                'nombre':estudio.tiene_motivo.nombre
                            },
                            'diagnostico_obligatorio':{
                                'id':estudio.diagnostico_obligatorio.id,
                                'nombre':estudio.diagnostico_obligatorio.nombre
                            },
                            'diagnostico_opcional_1':diagnostico_opcional_1_json,
                            'diagnostico_opcional_2':diagnostico_opcional_2_json,
                            'paciente':{
                                'datos':serializeSQLAlchemy(
                                    estudio.tiene_paciente,
                                    ['obra_social_id']
                                    ),
                                'obra_social':serializeSQLAlchemy(
                                    estudio.tiene_paciente.asociado_a
                                    )
                            }
                        },
                        'atributos':atributos
                    },
                },
            }
            # Respuesta correcta
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        # Respuestas de error            
        except Exception as e:
            print(str(e))
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )

    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        # Acá controlo que el tipo de estudio sea válido
        try:
            tipo_estudio = request.form['nombre_tipo']
        except:
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            # Acá controlo qué tipo de Estudio se va a dar de Alta
            if tipo_estudio == 'disincronia':
                form_estudio = formEstudioDisincronia(request.form)
                if not form_estudio.validate():
                    raise InvalidForm("Formulario invalido")                     
                nuevo_estudio = EstudioDisincroniaModel.create(form_estudio.data)

            elif tipo_estudio == 'salina_agitada':
                form_estudio = formEstudioSalinaAgitada(request.form)
                if not form_estudio.validate():
                    raise InvalidForm("Formulario invalido")
                nuevo_estudio = EstudioSalinaAgitadaModel.create(form_estudio.data)

            elif tipo_estudio == "2d_doppler":
                form_estudio = formEstudioDoppler(request.form)
                if not form_estudio.validate():
                    raise InvalidForm("Formulario invalido")
                nuevo_estudio = EstudioDopplerModel.create(form_estudio.data)

            elif tipo_estudio == 'congenitas':
                form_estudio = formEstudioCongenitas(request.form)
                if not form_estudio.validate():
                    raise InvalidForm("Formulario invalido")
                nuevo_estudio = EstudioCongenitasModel.create(form_estudio.data)

            elif tipo_estudio == 'transesofagico':
                form_estudio = formEstudioTransesofagico(request.form)
                if not form_estudio.validate():
                    raise InvalidForm("Formulario invalido")
                nuevo_estudio = EstudioTransesofagicoModel.create(form_estudio.data)

            else:
                form_estudio = formEstudioBasico(request.form)
                if not form_estudio.validate():
                    raise InvalidForm("Formulario invalido")
                nuevo_estudio = EstudioBasicoModel.create(form_estudio.data)
            
            # Fix por si son nulos los diagnosticos opcionales
            if nuevo_estudio.diagnostico_opcional_1 == None:
                diagnostico_opcional_1_json = None
            else:
                diagnostico_opcional_1_json = {
                    'id':nuevo_estudio.diagnostico_opcional_1.id,
                    'nombre':nuevo_estudio.diagnostico_opcional_1.nombre
                }

            if nuevo_estudio.diagnostico_opcional_2 == None:
                diagnostico_opcional_2_json = None
            else:
                diagnostico_opcional_2_json = {
                    'id':nuevo_estudio.diagnostico_opcional_2.id,
                    'nombre':nuevo_estudio.diagnostico_opcional_2.nombre
                }
            
            """   

            Preparado de JSON para devolver:
            Los campos no deseados se usan en las funciones serializeSQLAlchemy
            y serializeSQLAlchemyInheritance. 
            Son para iterar sobre los atributos de un objeto resultado y evitar
            esos campos.
            Se evitan porque hay algunos campos que son valores y otros que son
            foreign keys. Las foreign keys se asignan a mano, no las serializa la
            funcion. 

            """
            campos_no_deseados = [
                'id',
                'fecha',
                'especialista',
                'tipo',
                'motivo',
                'diagnostico_obligatorio_id',
                'diagnostico_obligatorio',
                'diagnostico_opcional_1_id',
                'diagnostico_opcional_1',
                'diagnostico_opcional_2',
                'diagnostico_opcional_2_id',
                'paciente',
                'tiene_motivo',
                'es_de_tipo',
                'llevado_a_cabo_por',
                'tiene_paciente',
                ]
            if tipo_estudio == 'salina_agitada':
                atributos = serializeSQLAlchemy(nuevo_estudio.tiene_conclusion_predefinida)
            elif tipo_estudio == 'transesofagico':
                atributos = {
                    'conclusion':nuevo_estudio.conclusion
                }                
            elif tipo_estudio == '2d_doppler':
                #campos_no_deseados.append("tiene_descripcion")
                #campos_no_deseados.append("descripcion_predefinida")
                atributos = serializeSQLAlchemy(nuevo_estudio, campos_no_deseados)  
                #desc_predefinida =  serializeSQLAlchemy(nuevo_estudio.tiene_descripcion) 
                #atributos["descripcion_predefinida"] = desc_predefinida 
                atributos["conclusion"] = nuevo_estudio.conclusion           
            else:
                atributos = serializeSQLAlchemyInheritance(nuevo_estudio,campos_no_deseados)
            datos = {
                'status':201,
                'body':{
                    'estudio':{
                        'informacion_general':{
                            'id':nuevo_estudio.id,
                            'fecha':str(nuevo_estudio.fecha),
                            'especialista':{
                                'id':nuevo_estudio.llevado_a_cabo_por.id,
                                'nombre':nuevo_estudio.llevado_a_cabo_por.nombre,
                            },
                            'tipo':{
                                'id':nuevo_estudio.es_de_tipo.id,
                                'nombre_id':nuevo_estudio.es_de_tipo.nombre,
                                'nombre_mostrado':nuevo_estudio.es_de_tipo.nombre_mostrado,
                            },
                            'motivo':{
                                'id':nuevo_estudio.tiene_motivo.id,
                                'nombre':nuevo_estudio.tiene_motivo.nombre,
                            },
                            'diagnostico_obligatorio':{
                                'id':nuevo_estudio.diagnostico_obligatorio.id,
                                'nombre':nuevo_estudio.diagnostico_obligatorio.nombre,
                            },
                            'diagnostico_opcional_1':diagnostico_opcional_1_json,
                            'diagnostico_opcional_2':diagnostico_opcional_2_json,
                            'paciente':{
                                'datos':serializeSQLAlchemy(
                                    nuevo_estudio.tiene_paciente,
                                    ['obra_social_id']
                                    ),
                                'obra_social':serializeSQLAlchemy(
                                    nuevo_estudio.tiene_paciente.asociado_a
                                    )
                            }
                        },
                        'atributos':atributos
                    },
                },
            }
            # Respuesta correcta
            return Response(
                json.dumps(datos), 
                status=201,
                mimetype='application/json'
                )            

        # Respuestas de error
        except Exception as e:
            if ('Formulario' in str(e)):
                print(str(e))
                datos = {
                    'status': 400, 
                    'body': 'Bad Request'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            elif ('cannot be null' in str(e)):
                print(str(e))
                datos = {
                    'status': 400, 
                    'body': 'Bad Request',
                    'details': 'Una de las Foreign ID que se enviaron no existen'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    ) 
            else:
                print(str(e))
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )


    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_estudio = request.args.get('id')
        if id_estudio == None:
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )            
        try:
            tipo_estudio = request.form['nombre_tipo']
        except:
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:

            """

            Acá se chequean dos cosas:
            Lo primero es qué update se ejecuta en base al tipo de estudio.
            Lo segundo que hace es chequear que los campos que se enviaron
            a través del request, seán válidos y coincidan con las reglas del
            formulario. 
            Hago una verificación manual y no uso el validate() de WTForm porque
            al ser una actualización, NO siempre me mandan TODOS los campos, ende
            recurro a una verificación manual. 
            validate() verifica que todos los campos esten presentes, cuando este
            no siempre es el caso.

            """

            if tipo_estudio == 'disincronia':
                datos_estudio = formEstudioDisincronia(request.form)
                for field in datos_estudio.data:
                    if ((field in request.form) and (datos_estudio.data[field] == None)):
                        raise InvalidForm("El campo "+field+" no posee un tipo válido.")
                estudio_actualizado = EstudioDisincroniaModel.update(id_estudio,datos_estudio.data)

            elif tipo_estudio == 'salina_agitada':
                datos_estudio = formEstudioSalinaAgitada(request.form)
                for field in datos_estudio.data:
                    if ((field in request.form) and (datos_estudio.data[field] == None)):
                        raise InvalidForm("El campo "+field+" no posee un tipo válido.")
                estudio_actualizado = EstudioSalinaAgitadaModel.update(id_estudio,datos_estudio.data)
            
            elif tipo_estudio == '2d_doppler':
                datos_estudio = formEstudioDoppler(request.form)
                for field in datos_estudio.data:
                    if ((field in request.form) and (datos_estudio.data[field] == None)):
                        raise InvalidForm("El campo "+field+" no posee un tipo válido.")
                estudio_actualizado = EstudioDopplerModel.update(id_estudio,datos_estudio.data)

            elif tipo_estudio == 'congenitas':
                datos_estudio = formEstudioCongenitas(request.form)
                for field in datos_estudio.data:
                    if ((field in request.form) and (datos_estudio.data[field] == None)):
                        raise InvalidForm("El campo "+field+" no posee un tipo válido.")
                estudio_actualizado = EstudioCongenitasModel.update(id_estudio,datos_estudio.data)

            elif tipo_estudio == 'transesofagico':
                datos_estudio = formEstudioTransesofagico(request.form)
                for field in datos_estudio.data:
                    if ((field in request.form) and (datos_estudio.data[field] == None)):
                        raise InvalidForm("El campo "+field+" no posee un tipo válido.")
                estudio_actualizado = EstudioTransesofagicoModel.update(id_estudio,datos_estudio.data)                                                
            
            else:
                datos_estudio = formEstudioBasico(request.form)
                for field in datos_estudio.data:
                    if ((field in request.form) and (datos_estudio.data[field] == None)):
                        raise InvalidForm("El campo "+field+" no posee un tipo válido.")
                estudio_actualizado = EstudioBasicoModel.update(id_estudio,datos_estudio.data)                                                

            # Fix por si son nulos los diagnosticos opcionales
            if estudio_actualizado.diagnostico_opcional_1 == None:
                diagnostico_opcional_1_json = None
            else:
                diagnostico_opcional_1_json = estudio_actualizado.diagnostico_opcional_1.nombre
            if estudio_actualizado.diagnostico_opcional_2 == None:
                diagnostico_opcional_2_json = None
            else:
                diagnostico_opcional_2_json = estudio_actualizado.diagnostico_opcional_2.nombre            

            campos_no_deseados = [
                'id',
                'fecha',
                'especialista',
                'tipo',
                'motivo',
                'diagnostico_obligatorio_id',
                'diagnostico_obligatorio',
                'diagnostico_opcional_1_id',
                'diagnostico_opcional_1',
                'diagnostico_opcional_2',
                'diagnostico_opcional_2_id',
                'paciente',
                'tiene_motivo',
                'es_de_tipo',
                'llevado_a_cabo_por',
                'tiene_paciente',
                ]
            if tipo_estudio == 'salina_agitada':
                atributos = serializeSQLAlchemy(estudio_actualizado.tiene_conclusion_predefinida)
            #elif tipo_estudio == 'transesofagico':
            #    atributos = serializeSQLAlchemy(estudio_actualizado.tiene_descripcion)
            elif tipo_estudio == '2d_doppler':
                #campos_no_deseados.append("tiene_descripcion")
                #campos_no_deseados.append("descripcion_predefinida")
                atributos = serializeSQLAlchemy(estudio_actualizado, campos_no_deseados)  
                #desc_predefinida =  serializeSQLAlchemy(estudio_actualizado.tiene_descripcion) 
                #atributos["descripcion_predefinida"] = desc_predefinida              
            else:
                atributos = serializeSQLAlchemyInheritance(estudio_actualizado,campos_no_deseados)
            datos = {
                'status':200,
                'message':"Estudio modificado exitosamente",
                'body':{
                    'estudio':{
                        'informacion_general':{
                            'id':estudio_actualizado.id,
                            'fecha':str(estudio_actualizado.fecha),
                            'especialista':estudio_actualizado.llevado_a_cabo_por.nombre,
                            'tipo':estudio_actualizado.es_de_tipo.nombre_mostrado,
                            'motivo':estudio_actualizado.tiene_motivo.nombre,
                            'diagnostico_obligatorio':estudio_actualizado.diagnostico_obligatorio.nombre,
                            'diagnostico_opcional_1':diagnostico_opcional_1_json,
                            'diagnostico_opcional_2':diagnostico_opcional_2_json,
                            'paciente':{
                                'datos':serializeSQLAlchemy(
                                    estudio_actualizado.tiene_paciente,
                                    ['obra_social_id']
                                    ),
                                'obra_social':serializeSQLAlchemy(
                                    estudio_actualizado.tiene_paciente.asociado_a
                                    )
                            }
                        },
                        'atributos':atributos
                    },
                },
            }
            # Respuesta correcta
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )

        # Respuestas de error
        except Exception as e:
            print(str(e))
            if ("La ID" in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request',
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            elif ("El campo" in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request',
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            elif ("Ese" in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request',
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    ) 
            elif ("Esa" in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request',
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                                                     
            elif ("Expected:" in str(e)):
                datos = {
                    'status': 400,
                    'message':'El nombre_tipo no coincide con el tipo del estudio pasado por ID', 
                    'body': 'Bad Request',
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                              
            else:
                print(str(e))
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )                                                                       


    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_estudio = request.args.get('id')
        if id_estudio == None:
            datos = {
                'status': 400, 
                'body': 'Bad Request',
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            EstudioBasicoModel.delete(id_estudio)
            datos = {
                'status': 200, 
                'body': 'Estudio borrado exitosamente',
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )            
        except Exception as e:
            if ('NoneType' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese estudio no existe',
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            else:
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error',
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )                                










