import json

from flask import Response, request
from flask_restful import Resource

from app.models.paciente import Paciente as PacienteModel
from app.forms.formPaciente import formPaciente
from app.helpers.serializeInheritance import serializeSQLAlchemyInheritance
from app.helpers.serialize import serializeSQLAlchemy
from app.helpers.auth_helper import authToken


class AllPacientes(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        page = request.args.get("page")
        per_page = request.args.get("perPage")
        if(page == None or page == ''):
            page = 1
        if(per_page == None or per_page == ''):
            per_page = 10
        try:
            pacientes = PacienteModel.get_all(int(page),int(per_page))
            if(pacientes[1] == []):
                datos = {
                    'status': 400, 
                    'body': 'Número de página inválido',
                    'pagina_maxima': pacientes[2]
                         }
                return Response(
                    json.dumps(datos),
                    status=400, 
                    mimetype='application/json'
                    )
            else:
                parsed_data = []
                for paciente in pacientes[1]:
                    parsed_data.append({
                        'datos':serializeSQLAlchemy(paciente,['obra_social_id']),
                        'obra_social':serializeSQLAlchemy(paciente.asociado_a)
                    })
                datos = {
                    'status':200,
                    'body': {
                        'pacientes': parsed_data,
                        'total': pacientes[0],
                        'pagina': int(page)
                    }
                }
                return Response(
                    json.dumps(datos),
                    status=200,
                    mimetype='application/json'
                )
        except Exception as e:
            datos = {
                'status': 500,
                'body': 'Internal Server Error',
            }
            return Response(
                json.dumps(datos),
                status=500,
                mimetype='application/json'
            )

class SearchPacientes(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        page = request.args.get("page")
        perPage = request.args.get("perPage")
        apellido = request.args.get("apellido")
        fecha_nac = request.args.get("fecha_nac")
        if apellido == None and fecha_nac == None:
            datos = {
                'status': 400,
                'body': 'Bad Request',
            }
            return Response(
                json.dumps(datos),
                status=400,
                mimetype='application/json'
            )
        if page == None:
            page = 1
        if perPage == None:
            perPage = 10            
        try:
            pacientes = PacienteModel.get_by_lastname_and_bd(int(page),int(perPage),apellido,fecha_nac)
            if(pacientes[0] == 0):
                datos = {
                    'status': 404, 
                    'body': 'No se encontraron pacientes'
                         }
                return Response(
                    json.dumps(datos),
                    status=404, 
                    mimetype='application/json'
                    )                            
            if(pacientes[1] == []):
                datos = {
                    'status': 400, 
                    'body': 'Número de página inválido',
                    'pagina_maxima': pacientes[2]
                         }
                return Response(
                    json.dumps(datos),
                    status=400, 
                    mimetype='application/json'
                    )
            parsed_data = []
            for paciente in pacientes[1]:
                parsed_data.append({
                    'datos':serializeSQLAlchemy(paciente,['obra_social_id']),
                    'obra_social':serializeSQLAlchemy(paciente.asociado_a)
                })
            datos = {
                'status':200,
                'body': {
                    'pacientes': parsed_data,
                    'total': pacientes[0],
                    'pagina': int(page)
                }
            }
            return Response(
                json.dumps(datos),
                status=200,
                mimetype='application/json'
            )            
        except Exception as e:
            datos = {
                'status': 500,
                'body': 'Internal Server Error',
            }
            return Response(
                json.dumps(datos),
                status=500,
                mimetype='application/json'
            )




class Paciente(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_paciente = request.args.get("id")
        dni_paciente = request.args.get("dni")
        if(dni_paciente==None and id_paciente==None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )  
        try:
            if(dni_paciente == None):
                paciente = PacienteModel.get_by_id(id_paciente)
            elif (id_paciente == None):
                paciente = PacienteModel.get_by_dni(dni_paciente)
                  
            datos = {
                'status': 200, 
                'body':{
                    'paciente':{
                        'datos':serializeSQLAlchemy(paciente,['obra_social_id']),
                        'obra_social':serializeSQLAlchemy(paciente.asociado_a)
                        }
                }
            }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        except Exception as e:
            if('__table__' in str(e)):
                datos = {
                'status':404,
                'body':'Ese paciente no existe'
                }
                return Response(
                    json.dumps(datos),
                    status=404,
                    mimetype='application/json'
                    )
            else:
                datos = {
                    'status':500,
                    'body':'Internal Server Error',
                    'msg': str(e)
                }
                return Response(
                    json.dumps(datos),
                    status=500,
                    mimetype='application/json'
                    )
    
    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        form_paciente = formPaciente(request.form)
        if(not form_paciente.validate()):
            datos = {
                'status':400,
                'body':'Bad Request'
            }
            return Response(
                json.dumps(datos),
                status=400,
                mimetype='application/json'
            )
        else:
            try:
                if (form_paciente.dni.data == ""):
                    form_paciente.dni.data = None
                nuevo_paciente = PacienteModel.create(form_paciente.data)
                datos = {
                    'status': 201,
                    'body': 'Paciente creado exitosamente',
                    'paciente': {
                        'datos':serializeSQLAlchemy(nuevo_paciente,['obra_social_id']),
                        'obra_social':serializeSQLAlchemy(nuevo_paciente.asociado_a)
                    }
                }
                return Response(
                    json.dumps(datos),
                    status=201,
                    mimetype='application/json'
                )
            except Exception as e:
                print(str(e))
                if('Duplicate' in str(e)):
                    datos = {
                        'status':400,
                        'body':'Ese DNI ya está registrado'
                    }
                    return Response(
                        json.dumps(datos),
                        status=400,
                        mimetype='application/json'
                    )                                        
                else:
                    datos = {
                        'status':500,
                        'body':'Internal Server Error'
                    }
                    return Response(
                        json.dumps(datos),
                        status=500,
                        mimetype='application/json'
                    )                    

    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        try:
            id_paciente = request.args.get("id")
            if(id_paciente==None):
                datos = {
                    'status':400,
                    'body':'Bad Request'
                }
                return Response(
                    json.dumps(datos),
                    status=400,
                    mimetype='application/json'
                )
            else:
                PacienteModel.delete(id_paciente)
                datos = {
                    'status':200,
                    'body':'Paciente eliminado exitosamente'
                }
                return Response(
                    json.dumps(datos),
                    status=200,
                    mimetype='application/json'
                )                
        except Exception as e:
            if('builtins.NoneType' in str(e)):
                datos = {
                    'status':404,
                    'body':'Ese paciente no existe'
                }
                return Response(
                    json.dumps(datos),
                    status=404,
                    mimetype='application/json'
                )
            elif('asociados' in str(e)):
                datos = {
                    'status':400,
                    'body':'Bad Request',
                    'details':'Ese paciente tiene estudios asociados'
                }
                return Response(
                    json.dumps(datos),
                    status=400,
                    mimetype='application/json'
                )                
            else:
                print(str(e))
                datos = {
                    'status':500,
                    'body':'Internal Server Error'
                }
                return Response(
                    json.dumps(datos),
                    status=500,
                    mimetype='application/json'
                )                                      
                
    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_paciente = request.args.get("id")
        datos_nuevos = formPaciente(request.form).data
        if(id_paciente == None):
            datos = {
                'status':400,
                'body':'Bad Request'
            }
            return Response(
                json.dumps(datos),
                status=400,
                mimetype='application/json'
            )
        try:
            paciente_a_actualizar = PacienteModel.get_by_id(id_paciente)
            if(paciente_a_actualizar == None):
                datos = {
                    'status':404,
                    'body':'Ese paciente no existe'
                }
                return Response(
                    json.dumps(datos),
                    status=404,
                    mimetype='application/json'
                )                 
            p_actualizado = PacienteModel.update(paciente_a_actualizar,datos_nuevos)
            datos = {
                'status':200,
                'body':'Paciente actualizado exitosamente',
                'paciente': {
                    'datos': serializeSQLAlchemy(p_actualizado,['obra_social_id']),
                    'obra_social': serializeSQLAlchemy(p_actualizado.asociado_a)
                }
            }
            return Response(
                json.dumps(datos),
                status=200,
                mimetype='application/json'
            )            
        except Exception as e:
            if('Duplicate entry' in str(e)):
                datos = {
                    'status':400,
                    'body':'Ese DNI ya está registrado'
                }
                return Response(
                    json.dumps(datos),
                    status=400,
                    mimetype='application/json'
                )
            elif ('Obra' in str(e)):
                datos = {
                    'status':404,
                    'body':'La Obra Social no existe'
                }
                return Response(
                    json.dumps(datos),
                    status=404,
                    mimetype='application/json'
                )                
            else:
                datos = {
                    'status':500,
                    'body':'Internal Server Error'
                }
                return Response(
                    json.dumps(datos),
                    status=500,
                    mimetype='application/json'
                )                                

class AllEstudios(Resource):

    def get(self, id_paciente):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        page = request.args.get("page")
        per_page = request.args.get("perPage")
        if(page == None or page == ''):
            page = 1
        if(per_page == None or per_page == ''):
            per_page = 10
        try:
            estudios = PacienteModel.get_estudios(id_paciente,int(page),int(per_page))
            paciente = PacienteModel.get_by_id(id_paciente)
            if(estudios[1] == []):
                datos = {
                    'status': 400, 
                    'body': 'Número de página inválido',
                    'pagina_maxima': estudios[2]
                         }
                return Response(
                    json.dumps(datos),
                    status=400, 
                    mimetype='application/json'
                    )
            else:
                parsed_data = []
                for estudio in estudios[1]:
                    if estudio.diagnostico_opcional_1 == None:
                        diagnostico_opcional_1_json = None
                    else:
                        diagnostico_opcional_1_json = {
                            'id':estudio.diagnostico_opcional_1.id,
                            'nombre':estudio.diagnostico_opcional_1.nombre
                        }
                    if estudio.diagnostico_opcional_2 == None:
                        diagnostico_opcional_2_json = None
                    else:
                        diagnostico_opcional_2_json = {
                            'id':estudio.diagnostico_opcional_2.id,
                            'nombre':estudio.diagnostico_opcional_2.nombre
                        }            

                    campos_no_deseados = [
                        'id',
                        'fecha',
                        'especialista',
                        'tipo',
                        'motivo',
                        'diagnostico_obligatorio_id',
                        'diagnostico_obligatorio',
                        'diagnostico_opcional_1_id',
                        'diagnostico_opcional_1',
                        'diagnostico_opcional_2',
                        'diagnostico_opcional_2_id',
                        'paciente',
                        'tiene_motivo',
                        'es_de_tipo',
                        'llevado_a_cabo_por',
                        'tiene_paciente',
                        ]
                    if estudio.tipo == 'salina_agitada':
                        atributos = serializeSQLAlchemy(estudio.tiene_conclusion_predefinida)
                    elif estudio.tipo == 'transesofagico':
                        atributos = {
                            'descripcion':estudio.descripcion,
                            'conclusion':estudio.conclusion,
                        }    
                    elif estudio.tipo == '2d_doppler':
                        atributos = serializeSQLAlchemy(estudio, campos_no_deseados)  
                        atributos["conclusion"] = estudio.conclusion
                    else:
                        atributos = serializeSQLAlchemyInheritance(estudio,campos_no_deseados)
                    parsed_data.append({
                        'estudio':{
                            'informacion_general':{
                                'id':estudio.id,
                                'fecha':str(estudio.fecha),
                                'especialista':{
                                    'id':estudio.llevado_a_cabo_por.id,
                                    'nombre':estudio.llevado_a_cabo_por.nombre
                                },
                                'tipo':{
                                    'id':estudio.es_de_tipo.id,
                                    'nombre_id':estudio.es_de_tipo.nombre,
                                    'nombre_mostrado':estudio.es_de_tipo.nombre_mostrado
                                },
                                'motivo':{
                                    'id':estudio.tiene_motivo.id,
                                    'nombre':estudio.tiene_motivo.nombre
                                },
                                'diagnostico_obligatorio':{
                                    'id':estudio.diagnostico_obligatorio.id,
                                    'nombre':estudio.diagnostico_obligatorio.nombre
                                },
                                'diagnostico_opcional_1':diagnostico_opcional_1_json,
                                'diagnostico_opcional_2':diagnostico_opcional_2_json,
                            },
                            'atributos':atributos
                        },
                    })
                datos = {
                    'status':200,
                    'body': {
                        'paciente': {
                            'datos': serializeSQLAlchemy(paciente,['obra_social_id']),
                            'obra_social': serializeSQLAlchemy(paciente.asociado_a)
                        },
                        'estudios': parsed_data,
                        'total': estudios[0],
                        'pagina': int(page)
                    }
                }
                return Response(
                    json.dumps(datos),
                    status=200,
                    mimetype='application/json'
                )
        except Exception as e:
            print(str(e))
            if("NoneType" in str(e)):
                datos = {
                    'status': 404,
                    'body': 'Ese Paciente no existe',
                }
                return Response(
                    json.dumps(datos),
                    status=404,
                    mimetype='application/json'
                )              
            datos = {
                'status': 500,
                'body': 'Internal Server Error',
            }
            return Response(
                json.dumps(datos),
                status=500,
                mimetype='application/json'
            )


