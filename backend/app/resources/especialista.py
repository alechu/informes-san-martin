import json

from flask import Response, request
from flask_restful import Resource

from app.models.especialista import Especialista as EspecialistaModel
from app.forms.formEspecialista import formEspecialista
from app.helpers.serialize import serializeSQLAlchemy
from app.helpers.auth_helper import authToken

class Especialista(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response           
        id_especialista = request.args.get("id")
        all_especialistas = request.args.get("all")
        if(id_especialista == None and all_especialistas == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )           
        if(id_especialista != None and all_especialistas != None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )

        try:
            if(all_especialistas == 'True'):
                especialistas = EspecialistaModel.get_all()
                serialized_data = []
                for esp in especialistas:
                    serialized_data.append(serializeSQLAlchemy(esp))
                datos = {
                    'status': 200, 
                    'body': serialized_data
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )               
            else:
                if(id_especialista != None):
                    esp = EspecialistaModel.get_by_id(id_especialista)
                    datos = {
                        'status': 200, 
                        'body': serializeSQLAlchemy(esp)
                        }
                    return Response(
                        json.dumps(datos), 
                        status=200,
                        mimetype='application/json'
                        )
                else:
                    datos = {
                        'status': 400, 
                        'body': 'Bad Request'
                        }
                    return Response(
                        json.dumps(datos), 
                        status=400,
                        mimetype='application/json'
                        )                    

        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese Especialista no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )                              
            else:
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        try:
            form = formEspecialista(request.form)
            if(not form.validate()):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                
            nuevo_especialista = EspecialistaModel.create(form.data)
            datos = {
                'status': 201, 
                'body': 'Especialista creado exitosamente',
                'especialista': serializeSQLAlchemy(nuevo_especialista)
                }
            return Response(
                json.dumps(datos), 
                status=201,
                mimetype='application/json'
                )
        except Exception as e:
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )

    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_especialista = request.args.get("id")
        nuevos_datos = formEspecialista(request.form)
        if(id_especialista == None or not nuevos_datos.validate()):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            esp_actualizado = EspecialistaModel.update(
                id_especialista,
                nuevos_datos.data
                )              
            datos = {
                'status': 200, 
                'body': 'Especialista actualizado exitosamente',
                'especialista': serializeSQLAlchemy(esp_actualizado)
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        except Exception as e:
            if ("no existe" in str(e)):
                datos = {
                    'status': 404,
                    'body': 'Not Found', 
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            else:
                print(str(e))           
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_especialista = request.args.get("id")
        if(id_especialista == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            result = EspecialistaModel.delete(id_especialista)
            if(result == True):
                datos = {
                    'status': 200, 
                    'body': 'Especialista eliminado exitosamente'
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )                          
        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese Especialista no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            elif ('asociados' in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Ese Especialista tiene estudios asociados'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            else:                                  
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )                                             