import json
import werkzeug

from flask import Response, request
from flask_restful import Resource

from app.models.user import User as UserModel

class Login(Resource):

    def post(self):
        data = {}
        try:
            data['username'] = request.form['username']
            data['password'] = request.form['password']
            result = UserModel.login(data['username'],data['password'])
            if result == False:
                datos = {
                    'status': 401, 
                    'body': 'Bad Credentials'
                    }
                return Response(
                    json.dumps(datos), 
                    status=401,
                    mimetype='application/json'
                    )
            else:
                datos = {
                    'status':200,
                    'user':result[0],
                    'token':result[1]
                }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )                                
        except werkzeug.exceptions.BadRequestKeyError:
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )