import json

from flask import Response, request
from flask_restful import Resource

from app.models.obraSocial import ObraSocial as ObraSocialModel
from app.forms.formObraSocial import formObraSocial
from app.helpers.serialize import serializeSQLAlchemy
from app.helpers.auth_helper import authToken

class ObraSocial(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_obra_social = request.args.get("id")
        all_obras = request.args.get("all")
        if(id_obra_social == None and all_obras == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )           
        if(id_obra_social != None and all_obras != None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )

        try:
            if(all_obras == 'True'):
                obras = ObraSocialModel.get_all()
                serialized_data = []
                for obra in obras:
                    serialized_data.append(serializeSQLAlchemy(obra))
                datos = {
                    'status': 200, 
                    'body': serialized_data
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )               
            else:
                if(id_obra_social != None):
                    obra = ObraSocialModel.get_by_id(id_obra_social)
                    datos = {
                        'status': 200, 
                        'body': serializeSQLAlchemy(obra)
                        }
                    return Response(
                        json.dumps(datos), 
                        status=200,
                        mimetype='application/json'
                        )
                else:
                    datos = {
                        'status': 400, 
                        'body': 'Bad Request'
                        }
                    return Response(
                        json.dumps(datos), 
                        status=400,
                        mimetype='application/json'
                        )                    

        except Exception as e:
            if('__table__' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Esa Obra Social no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )                                
            else:
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )                             

    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        try:
            form = formObraSocial(request.form)
            if(not form.validate()):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                
            nueva_obra = ObraSocialModel.create(form.data)
            datos = {
                'status': 201, 
                'body': 'Obra Social creada exitosamente',
                'obra_social': serializeSQLAlchemy(nueva_obra)
                }
            return Response(
                json.dumps(datos), 
                status=201,
                mimetype='application/json'
                )
        except Exception as e:
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )                          

    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_obra_social = request.args.get("id")
        if(id_obra_social == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            result = ObraSocialModel.delete(id_obra_social)
            if(result == 'tiene_pacientes'):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request',
                    'details': 'La Obra Social tiene pacientes asociados'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            if(result == True):
                datos = {
                    'status': 200, 
                    'body': 'Obra Social eliminada exitosamente'
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )                          
        except Exception as e:
            if('NoneType' in str(e)):
                datos = {
                    'status': 400, 
                    'body': 'Esa Obra Social no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                  
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )              

    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        id_obra_social = request.args.get("id")
        if(id_obra_social == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        nuevos_datos = formObraSocial(request.form)
        if(not nuevos_datos.validate()):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            obra_a_actualizar = ObraSocialModel.get_by_id(id_obra_social)
            if(obra_a_actualizar == None):
                datos = {
                    'status': 404, 
                    'body': 'Esa Obra Social no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )             
            obra_actualizada = ObraSocialModel.update(obra_a_actualizar,nuevos_datos.data)               
            datos = {
                'status': 200, 
                'body': 'Obra Social actualizada exitosamente',
                'obra_social': serializeSQLAlchemy(obra_actualizada)
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        except Exception as e:
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )            

class AllPacientesObra(Resource):

    def get(self, id_obra_social):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response   
        page = request.args.get("page")
        per_page = request.args.get("perPage")
        if(page == None or page == ''):
            page = 1
        if(per_page == None or per_page == ''):
            per_page = 10
        try:
            p_obras = ObraSocialModel.get_pacientes(int(id_obra_social),int(page),int(per_page))
            if(p_obras == False):
                datos = {
                    'status': 404, 
                    'body': 'Esa Obra Social no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            if(p_obras[0] == 0):
                datos = {
                    'status': 400, 
                    'body': 'Esa Obra Social no tiene pacientes asociados'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                                
            if (p_obras[1] == []):
                datos = {
                    'status': 400, 
                    'body': 'Número de página inválido',
                    'pagina_maxima': p_obras[2]
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            serialized_data = []
            for paciente in p_obras[1]:
                serialized_data.append(serializeSQLAlchemy(paciente,['obra_social_id']))
            datos = {
                'status': 200, 
                'body': serialized_data,
                'totales': p_obras[0],
                'pagina': int(page)
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )            
        except Exception as e:
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )             


                    