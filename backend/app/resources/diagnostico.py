import json

from flask import Response, request
from flask_restful import Resource

from app.helpers.serialize import serializeSQLAlchemy
from app.helpers.exceptions import NotEmptyRelationships
from app.helpers.auth_helper import authToken

from app.models.diagnostico import Diagnostico as DiagnosticoModel
from app.models.estudio import Estudio as EstudioModel

from app.forms.formDiagnostico import formDiagnostico

class Diagnostico(Resource):

    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response            
        id_diagnostico = request.args.get("id")
        all_diagnosticos = request.args.get("all")
        if(id_diagnostico == None and all_diagnosticos == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )           
        if(id_diagnostico != None and all_diagnosticos != None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )

        try:
            if(all_diagnosticos == 'True'):
                diagnosticos = DiagnosticoModel.get_all()
                serialized_data = []
                for diag in diagnosticos:
                    serialized_data.append(serializeSQLAlchemy(diag))
                datos = {
                    'status': 200, 
                    'body': serialized_data
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )               
            else:
                if(id_diagnostico != None):
                    diag = DiagnosticoModel.get_by_id(id_diagnostico)
                    datos = {
                        'status': 200, 
                        'body': serializeSQLAlchemy(diag)
                        }
                    return Response(
                        json.dumps(datos), 
                        status=200,
                        mimetype='application/json'
                        )
                else:
                    datos = {
                        'status': 400, 
                        'body': 'Bad Request'
                        }
                    return Response(
                        json.dumps(datos), 
                        status=400,
                        mimetype='application/json'
                        )                    

        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese Diagnóstico no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )                              
            else:
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def post(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response           
        try:
            form = formDiagnostico(request.form)
            if(not form.validate()):
                datos = {
                    'status': 400, 
                    'body': 'Bad Request'
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )                
            nuevo_diagnostico = DiagnosticoModel.create(form.data)
            datos = {
                'status': 201, 
                'body': 'Diagnóstico creado exitosamente',
                'diagnostico': serializeSQLAlchemy(nuevo_diagnostico)
                }
            return Response(
                json.dumps(datos), 
                status=201,
                mimetype='application/json'
                )
        except Exception as e:
            datos = {
                'status': 500, 
                'body': 'Internal Server Error'
                }
            return Response(
                json.dumps(datos), 
                status=500,
                mimetype='application/json'
                )

    def put(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response           
        id_diagnostico = request.args.get("id")
        nuevos_datos = formDiagnostico(request.form)
        if(id_diagnostico == None or not nuevos_datos.validate()):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            diag_actualizado = DiagnosticoModel.update(
                id_diagnostico,
                nuevos_datos.data
                )              
            datos = {
                'status': 200, 
                'body': 'Diagnóstico actualizado exitosamente',
                'diagnostico': serializeSQLAlchemy(diag_actualizado)
                }
            return Response(
                json.dumps(datos), 
                status=200,
                mimetype='application/json'
                )
        except Exception as e:
            if ("no existe" in str(e)):
                datos = {
                    'status': 404,
                    'body': 'Not Found', 
                    'details': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            else:
                print(str(e))           
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )

    def delete(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response           
        id_diagnostico = request.args.get("id")
        if(id_diagnostico == None):
            datos = {
                'status': 400, 
                'body': 'Bad Request'
                }
            return Response(
                json.dumps(datos), 
                status=400,
                mimetype='application/json'
                )
        try:
            res = EstudioModel.get_all_diags(id_diagnostico)
            if res == True:
                raise NotEmptyRelationships("Ese Diagnóstico tiene estudios asociados")
            result = DiagnosticoModel.delete(id_diagnostico)
            if(result == True):
                datos = {
                    'status': 200, 
                    'body': 'Diagnóstico eliminado exitosamente'
                    }
                return Response(
                    json.dumps(datos), 
                    status=200,
                    mimetype='application/json'
                    )                          
        except Exception as e:
            if('no existe' in str(e)):
                datos = {
                    'status': 404, 
                    'body': 'Ese Diagnóstico no existe'
                    }
                return Response(
                    json.dumps(datos), 
                    status=404,
                    mimetype='application/json'
                    )
            elif ('asociados' in str(e)):
                datos = {
                    'status': 400, 
                    'body': str(e)
                    }
                return Response(
                    json.dumps(datos), 
                    status=400,
                    mimetype='application/json'
                    )
            else:
                print(str(e))                                  
                datos = {
                    'status': 500, 
                    'body': 'Internal Server Error'
                    }
                return Response(
                    json.dumps(datos), 
                    status=500,
                    mimetype='application/json'
                    )                                             