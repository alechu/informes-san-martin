import json

from flask import Response, request
from flask_restful import Resource

from app.models.estudio import Estudio as EstudioModel
from app.forms.formEstadisticas import formEstadisticas
from app.helpers.serialize import serializeSQLAlchemy
from app.helpers.auth_helper import authToken


class Estadisticas(Resource):
    
    def get(self):
        header = request.headers.get("Authorization")
        response = authToken(header)
        if response:
            return response           
        form = formEstadisticas(request.args)
        if not form.validate():
            data = {
                'status':400,
                'body':'Bad Request'
            }
            return Response(
                json.dumps(data),
                status=400,
                mimetype='application/json',
            )
        dataset = EstudioModel.get_all_stats(form.data)
        if dataset == []:
            data = {
                'status':404,
                'body': 'Not Found',
                'details':'No hay estudios que coincidan con los parámetros solicitados'
            }
            return Response(
                json.dumps(data),
                status=404,
                mimetype='application/json',
            )
        total = 0
        parsed_data = []
        if form.data['agrupado_por'] == 'dia':
            for set_estudios in dataset:
                parsed_data.append({
                    'dia':str(set_estudios[0]),
                    'cantidad':str(set_estudios[1])
                })
                total = total + set_estudios[1]
        elif form.data['agrupado_por'] == 'mes':
            for set_estudios in dataset:
                parsed_data.append({
                    'mes':str(set_estudios[0]),
                    'cantidad':str(set_estudios[1])
                })
                total = total + set_estudios[1]
        else:
            for set_estudios in dataset:
                parsed_data.append({
                    'año':str(set_estudios[0]),
                    'cantidad':str(set_estudios[1])
                })
                total = total + set_estudios[1]
        data = {
            'status':200,
            'body':{
                'total_estudios':total,
                'dataset':parsed_data
                }
        }
        return Response(
            json.dumps(data),
            status=200,
            mimetype='application/json',
        )                                





