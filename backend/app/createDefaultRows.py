from app.models.tipo_estudio import TipoEstudio
from app.models.conclusion_predefinida import ConclusionPredefinida
from app.models.especialista import Especialista
from app.models.motivo import Motivo
from app.models.diagnostico import Diagnostico

def createDefault():
    tipos_a_insertar= [
        ('basico','Básico'),
        ('disincronia','Disincronía'),
        ('salina_agitada','Salina Agitada'),
        ('2d_doppler','2D Doppler Color'),
        ('congenitas','Congénitas'),
        ('transesofagico','Transesofágico'),
    ]

    for tipo in tipos_a_insertar:
        dict = {
            'nombre':tipo[0],
            'nombre_mostrado': tipo[0]
        }
        estudio = TipoEstudio.create(dict)
        dict['nombre_mostrado'] = tipo[1]
        TipoEstudio.update(estudio.id,dict)
    

    conc_predefinidas_a_insertar = [
        (
            'Se  visualiza repleción de burbujas en cavidades  derechas, no observándose la aparición de las mismas en cavidades izquierdas después del 3° latido, tanto en reposo, como durante la sensibilización con maniobra de Valsalva.',
            'Estudio (-) para shunt' 
        ),
        (
            'Se  visualiza repleción de burbujas en cavidades  derechas, observándose la aparición de las mismas en cavidades izquierdas entre el  3°/4° latido, tanto en reposo, como durante la sensibilización con maniobra de Valsalva.',
            'Foramen Oval Permeable con shunt derecha/ izquierda. CIA'
        ),
        (
            'Se visualiza repleción de burbujas en cavidades derechas con aparición de las mismas en cavidades izquierdas luego del 7° latido.',
            'Shunt intrapulmonar'
        ),
        (
            'Se  visualiza a aparición de burbujas a través del seno Coronario replecionando las cavidades derechas.',
            'Vena Cava superior izquierda persistente'
        )
    ]

    for conclusion in conc_predefinidas_a_insertar:
        dict = {
            'escenario': conclusion[0],
            'conclusion': conclusion[1],
        }
        ConclusionPredefinida.create(dict)

    especialistas_a_insertar = [
        'Dra de Iraola, Ana',
        'Dra Faliva Mai, Gianina',
        'Dra Gómez, Daniela',
        'Dr Pagola Juan',
        'Dr Pizzini Luis',
        'Dra Mardzin, Brenda',
    ]

    for especialista in especialistas_a_insertar:
        dict = {
            'nombre': especialista
        }
        Especialista.create(dict)


    motivos_a_insertar = [
        'Control',
        'Factores de riesgo CV',
        'Cardiopatía isquémica',
        'Insuficiencia cardiaca',
        'Cardiopatías congénitas',
        'Endocarditis infecciosa',
        'Arritmia',
        'Enfermedad de Chagas',
        'Hipertensión pulmonar',
        'Tratamiento oncológico',
        'Insuficiencia renal',
        'Enfermedades autoinmunes',
        'Enfermedades respiratorias',
        'Accidente Cerebro vascular',
        'Síndrome febril prolongado',
        'Enfermedad vascular periférica',
        'Miocardiopatía hipertrófica',
        'Amiloidosis',
        'Derrame pericárdico',
        'FOP',
    ]

    for motivo in motivos_a_insertar:
        dict = {
            'nombre': motivo
        }
        Motivo.create(dict)


    diagnosticos_a_insertar = [
        'Normal',
        'Cardiopatía isquémica',
        'Cardiopatía HTA',
        'Cardiopatía Chagásica',
        'Miocardiopatía dilatada',
        'Deterioro severo de la función VI',
        'Disfunción diastólica aislada',
        'Cardiopatía Congénita compleja',
        'CIA',
        'CIV',
        'Aneurisma del Septum interauricular',
        'FOP',
        'Valvulopatía mitral',
        'Valvulopatía aórtica',
        'Valvulopatías derechas',
        'Patología de aorta',
        'Endocarditis infecciosa',
        'Masas intracavitarias',
        'Hipertensión Pulmonar',
        'Prótesis valvulares',
        'Dispositivos de cierre',
        'Derrame pericárdico',
        'Taponamiento ecográfico',
        'Pericarditis constrictiva',
        'Miocardiopatía hipertrófica',
        'Miocardiopatía infiltrativa',
        'Tromboembolismo pulmonar',
        'Viabilidad (+)',
        'Discincronía (+)',
        'Enfermedad Carotídea no significativa',
        'Enfermedad Carotídea significativa',
        'Enfermedad vascular no significativa',
        'Enfermedad vascular significativa',
    ]

    for diag in diagnosticos_a_insertar:
        dict = {
            'nombre': diag
        }
        Diagnostico.create(dict)




