import secrets

"""

Aca hay que configurar el .env con los datos de:

DB_NAME
DB_HOST
DB_USER
DB_PASS

SECRET_KEY (para el JWT Token)

Ejecutar /updatedb
Ejecutar /createDefault

Posiblemente configurar SECRET_HASH_KEY (para hashear la password)


"""


env_file = open(".env", "w+")
env_file.close()
env_file = open(".env", "a+")
env_file.write("FLASK_ENV=production\n")
env_file.write("DB_NAME=san_martin\n")
env_file.write("DB_HOST=localhost\n")
env_file.write("DB_USER=root\n")
env_file.write("DB_PASS=\n")
env_file.write("DB_PASS_RANDOM="+secrets.token_urlsafe(16)+"\n")
env_file.write("SECRET_KEY="+secrets.token_hex(32))
env_file.close()


