# API Informes San Martín

## Configuración Inicial

- Estar parado en la raíz del backend (informes-san-martin/backend)

- Crear venv con `python -m venv venv` en Windows ó `python3 -m venv venv` en Linux

- Crear un archivo llamado `config.py` en la raíz del backend y copiar el contenido

- Crear un archivo llamado `.env` en la raíz del backend y copiar el contenido

- Activar el venv con `venv\Scripts\activate` (Windows) ó `source /venv/bin/activate` (Linux)

- Instalar paquetes con `pip install -r requirements.txt`

- Correr la API con `flask run`

### Contenido de config.py

```
from os import environ

class BaseConfig(object):
    """Base configuration."""

    DEBUG = None
    DB_HOST = "bd_name"
    DB_USER = "db_user"
    DB_PASS = "db_pass"
    DB_NAME = "db_name"
    DB_DIALECT = "mysql"
    DB_DRIVER = "pymysql"
    DB_PORT = "3306"
    SQLALCHEMY_DATABASE_URI="{0}+{1}://{2}:{3}@{4}:{5}/{6}".format(DB_DIALECT, DB_DRIVER, DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    SECRET_KEY = "secret"


class DevelopmentConfig(BaseConfig):
    """Development configuration."""

    ENV = "development"
    DEBUG = environ.get("DEBUG", True)
    DB_HOST = environ.get("DB_HOST", "MY_DB_HOST")
    DB_USER = environ.get("DB_USER", "MY_DB_USER")
    DB_PASS = environ.get("DB_PASS", "MY_DB_PASS")
    DB_NAME = environ.get("DB_NAME", "MY_DB_NAME")
    SQLALCHEMY_DATABASE_URI = "{0}+{1}://{2}:{3}@{4}:{5}/{6}".format(BaseConfig.DB_DIALECT, BaseConfig.DB_DRIVER, DB_USER, DB_PASS, DB_HOST, BaseConfig.DB_PORT, DB_NAME)

class ProductionConfig(BaseConfig):
    """Production configuration."""

    ENV = "production"
    DEBUG = environ.get("DEBUG", False)
    DB_HOST = environ.get("DB_HOST", "MY_DB_HOST")
    DB_USER = environ.get("DB_USER", "MY_DB_USER")
    DB_PASS = environ.get("DB_PASS", "MY_DB_PASS")
    DB_NAME = environ.get("DB_NAME", "MY_DB_NAME")
    SQLALCHEMY_DATABASE_URI = "{0}+{1}://{2}:{3}@{4}:{5}/{6}".format(BaseConfig.DB_DIALECT, BaseConfig.DB_DRIVER, DB_USER, DB_PASS, DB_HOST, BaseConfig.DB_PORT, DB_NAME)


config = dict(
    development=DevelopmentConfig, production=ProductionConfig
)

```

### Contenido de .env

```
FLASK_ENV=development
DB_HOST=<completar>
DB_USER=<completar>
DB_PASS=<completar>
DB_NAME=<completar>

```

## Endpoints

### 🔶 Paciente

- GET

  - `/api/paciente?id=<id>`
  - `/api/paciente?dni=<dni>`

    Retorna un paciente en específico  
    Puede ser en base a su ID o DNI

    #### Success Reponse

    ```
    "status": 200,
    "body": {
        "paciente": {
            "datos": {
                "id": 1,
                "nombre": "Name",
                "apellido": "LastName",
                "dni": "123456",
                "fecha_nac": "2020-10-03",
                "sexo": "Masculino",
                "estado": "Internado",
                "localidad": "La Plata"
            },
            "obra_social": {
                "id": 2,
                "nombre": "OSDE"
            }
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese paciente no existe"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

  - `/api/paciente/all?page=<page>&perPage=<int>`

    Retorna todos los pacientes.  
    Los resultados pueden ser paginados.  
    Si no se pasan los parámetros 'page' o 'perPage',  
    ambos defaultean a 1 y 10 respectivamente.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": {
            "pacientes": [
                {
                    "datos": {
                        "id": 1,
                        "nombre": "Name",
                        "apellido": "Last Name",
                        "dni": "123456",
                        "fecha_nac": "2020-10-03",
                        "sexo": "Masculino",
                        "estado": "Internado",
                        "localidad": "Otro"
                    },
                    "obra_social": {
                        "id": 2,
                        "nombre": "OSDE"
                    }
                },
                {
                    "datos": {
                        "id": 2,
                        "nombre": "Name",
                        "apellido": "Last Name",
                        "dni": "1234567",
                        "fecha_nac": "2020-10-03",
                        "sexo": "Femenino",
                        "estado": "Ambulatorio",
                        "localidad": "La Plata"
                    },
                    "obra_social": {
                        "id": 2,
                        "nombre": "I.O.M.A"
                    }
                }
            ],
            "total": 5,
            "pagina": 1
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Número de página inválido",
        "pagina_maxima": <numero>
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

  - `/api/paciente/<id_paciente>/estudios?page=<int>&perPage=<int>`

    Retorna todos los estudios de un Paciente.  
    Los resultados pueden ser paginados.  
    Si no se pasan los parámetros 'page' o 'perPage',  
    ambos defaultean a 1 y 10 respectivamente.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": {
            "paciente": {
                "datos": {
                    "id": 2,
                    "nombre": "Nombre",
                    "apellido": "Apellido",
                    "dni": "123",
                    "fecha_nac": "2020-12-08",
                    "sexo": "Femenino",
                    "estado": "ambulatorio",
                    "localidad": "La Plata"
                },
                "obra_social": {
                    "id": 1,
                    "nombre": "I.O.M.A"
                }
            },
            "estudios": [
                {
                    "estudio": {
                        "informacion_general": {
                            "id": 2,
                            "fecha": "2020-10-15",
                            "descripcion": "Cavidades de dimensiones normales",
                            "especialista": "Dra de Iraola, Ana",
                            "tipo": "Disincronía",
                            "motivo": "Control",
                            "diagnostico_obligatorio": "Normal",
                            "diagnostico_opcional_1": "Normal",
                            "diagnostico_opcional_2": "Normal"
                        },
                        "atributos": {
                            "conclusion": "vamos carajo",
                            "retraso_septal": null,
                            "dpt": null,
                            "str": null,
                            "llenado_distolico": null,
                            "diferencia_periodos": null
                        }
                    }
                },
                {
                    "estudio": {
                        "informacion_general": {
                            "id": 3,
                            "fecha": "2020-10-15",
                            "descripcion": "Cavidades de dimensiones normales",
                            "especialista": "Dra de Iraola, Ana",
                            "tipo": "Salina Agitada",
                            "motivo": "Control",
                            "diagnostico_obligatorio": "Normal",
                            "diagnostico_opcional_1": "Normal",
                            "diagnostico_opcional_2": "Normal"
                        },
                        "atributos": {
                            "id": 1,
                            "escenario": "Se  visualiza repleción de burbujas en cavidades
                            derechas, no observándose la aparición de las mismas en cavidades
                            izquierdas después del 3° latido, tanto en reposo, como durante la
                            sensibilización con maniobra de Valsalva.",
                            "conclusion": "Estudio (-) para shunt"
                        }
                    }
                }
            ],
            "total": 9,
            "pagina": 1
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese Paciente no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Número de página inválido",
        "pagina_maxima": <int>
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/paciente`

    Crea un nuevo paciente

  - Esquema Formulario

    - nombre: String(50)

    - apellido: String(50)

    - dni: Integer(10)

    - sexo: String(10)

    - estado: String(25)

    - localidad: String(30)

    - fecha_nac: Date(YYYY-MM-DD)

    - obra_social_id: FK_id_obra_social

    #### ✔️ Success Response

    ```
    {
        "status": "201",
        "body": "Paciente creado exitosamente",
        "paciente": {
            "datos": {
                "id": 7,
                "nombre": "Name",
                "apellido": "Last Name",
                "dni": "123456",
                "fecha_nac": "2020-10-03",
                "sexo": "Femenino",
                "estado": "Ambulatorio",
                "localidad": "La Plata"
            },
            "obra_social": {
                "id": 1,
                "nombre": "I.O.M.A"
            }
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Ese DNI ya está registrado"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/paciente?id=<id>`

    Modifica un paciente en base a su ID

  - Esquema Formulario

    - nombre: String(50)

    - apellido: String(50)

    - dni: Integer(10)

    - sexo: String(10)

    - estado: String(25)

    - fecha_nac: Date(YYYY-MM-DD)

    - obra_social_id: FK_id_obra_social

    Al ser una actualización, NO es obligatorio enviar todos los campos del formulario.

    #### Success Reponse

    ```
    {
        "status": 200,
        "body": "Paciente actualizado exitosamente",
        "paciente": {
            "datos": {
                "id": 1,
                "nombre": "New Name",
                "apellido": "New Last Name",
                "dni": "12345678",
                "fecha_nac": "2020-11-02",
                "sexo": "Masculino",
                "estado": "Internado",
                "localidad": "Otro"
            },
            "obra_social": {
                "id": 1,
                "nombre": "I.O.M.A"
            }
        }
    }

    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Ese DNI ya está registrado"
    }
    ```

    ```
    {
        "status": 404,
        "body": "Ese paciente no existe"
    }
    ```

    ```
    {
        "status": 404,
        "body": "La Obra Social no existe"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- DELETE

  - `/api/paciente?id=<id>`

    Borra un paciente en base a su ID

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Paciente eliminado exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese paciente no existe"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

### 🔶 Estudio

- GET

  - `/api/estudio?id=<id>`

    Retorna un Estudio en base a su ID.
    Este Endpoint retorna cualquier tipo de Estudio.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": {
            "estudio": {
                "informacion_general": {
                    "id": 1,
                    "fecha": "2020-10-15",
                    "descripcion": "Cavidades de dimensiones normales",
                    "especialista": "Dra de Iraola, Ana",
                    "tipo": "Congénitas",
                    "motivo": "Control",
                    "diagnostico_obligatorio": "Normal",
                    "diagnostico_opcional_1": "Normal",
                    "diagnostico_opcional_2": "Normal",
                    "paciente": {
                        "datos": {
                            "id": 2,
                            "nombre": "Nombre",
                            "apellido": "Apellido",
                            "dni": "123",
                            "fecha_nac": "2020-12-08",
                            "sexo": "Femenino",
                            "estado": "ambulatorio",
                            "localidad": "La Plata"
                        },
                        "obra_social": {
                            "id": 1,
                            "nombre": "I.O.M.A"
                        }
                    }
                },
                "atributos": {
                    "conclusion": "Una conclusión",
                    //Acá los atributos varían en base al Tipo
                }
            }
        }
    }

    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request",
    }
    ```

    ```
    {
        "status": 404,
        "body": "Esa Estudio no existe"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/estudio`

    Crea un nuevo Estudio en base a su Tipo.  
    Este Endpoint puede ser utilizado para crear cualquier tipo de Estudio.  
    La forma de diferenciar uno de otro, es enviando el identificador de cada  
    uno a través del cuerpo del mensaje.

    A continuación se lista los diferentes esquemas de los formularios en base  
    a los tipos de Estudios.

  - Esquema Formulario Tipo Básico

    - nombre_tipo: FK_nombre \*

      \* Éste es el identificador de Tipo previamente mencionado, en este caso  
       si se quiere dar de alta un Estudio de Tipo Básico, se debe enviar la FK
      'basico'.  
      Nota: Cuando se da de alta un nuevo Tipo de Estudio, para dar de alta un  
      Estudio de ese nuevo Tipo, deben enviar la FK como cualquier otro, no  
       obstante, sin importar la FK que sea, todos los tipos creados por el usuario  
       serán tratados como Básicos.

    - fecha: Date(YYYY-MM-DD)
    - conclusion: String(1000)
    - id_especialista: FK_id_especialista
    - id_motivo: FK_id_motivo
    - id_diagnostico_obligatorio: FK_id_diagnostico
    - id_diagnostico_opcional_1: FK_id_diagnostico (OPCIONAL)
    - id_diagnostico_opcional_2: FK_id_diagnostico (OPCIONAL)
    - id_descripcion_predefinida: FK_id_descripcion_predefinida
    - id_paciente: FK_id_paciente

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": {
            "estudio": {
                "informacion_general": {
                    "id": 6,
                    "fecha": "2020-10-15",
                    "descripcion": "Cavidades de dimensiones normales",
                    "especialista": "Dra de Iraola, Ana",
                    "tipo": "Básico",
                    "motivo": "Control",
                    "diagnostico_obligatorio": "Normal",
                    "diagnostico_opcional_1": "Normal",
                    "diagnostico_opcional_2": "Normal",
                    "paciente": {
                        "datos": {
                            "id": 2,
                            "nombre": "Nombre",
                            "apellido": "Apellido",
                            "dni": "123",
                            "fecha_nac": "2020-12-08",
                            "sexo": "Femenino",
                            "estado": "ambulatorio",
                            "localidad": "La Plata"
                        },
                        "obra_social": {
                            "id": 1,
                            "nombre": "I.O.M.A"
                        }
                    }
                },
                "atributos": {
                    "conclusion": "Una Conclusión"
                }
            }
        }
    }
    ```

  - Esquema Formulario Tipo Disincronía

    - nombre_tipo: 'disincronia'

    Todos los campos que conforman al Estudio Básico más  
    los siguientes campos opcionales:

    - retraso_septal: Decimal(places=4)

    - dpt: Decimal(places=4)

    - str: Decimal(places=4)

    - llenado_distolico: Decimal(places=4)

    - diferencia_periodos: Decimal(places=4)

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": {
            "estudio": {
                "informacion_general": {
                    "id": 7,
                    "fecha": "2020-10-15",
                    "descripcion": "Cavidades de dimensiones normales",
                    "especialista": "Dra de Iraola, Ana",
                    "tipo": "Disincronía",
                    "motivo": "Control",
                    "diagnostico_obligatorio": "Normal",
                    "diagnostico_opcional_1": "Normal",
                    "diagnostico_opcional_2": "Normal",
                    "paciente": {
                        "datos": {
                            "id": 2,
                            "nombre": "Nombre",
                            "apellido": "Apellido",
                            "dni": "123",
                            "fecha_nac": "2020-12-08",
                            "sexo": "Femenino",
                            "estado": "ambulatorio",
                            "localidad": "La Plata"
                        },
                        "obra_social": {
                            "id": 1,
                            "nombre": "I.O.M.A"
                        }
                    }
                },
                "atributos": {
                    "conclusion": "Una Conclusión",
                    "retraso_septal": 10.44,
                    "dpt": 20.33,
                    "str": 10.11,
                    "llenado_distolico": 55.998,
                    "diferencia_periodos": 44.99
                }
            }
        }
    }
    ```

  - Esquema Formulario Tipo Salina Agitada

    - nombre_tipo: 'salina_agitada'

    Todos los campos que conforman al Estudio Básico más  
    los siguientes campos:

    - id_conclusion_predefinida: FK_conclusion_predefinida

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": {
            "estudio": {
                "informacion_general": {
                    "id": 8,
                    "fecha": "2020-10-15",
                    "descripcion": "Cavidades de dimensiones normales",
                    "especialista": "Dra de Iraola, Ana",
                    "tipo": "Salina Agitada",
                    "motivo": "Control",
                    "diagnostico_obligatorio": "Normal",
                    "diagnostico_opcional_1": "Normal",
                    "diagnostico_opcional_2": "Normal",
                    "paciente": {
                        "datos": {
                            "id": 2,
                            "nombre": "Nombre",
                            "apellido": "Apellido",
                            "dni": "123",
                            "fecha_nac": "2020-12-08",
                            "sexo": "Femenino",
                            "estado": "ambulatorio",
                            "localidad": "La Plata"
                        },
                        "obra_social": {
                            "id": 1,
                            "nombre": "I.O.M.A"
                        }
                    }
                },
                "atributos": {
                    "id": 1,
                    "escenario": "Se  visualiza repleción de burbujas en cavidades  derechas,
                        no observándose la aparición de las mismas en cavidades izquierdas
                        después del 3° latido, tanto en reposo, como durante la sensibilización
                        con maniobra de Valsalva.",
                    "conclusion": "Estudio (-) para shunt"
                }
            }
        }
    }
    ```

  - Esquema Formulario Tipo 2D y Doppler Color

    - nombre_tipo: '2d_doppler'

    Todos los campos que conforman al Estudio Básico más  
    los siguientes campos opcionales:

    #### Valores VB

    - vb_SIV: Decimal(places=4)

    - vb_DSVI: Decimal(places=4)

    - vb_DDVI: Decimal(places=4)

    - vb_PP: Decimal(places=4)

    - vb_FEY: Decimal(places=4)

    - vb_Al_Area: Decimal(places=4)

    - vb_Al_Vol_SC: Decimal(places=4)

    - vb_Aorta: Decimal(places=4)

    - vb_Ap_Vao: Decimal(places=4)

    - vb_TSVI: Decimal(places=4)

    #### Valores DP

    - dp_Onda_S_Septal: Decimal(places=4)

    - dp_Onda_S_Lateral: Decimal(places=4)

    - dp_Onda_e: Decimal(places=4)

    - dp_Relacion_E_e: Decimal(places=4)

    - dp_Onda_S_Vd: Decimal(places=4)

    #### <== Valores Doppler ==>

    ##### Tricúspide

    - vd_Pad: Decimal(places=4)

    - vd_PmAP: Decimal(places=4)

    - vd_PAP: Decimal(places=4)

    - vd_Gradiente_Pico_TT: Decimal(places=4)

    - vd_velocidad_Regurgitante_TT: Decimal(places=4)

    - vd_Insuficiencia_Tricuspidea: Decimal(places=4)

    ##### Pulmonar

    - vd_Velocidad_Maxima_A_Pulmonar = Decimal(places=4)

    - vd_Gradiente_Maximo = Decimal(places=4)

    - vd_Tiempo_Al_Pico = Decimal(places=4)

    - vd_Insuficiencia_Pulmonar = Decimal(places=4)

    - vd_Gradiente_Protosistolico = Decimal(places=4)

    - vd_Gradiente_Telesistolico = Decimal(places=4)

    - vd_QP_QS = Decimal(places=4)

    ##### Mitral

    - vd_Velocidad_Onda_E = Decimal(places=4)

    - vd_Velocidad_Onda_A = Decimal(places=4)

    - vd_Gradiente_Medio_Trasmitral = Decimal(places=4)

    - vd_Insuficiencia_Mitral = Decimal(places=4)

    - vd_ORE = Decimal(places=4)

    - vd_Volumen_Regurgitante = Decimal(places=4)

    - vd_Area_VM_THP = Decimal(places=4)

    - vd_Dp_dt = Decimal(places=4)

    ##### Aórtica

    - vd_Flujo_Reverso_AA = Decimal(places=4)

    - vd_Flujo_Reverso_ADT = Decimal(places=4)

    - vd_THP = Decimal(places=4)

    - vd_Insuficiencia_Aortica = Decimal(places=4)

    - vd_Gradiente_Medio_Ao = Decimal(places=4)

    - vd_Gradiente_Maximo_Ao = Decimal(places=4)

    - vd_Velocidad_Maxima = Decimal(places=4)

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": {
            "estudio": {
                "informacion_general": {
                    "id": 9,
                    "fecha": "2020-10-15",
                    "descripcion": "Cavidades de dimensiones normales",
                    "especialista": "Dra de Iraola, Ana",
                    "tipo": "2D Doppler Color",
                    "motivo": "Control",
                    "diagnostico_obligatorio": "Normal",
                    "diagnostico_opcional_1": "Normal",
                    "diagnostico_opcional_2": "Normal",
                    "paciente": {
                        "datos": {
                            "id": 2,
                            "nombre": "Nombre",
                            "apellido": "Apellido",
                            "dni": "123",
                            "fecha_nac": "2020-12-08",
                            "sexo": "Femenino",
                            "estado": "ambulatorio",
                            "localidad": "La Plata"
                        },
                        "obra_social": {
                            "id": 1,
                            "nombre": "I.O.M.A"
                        }
                    }
                },
                "atributos": {
                    "conclusion": "Una Conclusión",
                    "vb_SIV": 10.54,
                        .
                        .
                        .
                        // 44 atributos omitidos
                    "vd_Velocidad_Maxima": 11.0
                }
            }
        }
    }
    ```

  - Esquema Formulario Tipo Congénitas

    - nombre_tipo: 'congenitas'

    Todos los campos que conforman al Estudio Básico más  
    todos los campos que conforman al Estudio 2D y Doppler Color  
    más los siguientes campos opcionales:

    - situs: String(50)

    - conexion_ventriculo_arterial: String(50)

    - conexion_auriculo_ventricular: String(50)

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": {
            "estudio": {
                "informacion_general": {
                    "id": 10,
                    "fecha": "2020-10-15",
                    "descripcion": "Cavidades de dimensiones normales",
                    "especialista": "Dra de Iraola, Ana",
                    "tipo": "Congénitas",
                    "motivo": "Control",
                    "diagnostico_obligatorio": "Normal",
                    "diagnostico_opcional_1": "Normal",
                    "diagnostico_opcional_2": "Normal",
                    "paciente": {
                        "datos": {
                            "id": 2,
                            "nombre": "Nombre",
                            "apellido": "Apellido",
                            "dni": "123",
                            "fecha_nac": "2020-12-08",
                            "sexo": "Femenino",
                            "estado": "ambulatorio",
                            "localidad": "La Plata"
                        },
                        "obra_social": {
                            "id": 1,
                            "nombre": "I.O.M.A"
                        }
                    }
                },
                "atributos": {
                    "conclusion": "Una Conclusión",
                    "vb_SIV": 10.54,
                        .
                        .
                        .
                        // 44 atributos omitidos
                    "vd_Velocidad_Maxima": 11.0,
                    "situs": "SOLITUS",
                    "conexion_ventriculo_arterial": null,
                    "conexion_auriculo_ventricular": null
                }
            }
        }
    }
    ```

  - ❌ Error Responses

    Todos los Estudios tienen las mismas respuestas de error  
    sin importar el tipo.

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request",
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request",
        "details": "Una de las Foreign ID que se enviaron no existen"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/estudio?id=<id>`

    Modifica un Estudio en base a su ID

    Para simplicar esta parte, se tendrán en cuenta los campos  
    previamente mencionados en el método POST.

    Lo importante de este Endpoint es que se tiene que enviar  
    OBLIGATORIAMENTE el ID y el Tipo del Estudio a modificar.

    El ID se pasa como parámetro y el tipo se pasa a través del  
    body con el nombre 'nombre_tipo'.

    Al ser una modificación CUALQUIERA del resto de los campos  
    de un Estudio son OPCIONALES al momento de enviar la solicitud.

    Nota: NO se puede modificar el campo Tipo de un Estudio.  
    Es decir, un estudio 'basico' no puede pasar a ser un '2d_doppler'.

  - Esquema de Formulario

    Lo importante acá es que el campo Tipo del Estudio que están  
    modificando por ID y el nombre_tipo que están enviando a través  
    del body coincidan.

    - nombre_tipo: FK_nombre_tipo_estudio (OBLIGATORIO)
    - Todos los otros campos que se quieran modificar (OPCIONALES) \*

      \* Se respetan todos los identificadores de los formularios usados en  
       el Endpoint de POST

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "message": "Estudio modificado exitosamente",
        "body": {
            "estudio": {
                "informacion_general": {
                    "id": 5,
                    "fecha": "2020-10-15",
                    "descripcion": "Cavidades de dimensiones normales",
                    "especialista": "Dra de Iraola, Ana",
                    "tipo": "Un Nuevo Tipo",
                    "motivo": "Control",
                    "diagnostico_obligatorio": "Normal",
                    "diagnostico_opcional_1": "Normal",
                    "diagnostico_opcional_2": "Normal",
                    "paciente": {
                        "datos": {
                            "id": 2,
                            "nombre": "Nombre",
                            "apellido": "Apellido",
                            "dni": "123",
                            "fecha_nac": "2020-12-08",
                            "sexo": "Femenino",
                            "estado": "ambulatorio",
                            "localidad": "La Plata"
                        },
                        "obra_social": {
                            "id": 1,
                            "nombre": "I.O.M.A"
                        }
                    }
                },
                "atributos": {
                    "conclusion": "Una conclusión modificada"
                    // Acá los atributos varían en base al Tipo
                }
            }
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request",
        "details": "El campo [campo]* no posee un tipo válido."
    }
    ```

        * campo puede ser cualquiera de los campos que se envían que NO son
        una FK

    ```
    {
        "status": 400,
        "body": "Bad Request",
        "details": "Ese [ForeignKey] no existe"
    }
    ```

    - ForeignKey puede ser:
      - Tipo de Estudio
      - Especialista
      - Motivo
      - Diagnóstico
      - Descripción
      - Paciente

    ```
    {
        "status": 400,
        "message": "El nombre_tipo no coincide con el tipo del estudio pasado por ID",
        "body": "Bad Request",
        "details": "Expected: congenitas. Got: un_nuevo_tipo"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- DELETE

  - `/api/estudio?id=<id>`

    Borra un Estudio en base a su ID

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Estudio borrado exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese estudio no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

### 🔶 Obra Social

- GET

  - `/api/obra_social?id=<id>`

    Retorna una Obra Social en base a su ID

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": {
            "id": 1,
            "nombre": "Obra Social Example"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Esa Obra Social no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

  - `/api/obra_social?all=True`

    Retorna todas las Obras Sociales

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": [
            {
                "id": 1,
                "nombre": "Obra Social 1"
            },
            {
                "id": 2,
                "nombre": "Obra Social 2"
            },
            {
                "id": 3,
                "nombre": "Obra Social 3"
            },
            {
                "id": 4,
                "nombre": "Obra Social 4"
            }
        ]
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

  - `/api/obra_social/<id_obra_social>/pacientes?page=<int>&perPage=<int>`

    Retorna todos los pacientes asociados a una Obra Social  
    Los resultados pueden ser paginados  
    Si no se pasan los parametros 'page' o 'perPage', ambos defaultean a 1 y 10 respectivamente

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": [
            {
                "id": 2,
                "nombre": "Name A",
                "apellido": "Last Name A",
                "dni": "123456",
                "fecha_nac": "2020-10-03",
                "sexo": "Masculino",
                "estado": "Internado",
                "localidad": "Otro"
            },
            {
                "id": 4,
                "nombre": "Name B",
                "apellido": "Last Name B",
                "dni": "1234567",
                "fecha_nac": "2020-11-03",
                "sexo": "Femenino",
                "estado": "Ambulatorio",
                "localidad": "La Plata"
            }
        ],
        "totales": 3,
        "pagina": 1
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Número de página inválido",
        "pagina_maxima": <int>
    }
    ```

    ```
    {
        "status": 400,
        "body": "Esa Obra Social no tiene pacientes asociados"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/obra_social`

    Crea una nueva Obra Social

  - Esquema Formulario

    - nombre: String(50)

    #### ✔️ Success Response

    ```
    {
        "status": "201",
        "body": "Obra Social creada exitosamente",
        "obra_social": {
            "id": 6,
            "nombre": "New Obra Social"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/obra_social?id=<id>`

    Modifica una Obra Social en base a su ID

  - Esquema formulario

    - nombre: String(50)

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Obra Social actualizada exitosamente",
        "obra_social": {
            "id": 1,
            "nombre": "Modified Obra Social"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Esa Obra Social no existe"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- DELETE

  - `/api/obra_social?id=<id>`

    Elimina una Obra Social es base a su ID  
    Si la Obra Social tiene pacientes asociados, NO se podrá eliminar

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Obra Social eliminada exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request",
        "details": "La Obra Social tiene pacientes asociados"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Esa Obra Social no existe"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

### 🔶 Tipo de Estudio

- GET

  - `/api/tipo_estudio?id=<id>`
  - `/api/tipo_estudio?all=True`

    Retorna un Tipo de Estudio en base a su ID o  
    retorna todos los Tipos si se pasa el parámetro 'all'  
    en 'True'

    #### ✔️ Success Responses

    ```
    {
        "status": 200,
        "body": {
            "id": 1,
            "nombre": "basico",
            "nombre_mostrado": "Básico"
        }
    }
    ```

    ```
    {
        "status": 200,
        "body": [
            {
                "id": 1,
                "nombre": "basico",
                "nombre_mostrado": "Básico"
            },
            {
                "id": 2,
                "nombre": "disincronia",
                "nombre_mostrado": "Disincronía"
            },
            {
                "id": 3,
                "nombre": "salina_agitada",
                "nombre_mostrado": "Salina Agitada"
            },
            {
                "id": 4,
                "nombre": "2d_doppler",
                "nombre_mostrado": "2D Doppler Color"
            },
            {
                "id": 5,
                "nombre": "congenitas",
                "nombre_mostrado": "Congénitas"
            },
            {
                "id": 6,
                "nombre": "un_nuevo_tipo",
                "nombre_mostrado": "Un Nuevo Tipo"
            }
        ]
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese Tipo de Estudio no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/tipo_estudio`

    Crea un nuevo Tipo de Estudio

  - Esquema Formulario

    - nombre: String(200) \*

    - nombre_mostrado: String(200)

    \* El nombre va a ser el identificador del Tipo de Estudio.  
    A su vez, 'nombre' va a ser una FK, ende, es necesario que  
    se le aplique una sanitización antes de enviarlo a la API.  
    La API lo único que hace es reemplazar los espacios del String  
    con underscores y pasar el String a lowercase.  
    El nombre debería generarse a partir del campo nombre_mostrado.

    A continuación dejo un ejemplo de sanitización por parte del Front:

    ```
    var nombre_mostrado = // Acá asigno el valor del Input a una variable
    var campoNormalizado = nombre_mostrado.replace(/ /g,'_');
    campoNormalizado = campoNormalizado.normalize("NFD").replace(/[\u0300-\u036f]/g,"");
    campoNormalizado = campoNormalizado.replace(/[^a-z\d\s]+/gi, "");

    // El resultado de esto es lo siguiente:
    // Supongamos que el usuario ingresó el String "Un Nuevo Tipo++🚹🚹🚹ççç"
    // Luego de la sanitización, quedaría el String "Un_Nuevo_Tipo"
    // Ustedes deberían enviar el String sanitizado en el campo 'nombre'
    // y el String NO sanitizado en el campo 'nombre_mostrado'
    ```

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": "Tipo de Estudio creado exitosamente",
        "tipo_estudio": {
            "id": 9,
            "nombre": "un_nuevo_tipo_2",
            "nombre_mostrado": "Un Nuevo Tipo 2"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/tipo_estudio?id=<id>`

    Modifica un Estudio en base a su ID

    Acá para evitar complicaciones de actualizaciones de  
    Foreign Keys, ya que 'nombre' es una FK, ese campo NO se puede  
    modificar.

    Una vez generado el identificador del Tipo, éste mismo NO cambia.  
    Lo que si se puede modificar es el nombre_mostrado.

  - Esquema Formulario

    - nombre_mostrado: String(200)

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Tipo actualizado exitosamente",
        "tipo_estudio": {
            "id": 9,
            "nombre": "un_nuevo_tipo_2",
            "nombre_mostrado": "Cambiando"
        }
    }
    ```

- DELETE

  - `/api/tipo_estudio?id=<id>`

    Borra un Tipo de Estudio en base a su ID

    Si el Tipo de Estudio tiene Estudios asociados, NO  
    se podrá borrar.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Tipo de Estudio eliminado exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese Tipo de Estudio no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Ese Tipo de Estudio tiene estudios asociados"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

### 🔶 Conclusión Predefinida

- GET

  - `/api/conclusion?id=<id>`

    Retorna una conclusión predefinida en base a su ID

  - `/api/conclusion?all=True`

    Retorna todas las conclusiones predefinidas

    #### ✔️ Success Responses

    - `id`

    ```
    {
        "status": 200,
        "body": {
            "id": 2,
            "escenario": "Se  visualiza repleción de burbujas en cavidades
            derechas, observándose la aparición de las mismas en cavidades
            izquierdas entre el  3°/4° latido, tanto en reposo, como
            durante la sensibilización con maniobra de Valsalva.",
            "conclusion": "Foramen Oval Permeable con shunt derecha/
                           izquierda. CIA"
        }
    }
    ```

    - `all`

    ```
    {
        "status": 200,
        "body": [
            {
                "id": 1,
                "escenario": "Se  visualiza repleción de burbujas en cavidades  derechas, no
                observándose la aparición de las mismas en cavidades izquierdas después del 3°
                latido, tanto en reposo, como durante la sensibilización con maniobra de
                Valsalva.",
                "conclusion": "Estudio (-) para shunt"
            },
            {
                "id": 2,
                "escenario": "Se  visualiza repleción de burbujas en cavidades  derechas,
                observándose la aparición de las mismas en cavidades izquierdas entre el  3°/
                4° latido, tanto en reposo, como durante la sensibilización con maniobra de
                Valsalva.",
                "conclusion": "Foramen Oval Permeable con shunt derecha/ izquierda. CIA"
            },
            {
                "id": 3,
                "escenario": "Se visualiza repleción de burbujas en cavidades derechas con
                aparición de las mismas en cavidades izquierdas luego del 7° latido.",
                "conclusion": "Shunt intrapulmonar"
            },
            {
                "id": 4,
                "escenario": "Se  visualiza a aparición de burbujas a través del seno
                Coronario replecionando las cavidades derechas.",
                "conclusion": "Vena Cava superior izquierda persistente"
            }
        ]
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Esa Conclusión predefinida no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/conclusion`

    Crea una nueva Conclusión Predefinida

  - Esquema Formulario

    - escenario: String(1000)

    - conclusion: String(400)

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": "Conclusión predefinida creada exitosamente",
        "conclusion_predefinida": {
            "id": 5,
            "escenario": "Un Escenario",
            "conclusion": "Una Conclusión"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/conclusion?id=<id>`

    Modifica una Conclusión Predefinida en base a su ID

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Conclusión predefinida actualizada exitosamente",
        "conclusion_predefinida": {
            "id": 5,
            "escenario": "Un Escenario modificado",
            "conclusion": "Una Conclusión modificada"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Not Found",
        "details": "Esa conclusión predefinida no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- DELETE

  - `/api/conclusion?id=<id>`

    Elimina una Conclusión Predefinida en base a su ID  
    Si la Conclusión Predefinida tiene estudios asociados, NO se podrá eliminar.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Conclusión predefinida eliminada exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Esa Conclusión predefinida tiene estudios asociados"
    }
    ```

    ```
    {
        "status": 404,
        "body": "Esa Conclusión predefinida no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

### 🔶 Descripción Predefinida

- GET

  - `/api/descripcion?id=<id>`

    Retorna una Descripción Predefinida en base a su ID

  - `/api/descripcion?all=True`

    Retorna todas las Descripciones Predefinidas

    #### ✔️ Success Responses

    - `id`

    ```
    {
        "status": 200,
        "body": {
            "id": 1,
            "nombre": "Cavidades de dimensiones normales"
        }
    }
    ```

    - `all`

    ```
    {
        "status": 200,
        "body": [
            {
                "id": 1,
                "nombre": "Cavidades de dimensiones normales"
            },
            {
                "id": 2,
                "nombre": "Función sistólica de VI  conservadaRaíz de aorta de dimensiones
                normales, con relación sinotubular conservada"
            },
            .
            .
            .
            // Resultados omitidos
            {
                "id": 10,
                "nombre": "Pericardio libre."
            }
        ]
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Esa Descripción predefinida no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/descripcion`

    Crea una nueva Descripción Predefinida

  - Esquema Formulario

    - nombre: String(1000)

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": "Descripción predefinida creada exitosamente",
        "descripcion_predefinida": {
            "id": 11,
            "nombre": "Una Descripción Predefinida"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/descripcion?id=<id>`

    Modifica una Descripción Predefinida en base a su ID

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Descripción predefinida actualizada exitosamente",
        "descripcion_predefinida": {
            "id": 11,
            "nombre": "Una Descripción Predefinida modificada"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Not Found",
        "details": "Esa Descripción predefinida no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- DELETE

  - `/api/descripcion?id=<id>`

    Elimina una Descripción Predefinida en base a su ID  
    Si la Descripción Predefinida tiene estudios asociados, NO se podrá eliminar.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Descripción predefinida eliminada exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Esa Descripción predefinida tiene estudios asociados"
    }
    ```

    ```
    {
        "status": 404,
        "body": "Esa Descripción predefinida no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

### 🔶 Diagnóstico

- GET

  - `/api/diagnostico?id=<id>`

    Retorna un Diagnóstico en base a su ID

  - `/api/diagnostico?all=True`

    Retorna todos los Diagnósticos

    #### ✔️ Success Responses

    - `id`

    ```
    {
        "status": 200,
        "body": {
            "id": 1,
            "nombre": "Normal"
        }
    }
    ```

    - `all`

    ```
    {
        "status": 200,
        "body": [
            {
                "id": 1,
                "nombre": "Normal"
            },
            {
                "id": 2,
                "nombre": "Cardiopatía isquémica"
            },
            {
                "id": 3,
                "nombre": "Cardiopatía HTA"
            },
            .
            .
            .
            // Resultados omitidos
            {
                "id": 33,
                "nombre": "Enfermedad vascular significativa"
            }
        ]
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese Diagnóstico no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/diagnostico`

    Crea un nuevo Diagnóstico

  - Esquema Formulario

    - nombre: String(1000)

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": "Diagnóstico creado exitosamente",
        "diagnostico": {
            "id": 34,
            "nombre": "Un Diagnóstico"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/diagnostico?id=<id>`

    Modifica un Diagnóstico en base a su ID

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Diagnóstico actualizado exitosamente",
        "diagnostico": {
            "id": 34,
            "nombre": "Un Diagnóstico modificado"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Not Found",
        "details": "Ese Diagnóstico no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- DELETE

  - `/api/diagnostico?id=<id>`

    Elimina un Diagnóstico en base a su ID  
    Si el Diagnóstico tiene estudios asociados, NO se podrá eliminar.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Diagnóstico eliminado exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Ese Diagnóstico tiene estudios asociados"
    }
    ```

    ```
    {
        "status": 404,
        "body": "Ese Diagnóstico no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

### 🔶 Especialista

- GET

  - `/api/especialista?id=<id>`

    Retorna un Especialista en base a su ID

  - `/api/especialista?all=True`

    Retorna todos los Especialistas

    #### ✔️ Success Responses

    - `id`

    ```
    {
        "status": 200,
        "body": {
            "id": 1,
            "nombre": "Dra de Iraola, Ana"
        }
    }
    ```

    - `all`

    ```
    {
        "status": 200,
        "body": [
            {
                "id": 1,
                "nombre": "Dra de Iraola, Ana"
            },
            {
                "id": 2,
                "nombre": "Dra Faliva Mai, Gianina"
            },
            {
                "id": 3,
                "nombre": "Dra Gómez, Daniela"
            },
            {
                "id": 4,
                "nombre": "Dr Pagola Juan"
            },
            {
                "id": 5,
                "nombre": "Dr Pizzini Luis"
            },
            {
                "id": 6,
                "nombre": "Dra Mardzin, Brenda"
            }
        ]
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese Especialista no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/especialista`

    Crea un nuevo Especialista

  - Esquema Formulario

    - nombre: String(1000)

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": "Especialista creado exitosamente",
        "especialista": {
            "id": 7,
            "nombre": "Un Especialista"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/especialista?id=<id>`

    Modifica un Especialista en base a su ID

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Especialista actualizado exitosamente",
        "especialista": {
            "id": 7,
            "nombre": "Un Especialista Modificado"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Not Found",
        "details": "Ese Especialista no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- DELETE

  - `/api/especialista?id=<id>`

    Elimina un Especialista en base a su ID  
    Si el Especialista tiene estudios asociados, NO se podrá eliminar.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Especialista eliminado exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Ese Especialista tiene estudios asociados"
    }
    ```

    ```
    {
        "status": 404,
        "body": "Ese Especialista no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

### 🔶 Motivo

- GET

  - `/api/motivo?id=<id>`

    Retorna un Motivo en base a su ID

  - `/api/motivo?all=True`

    Retorna todos los Motivos

    #### ✔️ Success Responses

    - `id`

    ```
    {
        "status": 200,
        "body": {
            "id": 1,
            "nombre": "Control"
        }
    }
    ```

    - `all`

    ```
    {
        "status": 200,
        "body": [
            {
                "id": 14,
                "nombre": "Accidente Cerebro vascular"
            },
            {
                "id": 18,
                "nombre": "Amiloidosis"
            },
            {
                "id": 7,
                "nombre": "Arritmia"
            },
            {
                "id": 3,
                "nombre": "Cardiopatía isquémica"
            },
            {
                "id": 5,
                "nombre": "Cardiopatías congénitas"
            },
            .
            .
            .
            // Resultados omitidos
        ]
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Ese Motivo no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- POST

  - `/api/motivo`

    Crea un nuevo Motivo

  - Esquema Formulario

    - nombre: String(200)

    #### ✔️ Success Response

    ```
    {
        "status": 201,
        "body": "Motivo creado exitosamente",
        "motivo": {
            "id": 21,
            "nombre": "Un Motivo"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- PUT

  - `/api/motivo?id=<id>`

    Modifica un Especialista en base a su ID

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Motivo actualizado exitosamente",
        "motivo": {
            "id": 21,
            "nombre": "Un Motivo Modificado"
        }
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 404,
        "body": "Not Found",
        "details": "Ese Motivo no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```

- DELETE

  - `/api/motivo?id=<id>`

    Elimina un Motivo en base a su ID  
    Si el Motivo tiene estudios asociados, NO se podrá eliminar.

    #### ✔️ Success Response

    ```
    {
        "status": 200,
        "body": "Motivo eliminado exitosamente"
    }
    ```

    #### ❌ Error Responses

    ```
    {
        "status": 400,
        "body": "Ese Motivo tiene estudios asociados"
    }
    ```

    ```
    {
        "status": 404,
        "body": "Ese Motivo no existe"
    }
    ```

    ```
    {
        "status": 400,
        "body": "Bad Request"
    }
    ```

    ```
    {
        "status": 500,
        "body": "Internal Server Error"
    }
    ```
