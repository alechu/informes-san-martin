# Informes San Martín

## <h2>Requisitos</h2>

- Nodejs version >= 12.18.3
- npm >= 6.14.6

## <h2>Organización de carpetas</h2>

```
.
├── frontend                        # Acá va a ir todo lo relacionado al frontend
|    |-- TemplateHospital           # Acá está la template principal
|        |-- documentacion          # Documentación sobre los componentes de la template
|        |-- template               # Todo el contenido principal de la template
|            |-- node_modules
|            |-- public
|            |-- src
|            |-- package-lock.json  # Información sobre el proyecto
|            |-- package.json
|            |-- README.md          # README de la template
|
|-- .gitignore
|-- README.md                       # Éste README

```

## Puesta en producción

### Setup General

1. Instalar Microsoft Visual C++ 2008 Redistributable Package (x86)

2. Instalar Microsoft Visual C++ 2015 Redistributable Package (x64)

3. Instalar Xampp

4. Instalar Python version >= 3.8.5

### Setup Backend

1. Abrir Xampp como administrador y elegir la opcion de instalar Apache y MySQL como servicios
2. Entrar a phpmyadmin y crear una base de datos con el nombre "san_martin"
3. Ir a la raíz del backend del proyecto y ejecutar los siguientes comandos

   - `pip install -r requirements.txt`

4. Correr en la consola el archivo "firstProdSetup.py"

5. Crear un archivo llamado "config.py" y copiar el contenido del config.py mencionado en el
   README.md del Repo

6. Reemplazar las variables en la ProductionConfig del "config.py"

7. Ejecutar el backend en una consola con el comando `flask run`

8. Correr en la consola el archivo "createDBForProd.py"

### Setup Frontend

1. Ir a frontend/TemplateHospital/template y ejecutar `npm run build`

2. Esto va a crear una carpeta build, cuyos contenido debemos copiar en la carpeta htdocs de xampp

3. Crear un archivo .htaccess en la raiz de htdocs y copiar lo siguiente

   ```
   Options -MultiViews
   RewriteEngine On
   RewriteCond %{REQUEST_FILENAME} !-f
   RewriteRule ^ index.html [QSA,L]
   ```

4. Reiniciar apache en xampp, y ya con esto el frontend deberia estar corriendo en http://localhost

### Setup de Seguridad

Como último paso vamos a asegurar phpmyadmin con una contraseña por si acaso, esa contraseña
se generó automáticamente al ejecutar el archivo "firstProdSetup.py" y se encuentra en el archivo .env ubicado en la raíz del backend.

1. Copiar la contraseña del archivo .env, se encuentra en la variable DB_PASS_RANDOM

2. Entrar a phpmyadmin y bajo la pestaña SQL ejecutar la siguiente query

   - `SET PASSWORD FOR 'root'@'localhost' = PASSWORD('la_password_que_copiaste');`

3. Ir a .\xampp\phpMyAdmin y editar el archivo "config.inc.php"

4. Acá tenemos que cambiar las variables

   - `$cfg['blowfish_secret'] = 'xampp'` ===> `Un string random generado con os.urandom(32) sin la parte de b'`
   - `$cfg['Servers'][$i]['auth_type'] = 'config';` ===> `cookie`
   - `$cfg['Servers'][$i]['password'] = '';` ===> `La contraseña que copiamos anteriormente`
   - `$cfg['Servers'][$i]['AllowNoPassword'] = false;` ===> `true`

5. Por último debemos volver al .env y setear la variable DB_PASS con la password que esta en la variable DB_PASS_RANDOM (Así el backend puede acceder a la db!)

6. Reiniciar MySQL y la Flask app
