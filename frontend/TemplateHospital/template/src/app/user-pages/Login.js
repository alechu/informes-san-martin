import React, { Component } from "react";
import { Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";

import Background from "../../assets/images/Hospital-San-Martin-edit.jpg";

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mostrarAlerta: false,
      mensajeAlerta: "Sesión expirada. Por favor, iniciá sesión de nuevo.",
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.checkAlert();
  }

  checkAlert() {
    var alert = localStorage.getItem("alert");
    localStorage.removeItem("alert");
    if (alert !== null) {
      if (alert.includes("JWT")) {
        this.setState(
          {
            mensajeAlerta:
              "Sesión expirada. Por favor, iniciá sesión de nuevo.",
            alertType: "error",
          },
          () => {
            this.setState({ mostrarAlerta: true });
          }
        );
      } else {
        this.setState(
          {
            mensajeAlerta: "Sesión cerrada exitosamente.",
            alertType: "success",
          },
          () => {
            this.setState({ mostrarAlerta: true });
          }
        );
      }
    }
  }

  handleSubmit(e) {
    var formulario = new FormData(document.getElementById("form-login"));
    fetch("http://" + process.env.REACT_APP_API_IP + "/api/user/login", {
      method: "POST",
      body: formulario,
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          this.setState(
            {
              mensajeAlerta:
                "Los datos son incorrectos, por favor verificalos y volvé a intentar.",
              alertType: "error",
            },
            () => {
              this.setState({
                mostrarAlerta: true,
              });
            }
          );
        } else {
          var agregar_minutos = function (dt, minutes) {
            return new Date(dt.getTime() + minutes * 60000);
          };
          var expireTime = agregar_minutos(new Date(), 480).toUTCString();
          document.cookie =
            "token=" +
            data.token +
            ";SameSite=Lax; expires=" +
            expireTime +
            "; path=/";
          this.props.history.push("/dashboard");
        }
      });
    e.preventDefault();
  }

  render() {
    return (
      <div
        style={{
          backgroundImage: "url(" + Background + ")",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
        <div className="d-flex align-items-center auth px-0">
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <Collapse in={this.state.mostrarAlerta}>
                <Alert variant="filled" severity={this.state.alertType}>
                  {this.state.mensajeAlerta}
                </Alert>
              </Collapse>
              <div className="auth-form-light text-center py-5 px-4 px-sm-5">
                <div className="brand-logo" style={{ marginBottom: "5px" }}>
                  <img
                    src={require("../../assets/images/logo.svg")}
                    alt="logo"
                    style={{ width: "100px" }}
                  />
                </div>
                <span
                  style={{
                    fontSize: 13,
                    color: "#297ced",
                  }}
                >
                  <strong>Hospital San Martín de La Plata</strong>
                </span>
                <h4>Bienvenido al sistema del Laboratorio de Ecocardiología</h4>
                <Divider style={{ margin: "20px 0px 20px 0px" }} />
                <h6 className="font-weight-light" style={{ marginTop: "10px" }}>
                  Por favor, iniciá sesión para continuar
                </h6>
                <Form
                  id="form-login"
                  className="pt-3"
                  onSubmit={this.handleSubmit}
                >
                  <Form.Group className="d-flex search-field">
                    <Form.Control
                      name="username"
                      placeholder="Usuario"
                      size="lg"
                      className="h-auto"
                    />
                  </Form.Group>
                  <Form.Group className="d-flex search-field">
                    <Form.Control
                      name="password"
                      type="password"
                      placeholder="Contraseña"
                      size="lg"
                      className="h-auto"
                    />
                  </Form.Group>
                  <div className="mt-3">
                    <Button
                      className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                      size="large"
                      style={{ backgroundColor: "#297ced", color: "white" }}
                      //onClick={this.handleSubmit}
                      type="submit"
                    >
                      ENTRAR
                    </Button>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
