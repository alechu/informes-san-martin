import React, {Component} from 'react';
import ChartJS from './ChartJs';
import ContenedorSeccion from '../otros/ContenedorSeccion';
import ShowChartIcon from '@material-ui/icons/ShowChart';


class Estadisticas extends Component {
    constructor(props) {
        super(props);

        this.state = {
            breadcrumb: [
                { pathname: '/estadisticas', state: '', texto: 'Estadísticas', icon: <ShowChartIcon /> } 
            ]
        }
    }

    // Para pasarle el breadcrumb al header
    componentDidMount() {
        this.props.actualizarHeader({
            breadcrumb: this.state.breadcrumb
        })
    }

    render() {

        return (
            <div>
                <ContenedorSeccion
                    nombre="Estadísticas"
                    descripcion="Podés ver la cantidad de estudios a lo largo del tiempo, aplicando diversos filtros"
                    componentes={[
                        <ChartJS />
                    ]}
                />
            </div>
        )
    }
}

export default Estadisticas;