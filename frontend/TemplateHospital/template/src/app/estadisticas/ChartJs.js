/*

/////////////////////////////////////////////////





Este tiene que descomponerse en DOS:

> FormularioEstadisticas.js
Los filtros con los que voy a armar el gŕafico.

> GraficoEstadisticas.js
El gráfico con ChartJS.

(ya están creados pero vacíos)




/////////////////////////////////////////////////

*/
import React, { Component } from "react";
import { Line } from "react-chartjs-2";
import moment from "moment";
import "moment/locale/es-mx";
import "moment/locale/es";
/* Material UI */
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import InputAdornment from "@material-ui/core/InputAdornment";
import DateFnsUtils from "@date-io/date-fns";
import { es } from "date-fns/locale";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";
import Autocomplete from "@material-ui/lab/Autocomplete";

const useStyles = (theme) => ({
  formControl: {
    marginBottom: theme.spacing(3),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
});

export class ChartJs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datosEstadisticas: {
        sexo: "",
        nacido_en: "",
        localidad: "",
        estado: "",
        tipo: "",
        diag_obligatorio: "",
        diag_opcional_1: "",
        diag_opcional_2: "",
        fechaInicio: new Date("2020-12-01".replace(/-/g, "/")),
        fechaFin: new Date(),
        agrupado_por: "dia",
        totalEstudios: 0,
      },
      tipos: [],
      diagnosticos: [],
      agrupadoDisabled: true,
      mensajeGrafico:
        "Llená el formulario de arriba y hacé click en Generar gráfico",
      mostrarGrafico: false,
      anos: [],

      //alert
      errorFechaInicio: false,
      errorFechaFin: false,
      mostrarAlert: false,
    };
    this.changeMostrarGrafico = this.changeMostrarGrafico.bind(this);
    this.submitEstadistica = this.submitEstadistica.bind(this);
    this.cambiarAgrupadoPor = this.cambiarAgrupadoPor.bind(this);
    this.fetchOpciones = this.fetchOpciones.bind(this);
    this.limpiarErrores = this.limpiarErrores.bind(this);
    this.cambiarAnoNacido = this.cambiarAnoNacido.bind(this);
    this.cambiarDiagObligatorio = this.cambiarDiagObligatorio.bind(this);
    this.cambiarDiagOpcional1 = this.cambiarDiagOpcional1.bind(this);
    this.cambiarDiagOpcional2 = this.cambiarDiagOpcional2.bind(this);
  }

  componentDidMount() {
    this.fetchOpciones();
    this.generateYears();
  }

  generateYears = function (startYear) {
    var currentYear = new Date().getFullYear(),
      years = [];
    startYear = startYear || 1900;
    while (startYear <= currentYear) {
      years.push((startYear++).toString());
    }
    this.setState({
      anos: years,
    });
  };

  async fetchOpciones() {
    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/tipo_estudio?all=True",
      {
        headers: new Headers({
          Authorization: "Bearer " + document.cookie.substring(6),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          var array = data.body;
          array.unshift({
            id: 0,
            nombre: "Sin especificar",
            nombre_mostrado: "Sin especificar",
          });
          this.setState({
            tipos: array,
          });
        }
      });

    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/diagnostico?all=True",
      {
        headers: new Headers({
          Authorization: "Bearer " + document.cookie.substring(6),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          this.setState({
            diagnosticos: data.body,
          });
        }
      });
  }

  changeMostrarGrafico() {
    this.setState({
      mostrarGrafico: !this.state.mostrarGrafico,
    });
  }

  cambiarAgrupadoPor(e) {
    this.setState(
      (prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          agrupado_por: e.target.value,
        },
      }),
      () => {
        this.submitEstadistica();
      }
    );
  }

  cambiarFechaInicio = (fecha) => {
    this.setState((prevState) => ({
      datosEstadisticas: {
        ...prevState.datosEstadisticas,
        fechaInicio: fecha,
      },
    }));
  };
  cambiarFechaFin = (fecha) => {
    this.setState((prevState) => ({
      datosEstadisticas: {
        ...prevState.datosEstadisticas,
        fechaFin: fecha,
      },
    }));
  };

  cambiarDato = (dato) => {
    this.setState((prevState) => ({
      datosEstadisticas: {
        ...prevState.datosEstadisticas,
        [dato.target.name]: dato.target.value,
      },
    }));
  };

  cambiarAnoNacido(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          nacido_en: newValue,
        },
      }));
    } else {
      this.setState((prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          nacido_en: "",
        },
      }));
    }
  }

  cambiarDiagObligatorio(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          diag_obligatorio: newValue,
        },
      }));
    } else {
      this.setState((prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          diag_obligatorio: "",
        },
      }));
    }
  }

  cambiarDiagOpcional1(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          diag_opcional_1: newValue,
        },
      }));
    } else {
      this.setState((prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          diag_opcional_1: "",
        },
      }));
    }
  }

  cambiarDiagOpcional2(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          diag_opcional_2: newValue,
        },
      }));
    } else {
      this.setState((prevState) => ({
        datosEstadisticas: {
          ...prevState.datosEstadisticas,
          diag_opcional_2: "",
        },
      }));
    }
  }

  limpiarErrores() {
    this.setState({
      errorFechaInicio: false,
      errorFechaFin: false,
      mostrarAlert: false,
    });
  }

  submitEstadistica() {
    this.limpiarErrores();
    const formulario = new FormData(
      document.getElementById("form_estadisticas")
    );

    var hayError = false;

    if (formulario.get("fecha_inicio") === "") {
      hayError = true;
      this.setState({
        errorFechaInicio: true,
        mostrarAlert: true,
      });
    }

    if (formulario.get("fecha_fin") === "") {
      hayError = true;
      this.setState({
        errorFechaFin: true,
        mostrarAlert: true,
      });
    }

    if (!hayError) {
      var datosParsed = new FormData();
      for (var unparsedKey of formulario.keys()) {
        if (unparsedKey !== "fecha_inicio" && unparsedKey !== "fecha_fin") {
          console.log(unparsedKey);
          if (formulario.get(unparsedKey) !== "") {
            if (formulario.get(unparsedKey) !== "Sin especificar")
              datosParsed.append(unparsedKey, formulario.get(unparsedKey));
          }
        }
      }

      if (this.state.datosEstadisticas.nacido_en !== "") {
        datosParsed.append("nacido_en", this.state.datosEstadisticas.nacido_en);
      }

      if (this.state.datosEstadisticas.diag_obligatorio !== "") {
        datosParsed.append(
          "diag_obligatorio",
          this.state.datosEstadisticas.diag_obligatorio.id
        );
      }

      if (this.state.datosEstadisticas.diag_opcional_1 !== "") {
        datosParsed.append(
          "diag_opcional_1",
          this.state.datosEstadisticas.diag_opcional_1.id
        );
      }

      if (this.state.datosEstadisticas.diag_opcional_2 !== "") {
        datosParsed.append(
          "diag_opcional_2",
          this.state.datosEstadisticas.diag_opcional_2.id
        );
      }

      datosParsed.append(
        "agrupado_por",
        this.state.datosEstadisticas.agrupado_por
      );

      const date_i = moment(
        formulario.get("fecha_inicio").replace(/\//g, "-"),
        "DD-MM-YYYY"
      );

      const date_f = moment(
        formulario.get("fecha_fin").replace(/\//g, "-"),
        "DD-MM-YYYY"
      );

      datosParsed.append("fecha_inicio", date_i.format("YYYY-MM-DD"));
      datosParsed.append("fecha_fin", date_f.format("YYYY-MM-DD"));

      const data = [...datosParsed.entries()];
      console.log(data);
      const asString = data
        .map((x) => `${encodeURIComponent(x[0])}=${encodeURIComponent(x[1])}`)
        .join("&");

      //url.search = new URLSearchParams(params).toString();

      fetch(
        "http://" +
          process.env.REACT_APP_API_IP +
          "/api/estadisticas?" +
          asString,
        {
          headers: new Headers({
            Authorization:
              "Bearer " +
              document.cookie.replace(
                /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                "$1"
              ),
          }),
        }
      )
        .then((res) => res.json())
        .then((data) => {
          if (data.status === 200) {
            this.data.labels = [];
            this.data.datasets[0].data = [];
            for (var dataSet in data.body.dataset) {
              if (this.state.datosEstadisticas.agrupado_por === "dia") {
                this.data.labels.push(
                  moment(data.body.dataset[dataSet].dia, "YYYY-MM-DD").format(
                    "DD-MM-YYYY"
                  )
                );
              } else {
                if (this.state.datosEstadisticas.agrupado_por === "mes") {
                  this.data.labels.push(
                    moment(data.body.dataset[dataSet].mes, "MM").format("MMMM")
                  );
                } else {
                  this.data.labels.push(data.body.dataset[dataSet].año);
                }
              }
              this.data.datasets[0].data.push(
                data.body.dataset[dataSet].cantidad
              );
            }
            this.setState(
              (prevState) => ({
                datosEstadisticas: {
                  ...prevState.datosEstadisticas,
                  totalEstudios: data.body.total_estudios,
                },
              }),
              () => {
                this.setState(
                  { mostrarGrafico: true, agrupadoDisabled: false },
                  () => {
                    window.scrollTo({
                      top: document.body.scrollHeight - 750,
                      behavior: "smooth",
                    });
                  }
                );
              }
            );
          } else {
            if (data.status === 404) {
              this.setState(
                {
                  mensajeGrafico: data.details,
                  mostrarGrafico: false,
                  agrupadoDisabled: true,
                },
                () => {
                  this.setState(
                    (prevState) => ({
                      datosEstadisticas: {
                        ...prevState.datosEstadisticas,
                        totalEstudios: 0,
                      },
                    }),
                    () => {
                      window.scrollTo({
                        top: document.body.scrollHeight,
                        behavior: "smooth",
                      });
                    }
                  );
                }
              );
            } else {
              if (data.status === 401) {
                localStorage.setItem("alert", "Expired/Invalid JWT Token");
                document.cookie =
                  "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                window.location.href = "/user-pages/login-1";
              }
            }
          }
        });
    } else {
      this.setState(
        {
          mensajeGrafico:
            "Llená el formulario de arriba y hacé click en Generar gráfico",
          mostrarGrafico: false,
          agrupadoDisabled: true,
        },
        () => {
          this.setState(
            (prevState) => ({
              datosEstadisticas: {
                ...prevState.datosEstadisticas,
                totalEstudios: 0,
              },
            }),
            () => {
              window.scrollTo({ behavior: "smooth", top: 0, bottom: 0 });
            }
          );
        }
      );
    }
  }

  data = {
    labels: [],
    datasets: [
      {
        label: "Cantidad de estudios",
        data: [],
        backgroundColor: "#15a2f7",
        borderColor: "#15a2f7",
        borderWidth: 3,
        fill: false,
        lineTension: 0,
      },
    ],
  };

  options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            precision: 0,
          },
        },
      ],
    },
    legend: {
      display: true,
    },
    elements: {
      point: {
        radius: 4,
      },
    },
  };

  render() {
    const mostrarGrafico = this.state.mostrarGrafico;

    const armarSelectTipos = () => {
      if (this.state.tipos) {
        return this.state.tipos.map((tipo) => (
          <MenuItem
            style={{ padding: "5px 15px 5px 15px" }}
            value={tipo.nombre}
            data-id={tipo.id}
          >
            {tipo.nombre_mostrado}
          </MenuItem>
        ));
      } else return "";
    };

    const armarSelectDiags = () => {
      if (this.state.diagnosticos) {
        return this.state.diagnosticos.map((diagnostico) => (
          <MenuItem
            style={{ padding: "5px 15px 5px 15px" }}
            value={diagnostico.id}
            data-id={diagnostico.id}
          >
            {diagnostico.nombre}
          </MenuItem>
        ));
      } else return "";
    };

    /* Para estilos del material ui */
    const { classes } = this.props;

    const textoInfoGrafico = () => {
      if (this.state.datosEstadisticas.agrupado_por === "ano") return "año";
      else if (this.state.datosEstadisticas.agrupado_por === "dia")
        return "día";
      else return this.state.datosEstadisticas.agrupado_por;
    };

    return (
      <div>
        {/* Formulario */}
        <Grid container spacing={3}>
          <form id="form_estadisticas">
            <Box p={1} flexGrow={1}>
              <Grid item xs={12}>
                <Paper
                  style={{
                    padding: "5px",
                    textAlign: "center",
                    background:
                      "linear-gradient(120deg, #2f78e9, #128bfc, #18bef1)",
                    marginTop: 15,
                  }}
                >
                  <b style={{ color: "white", fontSize: "20px" }}>
                    Estadísticas de Estudios
                  </b>

                  <br></br>
                  <Typography style={{ color: "white" }} variant="caption">
                    Completá los campos que quieras filtrar y hacé click en
                    "Generar gráfico"
                  </Typography>
                </Paper>
              </Grid>
            </Box>
            <Grid item xs={12} style={{ padding: 12 }}>
              <Card variant="outlined">
                <Box>
                  <Grid
                    item
                    xs={12}
                    style={{
                      background: "#2a2a2a",
                      padding: "10px 30px",
                    }}
                  >
                    <Typography
                      style={{
                        color: "white",
                      }}
                    >
                      Filtros
                    </Typography>
                  </Grid>
                </Box>
                <Collapse in={this.state.mostrarAlert}>
                  <Alert
                    style={{ margin: 16 }}
                    variant="filled"
                    severity="error"
                  >
                    Por favor completá los campos marcados en rojo
                  </Alert>
                </Collapse>
                <Box
                  display="flex"
                  flexDirection="row"
                  flexWrap="wrap"
                  p={2}
                  mx={1}
                  bgcolor="background.paper"
                >
                  <Grid item xs={12}>
                    <Box p={1}>
                      <Typography
                        style={{
                          fontSize: 15,
                        }}
                        color="textPrimary"
                        variant="h6"
                        gutterBomb
                      >
                        Datos de los pacientes
                      </Typography>
                    </Box>
                  </Grid>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl className={classes.formControl}>
                        <InputLabel id="gender_label">Sexo</InputLabel>
                        <Select
                          MenuProps={{ disableScrollLock: true }}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-venus-mars"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          labelId="gender_label"
                          id="sexo"
                          name="sexo"
                          value={this.state.datosEstadisticas.sexo}
                          onChange={this.cambiarDato}
                          displayEmpty
                          className={classes.selectEmpty}
                        >
                          <MenuItem value={"Sin especificar"}>
                            Sin especificar
                          </MenuItem>
                          <MenuItem value={"Masculino"}>Masculino</MenuItem>
                          <MenuItem value={"Femenino"}>Femenino</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl className={classes.formControl}>
                        <Autocomplete
                          id="nacido_en"
                          name="nacido_en"
                          options={this.state.anos}
                          //getOptionLabel={(option) => option}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          onChange={this.cambiarAnoNacido}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              style={{ marginTop: 4 }}
                              InputProps={{
                                ...params.InputProps,
                                startAdornment: (
                                  <>
                                    <InputAdornment position="start">
                                      <i
                                        style={{ fontSize: 20 }}
                                        className="far fa-calendar-alt"
                                      ></i>
                                    </InputAdornment>
                                    {params.InputProps.startAdornment}
                                  </>
                                ),
                              }}
                              label="Nacidos en el año"
                            />
                          )}
                        />
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl className={classes.formControl}>
                        <InputLabel shrink id="localidad_label">
                          Localidad
                        </InputLabel>
                        <Select
                          MenuProps={{ disableScrollLock: true }}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          labelId="localidad_label"
                          id="localidad"
                          name="localidad"
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-map-marker-alt"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          value={this.state.datosEstadisticas.localidad}
                          onChange={this.cambiarDato}
                          displayEmpty
                          className={classes.selectEmpty}
                        >
                          <MenuItem value={"Sin especificar"}>
                            Sin especificar
                          </MenuItem>
                          <MenuItem value={"La Plata"}>La Plata</MenuItem>
                          <MenuItem value={"Otro"}>Otro</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl className={classes.formControl}>
                        <InputLabel shrink id="estado_label">
                          Estado del paciente
                        </InputLabel>
                        <Select
                          MenuProps={{ disableScrollLock: true }}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          labelId="estado_label"
                          id="estado"
                          name="estado"
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-hospital-user"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          value={this.state.datosEstadisticas.estado}
                          onChange={this.cambiarDato}
                          displayEmpty
                          className={classes.selectEmpty}
                        >
                          <MenuItem value={"Sin especificar"}>
                            Sin especificar
                          </MenuItem>
                          <MenuItem value={"Ambulatorio"}>Ambulatorio</MenuItem>
                          <MenuItem value={"Internado"}>Internado</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Grid item xs={12}>
                    <Box style={{ margin: "0 8px 16px" }}>
                      <Divider></Divider>
                    </Box>
                  </Grid>
                  <Grid item xs={12}>
                    <Box p={1}>
                      <Typography
                        style={{
                          fontSize: 15,
                        }}
                        color="textPrimary"
                        variant="h6"
                        gutterBottom
                      >
                        Datos de los estudios
                      </Typography>
                    </Box>
                  </Grid>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl className={classes.formControl}>
                        <InputLabel shrink id="tipo_label">
                          Tipo de estudio
                        </InputLabel>
                        <Select
                          MenuProps={{ disableScrollLock: true }}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          labelId="tipo_label"
                          id="tipo"
                          name="tipo"
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-book-medical"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          value={this.state.datosEstadisticas.tipo}
                          onChange={this.cambiarDato}
                          displayEmpty
                          className={classes.selectEmpty}
                          children={armarSelectTipos(this.state.tipos)}
                        ></Select>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Grid item xs={12}>
                    <Typography
                      style={{
                        fontSize: 14,
                        paddingLeft: 7,
                        color: "textSecondary",
                      }}
                      gutterBottom
                    >
                      Diagnósticos
                    </Typography>
                  </Grid>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl className={classes.formControl}>
                        <Autocomplete
                          id="diag_obligatorio"
                          name="diag_obligatorio"
                          options={this.state.diagnosticos}
                          getOptionLabel={(option) => option.nombre}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          onChange={this.cambiarDiagObligatorio}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              style={{ marginTop: 4 }}
                              InputProps={{
                                ...params.InputProps,
                                startAdornment: (
                                  <>
                                    <InputAdornment position="start">
                                      <i
                                        className="fas fa-notes-medical"
                                        style={{ fontSize: "23px" }}
                                      ></i>
                                    </InputAdornment>
                                    {params.InputProps.startAdornment}
                                  </>
                                ),
                              }}
                              label="Diagnóstico obligatorio"
                            />
                          )}
                        />
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl className={classes.formControl}>
                        <Autocomplete
                          id="diag_opcional_1"
                          name="diag_opcional_1"
                          options={this.state.diagnosticos}
                          getOptionLabel={(option) => option.nombre}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          onChange={this.cambiarDiagOpcional1}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              style={{ marginTop: 4 }}
                              InputProps={{
                                ...params.InputProps,
                                startAdornment: (
                                  <>
                                    <InputAdornment position="start">
                                      <i
                                        className="fas fa-notes-medical"
                                        style={{ fontSize: "23px" }}
                                      ></i>
                                    </InputAdornment>
                                    {params.InputProps.startAdornment}
                                  </>
                                ),
                              }}
                              label="Diagnóstico opcional 1"
                            />
                          )}
                        />
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl className={classes.formControl}>
                        <Autocomplete
                          id="diag_opcional_2"
                          name="diag_opcional_2"
                          options={this.state.diagnosticos}
                          getOptionLabel={(option) => option.nombre}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          onChange={this.cambiarDiagOpcional2}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              style={{ marginTop: 4 }}
                              InputProps={{
                                ...params.InputProps,
                                startAdornment: (
                                  <>
                                    <InputAdornment position="start">
                                      <i
                                        className="fas fa-notes-medical"
                                        style={{ fontSize: "23px" }}
                                      ></i>
                                    </InputAdornment>
                                    {params.InputProps.startAdornment}
                                  </>
                                ),
                              }}
                              label="Diagnóstico opcional 2"
                            />
                          )}
                        />
                      </FormControl>
                    </Grid>
                  </Box>
                  <Grid item xs={12}>
                    <Typography
                      style={{
                        fontSize: 14,
                        paddingLeft: 7,
                        color: "textSecondary",
                      }}
                    >
                      Rango de fechas
                    </Typography>
                  </Grid>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl>
                        <MuiPickersUtilsProvider
                          locale={es}
                          utils={DateFnsUtils}
                        >
                          <KeyboardDatePicker
                            autoOk
                            cancelLabel="Cancelar"
                            okLabel={""}
                            DialogProps={{ disableScrollLock: true }}
                            margin="normal"
                            style={{
                              minWidth: 300,
                              maxWidth: 300,
                              marginTop: 1,
                            }}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  <i
                                    style={{ fontSize: 20 }}
                                    className="fas fa-calendar-day"
                                  ></i>
                                </InputAdornment>
                              ),
                            }}
                            InputLabelProps={{ shrink: true }}
                            placeholder="Ej: 01/09/2019"
                            invalidDateMessage="La fecha no es válida. Recordá: día/mes/año"
                            maxDateMessage="La fecha no puede ser mayor al máximo permitido"
                            format="dd/MM/yyyy"
                            id="fecha_inicio"
                            name="fecha_inicio"
                            label="Desde"
                            value={this.state.datosEstadisticas.fechaInicio}
                            onChange={this.cambiarFechaInicio}
                            KeyboardButtonProps={{
                              "aria-label": "change date",
                            }}
                            error={this.state.errorFechaInicio}
                          />
                        </MuiPickersUtilsProvider>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={4}>
                      <FormControl>
                        <MuiPickersUtilsProvider
                          locale={es}
                          utils={DateFnsUtils}
                        >
                          <KeyboardDatePicker
                            autoOk
                            cancelLabel="Cancelar"
                            okLabel={""}
                            DialogProps={{ disableScrollLock: true }}
                            style={{
                              minWidth: 300,
                              maxWidth: 300,
                              marginTop: 1,
                            }}
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  <i
                                    style={{ fontSize: 20 }}
                                    className="fas fa-calendar-day"
                                  ></i>
                                </InputAdornment>
                              ),
                            }}
                            InputLabelProps={{ shrink: true }}
                            placeholder="Ej: 03/09/2020"
                            invalidDateMessage="La fecha no es válida. Recordá: día/mes/año"
                            maxDateMessage="La fecha no puede ser mayor al máximo permitido"
                            format="dd/MM/yyyy"
                            margin="normal"
                            id="fecha_fin"
                            name="fecha_fin"
                            label="Hasta"
                            value={this.state.datosEstadisticas.fechaFin}
                            onChange={this.cambiarFechaFin}
                            KeyboardButtonProps={{
                              "aria-label": "change date",
                            }}
                            error={this.state.errorFechaFin}
                          />
                        </MuiPickersUtilsProvider>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Grid item xs={12}>
                    <Box p={1}>
                      <Button
                        variant="contained"
                        size="medium"
                        style={{ backgroundColor: "#297ced", color: "white" }}
                        onClick={this.submitEstadistica}
                      >
                        Generar gráfico
                      </Button>
                    </Box>
                  </Grid>
                </Box>
              </Card>
            </Grid>
          </form>
        </Grid>

        {/* Gráfico */}
        <Grid item xs={12}>
          <Card style={{ marginTop: 15 }} variant="outlined">
            <Box>
              <Grid
                item
                xs={12}
                style={{
                  background: "#2a2a2a",
                  padding: "10px 30px",
                }}
              >
                <Typography
                  style={{
                    color: "white",
                  }}
                >
                  Gráfico
                </Typography>
              </Grid>
            </Box>
            <Box
              display="flex"
              flexDirection="row"
              flexWrap="wrap"
              p={2}
              mx={1}
              bgcolor="background.paper"
            >
              <Grid item xs={12}>
                <Box display="flex" justifyContent="space-between">
                  <Box p={1}>
                    <Typography
                      style={{
                        fontSize: 15,
                      }}
                      color="textPrimary"
                      gutterBottom
                    >
                      Cantidad total de estudios:{" "}
                      <span
                        style={{
                          color: "#297ced",
                          fontSize: 18,
                          fontWeight: 900,
                        }}
                      >
                        {this.state.datosEstadisticas.totalEstudios}
                      </span>
                    </Typography>
                  </Box>
                  <Box px={3}>
                    {mostrarGrafico && (
                      <Alert severity="info" style={{ maxWidth: "600px" }}>
                        Podés pasar el cursor por uno de los puntos para ver{" "}
                        <b>con más claridad</b> la cantidad de estudios
                        registrados ese determinado {textoInfoGrafico()}.
                      </Alert>
                    )}
                  </Box>
                </Box>
              </Grid>
              <Box p={1}>
                <Grid item xs={4}>
                  <FormControl className={classes.formControl}>
                    <InputLabel shrink id="agrupado_label">
                      Agrupado por
                    </InputLabel>
                    <Select
                      MenuProps={{ disableScrollLock: true }}
                      style={{ minWidth: 300, maxWidth: 300 }}
                      startAdornment={
                        <InputAdornment position="start">
                          <i
                            className="fas fa-calendar-week"
                            style={{ fontSize: "25px" }}
                          ></i>
                        </InputAdornment>
                      }
                      labelId="agrupado_label"
                      id="agrupado_por"
                      name="agrupado_por"
                      disabled={this.state.agrupadoDisabled}
                      value={this.state.datosEstadisticas.agrupado_por}
                      onChange={this.cambiarAgrupadoPor}
                      displayEmpty
                      className={classes.selectEmpty}
                    >
                      <MenuItem value={"dia"}>Día</MenuItem>
                      <MenuItem value={"mes"}>Mes</MenuItem>
                      <MenuItem value={"ano"}>Año</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
              </Box>
              <Grid item xs={12}>
                {mostrarGrafico ? (
                  <Line data={this.data} options={this.options} />
                ) : (
                  <div id="mensaje-opciones-predefinidas">
                    <span>{this.state.mensajeGrafico}</span>
                  </div>
                )}
              </Grid>
            </Box>
          </Card>
        </Grid>
      </div>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(ChartJs);
