import React, { useEffect } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";

export default function EstudioDisincronia(props) {
  const [retraso_septal, setRetrasoSeptal] = React.useState("");
  const [DPT, setDPT] = React.useState("");
  const [STR, setSTR] = React.useState("");
  const [llenado_distolico, setLlenadoDistolico] = React.useState("");
  const [diferencia_periodos, setDiferenciaPeriodos] = React.useState("");
  const [conclusion, setConclusion] = React.useState("");

  const actualizarInput = (event) => {
    if (event.target.value.includes(".")) {
      const value = event.target.value;
      const arr = value.split(".");
      if (arr.length <= 2 && arr[1].length <= 2) {
        if (event.target.name === "retraso_septal") {
          setRetrasoSeptal(event.target.value);
        } else {
          if (event.target.name === "dpt") {
            setDPT(event.target.value);
          } else {
            if (event.target.name === "str") {
              setSTR(event.target.value);
            } else {
              if (event.target.name === "llenado_distolico") {
                setLlenadoDistolico(event.target.value);
              } else {
                if (event.target.name === "diferencia_periodos") {
                  setDiferenciaPeriodos(event.target.value);
                } else {
                  setConclusion(event.target.value);
                }
              }
            }
          }
        }
      }
    } else {
      if (event.target.name === "retraso_septal") {
        setRetrasoSeptal(event.target.value);
      } else {
        if (event.target.name === "dpt") {
          setDPT(event.target.value);
        } else {
          if (event.target.name === "str") {
            setSTR(event.target.value);
          } else {
            if (event.target.name === "llenado_distolico") {
              setLlenadoDistolico(event.target.value);
            } else {
              if (event.target.name === "diferencia_periodos") {
                setDiferenciaPeriodos(event.target.value);
              } else {
                setConclusion(event.target.value);
              }
            }
          }
        }
      }
    }
  };

  useEffect(() => {
    if (props.data != "creando") {
      setRetrasoSeptal(props.data.retraso_septal);
      setDPT(props.data.dpt);
      setSTR(props.data.str);
      setLlenadoDistolico(props.data.llenado_distolico);
      setDiferenciaPeriodos(props.data.diferencia_periodos);
      setConclusion(props.data.conclusion);
    }
  }, []);

  return (
    <div>
      <form id="form_datos_especificos">
        <Grid
          style={{ paddingTop: 25, paddingLeft: 25, paddingBottom: 25 }}
          container
          spacing={3}
        >
          <Grid item xs={3}>
            <TextField
              id="retraso_septal"
              name="retraso_septal"
              size="small"
              variant="outlined"
              type="number"
              label="Retraso Septal"
              onChange={(e) => actualizarInput(e)}
              value={retraso_septal}
            ></TextField>
          </Grid>
          <Grid item xs={3}>
            <TextField
              name="dpt"
              id="dpt"
              size="small"
              variant="outlined"
              type="number"
              label="DPT"
              onChange={(e) => actualizarInput(e)}
              value={DPT}
            ></TextField>
          </Grid>
          <Grid item xs={3}>
            <TextField
              name="str"
              id="str"
              type="number"
              size="small"
              variant="outlined"
              label="STR"
              onChange={(e) => actualizarInput(e)}
              value={STR}
            ></TextField>
          </Grid>
          <Grid item xs={3}>
            <TextField
              name="llenado_distolico"
              id="llenado_distolico"
              size="small"
              variant="outlined"
              type="number"
              label="Llenado Distólico"
              onChange={(e) => actualizarInput(e)}
              value={llenado_distolico}
            ></TextField>
          </Grid>
          <Grid item xs={3}>
            <TextField
              name="diferencia_periodos"
              id="diferencia_periodos"
              size="small"
              variant="outlined"
              type="number"
              label="Diferencia de Períodos"
              onChange={(e) => actualizarInput(e)}
              value={diferencia_periodos}
            ></TextField>
          </Grid>
          <Grid style={{ paddingRight: 37 }} item xs={12}>
            <FormControl fullWidth>
              <TextField
                error={props.errorConclusion}
                variant="outlined"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <i
                        style={{ fontSize: 25 }}
                        class="fas fa-file-signature"
                      ></i>
                    </InputAdornment>
                  ),
                }}
                required
                multiline
                rowsMax={Infinity}
                label="Conclusión"
                id="conclusion"
                name="conclusion"
                onChange={(e) => actualizarInput(e)}
                value={conclusion}
              ></TextField>
            </FormControl>
          </Grid>
        </Grid>
      </form>
    </div>
  );
}
