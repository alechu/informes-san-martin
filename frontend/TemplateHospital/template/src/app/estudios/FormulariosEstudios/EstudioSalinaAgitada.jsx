import React, { useEffect } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";

export default function EstudioSalinaAgitada(props) {
  const [conclusionesP, setConclusionesP] = React.useState([]);
  const [escenarioActivo, setEscenarioActivo] = React.useState("");
  const [conclusionActiva, setConclusionActiva] = React.useState("");

  const armarSelect = (arrayConclusiones) => {
    if (arrayConclusiones) {
      return arrayConclusiones.map((conclusion) => (
        <MenuItem
          style={{
            maxWidth: 700,
            wordWrap: "break-word",
            whiteSpace: "normal",
          }}
          value={conclusion.id}
          data-id={conclusion.id}
        >
          {conclusion.escenario}
        </MenuItem>
      ));
    } else return "";
  };

  const cambiarConclusion = (e) => {
    const conclusionAct = conclusionesP.filter(
      (conclusion) => conclusion.id == e.target.value
    )[0];
    props.cambiarConclusionPredefinida(conclusionAct.id);
    setConclusionActiva(conclusionAct.conclusion);
    setEscenarioActivo(conclusionAct.id);
  };

  useEffect(() => {
    fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/conclusion?all=True",
      {
        headers: new Headers({
          Authorization: "Bearer " + document.cookie.substring(6),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        }
        setConclusionesP(data.body);
      });
  }, []);

  useEffect(() => {
    if (props.data != "creando") {
      if (conclusionesP != []) {
        props.cambiarConclusionPredefinida(props.data.id);
        setConclusionActiva(props.data.conclusion);
        setEscenarioActivo(props.data.id);
      }
    }
  }, [conclusionesP]);
  return (
    <div>
      <form id="form_datos_especificos">
        <Box
          display="flex"
          flexDirection="column"
          flexWrap="wrap"
          p={1}
          m={1}
          bgcolor="background.paper"
        >
          <Box flexGrow={1} p={2}>
            <Grid item xs={12}>
              <FormControl error={props.errorEscenario} fullWidth>
                <InputLabel id="escenario">Escenario *</InputLabel>
                <Select
                  style={{ maxWidth: "950px" }}
                  startAdornment={
                    <InputAdornment position="start">
                      <i
                        className="fas fa-notes-medical"
                        style={{ fontSize: "25px" }}
                      ></i>
                    </InputAdornment>
                  }
                  MenuProps={{ disableScrollLock: true }}
                  id="escenario_select"
                  name="id_conclusion_predefinida"
                  labelId="escenario"
                  value={escenarioActivo}
                  onChange={(e) => cambiarConclusion(e)}
                  children={armarSelect(conclusionesP)}
                ></Select>
              </FormControl>
            </Grid>
          </Box>
          <Box flexGrow={1} p={1}>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <TextField
                  error={props.errorEscenario}
                  variant="outlined"
                  InputProps={{
                    readOnly: true,
                    startAdornment: (
                      <InputAdornment position="start">
                        <i
                          style={{ fontSize: 20 }}
                          class="fas fa-file-signature"
                        ></i>
                      </InputAdornment>
                    ),
                  }}
                  value={conclusionActiva}
                  label="Conclusión"
                  id="conclusion"
                  name="conclusion"
                ></TextField>
              </FormControl>
            </Grid>
          </Box>
        </Box>
      </form>
    </div>
  );
}
