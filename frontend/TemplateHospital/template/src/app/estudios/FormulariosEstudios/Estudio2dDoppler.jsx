import React from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import Typography from "@material-ui/core/Typography";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";

export default class Estudio2dDoppler extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      datosEstudio: {
        vb_DDVI: "",
        vb_DSVI: "",
        vb_SIV: "",
        vb_PP: "",
        vb_FEY: "",
        vb_Al_Area: "",
        vb_Al_Vol_SC: "",
        vb_Al_Vol_SC: "",
        vb_Aorta: "",
        vb_Ap_Vao: "",
        vb_TSVI: "",
        vd_Velocidad_Maxima: "",
        vd_Gradiente_Maximo_Ao: "",
        vd_Gradiente_Medio_Ao: "",
        vd_Insuficiencia_Aortica: "",
        vd_THP: "",
        vd_Flujo_Reverso_ADT: "",
        vd_Flujo_Reverso_AA: "",
        vd_Velocidad_Onda_E: "",
        vd_Velocidad_Onda_A: "",
        vd_Gradiente_Medio_Trasmitral: "",
        vd_Insuficiencia_Mitral: "",
        vd_ORE: "",
        vd_Volumen_Regurgitante: "",
        vd_Area_VM_THP: "",
        vd_Dp_dt: "",
        vd_Velocidad_Maxima_A_Pulmonar: "",
        vd_Gradiente_Maximo: "",
        vd_Tiempo_Al_Pico: "",
        vd_Insuficiencia_Pulmonar: "",
        vd_Gradiente_Protosistolico: "",
        vd_Gradiente_Telesistolico: "",
        vd_QP_QS: "",
        vd_Insuficiencia_Tricuspidea: "",
        vd_velocidad_Regurgitante_TT: "",
        vd_Gradiente_Pico_TT: "",
        vd_PAP: "",
        vd_PmAP: "",
        vd_Pad: "",
        dp_Onda_S_Septal: "",
        dp_Onda_S_Lateral: "",
        dp_Onda_e: "",
        dp_Relacion_E_e: "",
        dp_Onda_S_Vd: "",
        descripcion:
          "Se observaron cavidades de dimensiones y formas normales. Área de aurícula izquierda:" +
          "14.5cm². \n" +
          "Las paredes ventriculares tienen espesor y masa normales. \n" +
          "En condiciones de reposo, se observa motilidad parietal conservada. \n" +
          "Los parámetros sistólicos de función ventricular izquierda son normales. Fracción de" +
          " eyección estimada: 68%. Patrón diastólico normal. \n" +
          "La función sistólica del ventriculo derecho es normal. \n" +
          "Válvula aórtica tricúspide, con apertura conservada, sin estenosis ni insuficiencia. \n" +
          "Las demás válvulas no muestran alteraciones estructurales o funciones significativas. \n" +
          "No se observaron otros flujos intracavitarios o transvalvulares anormales. \n" +
          "Pericardio libre.",
        conclusion:
          "El estudio muestra un corazón sin anormalidades significativas para la edad del paciente. " +
          "En reposo no se observaron signos de isquemia.",
      },
      loadingDatos: true,
      descripcionesP: [],
    };

    this.actualizarInput = this.actualizarInput.bind(this);
    this.asignarDatos = this.asignarDatos.bind(this);
    this.fetchDescripciones = this.fetchDescripciones.bind(this);
    this.cambiarDescripcion = this.cambiarDescripcion.bind(this);
  }

  componentDidMount() {
    if (this.props.data != "creando") {
      this.asignarDatos(this.props.data);
    }
    this.fetchDescripciones();
  }

  fetchDescripciones() {
    fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/descripcion?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          if (data.status === 200) {
            this.setState({ descripcionesP: data.body });
          }
        }
      });
  }

  armarSelect = (arrayDescripciones) => {
    if (arrayDescripciones) {
      return arrayDescripciones.map((descripcion) => (
        <MenuItem
          style={{
            maxWidth: 700,
            wordWrap: "break-word",
            whiteSpace: "normal",
          }}
          value={descripcion.id}
          data-id={descripcion.id}
        >
          {descripcion.nombre}
        </MenuItem>
      ));
    } else return "";
  };

  cambiarDescripcion(e) {
    const descripcionAct = this.state.descripcionesP.filter(
      (descripcion) => descripcion.id == e.target.value
    )[0];
    this.props.cambiarDescripcionPredefinida(descripcionAct.id);
    this.setState((prevState) => ({
      datosEstudio: {
        ...prevState.datosEstudio,
        descripcion: descripcionAct.id,
      },
    }));
  }

  asignarDatos(data) {
    var datosEstudio = {};
    for (var key of Object.keys(data)) {
      if (key === "descripcion_predefinida") {
        datosEstudio["descripcion"] = data[key].id;
        this.props.cambiarDescripcionPredefinida(data[key].id);
      }
      datosEstudio[key] = data[key];
    }
    this.setState({ datosEstudio: datosEstudio }, () => {
      this.props.funcListo();
    });
  }

  actualizarInput(event) {
    event.persist();
    if (
      event.target.value.includes(".") &&
      event.target.name !== "descripcion" &&
      event.target.name !== "conclusion"
    ) {
      const value = event.target.value;
      const arr = value.split(".");
      if (arr.length <= 2 && arr[1].length <= 2) {
        this.setState((prevState) => ({
          datosEstudio: {
            ...prevState.datosEstudio,
            [event.target.name]: event.target.value,
          },
        }));
      }
    } else {
      this.setState((prevState) => ({
        datosEstudio: {
          ...prevState.datosEstudio,
          [event.target.name]: event.target.value,
        },
      }));
    }
  }

  render() {
    return (
      <div>
        <form id="form_datos_especificos">
          <Grid
            style={{
              paddingTop: 25,
              paddingLeft: 25,
              paddingRight: 25,
              paddingBottom: 25,
            }}
            container
            spacing={3}
          >
            {/* Valores Bidimensional */}
            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                }}
                color="primary"
                gutterBottom
              >
                Valores Bidimensional
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_DDVI"
                id="vb_DDVI"
                size="small"
                variant="outlined"
                type="number"
                label="DDVI (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_DDVI}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_DSVI"
                id="vb_DSVI"
                size="small"
                variant="outlined"
                type="number"
                label="DSVI (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_DSVI}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_SIV"
                id="vb_SIV"
                type="number"
                size="small"
                variant="outlined"
                label="SIV (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_SIV}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_PP"
                id="vb_PP"
                type="number"
                size="small"
                variant="outlined"
                label="PP (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_PP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_FEY"
                id="vb_FEY"
                size="small"
                variant="outlined"
                type="number"
                label="FEY %"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_FEY}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_Al_Area"
                id="vb_Al_Area"
                size="small"
                variant="outlined"
                type="number"
                label="AI área (cm2)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_Al_Area}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_Al_Vol_SC"
                id="vb_Al_Vol_SC"
                size="small"
                variant="outlined"
                type="number"
                label="AI Vol/SC"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_Al_Vol_SC}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_Aorta"
                id="vb_Aorta"
                size="small"
                variant="outlined"
                type="number"
                label="Aorta (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_Aorta}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_Ap_Vao"
                id="vb_Ap_Vao"
                size="small"
                variant="outlined"
                type="number"
                label="Ap VAo (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_Ap_Vao}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_TSVI"
                id="vb_TSVI"
                size="small"
                variant="outlined"
                type="number"
                label="TSVI (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_TSVI}
              ></TextField>
            </Grid>

            {/* Valores Doppler */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                }}
                color="primary"
                gutterBottom
              >
                Valores Doppler
              </Typography>
            </Grid>

            {/* Valvula Aortica */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                  paddingLeft: 1,
                  color: "black",
                }}
                color="textSecondary"
                gutterBottom
              >
                Válvula Aórtica
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Velocidad_Maxima"
                id="vd_Velocidad_Maxima"
                size="small"
                variant="outlined"
                type="number"
                label="V.máxima Ao (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Velocidad_Maxima}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Maximo_Ao"
                id="vd_Gradiente_Maximo_Ao"
                size="small"
                variant="outlined"
                type="number"
                label="G.máximo Ao (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Maximo_Ao}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Medio_Ao"
                id="vd_Gradiente_Medio_Ao"
                type="number"
                size="small"
                variant="outlined"
                label="G.medio Ao (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Medio_Ao}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Insuficiencia_Aortica"
                id="vd_Insuficiencia_Aortica"
                type="number"
                size="small"
                variant="outlined"
                label="Insuficiencia"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Insuficiencia_Aortica}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_THP"
                id="vd_THP"
                size="small"
                variant="outlined"
                type="number"
                label="THP (mseg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_THP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Flujo_Reverso_ADT"
                id="vd_Flujo_Reverso_ADT"
                size="small"
                variant="outlined"
                type="number"
                label="Flujo reverso ADT"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Flujo_Reverso_ADT}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Flujo_Reverso_AA"
                id="vd_Flujo_Reverso_AA"
                size="small"
                variant="outlined"
                type="number"
                label="Flujo reverso AA"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Flujo_Reverso_AA}
              ></TextField>
            </Grid>

            {/* Valvula Mitral */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                  paddingLeft: 1,
                  color: "black",
                }}
                color="textSecondary"
                gutterBottom
              >
                Válvula Mitral
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Velocidad_Onda_E"
                id="vd_Velocidad_Onda_E"
                size="small"
                variant="outlined"
                type="number"
                label="V.onda E (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Velocidad_Onda_E}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Velocidad_Onda_A"
                id="vd_Velocidad_Onda_A"
                size="small"
                variant="outlined"
                type="number"
                label="V.onda A (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Velocidad_Onda_A}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Medio_Trasmitral"
                id="vd_Gradiente_Medio_Trasmitral"
                type="number"
                size="small"
                variant="outlined"
                label="G.medio trasmitral (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Medio_Trasmitral}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Insuficiencia_Mitral"
                id="vd_Insuficiencia_Mitral"
                type="number"
                size="small"
                variant="outlined"
                label="Insuficiencia"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Insuficiencia_Mitral}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_ORE"
                id="vd_ORE"
                size="small"
                variant="outlined"
                type="number"
                label="ORE"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_ORE}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Volumen_Regurgitante"
                id="vd_Volumen_Regurgitante"
                size="small"
                variant="outlined"
                type="number"
                label="Volumen regurgitante (ml)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Volumen_Regurgitante}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Area_VM_THP"
                id="vd_Area_VM_THP"
                size="small"
                variant="outlined"
                type="number"
                label="Area VM THP (cm2)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Area_VM_THP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Dp_dt"
                id="vd_Dp_dt"
                size="small"
                variant="outlined"
                type="number"
                label="Dp/dt"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Dp_dt}
              ></TextField>
            </Grid>

            {/* Valvula  Pulmonar */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                  paddingLeft: 1,
                  color: "black",
                }}
                gutterBottom
              >
                Válvula Pulmonar
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Velocidad_Maxima_A_Pulmonar"
                id="vd_Velocidad_Maxima_A_Pulmonar"
                size="small"
                variant="outlined"
                type="number"
                label="V.máxima A.pulmonar (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Velocidad_Maxima_A_Pulmonar}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Maximo"
                id="vd_Gradiente_Maximo"
                size="small"
                variant="outlined"
                type="number"
                label="G.máximo (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Maximo}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Tiempo_Al_Pico"
                id="vd_Tiempo_Al_Pico"
                type="number"
                size="small"
                variant="outlined"
                label="Tiempo al pico (mseg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Tiempo_Al_Pico}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Insuficiencia_Pulmonar"
                id="vd_Insuficiencia_Pulmonar"
                type="number"
                size="small"
                variant="outlined"
                label="Insuficiencia"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Insuficiencia_Pulmonar}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Protosistolico"
                id="vd_Gradiente_Protosistolico"
                size="small"
                variant="outlined"
                type="number"
                label="G.protosistólico (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Protosistolico}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Telesistolico"
                id="vd_Gradiente_Telesistolico"
                size="small"
                variant="outlined"
                type="number"
                label="G.telesistólico (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Telesistolico}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_QP_QS"
                id="vd_QP_QS"
                size="small"
                variant="outlined"
                type="number"
                label="Qp/qs"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_QP_QS}
              ></TextField>
            </Grid>

            {/* Valvula  Tricúspide */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                  paddingLeft: 1,
                  color: "black",
                }}
                gutterBottom
              >
                Válvula Tricúspide
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Insuficiencia_Tricuspidea"
                id="vd_Insuficiencia_Tricuspidea"
                size="small"
                variant="outlined"
                type="number"
                label="Insuficiencia Tricúspidea"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Insuficiencia_Tricuspidea}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_velocidad_Regurgitante_TT"
                id="vd_velocidad_Regurgitante_TT"
                size="small"
                variant="outlined"
                type="number"
                label="V.Regurgitante TT (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_velocidad_Regurgitante_TT}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Pico_TT"
                id="vd_Gradiente_Pico_TT"
                type="number"
                size="small"
                variant="outlined"
                label="G.pico TT (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Pico_TT}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_PAP"
                id="vd_PAP"
                type="number"
                size="small"
                variant="outlined"
                label="PAP (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_PAP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_PmAP"
                id="vd_PmAP"
                size="small"
                variant="outlined"
                type="number"
                label="PmAP (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_PmAP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Pad"
                id="vd_Pad"
                size="small"
                variant="outlined"
                type="number"
                label="Pad (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Pad}
              ></TextField>
            </Grid>

            {/* Valores Doppler tisular */}
            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                }}
                color="primary"
                gutterBottom
              >
                Valores Doppler Tisular
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="dp_Onda_S_Septal"
                id="dp_Onda_S_Septal"
                size="small"
                variant="outlined"
                type="number"
                label="Onda S septal (cm/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.dp_Onda_S_Septal}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="dp_Onda_S_Lateral"
                id="dp_Onda_S_Lateral"
                size="small"
                variant="outlined"
                type="number"
                label="Onda S lateral (cm/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.dp_Onda_S_Lateral}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="dp_Onda_e"
                id="dp_Onda_e"
                type="number"
                size="small"
                variant="outlined"
                label="Onda e´ (cm/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.dp_Onda_e}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="dp_Relacion_E_e"
                id="dp_Relacion_E_e"
                type="number"
                size="small"
                variant="outlined"
                label="Relación E/e´"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.dp_Relacion_E_e}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="dp_Onda_S_Vd"
                id="dp_Onda_S_Vd"
                size="small"
                variant="outlined"
                type="number"
                label="Onda S Vd (cm/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.dp_Onda_S_Vd}
              ></TextField>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <TextField
                  error={this.props.errorDescripcion}
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <i
                          style={{ fontSize: 25 }}
                          className="fas fa-file-signature"
                        ></i>
                      </InputAdornment>
                    ),
                  }}
                  required
                  multiline
                  rowsMax={Infinity}
                  label="Descripción"
                  id="descripcion"
                  name="descripcion"
                  onChange={this.actualizarInput}
                  value={this.state.datosEstudio.descripcion}
                ></TextField>
              </FormControl>
            </Grid>
            <Grid style={{ paddingRight: 10 }} item xs={12}>
              <FormControl fullWidth>
                <TextField
                  error={this.props.errorConclusion}
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <i
                          style={{ fontSize: 25 }}
                          className="fas fa-file-signature"
                        ></i>
                      </InputAdornment>
                    ),
                  }}
                  required
                  multiline
                  rowsMax={Infinity}
                  label="Conclusión"
                  id="conclusion"
                  name="conclusion"
                  onChange={this.actualizarInput}
                  value={this.state.datosEstudio.conclusion}
                ></TextField>
              </FormControl>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
}
