import React, { useEffect } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";

export default function EstudioSalinaAgitada(props) {
  const [descripcion, setDescripcion] = React.useState("");
  const [conclusion, setConclusion] = React.useState("");

  const cambiarConclusion = (e) => {
    setConclusion(e.target.value);
  };

  const cambiarDescripcion = (e) => {
    setDescripcion(e.target.value);
  };

  useEffect(() => {
    if (props.data != "creando") {
      setDescripcion(props.data.descripcion);
      setConclusion(props.data.conclusion);
    } else {
      setDescripcion(
        "Cavidades de dimensiones normales.\n" +
          "Función sistólica de VI  conservada.\n" +
          "Raíz de aorta de dimensiones normales, con relación sinotubular conservada.\n" +
          "Válvula Aórtica trivalva con apertura conservada, sin estenosis, ni insuficiencia.\n" +
          "Válvula mitral con valvas finas, competente.\n" +
          "Válvulas derechas sin evidencia de patología orgánica.\n" +
          "Ambas orejuelas libres de trombos.\n" +
          "Septum interauricular, sin shunt visible mediante Doppler color.\n" +
          "No se observa patología del  arco aórtico, ni de aorta descendente hasta 40 cm de la arcada dentaria.\n" +
          "No se observaron masas intracavitarias, ni imágenes sugestivas de vegetaciones.\n" +
          "Pericardio libre."
      );
    }
  }, []);

  return (
    <div>
      <form id="form_datos_especificos">
        <Box
          display="flex"
          flexDirection="column"
          flexWrap="wrap"
          p={1}
          m={1}
          bgcolor="background.paper"
        >
          <Box flexGrow={1} p={1}>
            {/*             <Grid item xs={12}>
              <FormControl error={props.errorDescripcion} fullWidth>
                <InputLabel id="descripcion">Descripción *</InputLabel>
                <Select
                  style={{ maxWidth: "950px" }}
                  startAdornment={
                    <InputAdornment position="start">
                      <i
                        className="fas fa-notes-medical"
                        style={{ fontSize: "25px" }}
                      ></i>
                    </InputAdornment>
                  }
                  MenuProps={{ disableScrollLock: true }}
                  id="descripcion_select"
                  name="id_descripcion_predefinida"
                  labelId="descripcion"
                  value={descripcion}
                  onChange={(e) => cambiarDescripcion(e)}
                  children={armarSelect(descripcionesP)}
                ></Select>
              </FormControl>
            </Grid> */}
            <Grid item xs={12}>
              <FormControl fullWidth>
                <TextField
                  error={props.errorDescripcion}
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <i
                          style={{ fontSize: 20 }}
                          className="fas fa-file-signature"
                        ></i>
                      </InputAdornment>
                    ),
                  }}
                  required
                  multiline
                  rowsMax={Infinity}
                  label="Descripción"
                  id="descripcion"
                  name="descripcion"
                  onChange={(e) => cambiarDescripcion(e)}
                  value={descripcion}
                ></TextField>
              </FormControl>
            </Grid>
          </Box>
          <Box flexGrow={1} p={1}>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <TextField
                  error={props.errorConclusion}
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <i
                          style={{ fontSize: 20 }}
                          class="fas fa-file-signature"
                        ></i>
                      </InputAdornment>
                    ),
                  }}
                  onChange={(e) => cambiarConclusion(e)}
                  value={conclusion}
                  label="Conclusión *"
                  id="conclusion"
                  name="conclusion"
                  multiline
                  rowsMax={Infinity}
                ></TextField>
              </FormControl>
            </Grid>
          </Box>
        </Box>
      </form>
    </div>
  );
}
