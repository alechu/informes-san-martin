import React from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";

export default class EstudioCongenitas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      datosEstudio: {
        situs: "",
        A_V_Activo: "",
        V_A_Activo: "",
        vb_DDVI: "",
        vb_DSVI: "",
        vb_SIV: "",
        vb_PP: "",
        vb_FEY: "",
        vb_Al_Area: "",
        vb_Al_Vol_SC: "",
        vb_Al_Vol_SC: "",
        vb_Aorta: "",
        vb_Ap_Vao: "",
        vb_TSVI: "",
        vd_Velocidad_Maxima: "",
        vd_Gradiente_Maximo_Ao: "",
        vd_Gradiente_Medio_Ao: "",
        vd_Insuficiencia_Aortica: "",
        vd_THP: "",
        vd_Flujo_Reverso_ADT: "",
        vd_Flujo_Reverso_AA: "",
        vd_Velocidad_Onda_E: "",
        vd_Velocidad_Onda_A: "",
        vd_Gradiente_Medio_Trasmitral: "",
        vd_Insuficiencia_Mitral: "",
        vd_ORE: "",
        vd_Volumen_Regurgitante: "",
        vd_Dp_dt: "",
        vd_Velocidad_Maxima_A_Pulmonar: "",
        vd_Gradiente_Maximo: "",
        vd_Tiempo_Al_Pico: "",
        vd_Insuficiencia_Pulmonar: "",
        vd_QP_QS: "",
        vd_Insuficiencia_Tricuspidea: "",
        vd_velocidad_Regurgitante_TT: "",
        vd_Gradiente_Pico_TT: "",
        vd_PAP: "",
        vd_PmAP: "",
        vd_Pad: "",
        descripcion: "",
        conclusion: "",
      },
    };
    this.cambiarSitus = this.cambiarSitus.bind(this);
    this.cambiarAVActivo = this.cambiarAVActivo.bind(this);
    this.cambiarVAActivo = this.cambiarVAActivo.bind(this);
    this.actualizarInput = this.actualizarInput.bind(this);
    this.asignarDatos = this.asignarDatos.bind(this);
  }

  componentDidMount() {
    if (!this.props.data.creando) {
      this.asignarDatos(this.props.data.datosEstudio);
    }
  }
  cambiarSitus = (e) => {
    this.props.data.funcSitus(e.target.value);
    e.persist();
    this.setState((prevState) => ({
      datosEstudio: {
        ...prevState.datosEstudio,
        situs: e.target.value,
      },
    }));
  };

  cambiarAVActivo = (e) => {
    this.props.data.funcAV(e.target.value);
    e.persist();
    this.setState((prevState) => ({
      datosEstudio: {
        ...prevState.datosEstudio,
        A_V_Activo: e.target.value,
      },
    }));
  };

  cambiarVAActivo = (e) => {
    this.props.data.funcVA(e.target.value);
    e.persist();
    this.setState((prevState) => ({
      datosEstudio: {
        ...prevState.datosEstudio,
        V_A_Activo: e.target.value,
      },
    }));
  };

  actualizarInput(event) {
    event.persist();
    if (event.target.value.includes(".")) {
      const value = event.target.value;
      const arr = value.split(".");
      if (arr.length <= 2 && arr[1].length <= 2) {
        this.setState((prevState) => ({
          datosEstudio: {
            ...prevState.datosEstudio,
            [event.target.name]: event.target.value,
          },
        }));
      }
    } else {
      this.setState((prevState) => ({
        datosEstudio: {
          ...prevState.datosEstudio,
          [event.target.name]: event.target.value,
        },
      }));
    }
  }

  asignarDatos(data) {
    var datosEstudio = {};
    for (var key of Object.keys(data)) {
      datosEstudio[key] = data[key];
    }
    this.props.data.funcSitus(data.situs);
    this.props.data.funcAV(data.conexion_auriculo_ventricular);
    this.props.data.funcVA(data.conexion_ventriculo_arterial);

    this.setState({ datosEstudio: datosEstudio });
    this.setState((prevState) => ({
      datosEstudio: {
        ...prevState.datosEstudio,
        V_A_Activo: data.conexion_ventriculo_arterial,
      },
    }));
    this.setState((prevState) => ({
      datosEstudio: {
        ...prevState.datosEstudio,
        A_V_Activo: data.conexion_auriculo_ventricular,
      },
    }));
  }

  render() {
    return (
      <div>
        <form id="form_datos_especificos">
          <Grid
            style={{
              paddingTop: 25,
              paddingLeft: 25,
              paddingRight: 25,
              paddingBottom: 25,
            }}
            container
            spacing={3}
          >
            <Grid style={{ paddingLeft: 15 }} item xs={4}>
              <FormControl error={this.props.data.errorSitus}>
                <InputLabel id="label_situs">Situs *</InputLabel>
                <Select
                  style={{ minWidth: 300 }}
                  startAdornment={
                    <InputAdornment position="start">
                      <i
                        className="fas fa-notes-medical"
                        style={{ fontSize: "25px" }}
                      ></i>
                    </InputAdornment>
                  }
                  MenuProps={{ disableScrollLock: true }}
                  id="situs_select"
                  name="situs"
                  value={this.state.datosEstudio.situs}
                  onChange={this.cambiarSitus}
                  labelId="label_situs"
                >
                  <MenuItem data-id={"SOLITUS"} value="SOLITUS">
                    SOLITUS
                  </MenuItem>
                  <MenuItem data-id={"INVERSUS"} value="INVERSUS">
                    INVERSUS
                  </MenuItem>
                  <MenuItem data-id={"AMBIGUO"} value="AMBIGUO">
                    AMBIGUO
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid style={{ paddingLeft: 15 }} item xs={4}>
              <FormControl error={this.props.data.errorAV}>
                <InputLabel id="label_auriculo">
                  Auriculo-Ventricular *
                </InputLabel>
                <Select
                  style={{ minWidth: 300 }}
                  startAdornment={
                    <InputAdornment position="start">
                      <i
                        className="fas fa-notes-medical"
                        style={{ fontSize: "25px" }}
                      ></i>
                    </InputAdornment>
                  }
                  MenuProps={{ disableScrollLock: true }}
                  id="auriculo_select"
                  name="conexion_auriculo_ventricular"
                  value={this.state.datosEstudio.A_V_Activo}
                  onChange={this.cambiarAVActivo}
                  labelId="label_auriculo"
                >
                  <MenuItem value="Concordante">Concordante</MenuItem>
                  <MenuItem value="Discordante">Discordante</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid style={{ paddingLeft: 15 }} item xs={4}>
              <FormControl error={this.props.data.errorVA}>
                <InputLabel id="label_ventriculo">
                  Ventriculo-Arterial *
                </InputLabel>
                <Select
                  style={{ minWidth: 300 }}
                  startAdornment={
                    <InputAdornment position="start">
                      <i
                        className="fas fa-notes-medical"
                        style={{ fontSize: "25px" }}
                      ></i>
                    </InputAdornment>
                  }
                  MenuProps={{ disableScrollLock: true }}
                  id="ventriculo_select"
                  name="conexion_ventriculo_arterial"
                  value={this.state.datosEstudio.V_A_Activo}
                  onChange={this.cambiarVAActivo}
                  labelId="label_ventriculo"
                >
                  <MenuItem value="Concordante">Concordante</MenuItem>
                  <MenuItem value="Discordante">Discordante</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            {/* Valores Bidimensional */}
            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                }}
                color="primary"
                gutterBottom
              >
                Valores Bidimensional
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_DDVI"
                id="vb_DDVI"
                size="small"
                variant="outlined"
                type="number"
                label="DDVI (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_DDVI}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_DSVI"
                id="vb_DSVI"
                size="small"
                variant="outlined"
                type="number"
                label="DSVI (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_DSVI}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_SIV"
                id="vb_SIV"
                type="number"
                size="small"
                variant="outlined"
                label="SIV (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_SIV}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_PP"
                id="vb_PP"
                type="number"
                size="small"
                variant="outlined"
                label="PP (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_PP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_FEY"
                id="vb_FEY"
                size="small"
                variant="outlined"
                type="number"
                label="FEY %"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_FEY}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_Al_Area"
                id="vb_Al_Area"
                size="small"
                variant="outlined"
                type="number"
                label="AI área (cm2)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_Al_Area}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_Al_Vol_SC"
                id="vb_Al_Vol_SC"
                size="small"
                variant="outlined"
                type="number"
                label="AI Vol/SC"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_Al_Vol_SC}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_Aorta"
                id="vb_Aorta"
                size="small"
                variant="outlined"
                type="number"
                label="Aorta (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_Aorta}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_Ap_Vao"
                id="vb_Ap_Vao"
                size="small"
                variant="outlined"
                type="number"
                label="Ap VAo (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_Ap_Vao}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vb_TSVI"
                id="vb_TSVI"
                size="small"
                variant="outlined"
                type="number"
                label="TSVI (mm)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vb_TSVI}
              ></TextField>
            </Grid>

            {/* Valores Doppler */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                }}
                color="primary"
                gutterBottom
              >
                Valores Doppler
              </Typography>
            </Grid>

            {/* Valvula Aortica */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                  paddingLeft: 1,
                  color: "black",
                }}
                color="textSecondary"
                gutterBottom
              >
                Válvula Aórtica
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Velocidad_Maxima"
                id="vd_Velocidad_Maxima"
                size="small"
                variant="outlined"
                type="number"
                label="V.máxima Ao (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Velocidad_Maxima}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Maximo_Ao"
                id="vd_Gradiente_Maximo_Ao"
                size="small"
                variant="outlined"
                type="number"
                label="G.máximo Ao (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Maximo_Ao}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Medio_Ao"
                id="vd_Gradiente_Medio_Ao"
                type="number"
                size="small"
                variant="outlined"
                label="G.medio Ao (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Medio_Ao}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Insuficiencia_Aortica"
                id="vd_Insuficiencia_Aortica"
                type="number"
                size="small"
                variant="outlined"
                label="Insuficiencia"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Insuficiencia_Aortica}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_THP"
                id="vd_THP"
                size="small"
                variant="outlined"
                type="number"
                label="THP (mseg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_THP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Flujo_Reverso_ADT"
                id="vd_Flujo_Reverso_ADT"
                size="small"
                variant="outlined"
                type="number"
                label="Flujo reverso ADT"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Flujo_Reverso_ADT}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Flujo_Reverso_AA"
                id="vd_Flujo_Reverso_AA"
                size="small"
                variant="outlined"
                type="number"
                label="Flujo reverso AA"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Flujo_Reverso_AA}
              ></TextField>
            </Grid>

            {/* Valvula Mitral */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                  paddingLeft: 1,
                  color: "black",
                }}
                color="textSecondary"
                gutterBottom
              >
                Válvula Mitral
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Velocidad_Onda_E"
                id="vd_Velocidad_Onda_E"
                size="small"
                variant="outlined"
                type="number"
                label="V.onda E (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Velocidad_Onda_E}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Velocidad_Onda_A"
                id="vd_Velocidad_Onda_A"
                size="small"
                variant="outlined"
                type="number"
                label="V.onda A (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Velocidad_Onda_A}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Medio_Trasmitral"
                id="vd_Gradiente_Medio_Trasmitral"
                type="number"
                size="small"
                variant="outlined"
                label="G.medio trasmitral (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Medio_Trasmitral}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Insuficiencia_Mitral"
                id="vd_Insuficiencia_Mitral"
                type="number"
                size="small"
                variant="outlined"
                label="Insuficiencia"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Insuficiencia_Mitral}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_ORE"
                id="vd_ORE"
                size="small"
                variant="outlined"
                type="number"
                label="ORE"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_ORE}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Volumen_Regurgitante"
                id="vd_Volumen_Regurgitante"
                size="small"
                variant="outlined"
                type="number"
                label="Volumen regurgitante (ml)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Volumen_Regurgitante}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Dp_dt"
                id="vd_Dp_dt"
                size="small"
                variant="outlined"
                type="number"
                label="Dp/dt"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Dp_dt}
              ></TextField>
            </Grid>

            {/* Valvula  Pulmonar */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                  paddingLeft: 1,
                  color: "black",
                }}
                gutterBottom
              >
                Válvula Pulmonar
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Velocidad_Maxima_A_Pulmonar"
                id="vd_Velocidad_Maxima_A_Pulmonar"
                size="small"
                variant="outlined"
                type="number"
                label="V.máxima A.pulmonar (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Velocidad_Maxima_A_Pulmonar}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Maximo"
                id="vd_Gradiente_Maximo"
                size="small"
                variant="outlined"
                type="number"
                label="G.máximo (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Maximo}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Tiempo_Al_Pico"
                id="vd_Tiempo_Al_Pico"
                type="number"
                size="small"
                variant="outlined"
                label="Tiempo al pico (mseg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Tiempo_Al_Pico}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Insuficiencia_Pulmonar"
                id="vd_Insuficiencia_Pulmonar"
                type="number"
                size="small"
                variant="outlined"
                label="Insuficiencia"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Insuficiencia_Pulmonar}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_QP_QS"
                id="vd_QP_QS"
                size="small"
                variant="outlined"
                type="number"
                label="Qp/qs"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_QP_QS}
              ></TextField>
            </Grid>

            {/* Valvula  Tricúspide */}

            <Grid item xs={12}>
              <Typography
                style={{
                  fontSize: 15,
                  paddingLeft: 1,
                  color: "black",
                }}
                gutterBottom
              >
                Válvula Tricúspide
              </Typography>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Insuficiencia_Tricuspidea"
                id="vd_Insuficiencia_Tricuspidea"
                size="small"
                variant="outlined"
                type="number"
                label="Insuficiencia Tricúspidea"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Insuficiencia_Tricuspidea}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_velocidad_Regurgitante_TT"
                id="vd_velocidad_Regurgitante_TT"
                size="small"
                variant="outlined"
                type="number"
                label="V.Regurgitante TT (m/s)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_velocidad_Regurgitante_TT}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Gradiente_Pico_TT"
                id="vd_Gradiente_Pico_TT"
                type="number"
                size="small"
                variant="outlined"
                label="G.pico TT (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Gradiente_Pico_TT}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_PAP"
                id="vd_PAP"
                type="number"
                size="small"
                variant="outlined"
                label="PAP (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_PAP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_PmAP"
                id="vd_PmAP"
                size="small"
                variant="outlined"
                type="number"
                label="PmAP (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_PmAP}
              ></TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                name="vd_Pad"
                id="vd_Pad"
                size="small"
                variant="outlined"
                type="number"
                label="Pad (mmHg)"
                onChange={this.actualizarInput}
                value={this.state.datosEstudio.vd_Pad}
              ></TextField>
            </Grid>
            <Grid style={{ paddingRight: 10 }} item xs={12}>
              <FormControl fullWidth>
                <TextField
                  error={this.props.data.errorDescripcion}
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <i style={{ fontSize: 25 }} class="fas fa-edit"></i>
                      </InputAdornment>
                    ),
                  }}
                  required
                  multiline
                  rows={2}
                  label="Descripción"
                  id="descripcion"
                  name="descripcion"
                  onChange={this.actualizarInput}
                  value={this.state.datosEstudio.descripcion}
                ></TextField>
              </FormControl>
            </Grid>
            <Grid style={{ paddingRight: 10 }} item xs={12}>
              <FormControl fullWidth>
                <TextField
                  error={this.props.data.errorConclusion}
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <i
                          style={{ fontSize: 25 }}
                          class="fas fa-file-signature"
                        ></i>
                      </InputAdornment>
                    ),
                  }}
                  required
                  multiline
                  rowsMax={Infinity}
                  label="Conclusión"
                  id="conclusion"
                  name="conclusion"
                  onChange={this.actualizarInput}
                  value={this.state.datosEstudio.conclusion}
                ></TextField>
              </FormControl>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
}
