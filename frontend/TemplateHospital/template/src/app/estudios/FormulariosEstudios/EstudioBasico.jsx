import React, { useEffect } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";

export default function EstudioBasico(props) {
  const [conclusion, setConclusion] = React.useState("");

  useEffect(() => {
    if (props.data != "creando") {
      setConclusion(props.data.conclusion);
    }
  }, []);

  const cambiarInput = (event) => {
    setConclusion(event.target.value);
  };

  return (
    <div>
      <form id="form_datos_especificos">
        <Box
          display="flex"
          flexDirection="row"
          flexWrap="wrap"
          p={1}
          m={1}
          bgcolor="background.paper"
        >
          <Box flexGrow={1} p={0}>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <TextField
                  error={props.errorConclusion}
                  variant="outlined"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <i
                          style={{ fontSize: 25 }}
                          class="fas fa-file-signature"
                        ></i>
                      </InputAdornment>
                    ),
                  }}
                  required
                  multiline
                  rowsMax={Infinity}
                  label="Conclusión"
                  id="conclusion"
                  name="conclusion"
                  onChange={(e) => cambiarInput(e)}
                  value={conclusion}
                ></TextField>
              </FormControl>
            </Grid>
          </Box>
        </Box>
      </form>
    </div>
  );
}
