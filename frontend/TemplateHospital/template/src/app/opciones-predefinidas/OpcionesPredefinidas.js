import React, {Component} from 'react';
import ContenedorSeccion from '../otros/ContenedorSeccion';
import ContenedorOpcionesPredefinidas from './ContenedorOpcionesPredefinidas';
import ListIcon from '@material-ui/icons/List';


class OpcionesPredefinidas extends Component {
    constructor(props) {
        super(props);

        this.state = {
            breadcrumb: [
                { pathname: '/opciones-predefinidas', state: '', texto: 'Opciones predefinidas', icon: <ListIcon /> }
            ]
        }
    }

    // Para pasarle el breadcrumb al header
    componentDidMount() {
        this.props.actualizarHeader({
            breadcrumb: this.state.breadcrumb
        })
    }

    render() {

        return (
            <div>
                <ContenedorSeccion
                    nombre="Opciones predefinidas"
                    descripcion="Podés crear, editar o eliminar una opción predefinida"
                    componentes={[
                        <ContenedorOpcionesPredefinidas />
                    ]}
                />
            </div>
        )
    }
}

export default OpcionesPredefinidas;