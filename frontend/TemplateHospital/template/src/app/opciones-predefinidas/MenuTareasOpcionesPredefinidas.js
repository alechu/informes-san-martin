import React, { Component } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Box from "@material-ui/core/Box";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Tooltip from '@material-ui/core/Tooltip';
import Chip from '@material-ui/core/Chip';

// Icons
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

const styles = (theme) => ({
  menuItem: {
    "&:hover": {
      background: "#297ced",
      color: "white",
    },
  },
  menuItemBorrar: {
    color: "red",
    "&:hover": {
      background: "red",
      color: "white",
    },
  },
  menuItemLink: {
    "&:hover": {
      textDecoration: "none",
    },
  },
});

class MenuTareasOpcionesPredefinidas extends Component {
  constructor(props) {
    super(props);
    this.abrirTareas = this.abrirTareas.bind(this);
    this.cerrarTareas = this.cerrarTareas.bind(this);

    this.state = {
      anchorEl: null,
      esTipoDeEstudioDefault: false
    };

    this.cerrarModal = this.cerrarModal.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.opcion !== this.props.opcion) {
      this.setState({
        esTipoDeEstudioDefault: false
      });
      if (this.props.opcion.valor && this.props.opcion.tipo === "Tipo de estudio") {
        var tipo = this.props.opcion.valor.nombre;
        if (
          tipo === "basico" ||
          tipo === "disincronia" ||
          tipo === "salina_agitada" ||
          tipo === "2d_doppler" ||
          tipo === "congenitas" ||
          tipo === "transesofagico"
        ) {
          this.setState({
            esTipoDeEstudioDefault: true
          })
        }
      }
    }
  }

  // Abro dropdown
  abrirTareas = (event) => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };
  // Cierro dropdown
  cerrarTareas = () => {
    this.setState({
      anchorEl: null,
    });
  };

  cerrarModal() {
    this.setState({
      abrirModal: false,
    });
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        {!this.state.esTipoDeEstudioDefault
          ?
          <div>
            <button
              className="btn btn-success"
              aria-controls="simple-menu"
              aria-haspopup="true"
              onClick={this.abrirTareas}
            >
              Tareas
            </button>
            <Menu
              id="menu-tareas"
              anchorEl={this.state.anchorEl}
              keepMounted
              open={Boolean(this.state.anchorEl)}
              onClose={this.cerrarTareas}
            >
              <Link
                to={{
                  pathname: "/opciones-predefinidas/editar-opcion-predefinida",
                  state: { opcion: this.props.opcion },
                }}
                className={classes.menuItemLink}
              >
                <MenuItem className={classes.menuItem}>
                  <Box>
                    <EditIcon />
                  </Box>
                  <Box mx={1}>
                    <span>Editar</span>
                  </Box>
                </MenuItem>
              </Link>
              <MenuItem
                onClick={() => {
                  this.props.funcEliminar(this.props.opcion);
                }}
                className={classes.menuItemBorrar}
              >
                <Box>
                  <DeleteIcon />
                </Box>
                <Box mx={1}>
                  <span>Eliminar</span>
                </Box>
              </MenuItem>
            </Menu>
          </div>
          :
          // Si estoy mostrando los tipos de estudio por defecto...
          <div>
            <Tooltip title="Es un tipo de estudio por defecto, no se puede editar ni eliminar">
              <button
                className="btn btn-secondary"
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={this.abrirTareas}
              >
                Tareas
            </button>
            </Tooltip>
          </div>
        }
      </div>
    );
  }
}

export default withStyles(styles)(MenuTareasOpcionesPredefinidas);
