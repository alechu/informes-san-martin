import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import ContenedorSeccion from "../otros/ContenedorSeccion";
import { Redirect } from "react-router-dom";
import AddToPhotosIcon from "@material-ui/icons/AddToPhotos";
import { Link } from "react-router-dom";

export default class BotonCrearOpcionPredefinida extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log("Label (botón): " + this.props.label);
  }

  render() {
    return (
      <div style={{ padding: "20px 10px" }}>
        <Link
          to={{
            pathname: "/opciones-predefinidas/crear",
            state: {
              opcion: this.props.opcion,
            },
            label: this.props.label,
            text: this.props.text,
          }}
        >
          <Button
            variant="contained"
            style={{
              padding: "10px 20px",
              color: "white",
              backgroundColor: "green",
            }}
            endIcon={<AddToPhotosIcon />}
          >
            {this.props.text.includes("final de estudio")
              ? "Crear Diagnóstico final"
              : this.props.text}
          </Button>
        </Link>
      </div>
    );
  }
}
