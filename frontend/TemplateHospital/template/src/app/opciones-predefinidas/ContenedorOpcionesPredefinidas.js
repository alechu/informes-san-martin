import React, { Component } from 'react';
import ListadoOpcionesPredefinidas from './ListadoOpcionesPredefinidas';
import FormularioOpcionesPredefinidas from './FormularioOpcionesPredefinidas';
import Divider from '@material-ui/core/Divider';

export default class ContenedorOpcionesPredefinidas extends Component {
    constructor(props) {
        super(props);

        this.state = {
            seleccionoTipo: false,
            opcion: ''
        };
        this.seleccionarTipo = this.seleccionarTipo.bind(this);
    }

    seleccionarTipo(value) {
        this.setState({
            seleccionoTipo: true,
            opcion: value
        });    
    }

    render() {
        return (
            <div>
                <FormularioOpcionesPredefinidas
                    seleccionarTipo={this.seleccionarTipo}
                />
                <Divider style={{ margin: '20px 0px 20px 0px' }}></Divider>
                <ListadoOpcionesPredefinidas
                    seleccionoTipo={this.state.seleccionoTipo}
                    opcion={this.state.opcion}
                />
            </div>
        )
    }
}