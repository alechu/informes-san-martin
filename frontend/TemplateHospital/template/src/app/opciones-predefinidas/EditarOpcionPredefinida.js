/*

Este va a tener que llamar a FormularioEditarOpcionPredefinida.js

*/

import React, { Component } from 'react';
import ContenedorSeccion from '../otros/ContenedorSeccion';
import Box from '@material-ui/core/Box';
import InfoOpcionPredefinidaSeleccionada from './InfoOpcionPredefinidaSeleccionada';
import FormularioEditarOpcionPredefinida from './FormularioEditarOpcionPredefinida';
import ListIcon from '@material-ui/icons/List';
import EditIcon from '@material-ui/icons/Edit';

export default class EditarOpcionPredefinida extends Component {
    constructor(props) {
        super(props);

        this.state = {
            breadcrumb: [
                { pathname: '/opciones-predefinidas', state: '', texto: 'Opciones predefinidas', icon: <ListIcon /> },
                { pathname: '/opciones-predefinidas/editar-opcion-predefinida', state: { opcion: this.props.location.state.opcion }, texto: 'Editar opción predefinida', icon: <EditIcon /> }
            ]
        }
    };

    // Para pasarle el breadcrumb al header
    componentDidMount() {
        this.props.actualizarHeader({
            breadcrumb: this.state.breadcrumb
        })
    }

    render() {

        // Opción predefinida que voy a editar
        const opcion = this.props.location.state.opcion;

        return (
            <ContenedorSeccion
                nombre="Editar opción predefinida"
                descripcion="Podés editar la opción predefinida elegida"
                preComponentes={[
                    <InfoOpcionPredefinidaSeleccionada
                        opcion={opcion}/>
                ]}
                componentes={[
                    <FormularioEditarOpcionPredefinida 
                        opcion={opcion}
                    />
                ]}
            />
        )
    }
}