import React, { Component } from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import ModalOpcion from "../pacientes/ModalPaciente";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import InputAdornment from "@material-ui/core/InputAdornment";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";

/* Estructura de cada opción para el armado del formulario */
const opciones = {
  "Obra social": "obra_social",
  "Motivo de estudio": "motivo",
  "Tipo de estudio": "tipo_estudio",
  "Diagnóstico final de estudio": "diagnostico",
  "Médico especialista": "especialista",
  "Descripción predefinida": "descripcion",
  Escenario: "conclusion",
};

export default class FormularioEditarOpcionPredefinida extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tipo: opciones[`${this.props.opcion.tipo}`],
      datos: [],
      nombre: this.props.opcion.valor.nombre && null,
      escenario: this.props.opcion.valor.escenario,
      conclusion: this.props.opcion.valor.conclusion,
      abrirModalComun: false,
      abrirModalTipo: false,
      abrirModalConclusion: false,
      abrirModalConfirmacion: false,
      huboError: false,
      mensaje: "",
      label: this.props.label,
      text: this.props.text,
      hayLabel: null,
      id: this.props.opcion.valor.id,
      errores: {
        errorNombre: false,
        errorEscenario: false,
        errorConclusion: false,
        abrirAlert: false,
      },

      icon: "",
      iconColor: "",
      cargandoAccion: false,
      botonesModal: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmitTipoEstudio = this.handleSubmitTipoEstudio.bind(this);
    this.handleSubmitConclusion = this.handleSubmitConclusion.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
    this.cerrarModalNuevaOpcion = this.cerrarModalNuevaOpcion.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.abrirModalConfirmacion = this.abrirModalConfirmacion.bind(this);
    this.cerrarModalConfirmacion = this.cerrarModalConfirmacion.bind(this);
    this.cerrarModalNuevaOpcion = this.cerrarModalNuevaOpcion.bind(this);
  }

  componentDidMount() {
    let tipo = `${this.state.tipo}`;
    if (tipo === "tipo_estudio") {
      this.props.opcion.valor.nombre &&
        this.setState({
          nombre: this.props.opcion.valor.nombre_mostrado,
        });
    } else {
      this.props.opcion.valor.nombre &&
        this.setState({
          nombre: this.props.opcion.valor.nombre,
        });
    }
  }

  cerrarModal() {
    this.setState({
      abrirModal: false,
    });
  }

  cerrarModalConfirmacion() {
    this.setState({
      abrirModalConfirmacion: false,
    });
  }

  cerrarModalNuevaOpcion() {
    this.setState({
      abrirModal: false,
      formControls: {
        nombre: {
          value: "",
        },
        escenario: {
          value: "",
        },
        conclusion: {
          value: "",
        },
      },
    });
  }

  isNotEmpty(str) {
    return !(!str || 0 === str.length);
  }

  abrirModalConfirmacion() {
    this.setState({
      abrirModalConfirmacion: true,
    });
  }

  handleFormSubmit() {
    this.setState({
      abrirModalNormal: false,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState(
      (prevState) => ({
        errores: {
          ...prevState.errores,
          errorNombre: false,
          abrirAlert: false,
        },
      }),
      () => {
        if (this.state.nombre === "") {
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              errorNombre: true,
              abrirAlert: true,
            },
          }));
        } else {
          var datos = new FormData();

          datos.append("nombre", this.state.nombre);

          axios({
            method: "put",
            url: `http://${process.env.REACT_APP_API_IP}/api/${this.state.tipo}?id=${this.state.id}`,
            data: datos,
            headers: {
              Authorization:
                "Bearer " +
                document.cookie.replace(
                  /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                  "$1"
                ),
              "Content-Type": "multipart/form-data",
            },
          })
            .then((response) => {
              // Preparo el mensaje para el modal
              if (response.data.status == 200) {
                this.setState({
                  cargandoAccion: false,
                  icon: "fas fa-check-circle",
                  iconColor: "green",
                  abrirModal: true,
                  huboError: false,
                  mensaje: <span>{response.data.body}.</span>,
                  botonesModal: [
                    {
                      link: "/opciones-predefinidas",
                      color: "#3f51b5",
                      state: "",
                      texto: "Volver al listado de opciones predefinidas",
                    },
                    {
                      link: "",
                      color: "#3f51b5",
                      state: "",
                      texto: "Volver a editar",
                      onClick: this.cerrarModal,
                    },
                  ],
                });
              }
            })
            .catch((error) => {
              switch (error.response.data.status) {
                case 401:
                  localStorage.setItem("alert", "Expired/Invalid JWT Token");
                  document.cookie =
                    "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                  window.location.href = "/user-pages/login-1";
                  break;
              }
              // Armo un mensaje para mandarle al modal
              this.armarModalError(error);
            });
        }
      }
    );
  }

  handleSubmitTipoEstudio(event) {
    event.preventDefault();

    this.setState(
      (prevState) => ({
        errores: {
          ...prevState.errores,
          errorNombre: false,
          abrirAlert: false,
        },
      }),
      () => {
        if (this.state.nombre === "") {
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              errorNombre: true,
              abrirAlert: true,
            },
          }));
        } else {
          var datos = new FormData();
          datos.append("nombre_mostrado", this.state.nombre);
          axios({
            method: "put",
            url: `http://"+process.env.REACT_APP_API_IP+"/api/tipo_estudio?id=${this.state.id}`,
            data: datos,
            headers: {
              Authorization:
                "Bearer " +
                document.cookie.replace(
                  /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                  "$1"
                ),
              "Content-Type": "multipart/form-data",
            },
          })
            .then((response) => {
              // Preparo el mensaje para el modal
              if (response.data.status == 200) {
                this.setState({
                  cargandoAccion: false,
                  icon: "fas fa-check-circle",
                  iconColor: "green",
                  abrirModal: true,
                  huboError: false,
                  mensaje: <span>{response.data.body}.</span>,
                  botonesModal: [
                    {
                      link: "/opciones-predefinidas",
                      state: "",
                      color: "#3f51b5",
                      texto: "Volver al listado de opciones predefinidas",
                    },
                    {
                      link: "",
                      state: "",
                      color: "#3f51b5",
                      texto: "Volver a editar",
                      onClick: this.cerrarModal,
                    },
                  ],
                });
              }
            })
            .catch((error) => {
              switch (error.response.data.status) {
                case 401:
                  localStorage.setItem("alert", "Expired/Invalid JWT Token");
                  document.cookie =
                    "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                  window.location.href = "/user-pages/login-1";
                  break;
              }
              // Armo un mensaje para mandarle al modal
              this.armarModalError(error);
            });
        }
      }
    );
  }

  handleSubmitConclusion(event) {
    event.preventDefault();

    this.setState(
      (prevState) => ({
        errores: {
          ...prevState.errores,
          errorEscenario: false,
          errorConclusion: false,
          abrirAlert: false,
        },
      }),
      () => {
        var hayError = false;
        if (this.state.escenario === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              errorEscenario: true,
              abrirAlert: true,
            },
          }));
        }
        if (this.state.conclusion === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              errorConclusion: true,
              abrirAlert: true,
            },
          }));
        }
        if (!hayError) {
          var datos = new FormData();
          datos.append("escenario", this.state.escenario);
          datos.append("conclusion", this.state.conclusion);

          axios({
            method: "put",
            url: `http://"+process.env.REACT_APP_API_IP+"/api/conclusion?id=${this.state.id}`,
            data: datos,
            headers: {
              Authorization:
                "Bearer " +
                document.cookie.replace(
                  /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                  "$1"
                ),
              "Content-Type": "multipart/form-data",
            },
          })
            .then((response) => {
              // Preparo el mensaje para el modal
              if (response.data.status == 200) {
                this.setState({
                  cargandoAccion: false,
                  icon: "fas fa-check-circle",
                  iconColor: "green",
                  abrirModal: true,
                  huboError: false,
                  mensaje: <span>{response.data.body}.</span>,
                  botonesModal: [
                    {
                      link: "/opciones-predefinidas",
                      state: "",
                      color: "#3f51b5",
                      texto: "Volver al listado de opciones predefinidas",
                    },
                    {
                      link: "",
                      state: "",
                      color: "#3f51b5",
                      texto: "Volver a editar",
                      onClick: this.cerrarModal,
                    },
                  ],
                });
              }
            })
            .catch((error) => {
              switch (error.response.data.status) {
                case 401:
                  localStorage.setItem("alert", "Expired/Invalid JWT Token");
                  document.cookie =
                    "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                  window.location.href = "/user-pages/login-1";
                  break;
              }
              // Armo un mensaje para mandarle al modal
              this.armarModalError(error);
            });
        }
      }
    );
  }

  armarModalError(error) {
    var mensajeError = "";
    switch (error.status) {
      case 400:
        mensajeError = (
          <span>
            La opción predefinida no ha podido ser cargada. Posiblemente ya
            exista una con ese nombre.
          </span>
        );
        break;
      case 500:
        mensajeError =
          "Ocurrió un error con el servidor, por favor intentá de nuevo.";
        break;
    }
    this.setState({
      abrirModal: true,
      huboError: true,
      mensaje: mensajeError,
    });
  }

  changeHandler = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value,
    });
  };

  primeraLetraMayuscula(texto) {
    return (texto.charAt(0).toUpperCase() + texto.slice(1)).replace(/_/g, " ");
  }
  cancelar = () => {
    window.history.back();
  };

  render() {
    const { classes } = this.props;

    return (
      <div style={{ width: "100%", padding: "10px" }}>
        <Grid container>
          <Box p={1} flexGrow={1}>
            <Grid item xs={12}>
              <Paper
                style={{
                  padding: "5px",
                  textAlign: "center",
                  background:
                    "linear-gradient(120deg, #2f78e9, #128bfc, #18bef1)",
                }}
              >
                <b style={{ color: "white", fontSize: "20px" }}>
                  Editando{" "}
                  {this.props.opcion.tipo === "Escenario"
                    ? "Conclusión predefinida"
                    : this.props.opcion.tipo}
                </b>
                <br></br>
                <Typography style={{ color: "white" }} variant="caption">
                  Los campos requeridos están marcados con un *
                </Typography>
              </Paper>
            </Grid>
          </Box>
          <Grid item xs={12} style={{ padding: 12 }}>
            <Card variant="outlined">
              <Collapse in={this.state.errores.abrirAlert}>
                <Alert
                  style={{
                    marginTop: 10,
                    marginLeft: 20,
                    marginRight: 20,
                  }}
                  variant="filled"
                  severity="error"
                >
                  Por favor, completá los campos marcados en rojo
                </Alert>
              </Collapse>
              {(this.state.tipo === "obra_social" ||
                this.state.tipo === "motivo" ||
                this.state.tipo === "diagnostico" ||
                this.state.tipo === "especialista" ||
                this.state.tipo === "descripcion") && (
                <form onSubmit={this.handleSubmit}>
                  <Box
                    display="flex"
                    flexDirection="row"
                    p={2}
                    m={1}
                    bgcolor="background.paper"
                  >
                    <Box flexGrow={1}>
                      <Grid item xs={12}>
                        <TextField
                          fullWidth
                          variant="outlined"
                          aria-describedby="my-helper-text"
                          required
                          id={this.state.tipo}
                          name="nombre"
                          value={this.state.nombre}
                          onChange={this.changeHandler}
                          label={this.primeraLetraMayuscula(this.state.tipo)}
                          error={this.state.errores.errorNombre}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                {this.state.tipo === "obra_social" ? (
                                  <i
                                    className="fas fa-star-of-life"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : this.state.tipo === "motivo" ? (
                                  <i
                                    className="fas fa-question-circle"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : this.state.tipo === "diagnostico" ? (
                                  <i
                                    className="fas fa-notes-medical"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : this.state.tipo === "especialista" ? (
                                  <i
                                    className="fas fa-user-md"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : this.state.tipo === "descripcion" ? (
                                  <i
                                    className="fas fa-edit"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : null}
                              </InputAdornment>
                            ),
                          }}
                        />
                      </Grid>
                    </Box>
                  </Box>
                  <Box p={2}>
                    <Button
                      variant="contained"
                      style={{
                        marginLeft: 5,
                        backgroundColor: "dodgerblue",
                        color: "white",
                      }}
                      size="large"
                      endIcon={
                        <i
                          style={{ fontSize: "18px" }}
                          className="fas fa-save"
                        ></i>
                      }
                      onClick={this.handleSubmit}
                    >
                      Guardar Cambios
                    </Button>
                  </Box>
                </form>
              )}
              {this.state.tipo === "tipo_estudio" && (
                <form onSubmit={this.handleSubmitTipoEstudio}>
                  <Box
                    display="flex"
                    flexDirection="row"
                    p={2}
                    m={1}
                    bgcolor="background.paper"
                  >
                    <Box flexGrow={1}>
                      <Grid item xs={12}>
                        <TextField
                          fullWidth
                          variant="outlined"
                          aria-describedby="my-helper-text"
                          required
                          id="nombre"
                          name="nombre"
                          value={this.state.nombre}
                          onChange={this.changeHandler}
                          label="Nombre de Tipo de estudio"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <i
                                  className="fas fa-book-medical"
                                  style={{ fontSize: "25px" }}
                                ></i>
                              </InputAdornment>
                            ),
                          }}
                          error={this.state.errores.errorNombre}
                        />
                      </Grid>
                    </Box>
                  </Box>

                  <Box p={2}>
                    <Button
                      variant="contained"
                      style={{
                        marginLeft: 5,
                        backgroundColor: "dodgerblue",
                        color: "white",
                      }}
                      size="large"
                      endIcon={
                        <i
                          style={{ fontSize: "18px" }}
                          className="fas fa-save"
                        ></i>
                      }
                      onClick={this.handleSubmitTipoEstudio}
                    >
                      Guardar Cambios
                    </Button>
                  </Box>
                </form>
              )}
              {this.state.tipo === "conclusion" && (
                <form onSubmit={this.handleSubmitConclusion}>
                  <Box
                    display="flex"
                    flexDirection="column"
                    p={2}
                    m={1}
                    bgcolor="background.paper"
                  >
                    <Box flexGrow={1}>
                      <Grid item xs={12}>
                        <TextField
                          variant="outlined"
                          aria-describedby="my-helper-text"
                          required
                          id="escenario"
                          name="escenario"
                          value={this.state.escenario}
                          onChange={this.changeHandler}
                          label="Escenario"
                          multiline
                          fullWidth
                          rows={3}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <i
                                  style={{ fontSize: "25px" }}
                                  className="fas fa-notes-medical"
                                ></i>
                              </InputAdornment>
                            ),
                          }}
                          error={this.state.errores.errorEscenario}
                        />
                      </Grid>
                    </Box>
                    <Box flexGrow={1}>
                      <Grid item xs={12} style={{ paddingTop: 15 }}>
                        <TextField
                          variant="outlined"
                          aria-describedby="my-helper-text"
                          required
                          id="conclusion"
                          name="conclusion"
                          value={this.state.conclusion}
                          onChange={this.changeHandler}
                          label="Conclusión"
                          fullWidth
                          multiline
                          rows={3}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <i
                                  style={{ fontSize: "25px" }}
                                  className="fas fa-file-signature"
                                ></i>
                              </InputAdornment>
                            ),
                          }}
                          error={this.state.errores.errorConclusion}
                        />
                      </Grid>
                      <Box>
                        <Button
                          variant="contained"
                          style={{
                            backgroundColor: "dodgerblue",
                            color: "white",
                            marginTop: 15,
                          }}
                          size="large"
                          endIcon={
                            <i
                              style={{ fontSize: "18px" }}
                              className="fas fa-save"
                            ></i>
                          }
                          onClick={this.handleSubmitConclusion}
                        >
                          Guardar Cambios
                        </Button>
                      </Box>
                    </Box>
                  </Box>
                </form>
              )}
            </Card>
          </Grid>
        </Grid>

        <ModalOpcion
          abrirModal={this.state.abrirModal}
          cerrarModal={this.cerrarModal}
          mensaje={this.state.mensaje}
          cargandoAccion={this.state.cargandoAccion}
          icon={this.state.icon}
          iconColor={this.state.iconColor}
          botones={this.state.botonesModal}
        />
      </div>
    );
  }
}
