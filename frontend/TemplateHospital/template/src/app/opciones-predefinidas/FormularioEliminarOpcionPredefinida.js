import React, { Component } from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import ChartComponent from "react-chartjs-2";
import ModalOpcion from "./ModalOpcion";
import ModalConfirmar from "./ModalConfirmar";

/* Estructura de cada opción para el armado del formulario */
const opciones = {
  "Obra social": "obra_social",
  "Motivo de estudio": "motivo",
  "Tipo de estudio": "tipo_estudio",
  Diagnostico: "diagnostico",
  Especialista: "especialista",
  Descripcion: "descripcion",
  Conclusion: "conclusion",
};

export default class FormularioEditarOpcionPredefinida extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tipo: opciones[`${this.props.opcion.tipo}`],
      nombre: this.props.opcion.valor.nombre,
      escenario: this.props.opcion.valor.escenario,
      conclusion: this.props.opcion.valor.conclusion,
      abrirModal: false,
      huboError: false,
      mensaje: "",
      label: this.props.label,
      text: this.props.text,
      hayLabel: null,
      id: this.props.opcion.valor.id,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    var datos = new FormData();
    datos.append("id", this.state.id);
    console.log(this.state.id);

    axios({
      method: "delete",
      url: `http://${process.env.REACT_APP_API_IP}/api/${this.state.tipo}?id=${this.state.id}`,
      data: datos,
      headers: {
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        // Preparo el mensaje para el modal
        if (response.data.status == 200) {
          this.setState({
            abrirModal: true,
            huboError: false,
            mensaje: <span>{response.body}</span>,
          });
        }
      })
      .catch((error) => {
        // Armo un mensaje para mandarle al modal
        switch (error.response.data.status) {
          case 401:
            localStorage.setItem("alert", "Expired/Invalid JWT Token");
            document.cookie =
              "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            window.location.href = "/user-pages/login-1";
            break;
        }
        this.armarModalError(error);
      });
  }

  armarModalError(error) {
    var mensajeError = "";
    switch (error.status) {
      case 400:
        mensajeError = (
          <span>
            La opción predefinida no ha podido ser cargada. Posiblemente ya
            exista una con ese nombre.
          </span>
        );
        break;
      case 500:
        mensajeError =
          "Ocurrió un error con el servidor, por favor intentá de nuevo.";
        break;
    }
    this.setState({
      abrirModal: true,
      huboError: true,
      mensaje: mensajeError,
    });
  }

  cerrarModal() {
    this.setState({
      abrirModal: false,
    });
  }

  render() {
    const { classes } = this.props;

    return (
      <Box display="flex">
        <div style={{ width: "100%", padding: "10px" }}>
          <Grid container>
            <Grid item xs={12}>
              <h3>Eliminar opción predefinida</h3>
            </Grid>
            <Grid item container xs={6}>
              <Box>
                Este apartado eliminará la opción predefinida seleccionada.{" "}
              </Box>
              <Box>No se podrá eliminar si esta tiene un estudio asociado.</Box>
              <Box>¿Está seguro de querer eliminar esta opción?</Box>
            </Grid>
            <Grid item xs={6} spacing={3}>
              <Button
                variant="contained"
                color="primary"
                onClick={this.handleSubmit}
              >
                Eliminar opción
              </Button>
              {/**<Button
                            variant="contained"
                            color="secondary"
                            onClick={this.cancelar()}
                        >Cancelar
                        </Button>*/}
            </Grid>
          </Grid>
        </div>
        {this.state.huboError ? (
          <ModalOpcion
            abrirModal={this.state.abrirModal}
            cerrarModal={this.cerrarModal}
            mensaje="La opción no ha podido ser eliminada: hay estudios asociados a la misma."
            esError={true}
            botones={[
              {
                link: "/opciones-predefinidas",
                state: "",
                texto: "Volver al listado de opciones predefinidas",
                onClick: "",
              },
            ]}
          />
        ) : (
          <ModalOpcion
            abrirModal={this.state.abrirModal}
            cerrarModal={this.cerrarModal}
            mensaje={this.state.mensaje}
            esError={false}
            botones={[
              {
                link: "/opciones-predefinidas",
                state: "",
                texto: "Ir al listado de opciones predefinidas",
                onClick: "",
              },
            ]}
          />
        )}
      </Box>
    );
  }
}
