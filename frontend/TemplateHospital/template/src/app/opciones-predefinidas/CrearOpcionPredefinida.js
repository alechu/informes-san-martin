import React, { Component } from "react";
import FormularioCrearOpcionPredefinida from "./FormularioCrearOpcionPredefinida";
import ContenedorSeccion from "../otros/ContenedorSeccion";
import ListIcon from "@material-ui/icons/List";
import LibraryAddIcon from "@material-ui/icons/LibraryAdd";

export default class CrearOpcionPredefinida extends Component {
  constructor(props) {
    super(props);

    this.state = {
      breadcrumb: [
        {
          pathname: "/opciones-predefinidas",
          state: "",
          texto: "Opciones predefinidas",
          icon: <ListIcon />,
        },
        {
          pathname: "/opciones-predefinidas/crear",
          state: { paciente: this.props.location.state.opcion },
          texto: "Crear opción predefinida",
          icon: <LibraryAddIcon />,
        },
      ],
    };
  }

  // Para pasarle el breadcrumb al header
  componentDidMount() {
    this.props.actualizarHeader({
      breadcrumb: this.state.breadcrumb,
    });
  }

  render() {
    // Paciente al que le voy a agregar un estudio.
    const opcion = this.props.location.state.opcion;

    return (
      <ContenedorSeccion
        nombre="Crear opción predefinida"
        descripcion="Podés agregar opciones predefinidas a las ya existentes"
        componentes={[
          <FormularioCrearOpcionPredefinida
            label={this.props.location.label}
            text={this.props.location.text}
            opcion={opcion}
          />,
        ]}
      />
    );
  }
}
