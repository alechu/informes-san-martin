import React, { Component } from "react";
import Box from "@material-ui/core/Box";

export default class InfoOpcionPredefinidaSeleccionada extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log(this.props.opcion);
  }

  render() {
    const opcion = this.props.opcion;

    return (
      <div style={{ width: "100%", background: "white", padding: "20px 30px" }}>
        <Box display="flex" flexDirection="column">
          <Box>
            Tipo de opción predefinida: <strong>{opcion.tipo}</strong>
          </Box>
          {this.props.opcion.tipo !== "Escenario" ? (
            <Box>
              Texto:{" "}
              <span style={{ color: "#2380f1" }}>{opcion.valor.nombre}</span>
            </Box>
          ) : null}
        </Box>
      </div>
    );
  }
}
