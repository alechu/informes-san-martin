import React, { Component, useState } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Alert from "@material-ui/lab/Alert";
import BotonCrearOpcionPredefinida from "./BotonCrearOpcionPredefinida";

/* Opciones con texto plural y género para armar bien los labels */
const opciones = [
  {
    value: "obra_social",
    texto: "Obra social",
    textoPlural: "Obras sociales",
    genero: 1,
  },
  {
    value: "motivo",
    texto: "Motivo de estudio",
    textoPlural: "Motivos de estudio",
    genero: 0,
  },
  {
    value: "tipo_estudio",
    texto: "Tipo de estudio",
    textoPlural: "Tipos de estudio",
    genero: 0,
  },
  {
    value: "diagnostico",
    texto: "Diagnóstico final de estudio",
    textoPlural: "Diagnósticos finales de estudio",
    genero: 0,
  },
  {
    value: "especialista",
    texto: "Médico especialista",
    textoPlural: "Médicos especialistas",
    genero: 0,
  },
  /*   {
    value: "descripcion",
    texto: "Descripción predefinida",
    textoPlural: "Descripciones predefinidas",
    genero: 1,
  }, */
  {
    value: "conclusion",
    texto: "Conclusión predefinida",
    textoPlural: "Conclusiones predefinidas",
    genero: 1,
  },
];

const menuItemsOpciones = opciones.map((opcion) => (
  <MenuItem style={{ padding: "5px 15px 5px 15px" }} value={opcion}>
    {opcion.texto}
  </MenuItem>
));

export default class FormularioOpcionesPredefinidas extends Component {
  constructor(props) {
    super(props);

    this.state = {
      opcion: "",
    };

    this.seleccionarTipo = this.seleccionarTipo.bind(this);
  }

  seleccionarTipo(event) {
    this.setState({
      opcion: event.target.value,
    });
    this.props.seleccionarTipo(event.target.value);
  }

  render() {
    const labelParaCrear = () => {
      if (this.state.opcion) {
        return (
          "Ingresá " +
          (this.state.opcion.genero == 0 ? "el nuevo " : "la nueva ") +
          this.state.opcion.texto.toLowerCase()
        );
      } else return "";
    };

    return (
      <Grid container spacing={5} style={{ padding: "20px 30px 5px" }}>
        <Grid item xs={4}>
          <div>
            <h5>
              Elegí la opción a la cual querés agregarle, editarle o eliminarle
              una entrada:
            </h5>
          </div>
        </Grid>

        <Grid item xs={4}>
          <FormControl style={{ minWidth: "200px" }}>
            <InputLabel id="opcion-label">Opción predefinida</InputLabel>
            <Select
              MenuProps={{ disableScrollLock: true }}
              labelId="opcion-label"
              id="opcion"
              onChange={this.seleccionarTipo}
              children={menuItemsOpciones}
            ></Select>
          </FormControl>
        </Grid>
        {this.state.opcion && (
          <Grid item xs={4}>
            <BotonCrearOpcionPredefinida
              variant="contained"
              color="primary"
              onClick={this.crearOpcionPredefinida}
              text={`Crear ${this.state.opcion.texto.toLowerCase()}`}
              opcion={this.state.opcion}
              label={labelParaCrear()}
            />
          </Grid>
        )}
      </Grid>
    );
  }
}
