import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import ModalOpcion from "../pacientes/ModalPaciente";
import Typography from "@material-ui/core/Typography";
import InputAdornment from "@material-ui/core/InputAdornment";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";

/* Estructura de cada opción para el armado del formulario */
const opciones = {
  obra_social: ["nombre"],
  motivo: ["nombre"],
  tipo_estudio: ["nombre", "nombre_mostrado"],
  diagnostico: ["nombre"],
  especialista: ["nombre"],
  descripcion: ["nombre"],
  conclusion: ["escenario", "conclusion"],
};

export default class FormularioCrearOpcionPredefinida extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tipo: this.props.opcion.value,
      datos: [],
      formControls: {
        nombre: {
          value: "",
        },
        escenario: {
          value: "",
        },
        conclusion: {
          value: "",
        },
      },
      abrirModal: false,
      huboError: false,
      mensaje: "",
      label: this.props.label,
      text: this.props.text,
      hayLabel: "",
      errores: {
        errorNombre: false,
        errorEscenario: false,
        errorConclusion: false,
        abrirAlert: false,
      },

      icon: "",
      iconColor: "",
      cargandoAccion: false,
      botonesModal: [],
      desactivarCruz: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmitTipoEstudio = this.handleSubmitTipoEstudio.bind(this);
    this.handleSubmitConclusion = this.handleSubmitConclusion.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
    this.cerrarModalNuevaOpcion = this.cerrarModalNuevaOpcion.bind(this);
  }

  componentDidMount() {
    console.log(this.state.tipo);
    let tipo = `${this.state.tipo}`;
    this.setState({
      datos: opciones[`${tipo}`],
      hayLabel: this.isNotEmpty(this.state.label),
    });
  }

  cerrarModal() {
    this.setState({
      abrirModal: false,
    });
  }

  cerrarModalNuevaOpcion() {
    this.setState({
      abrirModal: false,
      formControls: {
        nombre: {
          value: "",
        },
        escenario: {
          value: "",
        },
        conclusion: {
          value: "",
        },
      },
    });
  }

  isNotEmpty(str) {
    return !(!str || 0 === str.length);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState(
      (prevState) => ({
        errores: {
          ...prevState.errores,
          errorNombre: false,
          abrirAlert: false,
        },
      }),
      () => {
        if (this.state.formControls.nombre.value === "") {
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              errorNombre: true,
              abrirAlert: true,
            },
          }));
        } else {
          var datos = new FormData();
          datos.append("nombre", this.state.formControls.nombre.value);

          axios({
            method: "post",
            url: `http://${process.env.REACT_APP_API_IP}/api/${this.state.tipo}`,
            data: datos,
            headers: {
              Authorization:
                "Bearer " +
                document.cookie.replace(
                  /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                  "$1"
                ),
              "Content-Type": "multipart/form-data",
            },
          })
            .then((response) => {
              // Preparo el mensaje para el modal
              if (response.data.status == 201) {
                console.log(response.data.body);
                this.setState({
                  cargandoAccion: false,
                  icon: "fas fa-check-circle",
                  iconColor: "green",
                  abrirModal: true,
                  huboError: false,
                  desactivarCruz: true,
                  mensaje: <span>{response.data.body}.</span>,
                  botonesModal: [
                    {
                      texto: "Ir al listado de opciones predefinidas",
                      color: "#3f51b5",
                      link: "/opciones-predefinidas",
                      state: "",
                    },
                    {
                      texto: "Crear otro/a",
                      color: "#3f51b5",
                      onClick: this.cerrarModal,
                    },
                  ],
                });
              }
            })
            .catch((error) => {
              // Armo un mensaje para mandarle al modal
              switch (error.response.data.status) {
                case 401:
                  localStorage.setItem("alert", "Expired/Invalid JWT Token");
                  document.cookie =
                    "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                  window.location.href = "/user-pages/login-1";
                  break;
              }
              this.armarModalError(error);
            });
        }
      }
    );
  }

  sanitizarEntrada(input) {
    var nombre_mostrado = input;
    var campoNormalizado = nombre_mostrado.replace(/ /g, "_");
    campoNormalizado = campoNormalizado
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "");
    campoNormalizado = campoNormalizado.replace(/[^a-z\d\s]+/gi, "");
    return campoNormalizado;
  }

  handleSubmitTipoEstudio(event) {
    event.preventDefault();

    this.setState(
      (prevState) => ({
        errores: {
          ...prevState.errores,
          errorNombre: false,
          abrirAlert: false,
        },
      }),
      () => {
        if (this.state.formControls.nombre.value === "") {
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              errorNombre: true,
              abrirAlert: true,
            },
          }));
        } else {
          var datos = new FormData();
          datos.append(
            "nombre",
            this.sanitizarEntrada(this.state.formControls.nombre.value)
          );
          datos.append("nombre_mostrado", this.state.formControls.nombre.value);

          axios({
            method: "post",
            url: `http://"+process.env.REACT_APP_API_IP+"/api/tipo_estudio`,
            data: datos,
            headers: {
              Authorization:
                "Bearer " +
                document.cookie.replace(
                  /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                  "$1"
                ),
              "Content-Type": "multipart/form-data",
            },
          })
            .then((response) => {
              // Preparo el mensaje para el modal
              if (response.data.status == 201) {
                this.setState({
                  cargandoAccion: false,
                  icon: "fas fa-check-circle",
                  iconColor: "green",
                  abrirModal: true,
                  huboError: false,
                  desactivarCruz: true,
                  mensaje: <span>{response.data.body}.</span>,
                  botonesModal: [
                    {
                      texto: "Ir al listado de opciones predefinidas",
                      color: "#3f51b5",
                      link: "/opciones-predefinidas",
                      state: "",
                    },
                    {
                      texto: "Crear otro/a",
                      color: "#3f51b5",
                      onClick: this.cerrarModal,
                    },
                  ],
                });
              }
            })
            .catch((error) => {
              switch (error.response.data.status) {
                case 401:
                  localStorage.setItem("alert", "Expired/Invalid JWT Token");
                  document.cookie =
                    "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                  window.location.href = "/user-pages/login-1";
                  break;
              }
              // Armo un mensaje para mandarle al modal
              this.armarModalError(error);
            });
        }
      }
    );
  }

  handleSubmitConclusion(event) {
    event.preventDefault();

    this.setState(
      (prevState) => ({
        errores: {
          ...prevState.errores,
          errorEscenario: false,
          errorConclusion: false,
          abrirAlert: false,
        },
      }),
      () => {
        var hayError = false;
        if (this.state.formControls.escenario.value === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              errorEscenario: true,
              abrirAlert: true,
            },
          }));
        }
        if (this.state.formControls.conclusion.value === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              errorConclusion: true,
              abrirAlert: true,
            },
          }));
        }
        if (!hayError) {
          var datos = new FormData();
          datos.append("escenario", this.state.formControls.escenario.value);
          datos.append("conclusion", this.state.formControls.conclusion.value);

          axios({
            method: "post",
            url: `http://"+process.env.REACT_APP_API_IP+"/api/conclusion`,
            data: datos,
            headers: {
              Authorization:
                "Bearer " +
                document.cookie.replace(
                  /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                  "$1"
                ),
              "Content-Type": "multipart/form-data",
            },
          })
            .then((response) => {
              // Preparo el mensaje para el modal
              if (response.data.status == 201) {
                this.setState({
                  cargandoAccion: false,
                  icon: "fas fa-check-circle",
                  iconColor: "green",
                  abrirModal: true,
                  huboError: false,
                  desactivarCruz: true,
                  mensaje: <span>{response.data.body}.</span>,
                  botonesModal: [
                    {
                      texto: "Ir al listado de opciones predefinidas",
                      color: "#3f51b5",
                      link: "/opciones-predefinidas",
                      state: "",
                    },
                    {
                      texto: "Crear otro/a",
                      color: "#3f51b5",
                      onClick: this.cerrarModal,
                    },
                  ],
                });
              }
            })
            .catch((error) => {
              // Armo un mensaje para mandarle al modal
              switch (error.response.data.status) {
                case 401:
                  localStorage.setItem("alert", "Expired/Invalid JWT Token");
                  document.cookie =
                    "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                  window.location.href = "/user-pages/login-1";
                  break;
              }
              this.armarModalError(error);
            });
        }
      }
    );
  }

  armarModalError(error) {
    var mensajeError = "";
    switch (error.status) {
      case 400:
        mensajeError = (
          <span>
            La opción predefinida no se pudo crear. Posiblemente ya exista una
            con ese nombre.
          </span>
        );
        break;
      case 500:
        mensajeError =
          "Ocurrió un error con el servidor, por favor intentá de nuevo.";
        break;
    }
    this.setState({
      abrirModal: true,
      huboError: true,
      mensaje: mensajeError,
      color: "#dc004e",
    });
  }

  changeHandler = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      formControls: {
        ...this.state.formControls,
        [name]: {
          ...this.state.formControls[name],
          value,
        },
      },
    });
  };

  primeraLetraMayuscula(texto) {
    return (texto.charAt(0).toUpperCase() + texto.slice(1)).replace(/_/g, " ");
  }
  cancelar = () => {
    window.history.back();
  };

  render() {
    const { classes } = this.props;

    return (
      <div style={{ width: "100%", padding: "10px" }}>
        <Grid container>
          <Box p={1} flexGrow={1}>
            <Grid item xs={12}>
              <Paper
                style={{
                  padding: "5px",
                  textAlign: "center",
                  background:
                    "linear-gradient(120deg, #2f78e9, #128bfc, #18bef1)",
                }}
              >
                <b style={{ color: "white", fontSize: "20px" }}>
                  {this.state.hayLabel ? (
                    <span>{this.state.label}</span>
                  ) : (
                    <span>Ingresá la nueva opción</span>
                  )}
                </b>
                <br></br>
                <Typography style={{ color: "white" }} variant="caption">
                  Los campos requeridos están marcados con un *
                </Typography>
              </Paper>
            </Grid>
          </Box>
          <Grid item xs={12} style={{ padding: 12 }}>
            <Card variant="outlined">
              <Collapse in={this.state.errores.abrirAlert}>
                <Alert
                  style={{
                    marginTop: 10,
                    marginLeft: 20,
                    marginRight: 20,
                  }}
                  variant="filled"
                  severity="error"
                >
                  Por favor, completá los campos marcados en rojo
                </Alert>
              </Collapse>
              {(this.state.tipo === "obra_social" ||
                this.state.tipo === "motivo" ||
                this.state.tipo === "diagnostico" ||
                this.state.tipo === "especialista" ||
                this.state.tipo === "descripcion") && (
                <form onSubmit={this.handleSubmit}>
                  <Box
                    display="flex"
                    flexDirection="row"
                    p={2}
                    m={1}
                    bgcolor="background.paper"
                  >
                    <Box flexGrow={1}>
                      <Grid item xs={12}>
                        <TextField
                          fullWidth
                          variant="outlined"
                          aria-describedby="my-helper-text"
                          required
                          error={this.state.errores.errorNombre}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                {this.state.tipo === "obra_social" ? (
                                  <i
                                    className="fas fa-star-of-life"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : this.state.tipo === "motivo" ? (
                                  <i
                                    className="fas fa-question-circle"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : this.state.tipo === "diagnostico" ? (
                                  <i
                                    className="fas fa-notes-medical"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : this.state.tipo === "especialista" ? (
                                  <i
                                    className="fas fa-user-md"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : this.state.tipo === "descripcion" ? (
                                  <i
                                    className="fas fa-edit"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                ) : null}
                              </InputAdornment>
                            ),
                          }}
                          id={this.state.tipo}
                          name="nombre"
                          value={this.state.formControls.nombre.value}
                          onChange={this.changeHandler}
                          label={
                            this.state.text
                              ? this.primeraLetraMayuscula(
                                  this.state.text.substring(6)
                                )
                              : this.primeraLetraMayuscula(this.state.tipo)
                          }
                        />
                      </Grid>
                    </Box>
                  </Box>

                  <Box p={2}>
                    <Button
                      style={{
                        marginLeft: "5px",
                        color: "white",
                        backgroundColor: "green",
                      }}
                      variant="contained"
                      size="large"
                      onClick={this.handleSubmit}
                      endIcon={
                        <i
                          style={{ fontSize: "18px" }}
                          className="fas fa-check-circle"
                        ></i>
                      }
                    >
                      {this.state.text ? this.state.text : "Crear Opción"}
                    </Button>
                  </Box>
                </form>
              )}
              {this.state.tipo === "tipo_estudio" && (
                <form onSubmit={this.handleSubmitTipoEstudio}>
                  <Box
                    display="flex"
                    flexDirection="row"
                    p={2}
                    m={1}
                    bgcolor="background.paper"
                  >
                    <Box flexGrow={1}>
                      <Grid item xs={12}>
                        <TextField
                          variant="outlined"
                          fullWidth
                          aria-describedby="my-helper-text"
                          required
                          id="nombre"
                          name="nombre"
                          value={this.state.formControls.nombre.value}
                          onChange={this.changeHandler}
                          label="Nombre del Tipo de estudio"
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <i
                                  style={{ fontSize: "25px" }}
                                  className="fas fa-book-medical"
                                ></i>
                              </InputAdornment>
                            ),
                          }}
                          error={this.state.errores.errorNombre}
                        />
                      </Grid>
                    </Box>
                  </Box>
                  <Box p={2}>
                    <Button
                      style={{
                        marginLeft: "5px",
                        color: "white",
                        backgroundColor: "green",
                      }}
                      size="large"
                      variant="contained"
                      onClick={this.handleSubmitTipoEstudio}
                      endIcon={
                        <i
                          style={{ fontSize: "18px" }}
                          className="fas fa-check-circle"
                        ></i>
                      }
                    >
                      Crear tipo de Estudio
                    </Button>
                  </Box>
                </form>
              )}
              {this.state.tipo === "conclusion" && (
                <form onSubmit={this.handleSubmitConclusion}>
                  <Box
                    display="flex"
                    flexDirection="column"
                    p={2}
                    m={1}
                    bgcolor="background.paper"
                  >
                    <Box flexGrow={1}>
                      <Grid item xs={12}>
                        <TextField
                          variant="outlined"
                          aria-describedby="my-helper-text"
                          required
                          id="escenario"
                          name="escenario"
                          value={this.state.formControls.escenario.value}
                          onChange={this.changeHandler}
                          label="Escenario"
                          multiline
                          fullWidth
                          rows={3}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <i
                                  style={{ fontSize: "25px" }}
                                  className="fas fa-notes-medical"
                                ></i>
                              </InputAdornment>
                            ),
                          }}
                          error={this.state.errores.errorEscenario}
                        />
                      </Grid>
                    </Box>
                    <Box flexGrow={1}>
                      <Grid item xs={12} style={{ paddingTop: 15 }}>
                        <TextField
                          variant="outlined"
                          aria-describedby="my-helper-text"
                          required
                          id="conclusion"
                          name="conclusion"
                          value={this.state.formControls.conclusion.value}
                          onChange={this.changeHandler}
                          label="Conclusión"
                          fullWidth
                          multiline
                          rows={3}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <i
                                  style={{ fontSize: "25px" }}
                                  className="fas fa-file-signature"
                                ></i>
                              </InputAdornment>
                            ),
                          }}
                          error={this.state.errores.errorConclusion}
                        />
                      </Grid>
                    </Box>
                  </Box>
                  <Box p={2}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.handleSubmitConclusion}
                      style={{
                        marginLeft: "5px",
                        color: "white",
                        backgroundColor: "green",
                      }}
                      size="large"
                      endIcon={
                        <i
                          style={{ fontSize: "18px" }}
                          className="fas fa-check-circle"
                        ></i>
                      }
                    >
                      Crear conclusión
                    </Button>
                  </Box>
                </form>
              )}
            </Card>
          </Grid>
        </Grid>

        <ModalOpcion
          abrirModal={this.state.abrirModal}
          cerrarModal={this.cerrarModal}
          mensaje={this.state.mensaje}
          cargandoAccion={this.state.cargandoAccion}
          icon={this.state.icon}
          iconColor={this.state.iconColor}
          botones={this.state.botonesModal}
          desactivarCruz={this.state.desactivarCruz}
        />
      </div>
    );
  }
}
