import React, { Component } from 'react';
import ContenedorSeccion from '../otros/ContenedorSeccion';
import Box from '@material-ui/core/Box';
import InfoOpcionPredefinidaSeleccionada from './InfoOpcionPredefinidaSeleccionada';
import FormularioEliminarOpcionPredefinida from './FormularioEliminarOpcionPredefinida';
import ListIcon from '@material-ui/icons/List';
import CancelIcon from '@material-ui/icons/Edit';

export default class EditarOpcionPredefinida extends Component {
    constructor(props) {
        super(props);

        this.state = {
            breadcrumb: [
                { pathname: '/opciones-predefinidas', 
                    state: '', 
                    texto: 'Opciones predefinidas', 
                    icon: <ListIcon /> },
                { pathname: '/opciones-predefinidas/eliminar_opcion_predefinida', 
                    state: { 
                        opcion: this.props.location.state.opcion 
                    }, 
                    texto: 'Eliminr opción predefinida', 
                    icon: <CancelIcon /> 
                }
            ]
        }
    };

    // Para pasarle el breadcrumb al header
    componentDidMount() {
        this.props.actualizarHeader({
            breadcrumb: this.state.breadcrumb
        })
    }

    render() {

        // Opción predefinida que voy a editar
        const opcion = this.props.location.state.opcion;

        return (
            <ContenedorSeccion
                nombre="Editar opción predefinida"
                descripcion="Podés editar la opción predefinida elegida"
                preComponentes={[
                    <InfoOpcionPredefinidaSeleccionada
                        opcion={opcion}/>
                ]}
                componentes={[
                    <FormularioEliminarOpcionPredefinida 
                        opcion={opcion}
                    />
                ]}
            />
        )
    }
}