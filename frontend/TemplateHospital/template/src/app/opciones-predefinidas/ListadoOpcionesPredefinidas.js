import MUIDataTable from "mui-datatables";
import React, { Component } from "react";
import MenuTareasOpcionesPredefinidas from "./MenuTareasOpcionesPredefinidas";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import ErrorIcon from "@material-ui/icons/Error";
import ModalPaciente from "../pacientes/ModalPaciente";

export default class ListadoOpcionesPrefedinidas extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
      isLoaded: false,

      obrasSociales: [],
      motivos: [],
      tiposDeEstudios: [],
      diagnosticos: [],
      especialistas: [],
      descripciones: [],
      conclusiones: [],

      // Tabla
      columns: [],
      data: [],
      options: [],

      // modal
      abrirModal: false,
      mensaje: "",
      cargandoAccion: false,
      icon: "fas fa-minus-circle",
      iconColor: "rgb(255, 71, 26)",
      botonesModal: [],
      idOpcion: "",
      tipoOpcion: "",
    };

    this.setOpcion = this.setOpcion.bind(this);
    this.fetchOpciones = this.fetchOpciones.bind(this);
    this.textoPlural = this.textoPlural.bind(this);
    this.textoSingular = this.textoSingular.bind(this);
    this.primeraLetraMayuscula = this.primeraLetraMayuscula.bind(this);
    this.armarData = this.armarData.bind(this);
    this.typeChecker = this.typeChecker.bind(this);
    this.eliminarOpcionPredefinida = this.eliminarOpcionPredefinida.bind(this);
    this.eliminarOpcionPredefinidaAPI = this.eliminarOpcionPredefinidaAPI.bind(
      this
    );
    this.cerrarModal = this.cerrarModal.bind(this);

    this.armarColumnas = this.armarColumnas.bind(this);
  }

  // Me trae de la api la opción que quiero:
  // - opcionAPI: tal cual va en la ruta, en el endpoint
  // - opcionEstado: tal cual está en el estado de esta clase
  fetchOpciones = async (opcionAPI, opcionEstado) => {
    var url =
      "http://" +
      process.env.REACT_APP_API_IP +
      "/api/" +
      opcionAPI +
      "?all=True";
    await fetch(url, {
      headers: new Headers({
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          // Les agrego la columna tareas
          var obj = data.body;
          for (var i = 0; i < obj.length; i++) {
            obj[i]["tareas"] = "";
          }
          // Guardo las opciones en el estado
          this.setOpcion(opcionEstado, obj);
        }
      });
  };

  setOpcion(opcionEstado, obj) {
    this.setState(
      {
        [opcionEstado]: obj,
      },
      () => {
        this.setState(
          {
            data: obj,
          },
          () => {
            this.setState({
              isLoaded: true,
            });
          }
        );
      }
    );
  }

  // Chequeo si se seleccionó una opción predefinida desde el form
  componentDidUpdate(prevProps) {
    if (prevProps.opcion.texto !== this.props.opcion.texto) {
      this.setState({
        isLoaded: false
      }, () => {
        this.armarData();
      })
    }
  }

  // Arma la tabla con data temporal, dependiendo qué opción se eligió

  // Debería llamar a la API en cada caso, para no traer todo al pedo
  // y también chequear si el estado de dicha opción ya se cargó alguna vez
  armarData() {
    if (this.props.opcion.texto) {
      var data = [];
      switch (this.props.opcion.texto.toLowerCase()) {
        case "obra social":
          if (!this.state.obrasSociales.length) {
            this.setState(
              {
                isLoaded: false,
              },
              () => this.fetchOpciones("obra_social", "obrasSociales")
            );
          }
          else this.setState({
            isLoaded: true
          });
          data = this.state.obrasSociales;
          break;
        case "motivo de estudio":
          if (!this.state.motivos.length) {
            this.setState(
              {
                isLoaded: false,
              },
              () => this.fetchOpciones("motivo", "motivos")
            );
          }
          else this.setState({
            isLoaded: true
          });
          data = this.state.motivos;
          break;
        case "tipo de estudio":
          if (!this.state.tiposDeEstudios.length) {
            this.setState(
              {
                isLoaded: false,
              },
              () => this.fetchOpciones("tipo_estudio", "tiposDeEstudios")
            );
          }
          else this.setState({
            isLoaded: true
          });
          data = this.state.tiposDeEstudios;
          break;
        case "diagnóstico final de estudio":
          if (!this.state.diagnosticos.length) {
            this.setState(
              {
                isLoaded: false,
              },
              () => this.fetchOpciones("diagnostico", "diagnosticos")
            );
          }
          else this.setState({
            isLoaded: true
          });
          data = this.state.diagnosticos;
          break;
        case "médico especialista":
          if (!this.state.especialistas.length) {
            this.setState(
              {
                isLoaded: false,
              },
              () => this.fetchOpciones("especialista", "especialistas")
            );
          }
          else this.setState({
            isLoaded: true
          });
          data = this.state.especialistas;
          break;
        case "conclusión predefinida":
          if (!this.state.conclusiones.length) {
            this.setState(
              {
                isLoaded: false,
              },
              () => this.fetchOpciones("conclusion", "conclusiones")
            );
          }
          else this.setState({
            isLoaded: true
          });
          data = this.state.conclusiones;
          break;
        case "descripción predefinida":
          if (!this.state.descripciones.length) {
            this.setState(
              {
                isLoaded: false,
              },
              () => this.fetchOpciones("descripcion", "descripciones")
            );
          }
          else this.setState({
            isLoaded: true
          });
          data = this.state.descripciones;
          break;
      }
      this.setState(
        {
          data: data,
        }
      )
    }
  }

  typeChecker() {
    if (this.props.opcion.texto) {
      if (this.props.opcion.texto.toLowerCase() === "tipo de estudio") {
        return "nombre_mostrado";
      } else {
        if (
          this.props.opcion.texto.toLowerCase() === "conclusión predefinida"
        ) {
          return "escenario";
        } else {
          return "nombre";
        }
      }
    } else {
      return "nombre";
    }
  }

  conclusionPredefinidaLabel() {
    if (this.props.opcion.texto) {
      if (this.props.opcion.texto === "Conclusion predefinida") {
        return "Conclusión";
      } else return null;
    }
  }
  conclusionPredefinidaNombre() {
    if (this.props.opcion.texto) {
      if (this.props.opcion.texto === "Conclusion predefinida") {
        return "conclusion";
      } else return null;
    }
  }

  // Devuelve el texto en singular
  textoSingular() {
    if (this.props.opcion.texto) {
      if (this.props.opcion.texto === "Conclusión predefinida") {
        return "Escenario";
      } else {
        return this.props.opcion.texto;
      }
    } else return "";
  }
  // Devuelve el texto en plural
  textoPlural() {
    if (this.props.opcion.textoPlural) return this.props.opcion.textoPlural;
    else return "";
  }
  // Devuelve el texto singular con la primera letra mayúscula
  primeraLetraMayuscula(texto) {
    return texto.charAt(0).toUpperCase() + texto.slice(1);
  }

  cerrarModal() {
    if (this.state.idOpcion === "terminado") {
      window.location.reload();
    } else {
      this.setState({
        abrirModal: false,
      });
    }
  }

  eliminarOpcionPredefinida(opcion) {
    this.setState(
      {
        idOpcion: opcion.valor.id,
        tipoOpcion: opcion.tipo,
      },
      () => {
        this.setState(
          {
            botonesModal: [
              {
                texto: "Confirmar",
                color: "green",
                onClick: this.eliminarOpcionPredefinidaAPI,
              },
              {
                texto: "Rechazar",
                color: "crimson",
                onClick: this.cerrarModal,
              },
            ],
            mensaje: (
              <div style={{ textAlign: "center" }}>
                <h2 style={{ color: "rgb(255, 71, 26)" }}>¡Cuidado!</h2>
                <h5>
                  Está a punto de eliminar la siguiente{" "}
                  <b>opción predefinida</b>:
                </h5>
                <ul style={{ textAlign: "left", marginLeft: 100 }}>
                  <li>
                    <h5>
                      Tipo:{" "}
                      <b>
                        {opcion.tipo === "Escenario"
                          ? "Conclusión predefinida"
                          : opcion.tipo}
                      </b>
                    </h5>
                  </li>
                  {opcion.tipo !== "Escenario" ? (
                    <li>
                      <h5>
                        Nombre:{" "}
                        <b>
                          {opcion.tipo === "Tipo de estudio"
                            ? opcion.valor.nombre_mostrado
                            : opcion.valor.nombre}
                        </b>
                      </h5>
                    </li>
                  ) : (
                      <div>
                        <li>
                          <h5>
                            Escenario: <b>{opcion.valor.escenario}</b>
                          </h5>
                        </li>
                        <li>
                          <h5>
                            Conclusión: <b>{opcion.valor.conclusion}</b>
                          </h5>
                        </li>
                      </div>
                    )}
                </ul>
                <h5>
                  <h5>
                    Esta acción es <b>permanente</b> y{" "}
                    <b>NO se puede deshacer</b>
                  </h5>
                </h5>
              </div>
            ),
          },
          () => {
            this.setState({ abrirModal: true });
          }
        );
      }
    );
  }

  armarColumnas() {
    var arr = [];
    arr.push(
      {
        name: "tareas",
        label: "Tareas",
        options: {
          filter: false,
          customBodyRenderLite: (dataIndex) => {
            return (
              <MenuTareasOpcionesPredefinidas
                opcion={{
                  tipo: this.textoSingular(),
                  valor: this.state.data[dataIndex],
                }}
                funcEliminar={this.eliminarOpcionPredefinida}
              />
            );
          },
        },
      },
      {
        name: "id",
        label: "ID",
        options: {
          display: false,
        },
      },
      {
        name: this.typeChecker(),
        label: this.textoSingular(),
      }
    );
    if (this.props.opcion.texto === "Conclusión predefinida") {
      arr.push({
        name: "conclusion",
        label: "Conclusión",
      });
    }
    return arr;
  }

  async eliminarOpcionPredefinidaAPI() {
    this.setState({ cargandoAccion: true }, async () => {
      const keywords = {
        "Obra social": "obra_social",
        "Motivo de estudio": "motivo",
        "Tipo de estudio": "tipo_estudio",
        "Diagnóstico final de estudio": "diagnostico",
        "Médico especialista": "especialista",
        "Descripción predefinida": "descripcion",
        Escenario: "conclusion",
      };
      await fetch(
        `http://${process.env.REACT_APP_API_IP}/api/${
          keywords[this.state.tipoOpcion]
        }?id=${this.state.idOpcion}`,
        {
          headers: {
            Authorization:
              "Bearer " +
              document.cookie.replace(
                /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                "$1"
              ),
            "Content-Type": "application/json",
          },
          method: "DELETE",
        }
      )
        .then((res) => res.json())
        .then((data) => {
          if (data.status === 401) {
            localStorage.setItem("alert", "Expired/Invalid JWT Token");
            document.cookie =
              "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            window.location.href = "/user-pages/login-1";
          } else {
            if (data.status === 200) {
              this.setState(
                {
                  icon: "fas fa-check-circle",
                  iconColor: "green",
                  botonesModal: [
                    {
                      texto: "Aceptar",
                      onClick: this.cerrarModal,
                      color: "green",
                    },
                  ],
                  mensaje: <span>{data.body}.</span>,
                  idOpcion: "terminado",
                },
                () => {
                  this.setState({ cargandoAccion: false });
                }
              );
            } else {
              if (data.status === 400) {
                this.setState({ idOpcion: "" }, () => {
                  this.setState({
                    mensaje: (
                      <div>
                        <h2 style={{ color: "rgb(255, 153, 0)" }}>
                          Esta opción predefinida no se puede eliminar
                        </h2>
                        <h5>
                          Esta opción predefinida no se puede eliminar porque
                          tiene estudios asociados.
                        </h5>
                        <h5 style={{ marginBottom: 10 }}>
                          Si realmente desea eliminar la opción predefinida,
                          <br></br> elimine primero todos los estudios asociados
                          a la misma.
                        </h5>
                      </div>
                    ),
                    icon: "fas fa-exclamation-circle",
                    iconColor: "rgb(255, 153, 0)",
                    botonesModal: [
                      {
                        texto: "Entendido",
                        color: "#297ced",
                        onClick: this.cerrarModal,
                      },
                    ],
                    cargandoAccion: false,
                  });
                });
              } else {
                this.setState(
                  {
                    botonesModal: [
                      {
                        texto: "Reintentar",
                        color: "green",
                        onClick: this.eliminarOpcionPredefinidaAPI,
                      },
                      {
                        texto: "Cancelar",
                        color: "crimson",
                        onClick: this.cerrarModal,
                      },
                    ],
                    mensaje: "Hubo un problema, por favor intentá de nuevo",
                    icon: "fas fa-times-circle",
                    iconColor: "#dc004e",
                  },
                  () => {
                    this.setState({ cargandoAccion: false });
                  }
                );
              }
            }
          }
        });
    });
  }

  render() {
    const options = {
      filterType: "textField",
      selectableRows: false,
      download: false,
      print: false,
      searchOpen: true,
      searchPlaceholder: "",
      textLabels: {
        body: {
          noMatch: "No se registraron " + this.textoPlural().toLowerCase(),
          toolTip: "Ordenar",
          columnHeaderTooltip: (column) => `Sort for ${column.label}`,
        },
        pagination: {
          next: "Siguiente página",
          previous: "Página anterior",
          rowsPerPage:
            this.primeraLetraMayuscula(this.textoPlural()) + " por página:",
          displayRows: "de",
        },
        toolbar: {
          search: "Buscar " + this.textoSingular().toLowerCase(),
          downloadCsv: "Download CSV",
          print: "Print",
          viewColumns: "Elegir qué columnas ver",
          filterTable: "Filtrar tabla",
        },
        filter: {
          all: "All",
          title: "COLUMNAS",
          reset: "Reiniciar todos",
        },
        viewColumns: {
          title: "Columnas",
          titleAria: "Mostrar/Ocultar columnas",
        },
        selectedRows: {
          text: "fila/s seleccionada/s",
          delete: "Eliminar",
          deleteAria: "Eliminar filas seleccionadas",
        },
      },
    };

    const columns = this.armarColumnas();

    return (
      <div>
        {
          // Si ya seleccionó un tipo en el select...
          !this.props.seleccionoTipo ? (
            <div id="opciones-predefinidas-tabla-contenedor">
              <div id="mensaje-opciones-predefinidas">
                <span>Primero seleccioná una opción predefinida.</span>
              </div>
            </div>
          ) : // Una vez que se cargaron los datos...
            this.state.isLoaded ? (
              // si hay datos
              this.state.data && this.state.data !== "" ? (
                <MUIDataTable
                  title={"Listado de " + this.textoPlural().toLowerCase()}
                  data={this.state.data}
                  columns={columns}
                  options={options}
                />
              ) : (
                  <Grid
                    container
                    spacing={2}
                    style={{ margin: "20px 5px", alignItems: "center" }}
                  >
                    <Grid item>
                      <ErrorIcon color="disabled" />
                    </Grid>
                    <Grid item>
                      <Typography>
                        No se registraron{" " + this.textoPlural().toLowerCase()}.
                  </Typography>
                    </Grid>
                  </Grid>
                )
            ) : (
                <Grid
                  container
                  spacing={4}
                  style={{ margin: "20px 5px", alignItems: "center" }}
                >
                  <Grid item>
                    <CircularProgress></CircularProgress>
                  </Grid>
                  <Grid item>
                    <Typography>
                      Cargando {this.textoPlural().toLowerCase()}...
                </Typography>
                  </Grid>
                </Grid>
              )
        }
        <ModalPaciente
          abrirModal={this.state.abrirModal}
          cerrarModal={this.cerrarModal}
          mensaje={this.state.mensaje}
          cargandoAccion={this.state.cargandoAccion}
          icon={this.state.icon}
          iconColor={this.state.iconColor}
          botones={this.state.botonesModal}
        />
      </div>
    );
  }
}
