import { PDFViewer } from "@react-pdf/renderer";
import React, { Component, Suspense, lazy } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import Spinner from "../app/shared/Spinner";

/* ////////////// Del template ////////////// */

const Buttons = lazy(() => import("./basic-ui/Buttons"));
const Dropdowns = lazy(() => import("./basic-ui/Dropdowns"));
const Typography = lazy(() => import("./basic-ui/Typography"));
const FontAwesome = lazy(() => import("./icons/FontAwesome"));

const BasicElements = lazy(() => import("./form-elements/BasicElements"));

const Error404 = lazy(() => import("./user-pages/Error404"));
const Error500 = lazy(() => import("./user-pages/Error500"));

const Login = lazy(() => import("./user-pages/Login"));
const Register1 = lazy(() => import("./user-pages/Register"));

const BlankPage = lazy(() => import("./user-pages/BlankPage"));

/* ////////////// Nuevo ////////////// */

// Sección Pacientes
const Home = lazy(() => import("./home/Home"));
const Pacientes = lazy(() => import("./pacientes/Pacientes"));
const CrearPaciente = lazy(() => import("./pacientes/CrearPaciente"));
const EditarPaciente = lazy(() => import("./pacientes/EditarPaciente"));
const CrearEstudio = lazy(() => import("./pacientes/CrearEstudio"));
const EditarEstudio = lazy(() => import("./pacientes/EditarEstudio"));
const HistoriaClinica = lazy(() => import("./pacientes/HistoriaClinica"));

const EstudioCompleto = lazy(() => import("./pacientes/EstudioCompleto"));

// Sección Opciones predefinidas
const OpcionesPredefinidas = lazy(() =>
  import("./opciones-predefinidas/OpcionesPredefinidas")
);
const EditarOpcionPredefinida = lazy(() =>
  import("./opciones-predefinidas/EditarOpcionPredefinida")
);
const CrearOpcionPredefinida = lazy(() =>
  import("./opciones-predefinidas/CrearOpcionPredefinida")
);
const EliminarOpcionPredefinida = lazy(() =>
  import("./opciones-predefinidas/EliminarOpcionPredefinida")
);

// Sección Estadísticas
const Estadisticas = lazy(() => import("./estadisticas/Estadisticas"));

class AppRoutes extends Component {
  constructor(props) {
    super(props);

    this.actualizarHeader = this.actualizarHeader.bind(this);
  }

  actualizarHeader(breadcrumb) {
    this.props.actualizarHeader(breadcrumb);
  }

  render() {
    return (
      <Suspense fallback={<Spinner />}>
        <Switch>
          <Route
            exact
            path="/"
            render={(props) => (
              <Home {...props} actualizarHeader={this.actualizarHeader} />
            )}
          />

          <Route path="/basic-ui/buttons" component={Buttons} />
          <Route path="/basic-ui/dropdowns" component={Dropdowns} />
          <Route path="/basic-ui/typography" component={Typography} />
          <Route path="/icons/font-awesome" component={FontAwesome} />

          <Route
            path="/form-Elements/basic-elements"
            component={BasicElements}
          />

          <Route path="/user-pages/login-1" component={Login} />
          <Route path="/user-pages/register-1" component={Register1} />

          <Route path="/user-pages/error-404" component={Error404} />
          <Route path="/user-pages/error-500" component={Error500} />

          <Route path="/user-pages/blank-page" component={BlankPage} />

          {/* Sección Pacientes */}
          <Route
            exact
            path="/pacientes"
            render={(props) => (
              <Pacientes {...props} actualizarHeader={this.actualizarHeader} />
            )}
          />
          <Route
            exact
            path="/pacientes/crear-paciente"
            render={(props) => (
              <CrearPaciente
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />
          <Route
            exact
            path="/pacientes/editar-paciente"
            render={(props) => (
              <EditarPaciente
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />
          <Route
            exact
            path="/pacientes/crear-estudio"
            render={(props) => (
              <CrearEstudio
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />
          <Route
            exact
            path="/pacientes/historia-clinica/editar-estudio"
            render={(props) => (
              <EditarEstudio
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />
          <Route
            exact
            path="/pacientes/historia-clinica"
            render={(props) => (
              <HistoriaClinica
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />

          <Route
            path="/pacientes/historia-clinica/ver-estudio"
            render={(props) => (
              <EstudioCompleto
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />

          {/* Sección Opciones predefinidas */}
          <Route
            exact
            path="/opciones-predefinidas"
            render={(props) => (
              <OpcionesPredefinidas
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />
          <Route
            exact
            path="/opciones-predefinidas/crear"
            render={(props) => (
              <CrearOpcionPredefinida
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />
          <Route
            exact
            path="/opciones-predefinidas/editar-opcion-predefinida"
            render={(props) => (
              <EditarOpcionPredefinida
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />
          <Route
            exact
            path="/opciones-predefinidas/eliminar-opcion-predefinida"
            render={(props) => (
              <EliminarOpcionPredefinida
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />

          {/* Sección Estadísticas */}
          <Route
            exact
            path="/estadisticas"
            render={(props) => (
              <Estadisticas
                {...props}
                actualizarHeader={this.actualizarHeader}
              />
            )}
          />

          {/* Sección Home */}
          <Redirect exact to="/" />
        </Switch>
      </Suspense>
    );
  }
}

export default AppRoutes;
