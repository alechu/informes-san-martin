import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import MUIDataTable from "mui-datatables";
import MenuTareasEstudio from "./MenuTareasEstudio";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { Typography } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import ErrorIcon from "@material-ui/icons/Error";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import BotonCrearEstudio from "./BotonCrearEstudio";
import ModalPaciente from "./ModalPaciente";

// Icons
import CantEstudiosIcon from "@material-ui/icons/Description";

export default function ListadoHistoriaClinica(props) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [estudios, setEstudios] = useState([]);
  const [estudiosTabla, setEstudiosTabla] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [tieneEstudios, setTieneEstudios] = useState(false);
  const [paciente, setPaciente] = useState(null);
  const [abrirModal, setAbrirModal] = useState(false);
  const [mensajeModal, setMensajeModal] = useState("");
  const [cargandoAccion, setCargandoAccion] = useState(false);
  const [icon, setIcon] = useState("fas fa-minus-circle");
  const [iconColor, setIconColor] = useState("rgb(255, 71, 26)");
  const [botonesModal, setBotonesModal] = useState([]);
  const [cantEstudios, setCantEstudios] = useState(null);
  const [porPagina, setPorPagina] = useState(10);
  const [mostrarSnackbar, setMostrarSnackbar] = useState(false);
  const [desactivarCruz, setDesactivarCruz] = useState(false); // para el modal

  const history = useHistory();

  /*
    Estructura del this.props.paciente:

    id, nombre, apellido, dni, fecha_nac, sexo, estado, localidad, obra_social_id, obra_social_nombre

  */

  /* Me traigo los estudios del paciente
  ENDPOINT: /api/paciente/<id_paciente>/estudios?page=<int>&perPage=<int>
  */

  const fetchCantEstudios = async () => {
    var url =
      "http://" +
      process.env.REACT_APP_API_IP +
      "/api/paciente/" +
      props.paciente.id +
      "/estudios";
    await fetch(url, {
      headers: new Headers({
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCantEstudios(data.body.total);
      });
  };

  var idEstudio = "";
  const fetchItems = async () => {
    await fetch(
      "http://" +
        process.env.REACT_APP_API_IP +
        "/api/paciente/" +
        props.paciente.id +
        "/estudios?page=1&perPage=9999999999",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          history.push("/user-pages/login-1");
        } else {
          // Guardo los estudios en el estado
          setEstudios(data.body.estudios);
          // Armo la tabla (columnas y datos)
          armarTabla(data.body.estudios);
        }
      });
  };

  const cerrarModal = () => {
    if (idEstudio !== "") {
      window.location.reload();
    } else {
      setAbrirModal(false);
    }
  };

  const funcEliminar = (id_estudio) => {
    idEstudio = id_estudio;
    setDesactivarCruz(false);
    setBotonesModal([
      {
        texto: "Confirmar",
        color: "green",
        onClick: eliminarEstudioAPI,
      },
      {
        texto: "Rechazar",
        color: "crimson",
        onClick: cerrarModal,
      },
    ]);
    setMensajeModal(
      <div>
        <h2 style={{ color: "rgb(255, 71, 26)" }}>¡Cuidado!</h2>
        <h5>Está a punto de eliminar un Estudio</h5>
        <h5>
          Esta acción es <b>permanente</b> y <b>NO se puede deshacer</b>
        </h5>
      </div>
    );
    setAbrirModal(true);
  };

  const eliminarEstudioAPI = async () => {
    setCargandoAccion(true);
    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/estudio?id=" + idEstudio,
      {
        method: "DELETE",
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 200) {
          fetchCantEstudios();
          setDesactivarCruz(true);
          setBotonesModal([
            {
              texto: "Aceptar",
              color: "green",
              onClick: cerrarModal,
            },
          ]);
          setMensajeModal("Estudio eliminado exitosamente");
          setIcon("fas fa-check-circle");
          setIconColor("green");
          setCargandoAccion(false);
        } else {
          if (data.status === 401) {
            localStorage.setItem("alert", "Expired/Invalid JWT Token");
            document.cookie =
              "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            history.push("/user-pages/login-1");
          } else {
            setDesactivarCruz(true);
            setBotonesModal([
              {
                texto: "Reintentar",
                color: "green",
                onClick: eliminarEstudioAPI,
              },
              {
                texto: "Cancelar",
                color: "crimson",
                onClick: cerrarModal,
              },
            ]);
            setMensajeModal("Hubo un problema, por favor intentá de nuevo");
            setIcon("fas fa-times-circle");
            setIconColor("#dc004e");
            setCargandoAccion(false);
          }
        }
      });
  };

  // Columnas de la tabla
  const columns = [
    {
      name: "tareas",
      label: "Tareas",
      options: {
        filter: false,
        customBodyRenderLite: (dataIndex) => {
          return (
            <MenuTareasEstudio
              estudio={estudios[dataIndex].estudio}
              paciente={props.paciente}
              funcEliminar={funcEliminar}
            />
          );
        },
      },
    },
    {
      name: "fecha de estudio",
      label: "Fecha de estudio",
    },
    {
      name: "tipo de estudio",
      label: "Tipo de estudio",
    },
    {
      name: "motivo",
      label: "Motivo",
    },
  ];

  const armarTabla = (estudios) => {
    if (estudios) {
      var estudiosParaTabla = [];
      // Armo cada estudio y lo pusheo a la var temporal estudiosParaTabla
      for (var e of estudios) {
        /* La estructura de un estudio es:
  
        e.estudio.informacion_general
          id, fecha, descripcion, especialista, tipo, motivo, diagnostico_obligatorio, diagnostico_opcional_1, diagnostico_opcional_2
  
        e. estudio.atributos
          [varían]
        
        */
        var estudio = {
          tareas: "",
          "fecha de estudio": e.estudio.informacion_general.fecha,
          "tipo de estudio": e.estudio.informacion_general.tipo.nombre_mostrado,
          motivo: e.estudio.informacion_general.motivo.nombre,
        };
        estudiosParaTabla.push(estudio);
      }
      // Los asigno al estado para mandar al MUI
      setEstudiosTabla(estudiosParaTabla);
      setTieneEstudios(true);
    } else {
      setTieneEstudios(false);
    }
    setIsLoaded(true);
  };

  useEffect(() => {
    if (props.location === undefined) setPaciente(props.paciente);
    else setPaciente(props.location.state.data.paciente);
    fetchItems();
    fetchCantEstudios();
  }, []);

  /* Abrir tareas */
  const abrirTareas = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const cerrarTareas = () => {
    setAnchorEl(null);
  };

  const cambioCantidadPorPagina = (cant) => {
    setPorPagina(cant);
    setMostrarSnackbar(true);
  };

  const cerrarSnackbar = () => {
    setMostrarSnackbar(false);
  };

  const options = {
    filterType: "textField",
    selectableRows: false,
    download: false,
    print: false,
    searchOpen: true,
    searchPlaceholder: "Buscá por cualquier atributo",
    onChangeRowsPerPage: cambioCantidadPorPagina,
    rowsPerPageOptions: [5, 10, 15, 20, 30, 50],
    textLabels: {
      body: {
        noMatch: "No se encontró ningún estudio",
        toolTip: "Ordenar",
        columnHeaderTooltip: (column) => `Sort for ${column.label}`,
      },
      pagination: {
        next: "Siguiente página",
        previous: "Página anterior",
        rowsPerPage: "Estudios por página:",
        displayRows: "de",
      },
      toolbar: {
        search: "Buscar estudio",
        downloadCsv: "Download CSV",
        print: "Print",
        viewColumns: "Elegir qué columnas ver",
        filterTable: "Filtrar tabla",
      },
      filter: {
        all: "All",
        title: "COLUMNAS",
        reset: "Reiniciar todos",
      },
      viewColumns: {
        title: "Columnas",
        titleAria: "Mostrar/Ocultar columnas",
      },
      selectedRows: {
        text: "fila/s seleccionada/s",
        delete: "Eliminar",
        deleteAria: "Eliminar filas seleccionadas",
      },
    },
  };

  return (
    <div>
      {isLoaded && tieneEstudios && (
        <Box display="flex" alignItems="center">
          <BotonCrearEstudio paciente={props.paciente} />
          <Box mx={3}>
            <Box>
              <CantEstudiosIcon
                style={{
                  fontSize: 22,
                }}
                color="inherit"
              />{" "}
              {cantEstudios ? (
                <span
                  style={{
                    color: "#297ced",
                    fontSize: 21,
                    fontWeight: 900,
                    verticalAlign: "middle",
                  }}
                >
                  {cantEstudios}
                </span>
              ) : (
                <CircularProgress />
              )}
            </Box>
            <Divider
              style={{
                marginBottom: 1,
              }}
            ></Divider>
            <Box>
              <Typography
                variant="button"
                style={{
                  color: "gray",
                  fontSize: 12,
                }}
              >
                Cantidad de estudios de <strong>{props.paciente.nombre}</strong>
              </Typography>
            </Box>
          </Box>
        </Box>
      )}
      {isLoaded ? (
        tieneEstudios ? (
          <MUIDataTable
            title={"Historia clínica"}
            data={estudiosTabla}
            columns={columns}
            options={options}
          />
        ) : (
          <div>
            <Grid
              container
              spacing={2}
              style={{ margin: "20px 5px", alignItems: "center" }}
            >
              <Grid item>
                <ErrorIcon color="disabled" />
              </Grid>
              <Grid item>
                <Typography>
                  {props.paciente.nombre} no tiene estudios asociados.
                </Typography>
              </Grid>
            </Grid>
            <Link
              to={{
                pathname: "/pacientes/crear-estudio",
                state: { paciente: props.paciente },
              }}
            >
              <Button variant="contained" color="primary">
                Crearle un estudio
              </Button>
            </Link>
          </div>
        )
      ) : (
        <Grid
          container
          spacing={4}
          style={{ margin: "20px 5px", alignItems: "center" }}
        >
          <Grid item>
            <CircularProgress></CircularProgress>
          </Grid>
          <Grid item>
            <Typography>Cargando estudios...</Typography>
          </Grid>
        </Grid>
      )}

      <ModalPaciente
        abrirModal={abrirModal}
        cerrarModal={cerrarModal}
        mensaje={mensajeModal}
        cargandoAccion={cargandoAccion}
        icon={icon}
        iconColor={iconColor}
        botones={botonesModal}
        desactivarCruz={desactivarCruz}
      />

      {/* Snackbar para avisar el cambio de perPage */}
      <Snackbar
        open={mostrarSnackbar}
        autoHideDuration={3000}
        onClose={cerrarSnackbar}
      >
        <Alert variant="filled" onClose={cerrarSnackbar} severity="success">
          Ahora se muestran {porPagina} estudios por página
        </Alert>
      </Snackbar>
    </div>
  );
}
