import React from 'react';
import { Page, Text, View, Document, StyleSheet, PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';
import { withStyles } from "@material-ui/core/styles";

const fontSize = 12;

// Create styles
const styles = StyleSheet.create({
    sectionPaciente: {
        margin: '6px 0 6px'
    },
    tituloPaciente: {
        fontSize: fontSize - 2,
        color: '#163b88',
        fontWeight: '600'
    },
    titulo: {
        fontSize: fontSize - 2,
        fontWeight: '600',
        marginBottom: 5
    },
    valor: {
        fontSize: fontSize - 2,
        fontWeight: 800
    },

    sectionEstudio: {
        marginBottom: 5
    },

    contenedorAtributo: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    textoDefecto: {
        fontSize: fontSize - 2,
        marginBottom: 3,
        color: '#444444'
    },

    tituloConclusion: {
        fontSize: fontSize - 2,
        fontWeight: '600'
    }
});

const dividerGrueso = (
    <View
        style={{
            marginTop: '7px',
            borderBottomColor: '#676767',
            borderBottomWidth: 1,
        }}
    />
);

export default function PDFInfoEstudioDisincronia(props) {
    return (
        <View>
            <View style={styles.sectionPaciente}>
                <View style={{ marginTop: 2, marginBottom: 3 }}>
                    <Text style={styles.titulo}>Indicadores de discincronía Intraventricular izquierda</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textoDefecto}>
                            Modo M: retraso septal-PP: {props.estudio.atributos.retraso_septal} ms.
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textoDefecto}>
                            Doppler pulsado tisular (DTcolor): valoración del tiempo al pico sistólico (Onda S) entre septum-pared lateral: {props.estudio.atributos.dpt} ms.
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textoDefecto}>
                            Speckle tracking radial: valoración del tiempo al pico de deformación en segmentos opuestos: {props.estudio.atributos.str} ms.
                        </Text>
                    </View>
                </View>
                <View style={{ marginTop: 7, marginBottom: 3 }}>
                    <Text style={styles.titulo}>Indicadores de discincronía Interventricular</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textoDefecto}>
                            La diferencia entre los periodos preeyectivos de ambos ventrículos fue de {props.estudio.atributos.diferencia_periodos} ms, considerándose discincronía interventricular un valor {'>'} a 40 ms o la presencia de PPEy VI {'>'} 140 ms.
                        </Text>
                    </View>
                </View>
                <View style={{ marginTop: 7, marginBottom: 3 }}>
                    <Text style={styles.titulo}>Indicadores de alteración en la secuencia aurículo-ventricular</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textoDefecto}>
                            Se valoró la duración del llenado diastólico en relación a la duración total del R-R, siendo de {props.estudio.atributos.llenado_distolico}%, considerándose asincronía AV una relación {'<'} 40%, o bien la superposición de las ondas diastólicas.
                        </Text>
                    </View>
                </View>

                <View style={{ marginTop: 7, marginBottom: 3 }}>
                    <Text style={styles.tituloConclusion}>Conclusión</Text>
                    <Text style={styles.textoDefecto}>{props.estudio.atributos.conclusion}</Text>
                </View>
            </View>
        </View>
    )
}