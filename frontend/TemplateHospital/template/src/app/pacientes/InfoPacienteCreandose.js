import React, { Component } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";

export default class InfoPacienteCreandose extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mostrarDatos: false
    }
  }

  hayDatos() {
    if (this.props.paciente) {
      var paciente = this.props.paciente;
      if (paciente.apellido || paciente.nombre ||
        paciente.dni || paciente.sexo ||
        paciente.estado || (paciente.fecha_nac && !isNaN(paciente.fecha_nac)) ||
        paciente.obra_social.id || paciente.localidad) {
        this.setState({
          mostrarDatos: true
        })
      }
      else {
        this.setState({
          mostrarDatos: false
        })
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.paciente !== this.props.paciente) {
      this.hayDatos();
    }
  }

  render() {
    const paciente = this.props.paciente;
    const tarea = this.props.tarea;

    const checkDatos = () => {
      if (paciente) {
        return (
          <p style={{ color: "gray" }}>
            Se mostrarán los datos a medida que llenes los campos.
          </p>
        );
      }
    };

    const mostrarNombre = () => {
      checkDatos();
      if (paciente.apellido || paciente.nombre) {
        if (paciente.apellido && !paciente.nombre) {
          return (
            <h5>
              <strong>{paciente.apellido}</strong>
            </h5>
          );
        } else if (paciente.nombre && !paciente.apellido) {
          return <h5>{paciente.nombre}</h5>;
        } else {
          return (
            <h5>
              <strong>{paciente.apellido}</strong>, {paciente.nombre}
            </h5>
          );
        }
      }
    };

    const mostrarDNI = () => {
      checkDatos();
      if (paciente.dni) {
        return <h5>DNI: {paciente.dni}</h5>;
      }
    };

    const mostrarSexo = () => {
      checkDatos();
      if (paciente.sexo) {
        return <h5>Sexo: {paciente.sexo}</h5>;
      }
    };

    const mostrarEstado = () => {
      checkDatos();
      if (paciente.estado) {
        return <h5>Estado: {paciente.estado}</h5>;
      }
    };

    const mostrarFechaNacimiento = () => {
      checkDatos();
      if (paciente.fecha_nac && !isNaN(paciente.fecha_nac)) {
        var fecha =
          paciente.fecha_nac.getDate() + '/' +
          (paciente.fecha_nac.getMonth() + 1).toString() + '/' +
          paciente.fecha_nac.getFullYear();

        return <h5>Fecha de nacimiento: {fecha}</h5>;
      }
    };

    const mostrarObraSocial = () => {
      checkDatos();
      if (paciente.obra_social.id) {
        return <h5>Obra social: {paciente.obra_social.nombre}</h5>;
      }
    };

    const mostrarLocalidad = () => {
      checkDatos();
      if (paciente.localidad) {
        return <h5>Localidad: {paciente.localidad}</h5>;
      }
    };

    return (
      <Box p={1} m={1}>
        <h4 style={{ fontSize: 18 }}>Paciente a {tarea == "crear" ? "agregar" : "editar"}</h4>
        <Divider></Divider>
        {this.state.mostrarDatos
          ? (
            <div style={{ padding: '20px 0px 0px' }}>
              {mostrarNombre()}
              {mostrarDNI()}
              {mostrarSexo()}
              {mostrarEstado()}
              {mostrarFechaNacimiento()}
              {mostrarObraSocial()}
              {mostrarLocalidad()}
            </div>
          )
          : <p style={{
            color: '#9c9c9c',
            fontSize: 15,
            marginTop: 5,
            marginBottom: 5
          }}>
            Primero ingresá los datos del paciente
            </p>
        }
      </Box>
    );
  }
}
