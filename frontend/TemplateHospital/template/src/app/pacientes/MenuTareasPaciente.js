import React, { Component } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Divider from "@material-ui/core/Divider";
import Box from "@material-ui/core/Box";
import { Link } from "react-router-dom";
import Fade from "@material-ui/core/Fade";
import { withStyles } from "@material-ui/core/styles";

// Icons
import NoteAddIcon from "@material-ui/icons/NoteAdd";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

const styles = (theme) => ({
  menuItem: {
    "&:hover": {
      background: "#297ced",
      color: "white",
    },
  },
  menuItemBorrar: {
    color: "red",
    "&:hover": {
      background: "red",
      color: "white",
    },
  },
  menuItemLink: {
    "&:hover": {
      textDecoration: "none",
    },
  },
});

class MenuTareasPaciente extends Component {
  constructor(props) {
    super(props);
    this.abrirTareas = this.abrirTareas.bind(this);
    this.cerrarTareas = this.cerrarTareas.bind(this);

    this.state = {
      anchorEl: null,
      cantEstudios: null,
      cantEstudiosLoaded: false,
    };
  }

  componentDidMount() {
    this.fetchCantEstudios();
  }

  componentDidUpdate(prevProps) {
    // Esto es para actualizar la cant de estudios,
    // sino no lo detectaba
    //console.log("Prev: " + prevProps.actualizar + "| Actual: " + this.props.actualizar);
    if (prevProps.actualizar !== this.props.actualizar) {
      this.fetchCantEstudios();
    }
  }

  // Me traigo la cant de estudios del paciente
  // para mostrarlo en el MenuItem
  fetchCantEstudios = async () => {
    var url =
      "http://" +
      process.env.REACT_APP_API_IP +
      "/api/paciente/" +
      this.props.paciente.id +
      "/estudios";
    await fetch(url, {
      headers: new Headers({
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          cantEstudios: data.body.total,
          cantEstudiosLoaded: true,
        });
      });
  };

  // Abro dropdown
  abrirTareas = (event) => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };
  // Cierro dropdown
  cerrarTareas = () => {
    this.setState({
      anchorEl: null,
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
        <button
          className="btn btn-success"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={this.abrirTareas}
        >
          Tareas
        </button>
        <Menu
          disableScrollLock={true}
          id="menu-tareas"
          anchorEl={this.state.anchorEl}
          keepMounted
          TransitionComponent={Fade}
          open={Boolean(this.state.anchorEl)}
          onClose={this.cerrarTareas}
        >
          <Link
            to={{
              pathname: "/pacientes/crear-estudio",
              state: { paciente: this.props.paciente },
            }}
            className={classes.menuItemLink}
          >
            <MenuItem className={classes.menuItem}>
              <Box>
                <NoteAddIcon />
              </Box>
              <Box mx={1}>
                <span>Crear estudio</span>
              </Box>
            </MenuItem>
          </Link>
          <Link
            to={{
              pathname: "/pacientes/historia-clinica",
              state: {
                paciente: this.props.paciente,
              },
            }}
            className={classes.menuItemLink}
          >
            <MenuItem className={classes.menuItem}>
              <Box>
                <LibraryBooksIcon />
              </Box>
              <Box mx={1}>
                <span>Ver historia clínica</span>
              </Box>
            </MenuItem>
          </Link>
          <Box
            style={{
              fontSize: 12,
              margin: "0 25px 5px 50px",
              color: "#555555",
            }}
          >
            {!this.state.cantEstudios ? (
              <span>No tiene estudios</span>
            ) : this.state.cantEstudios == 1 ? (
              <span>
                Tiene <strong>1</strong> estudio
              </span>
            ) : (
              <span>
                Tiene <strong>{this.state.cantEstudios}</strong> estudios
              </span>
            )}
          </Box>
          <Divider />
          <Link
            to={{
              pathname: "/pacientes/editar-paciente",
              state: { paciente: this.props.paciente },
            }}
            className={classes.menuItemLink}
          >
            <MenuItem className={classes.menuItem}>
              <Box>
                <EditIcon />
              </Box>
              <Box mx={1}>
                <span>Editar</span>
              </Box>
            </MenuItem>
          </Link>
          <MenuItem
            onClick={() => {
              this.props.funcEliminar(this.props.paciente);
            }}
            className={classes.menuItemBorrar}
          >
            <Box>
              <DeleteIcon />
            </Box>
            <Box mx={1}>
              <span>Eliminar</span>
            </Box>
          </MenuItem>
        </Menu>
      </div>
    );
  }
}
export default withStyles(styles)(MenuTareasPaciente);
