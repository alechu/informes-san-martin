import React, { Component } from "react";
import Box from "@material-ui/core/Box";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import InfoPacienteCreandose from "./InfoPacienteCreandose";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import SaveIcon from "@material-ui/icons/Save";
import DateFnsUtils from "@date-io/date-fns";
import { es } from "date-fns/locale";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";
import "moment/locale/es";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import ModalPaciente from "./ModalPaciente.js";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";

// Un poco de estilo
const styles = (theme) => ({
  gridItem: {
    margin: "10px 0 10px 0",
  },
  formControl: {
    minWidth: 250,
  },
});

class FormularioEditarPaciente extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: props.paciente.id,
      nombre: props.paciente.nombre,
      apellido: props.paciente.apellido,
      dni: props.paciente.dni,
      sexo: props.paciente.sexo,
      estado: props.paciente.estado,
      fecha_nac: new Date(props.paciente.fecha_nac.replace(/-/g, "/")),
      obra_social: {
        id: props.paciente.obra_social_id,
        nombre: props.paciente.obra_social_nombre,
      },
      localidad: props.paciente.localidad,

      isLoaded: false,
      obras_sociales: "",
      opcionesSelectObra: [],

      postId: "",
      abrirModal: false,
      mensaje: "¿Confirma los cambios?",
      cargandoAccion: false,
      iconColor: "rgb(48, 63, 159)",
      icon: "far fa-question-circle",
      botonesModal: [],

      errores: {
        errorNombre: false,
        errorApellido: false,
        errorSexo: false,
        errorEstado: false,
        errorObraSocial: false,
        errorFechaNac: false,
        errorLocalidad: false,
        abrirAlert: false,
      },
    };
    this.cambiarCampo = this.cambiarCampo.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.cambiarObraSocial = this.cambiarObraSocial.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
    this.preSubmit = this.preSubmit.bind(this);
    this.datosPaciente = this.datosPaciente.bind(this);
    this.formatDateFinal = this.formatDateFinal.bind(this);
  }

  datosPaciente(id) {
    return {
      id: id,
      nombre: this.state.nombre,
      apellido: this.state.apellido,
      dni: this.state.dni,
      sexo: this.state.sexo,
      estado: this.state.estado,
      fecha_nac: this.state.fecha_nac,
      obra_social: {
        id: this.state.obra_social.id,
        nombre: this.state.obra_social.nombre,
      },
      localidad: this.state.localidad,
    };
  }

  // Actualiza el estado del campo que se cambió (excepto fecha de nacimiento)
  cambiarCampo(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }
  // Actualiza el estado de la fecha de nacimiento
  cambiarFecha = (date) => {
    this.setState({
      fecha_nac: date,
    });
  };
  // Actualizo la obra social con el ID y el nombre
  cambiarObraSocial(event) {
    const { id } = event.currentTarget.dataset;
    this.setState({
      obra_social: {
        id: id,
        nombre: event.target.value,
      },
    });
  }

  // La paso al formato de la API
  formatDateFinal(date) {
    return moment(date).format("yyyy-MM-DD");
  }

  // Verifico si la fecha es válida
  fechaValida(date) {
    var d = this.formatDateFinal(date);
    console.log(moment(d).isValid());
    return moment(d).isValid();
  }

  noFaltanDatos() {
    var hayError = false;

    if (!this.state.nombre) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorNombre: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.apellido) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorApellido: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.sexo) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorSexo: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.estado) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorEstado: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.fecha_nac || !this.fechaValida(this.state.fecha_nac)) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorFechaNac: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.obra_social.id) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorObraSocial: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.localidad) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorLocalidad: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!hayError) {
      return true;
    } else {
      return false;
    }
  }

  limpiarErrores() {
    this.setState({
      errores: {
        errorNombre: false,
        errorApellido: false,
        errorSexo: false,
        errorEstado: false,
        errorObraSocial: false,
        errorFechaNac: false,
        errorLocalidad: false,
        abrirAlert: false,
      },
    });
  }

  preSubmit() {
    this.limpiarErrores();
    if (this.noFaltanDatos()) {
      this.setState(
        {
          iconColor: "rgb(48, 63, 159)",
          icon: "far fa-question-circle",
          mensaje: "¿Confirmar los cambios?",
          botonesModal: [
            {
              texto: "Confirmar",
              onClick: this.handleSubmit,
              color: "green",
            },
            {
              link: "",
              state: "",
              texto: "Rechazar",
              onClick: this.cerrarModal,
              color: "crimson",
            },
          ],
        },
        () => {
          this.setState({ abrirModal: true });
        }
      );
    }
  }

  // Submit del editar paciente
  handleSubmit(event) {
    this.setState({ cargandoAccion: true }, () => {
      var datos = new FormData();

      datos.append("nombre", this.state.nombre);
      datos.append("apellido", this.state.apellido);
      datos.append("dni", this.state.dni);
      datos.append("sexo", this.state.sexo);
      datos.append("estado", this.state.estado);
      datos.append("fecha_nac", this.formatDateFinal(this.state.fecha_nac));
      datos.append("obra_social_id", this.state.obra_social.id);
      datos.append("localidad", this.state.localidad);

      axios({
        method: "put",
        url:
          "http://" +
          process.env.REACT_APP_API_IP +
          "/api/paciente?id=" +
          this.state.id,
        data: datos,
        headers: {
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
          "Content-Type": "multipart/form-data",
        },
      })
        .then((response) => {
          // Preparo el mensaje para el modal
          this.setState({
            cargandoAccion: false,
            icon: "fas fa-check-circle",
            iconColor: "green",
            botonesModal: [
              {
                texto: "Crearle un estudio",
                color: "#3f51b5",
                link: "/pacientes/crear-estudio",
                state: {
                  key: "paciente",
                  // le paso el ID también, que lo tengo acá
                  // del response. Lo necesito para el formCrearEstudio
                  data: this.datosPaciente(response.data.paciente.datos.id),
                },
              },
              {
                texto: "Volver al listado de pacientes",
                color: "#3f51b5",
                link: "/pacientes",
                state: "",
              },
              {
                texto: "Seguir editando",
                color: "#3f51b5",
                onClick: this.cerrarModal,
              },
            ],
            mensaje: (
              <span>
                El paciente{" "}
                <strong>
                  {this.state.apellido}, {this.state.nombre}
                </strong>{" "}
                se ha editado con éxito.
              </span>
            ),
          });
        })
        .catch((error) => {
          // Armo un mensaje para mandarle al modal
          var botones = [];
          var mensajeError = "";
          switch (error.response.data.status) {
            case 400:
              mensajeError = (
                <span
                  style={{
                    padding: "10px 20px",
                    display: "block",
                    fontSize: 17,
                  }}
                >
                  El paciente{" "}
                  <strong>
                    {this.state.apellido}, {this.state.nombre}
                  </strong>{" "}
                  no se ha podido editar porque el nuevo DNI ingresado ya
                  pertenece a un paciente.
                </span>
              );
              botones.push(
                {
                  texto: "Aceptar",
                  color: "green",
                  onClick: this.cerrarModal,
                },
                {
                  texto: "Ir al listado de pacientes",
                  color: "#3f51b5",
                  link: "/pacientes",
                  state: "",
                }
              );
              break;
            case 401:
              localStorage.setItem("alert", "Expired/Invalid JWT Token");
              document.cookie =
                "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
              window.location.href = "/user-pages/login-1";
              break;
            case 500:
              mensajeError =
                "Ocurrió un error con el servidor, por favor intentá de nuevo.";
              break;
          }
          if (botones.length == 0) {
            botones.push(
              {
                texto: "Reintentar",
                color: "green",
                onClick: this.handleSubmit,
              },
              {
                texto: "Cancelar",
                color: "crimson",
                onClick: this.cerrarModal,
              }
            );
          }
          this.setState({
            icon: "fas fa-times-circle",
            iconColor: "#dc004e",
            abrirModal: true,
            mensaje: mensajeError,
            cargandoAccion: false,
            botonesModal: botones,
          });
        });
      event.preventDefault();
    });
  }

  componentDidMount() {
    // Me traigo las OBRAS SOCIALES
    fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/obra_social?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.status === 401) {
            localStorage.setItem("alert", "Expired/Invalid JWT Token");
            document.cookie =
              "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            window.location.href = "/user-pages/login-1";
          } else {
            this.setState(
              {
                isLoaded: true,
                obras_sociales: result.body,
              },
              () => {
                this.setState({
                  opcionesSelectObra: this.armarSelectObrasSociales(
                    this.state.obras_sociales
                  ),
                });
              }
            );
          }
        },
        // Nota de la docu: es importante manejar errores aquí y no en
        // un bloque catch() para que no interceptemos errores
        // de errores reales en los componentes.
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  // A este método llama el modal (component hijo) cuando necesita cerrarse
  cerrarModal() {
    this.setState({
      abrirModal: false,
    });
  }
  armarSelectObrasSociales = (obrasSociales) => {
    if (obrasSociales != "") {
      return obrasSociales.map((obra_social) => (
        <MenuItem
          style={{ padding: "5px 15px 5px 15px" }}
          value={obra_social.id}
          data-id={obra_social.id}
        >
          {obra_social.nombre}
        </MenuItem>
      ));
    } else return "";
  };

  render() {
    // Obras sociales

    // Localidades
    const armarSelectLocalidades = () => {
      return [
        <MenuItem style={{ padding: "5px 15px 5px 15px" }} value="La Plata">
          La Plata
        </MenuItem>,
        <MenuItem style={{ padding: "5px 15px 5px 15px" }} value="Otro">
          Otro
        </MenuItem>,
      ];
    };

    // Para el estilo
    const { classes } = this.props;

    // Me traigo sólo los campos del paciente del estado
    // Para más comodidad y para pasarle sólo esos datos al InfoPacienteCreandose.js
    const datosPaciente = {
      nombre: this.state.nombre,
      apellido: this.state.apellido,
      dni: this.state.dni,
      sexo: this.state.sexo,
      estado: this.state.estado,
      fecha_nac: this.state.fecha_nac,
      obra_social: {
        id: this.state.obra_social.id,
        nombre: this.state.obra_social.nombre,
      },
      localidad: this.state.localidad,
    };

    return (
      <div>
        <Grid container spacing={2}>
          <Box p={1} flexGrow={1}>
            <Grid item xs={12}>
              <Paper
                style={{
                  padding: "5px",
                  textAlign: "center",
                  background:
                    "linear-gradient(120deg, #2f78e9, #128bfc, #18bef1)",
                }}
              >
                <b style={{ color: "white", fontSize: "20px" }}>
                  Editando al paciente{" "}
                  {this.props.paciente.apellido +
                    ", " +
                    this.props.paciente.nombre}
                </b>
                <br></br>
                <Typography style={{ color: "white" }} variant="caption">
                  Los campos requeridos están marcados con un *
                </Typography>
              </Paper>
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={12} style={{ marginTop: 10, padding: 4 }}>
          <form onSubmit={this.handleSubmit}>
            <Card variant="outlined">
              <Collapse in={this.state.errores.abrirAlert}>
                <Alert
                  style={{
                    marginTop: 10,
                    marginLeft: 20,
                    marginRight: 20,
                  }}
                  variant="filled"
                  severity="error"
                >
                  Por favor, completá los campos marcados en rojo
                </Alert>
              </Collapse>
              <Typography
                style={{
                  fontSize: 15,
                  paddingTop: "15px",
                  paddingLeft: "15px",
                }}
                color="textSecondary"
                gutterBottom
              >
                Datos del paciente
              </Typography>
              <Box
                display="flex"
                flexDirection="row"
                flexWrap="wrap"
                p={2}
                m={1}
                bgcolor="background.paper"
              >
                <Box p={1}>
                  <Grid item xs={4}>
                    <TextField
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <i
                              style={{ fontSize: 20 }}
                              className="far fa-user-circle"
                            ></i>
                          </InputAdornment>
                        ),
                      }}
                      inputProps={{
                        maxlength: 50,
                      }}
                      style={{ display: "flex", minWidth: 300, maxWidth: 300 }}
                      aria-describedby="my-helper-text"
                      required
                      id="nombre"
                      name="nombre"
                      value={datosPaciente.nombre}
                      onChange={this.cambiarCampo}
                      label="Nombre"
                      placeholder="Ej: Jorge"
                      helperText="Debe tener como máximo 50 caracteres"
                      error={this.state.errores.errorNombre}
                    />
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <TextField
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <i
                              style={{ fontSize: 20 }}
                              className="far fa-user-circle"
                            ></i>
                          </InputAdornment>
                        ),
                      }}
                      inputProps={{
                        maxlength: 50,
                      }}
                      style={{ display: "flex", minWidth: 300, maxWidth: 300 }}
                      required
                      id="apellido"
                      name="apellido"
                      value={datosPaciente.apellido}
                      onChange={this.cambiarCampo}
                      label="Apellido"
                      placeholder="Ej: Gutiérrez"
                      helperText="Debe tener como máximo 50 caracteres"
                      error={this.state.errores.errorApellido}
                    />
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <TextField
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">
                            <i
                              style={{ fontSize: 20 }}
                              className="far fa-id-card"
                            ></i>
                          </InputAdornment>
                        ),
                      }}
                      style={{ display: "flex", minWidth: 300, maxWidth: 300 }}
                      id="dni"
                      name="dni"
                      type="number"
                      value={datosPaciente.dni}
                      onChange={this.cambiarCampo}
                      label="DNI"
                      placeholder="Ej: 19489231"
                      helperText="No es obligatorio, pero es recomendable"
                      onInput={(e) => {
                        e.target.value = Math.max(0, parseInt(e.target.value))
                          .toString()
                          .slice(0, 10);
                      }}
                    />
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <FormControl
                      style={{ display: "flex", minWidth: 300, maxWidth: 300 }}
                    >
                      <InputLabel
                        error={this.state.errores.errorSexo}
                        id="sexo"
                      >
                        Sexo
                      </InputLabel>
                      <Select
                        MenuProps={{ disableScrollLock: true }}
                        startAdornment={
                          <InputAdornment position="start">
                            <i
                              className="fas fa-venus-mars"
                              style={{ fontSize: "25px" }}
                            ></i>
                          </InputAdornment>
                        }
                        id="sexo"
                        name="sexo"
                        labelId="sexo"
                        value={datosPaciente.sexo}
                        onChange={this.cambiarCampo}
                        error={this.state.errores.errorSexo}
                      >
                        <MenuItem value={"Masculino"}>Masculino</MenuItem>
                        <MenuItem value={"Femenino"}>Femenino</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <FormControl
                      style={{ display: "flex", minWidth: 300, maxWidth: 300 }}
                    >
                      <InputLabel
                        error={this.state.errores.errorEstado}
                        id="estado"
                      >
                        Estado
                      </InputLabel>
                      <Select
                        MenuProps={{ disableScrollLock: true }}
                        startAdornment={
                          <InputAdornment position="start">
                            <i
                              className="fas fa-hospital-user"
                              style={{ fontSize: "25px" }}
                            ></i>
                          </InputAdornment>
                        }
                        id="estado"
                        name="estado"
                        labelId="estado"
                        value={datosPaciente.estado}
                        onChange={this.cambiarCampo}
                        error={this.state.errores.errorEstado}
                      >
                        <MenuItem value={"Ambulatorio"}>Ambulatorio</MenuItem>
                        <MenuItem value={"Internado"}>Internado</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <FormControl
                      style={{ display: "flex", minWidth: 300, maxWidth: 300 }}
                    >
                      <InputLabel
                        error={this.state.errores.errorObraSocial}
                        id="obra_social"
                      >
                        Obra social
                      </InputLabel>
                      <Select
                        MenuProps={{ disableScrollLock: true }}
                        startAdornment={
                          <InputAdornment position="start">
                            <i
                              className="fas fa-star-of-life"
                              style={{ fontSize: "25px" }}
                            ></i>
                          </InputAdornment>
                        }
                        id="obra_social_id"
                        name="obra_social_id"
                        labelId="obra_social"
                        value={datosPaciente.obra_social.id}
                        onChange={this.cambiarObraSocial}
                        children={this.state.opcionesSelectObra}
                        error={this.state.errores.errorObraSocial}
                      ></Select>
                    </FormControl>
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <MuiPickersUtilsProvider
                      locale={es}
                      disableScrollLock={true}
                      utils={DateFnsUtils}
                    >
                      <KeyboardDatePicker
                        style={{
                          display: "flex",
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <i
                                style={{ fontSize: 20 }}
                                className="fas fa-calendar-day"
                              ></i>
                            </InputAdornment>
                          ),
                        }}
                        okLabel={""}
                        autoOk
                        cancelLabel="Cancelar"
                        DialogProps={{ disableScrollLock: true }}
                        InputLabelProps={{ shrink: true }}
                        placeholder="Ej: 03/09/1994"
                        invalidDateMessage="La fecha no es válida. Recordá: día/mes/año"
                        maxDateMessage="La fecha no puede ser mayor al máximo permitido"
                        disableScrollLock={true}
                        name="fecha_nac"
                        format="dd/MM/yyyy"
                        margin="normal"
                        id="fecha_nac"
                        label="Fecha de nacimiento"
                        value={this.state.fecha_nac}
                        onChange={this.cambiarFecha}
                        KeyboardButtonProps={{
                          "aria-label": "Cambiar fecha",
                        }}
                        error={this.state.errores.errorFechaNac}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <FormControl
                      style={{
                        display: "flex",
                        minWidth: 300,
                        maxWidth: 300,
                        marginTop: 16,
                      }}
                    >
                      <InputLabel id="localidad">Localidad</InputLabel>
                      <Select
                        MenuProps={{ disableScrollLock: true }}
                        startAdornment={
                          <InputAdornment position="start">
                            <i
                              className="fas fa-map-marker-alt"
                              style={{ fontSize: "25px" }}
                            ></i>
                          </InputAdornment>
                        }
                        id="localidad"
                        name="localidad"
                        labelId="localidad"
                        value={datosPaciente.localidad}
                        onChange={this.cambiarCampo}
                        children={armarSelectLocalidades()}
                        error={this.state.errores.errorLocalidad}
                      ></Select>
                    </FormControl>
                  </Grid>
                </Box>
              </Box>
            </Card>
          </form>
        </Grid>
        <Grid item xs={12} style={{ padding: 4 }}>
          <Card variant="outlined">
            <InfoPacienteCreandose paciente={datosPaciente} tarea="editar" />
          </Card>
          <Button
            variant="contained"
            size="large"
            style={{
              marginTop: 10,
              color: "white",
              backgroundColor: "#1e90ff",
            }}
            endIcon={<SaveIcon style={{ marginBottom: 2.2 }}></SaveIcon>}
            onClick={this.preSubmit}
          >
            {" "}
            Guardar Cambios
          </Button>
        </Grid>

        <ModalPaciente
          abrirModal={this.state.abrirModal}
          cerrarModal={this.cerrarModal}
          mensaje={this.state.mensaje}
          cargandoAccion={this.state.cargandoAccion}
          icon={this.state.icon}
          iconColor={this.state.iconColor}
          botones={this.state.botonesModal}
        />

        {/* Llamo al modal de éxito o error con los botones y el texto que quiero que renderice */}
      </div>
    );
  }
}

export default withStyles(styles)(FormularioEditarPaciente);
