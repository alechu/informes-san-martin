import React from 'react';
import { Page, Text, View, Document, StyleSheet, PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';
import { withStyles } from "@material-ui/core/styles";

const fontSize = 12;

// Create styles
const styles = StyleSheet.create({
    sectionPaciente: {
        margin: '0 0 6px'
    },
    tituloPaciente: {
        fontSize: fontSize - 2,
        color: '#163b88',
        fontWeight: '600'
    },
    titulo: {
        fontSize: fontSize - 2,
        fontWeight: '600',
        marginBottom: 2
    },
    valor: {
        fontSize: fontSize - 3,
        fontWeight: 800
    },

    sectionEstudio: {
        marginBottom: 5
    },

    contenedorAtributo: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    textoDefecto: {
        fontSize: fontSize - 3,
        marginBottom: 5,
        color: '#444444'
    }
});

const dividerGrueso = (
    <View
        style={{
            marginTop: '5px',
            borderBottomColor: '#676767',
            borderBottomWidth: 1,
        }}
    />
);

export default function PDFInfoEstudioTransesofagico(props) {
    return (
        <View>
            {/* Info básica estudio */}
            <View style={styles.sectionPaciente}>
                <View style={{ marginTop: 2, marginBottom: 2 }}>
                    <Text style={styles.titulo}>Descripción</Text>
                    <Text style={styles.textoDefecto}>
                        {props.estudio.atributos.descripcion}
                    </Text>
                </View>
                <View style={{ marginBottom: 3 }}>
                    <Text style={styles.titulo}>Conclusión</Text>
                    <Text style={styles.textoDefecto}>{props.estudio.atributos.conclusion}</Text>
                </View>
            </View>
        </View>
    )
}