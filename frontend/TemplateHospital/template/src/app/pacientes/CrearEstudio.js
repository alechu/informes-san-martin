import React, { Component } from 'react';
import FormularioCrearEstudio from './FormularioCrearEstudio';
import ContenedorSeccion from '../otros/ContenedorSeccion';
import Box from '@material-ui/core/Box';
import InfoPacienteSeleccionado from './InfoPacienteSeleccionado';
import PersonIcon from '@material-ui/icons/Person';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';

export default class CrearEstudio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            breadcrumb: [
                { pathname: '/pacientes', state: '', texto: 'Pacientes', icon: <PersonIcon /> },
                { pathname: '/pacientes/crear-estudio', state: { paciente: this.props.location.state.paciente }, texto: 'Crear estudio', icon: <LibraryAddIcon /> }
            ]
        }
    };

    // Para pasarle el breadcrumb al header
    componentDidMount() {
        this.props.actualizarHeader({
            breadcrumb: this.state.breadcrumb
        })
    }

    render() {

        // Paciente al que le voy a agregar un estudio.
        const paciente = this.props.location.state.paciente;

        return (
            <ContenedorSeccion
                nombre="Crear estudio"
                descripcion="Podés crearle un estudio al paciente seleccionado"
                preComponentes={[
                    <InfoPacienteSeleccionado paciente={paciente} />
                ]}
                componentes={[
                    <FormularioCrearEstudio
                        paciente={paciente}
                    />
                ]}
            />
        )
    }
}