import React from 'react';
import { Page, Text, View, Document, StyleSheet, PDFViewer, PDFDownloadLink, Font } from '@react-pdf/renderer';
import { withStyles } from "@material-ui/core/styles";

const fontSize = 12;

// Create styles
const styles = StyleSheet.create({
    sectionPaciente: {
        margin: '6px 0 0px'
    },
    tituloPaciente: {
        fontSize: fontSize - 2,
        color: '#163b88',
        fontWeight: '600'
    },
    tituloDatoPaciente: {
        fontSize: fontSize - 2,
        fontWeight: '600'
    },
    valorDatoPaciente: {
        fontSize: fontSize - 3,
        color: '#444444'
    },

    sectionEstudio: {
        marginBottom: 5
    },

    contenedorAtributo: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    }


});

const dividerGrueso = (
    <View
        style={{
            marginTop: '5px',
            borderBottomColor: '#676767',
            borderBottomWidth: 1,
        }}
    />
);

export default function PDFInfoEstudioBasico(props) {

    function format(input) {
        var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
        if (!input || !input.match(pattern)) {
            return null;
        }
        return input.replace(pattern, '$3/$2/$1');
    };

    return (
        <View>
            {/* Info paciente */}
            <View style={styles.sectionPaciente}>
                <Text style={styles.tituloPaciente}>Paciente</Text>
                {dividerGrueso}
                <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center', marginBottom: 5 }}>
                    <Text style={styles.tituloDatoPaciente}>Apellido: </Text>
                    <Text style={styles.valorDatoPaciente}>{props.paciente.apellido} | </Text>
                    <Text style={styles.tituloDatoPaciente}>Nombre: </Text>
                    <Text style={styles.valorDatoPaciente}>{props.paciente.nombre} | </Text>
                    <Text style={styles.tituloDatoPaciente}>DNI: </Text>
                    <Text style={styles.valorDatoPaciente}>{props.paciente.dni ? (props.paciente.dni + ' | ') : 'No registrado | '}</Text>
                    <Text style={styles.tituloDatoPaciente}>Fecha de nacimiento: </Text>
                    <Text style={styles.valorDatoPaciente}>{format(props.paciente.fecha_nac)}</Text>
                </View>
            </View>

            {/* Info básica estudio */}
            <View style={styles.sectionPaciente}>
                <Text style={styles.tituloPaciente}>Estudio</Text>
                {dividerGrueso}
                <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center', marginBottom: 5 }}>
                    <Text style={styles.tituloDatoPaciente}>Fecha: </Text>
                    <Text style={styles.valorDatoPaciente}>{format(props.estudio.informacion_general.fecha)} | </Text>
                    <Text style={styles.tituloDatoPaciente}>Médico: </Text>
                    <Text style={styles.valorDatoPaciente}>{props.estudio.informacion_general.especialista.nombre} | </Text>
                    <Text style={styles.tituloDatoPaciente}>Tipo: </Text>
                    <Text style={styles.valorDatoPaciente}>{props.estudio.informacion_general.tipo.nombre_mostrado}</Text>
                </View>
                <View style={{ marginBottom: 3 }}>
                    <Text style={styles.tituloDatoPaciente}>Motivo</Text>
                    <Text style={styles.valorDatoPaciente}>{props.estudio.informacion_general.motivo.nombre}</Text>
                </View>
                {/* Ya no va más acá, porque tiene que ir después de los datos del estudio específico
                <View style={{ marginBottom: 3 }}>
                    <Text style={styles.tituloDatoPaciente}>Conclusión</Text>
                    <Text style={styles.valorDatoPaciente}>{props.estudio.atributos.conclusion}</Text>
                </View>
                */}
            </View>
        </View>
    )
}