import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

// Iconos
import CheckCircle from "@material-ui/icons/CheckCircle";
import Close from "@material-ui/icons/Close";
import { CircularProgress } from "@material-ui/core";

const styles = (theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  container: {
    //backgroundColor: theme.palette.background.paper,
    boxShadow: "0px 0px 30px #777777",
    borderRadius: "15px",
    minWidth: 400,
    flexGrow: 1,
    background: "white",
    padding: "10px 10px 30px 10px",
    minWidth: "350px",
    maxWidth: "600px",
  },
  mensaje: {
    textAlign: "center",
    fontSize: "30px",
    margin: "10px 0px 0px",
  },
  pregunta: {
    textAlign: "center",
    fontSize: "30px",
    margin: "5px 0 5px 0",
  },
  boton: {
    textAlign: "center",
  },
});

class ModalPacienteExito extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalAbierto: props.abrirModal,
    };

    this.abrirModal = this.abrirModal.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
    this.armarStateParaLink = this.armarStateParaLink.bind(this);
  }

  abrirModal = () => {
    this.setState({
      modalAbierto: true,
    });
  };

  cerrarModal = () => {
    this.setState({
      modalAbierto: false,
    });
  };

  armarStateParaLink = (states) => {
    var obj = {};
    //[boton.state.key]: boton.state.data
    // En el caso de que es un array
    if (Array.isArray(states)) {
      for (var i=0; i<states.length; i++) {
        obj[states[i].key] = states[i].data
      }
      return obj
    }

    // En el caso de que me mandaron un sólo estado
    else {
      return { [states.key]: states.data }
    }
  }

  render() {
    // Para el estilo
    const { classes } = this.props;
    const abrirModal = this.props.abrirModal;
    const mensaje = this.props.mensaje;
    const botones = this.props.botones;
    const icon = this.props.icon;
    const iconColor = this.props.iconColor;

    return (
      <div>
        <Modal
          disableScrollLock={true}
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={abrirModal}
          onClose={this.props.cerrarModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
          disableBackdropClick={this.props.desactivarCruz ? true : false}
        >
          <Fade in={abrirModal}>
            <div>
              <Grid container className={classes.container}>
                <Grid item xs={12}>
                  <Grid container justify="center" spacing={2}>
                    <Grid item xs={4}></Grid>
                    <Grid item xs={4}>
                      <div style={{ marginTop: "-85px", textAlign: "center" }}>
                        <i
                          style={{
                            color: iconColor,
                            fontSize: "120px",
                            background: "white",
                            borderRadius: "100%",
                            fill: "#72c343",
                          }}
                          className={icon}
                        ></i>
                      </div>
                    </Grid>
                    <Grid item xs={4} style={{ textAlign: "right" }}>
                      {!this.props.desactivarCruz &&
                      <Button
                        onClick={this.props.cerrarModal}>
                        <Close
                          style={{
                            border: "2px solid #3f51b5",
                            borderRadius: "5px",
                            color: '#3f51b5',
                          }}
                        ></Close>
                      </Button>
                      }
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12}>
                  <Grid container justify="center" spacing={2}>
                    <Grid item xs={12} className={classes.mensaje}>
                      <p
                        style={{
                          fontSize: ".65em",
                          color: "#555555",
                          marginBottom: "10px",
                        }}
                      >
                        {mensaje}
                      </p>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12}>
                  {this.props.cargandoAccion ? (
                    <Grid container justify="center">
                      <CircularProgress></CircularProgress>
                    </Grid>
                  ) : (
                    <Grid container justify="center" spacing={2}>
                      {/* Renderizo los botones que me pasan como props los forms */}
                      {botones.map((boton) => (
                        <Grid item className={classes.boton} key={boton.link}>
                          {boton.link ? (
                            <Link
                              to={{
                                pathname: boton.link,
                                //state: { [boton.state.key]: boton.state.data },
                                state: this.armarStateParaLink(boton.state)
                              }}
                            >
                              <Button
                                variant="contained"
                                style={{
                                  color: "white",
                                  backgroundColor: boton.color,
                                }}
                                onClick={boton.onClick}
                              >
                                {boton.texto}
                              </Button>
                            </Link>
                          ) : (
                            <Button
                              variant="contained"
                              style={{
                                color: "white",
                                backgroundColor: boton.color,
                              }}
                              onClick={boton.onClick}
                            >
                              {boton.texto}
                            </Button>
                          )}
                        </Grid>
                      ))}
                    </Grid>
                  )}
                </Grid>
              </Grid>
            </div>
          </Fade>
        </Modal>
      </div>
    );
  }
}

export default withStyles(styles)(ModalPacienteExito);
