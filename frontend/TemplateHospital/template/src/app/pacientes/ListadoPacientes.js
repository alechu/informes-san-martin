import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import MUIDataTable from "mui-datatables";
import Grid from "@material-ui/core/Grid";
import MenuTareasPaciente from "./MenuTareasPaciente";
import { Typography } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import ModalPaciente from "./ModalPaciente";

export default function ListadoPacientes(props) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [pacientes, setPacientes] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [columns, setColumns] = useState([]);
  const [rows, setRows] = useState([]);
  const [paginaActual, setPaginaActual] = useState([""]);
  const [abrirModal, setAbrirModal] = useState(false);
  const [mensajeModal, setMensajeModal] = useState("");
  const [cargandoAccion, setCargandoAccion] = useState(false);
  const [icon, setIcon] = useState("fas fa-minus-circle");
  const [iconColor, setIconColor] = useState("rgb(255, 71, 26)");
  const [botonesModal, setBotonesModal] = useState([]);
  const [actualizarMenu, setActualizarMenu] = useState(false);
  const [desactivarCruz, setDesactivarCruz] = useState(false);

  //Snackbar
  const [mostrarSnackbar, setMostrarSnackbar] = useState(false);

  var idPaciente = "";

  const history = useHistory();

  const eliminarPaciente = (paciente) => {
    idPaciente = paciente.id;
    setMensajeModal(
      <div style={{ textAlign: "center" }}>
        <h2 style={{ color: "rgb(255, 71, 26)" }}>¡Cuidado!</h2>
        <h5>
          Estás a punto de eliminar al siguiente <b>paciente</b>:
        </h5>
        <ul style={{ textAlign: "left", marginLeft: 100 }}>
          <li>
            <h5>
              Número de paciente: <b>{paciente.id}</b>
            </h5>
          </li>
          <li>
            <h5>
              Nombre: <b>{paciente.nombre}</b>
            </h5>
          </li>
          <li>
            <h5>
              Apellido: <b>{paciente.apellido}</b>
            </h5>
          </li>
          <li>
            <h5>
              DNI: <b>{paciente.dni !== null ? paciente.dni : "No posee"}</b>
            </h5>
          </li>
        </ul>
        <h5>
          <h5>
            Esta acción es <b>permanente</b> y <b>NO se puede deshacer</b>
          </h5>
        </h5>
      </div>
    );
    setIcon("fas fa-minus-circle");
    setIconColor("rgb(255, 71, 26)");
    setDesactivarCruz(false);
    setBotonesModal([
      {
        texto: "Confirmar",
        color: "green",
        onClick: eliminarPacienteAPI,
      },
      {
        texto: "Rechazar",
        color: "crimson",
        onClick: cerrarModal,
      },
    ]);
    setAbrirModal(true);
  };

  const eliminarPacienteAPI = async () => {
    setCargandoAccion(true);
    await fetch(
      "http://" +
        process.env.REACT_APP_API_IP +
        "/api/paciente?id=" +
        idPaciente,
      {
        method: "DELETE",
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.status === 400) {
          if (data.details === "Ese paciente tiene estudios asociados") {
            idPaciente = "";
            setMensajeModal(
              <div>
                <h3 style={{ color: "rgb(255, 153, 0)" }}>
                  Este paciente no se puede eliminar
                </h3>
                <h5>
                  Este paciente no se puede eliminar porque tiene estudios
                  asociados.
                </h5>
                <h5 style={{ marginBottom: 10 }}>
                  Si realmente desea eliminar al paciente,<br></br> elimine
                  primero todos los estudios asociados al mismo.
                </h5>
              </div>
            );
            setIcon("fas fa-exclamation-circle");
            setIconColor("rgb(255, 153, 0)");
            setDesactivarCruz(false);
            setBotonesModal([
              {
                texto: "Entendido",
                color: "#297ced",
                onClick: cerrarModal,
              },
            ]);
            setCargandoAccion(false);
          }
        } else {
          if (data.status === 200) {
            setMensajeModal("Paciente eliminado exitosamente");
            setIcon("fas fa-check-circle");
            setIconColor("green");
            setDesactivarCruz(true);
            setBotonesModal([
              {
                texto: "Aceptar",
                color: "green",
                onClick: cerrarModal,
              },
            ]);
            setCargandoAccion(false);
          } else {
            if (data.status === 401) {
              localStorage.setItem("alert", "Expired/Invalid JWT Token");
              document.cookie =
                "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
              history.push("/user-pages/login-1");
            }
          }
        }
      });
  };

  const cerrarModal = () => {
    if (idPaciente != "") {
      window.location.reload();
    } else {
      setAbrirModal(false);
    }
  };

  // Esto es para pasarle un objeto ordenado
  // al menú de tareas del listado
  function armarDataPacienteParaMenu(p) {
    return {
      id: p.datos.id,
      nombre: p.datos.nombre,
      apellido: p.datos.apellido,
      dni: p.datos.dni,
      fecha_nac: p.datos.fecha_nac,
      sexo: p.datos.sexo,
      estado: p.datos.estado,
      localidad: p.datos.localidad,
      obra_social_id: p.obra_social.id,
      obra_social_nombre: p.obra_social.nombre,
    };
  }

  // Arma la tabla (columnas y todos los pacientes)
  function armarTabla(pacientes) {
    // Armo las columnas
    var co = [];
    // La primer columna va a ser el botón de Tareas
    co.push({
      name: "tareas",
      label: "Tareas",
      options: {
        filter: false,
        customBodyRenderLite: (dataIndex) => {
          return (
            <MenuTareasPaciente
              paciente={armarDataPacienteParaMenu(pacientes[dataIndex])}
              actualizar={actualizarMenu}
              funcEliminar={eliminarPaciente}
            ></MenuTareasPaciente>
          );
        },
      },
    });
    // Le agrego la obra social
    var keys = Object.keys(pacientes[0].datos);
    keys.push("obra_social");

    // Itero por cada key para ir
    // armando cada columna
    for (var c of keys) {
      // Si es la columna del ID hago que arranque oculta
      // y le pongo ID de label
      if (c === "id") {
        var columna = {
          name: c,
          label: "ID",
          options: {
            display: false,
            sort: false,
          },
        };
      }
      // Si es la fecha de nacimiento le
      // acomodo un poco la label
      else if (c == "fecha_nac") {
        var columna = {
          name: c,
          label: "Fecha de nacimiento",
          options: {
            sort: false,
          },
        };
      }
      // Si es el DNI le cambio la label
      else if (c == "dni") {
        var columna = {
          name: c,
          label: "DNI",
          options: {
            sort: false,
          },
        };
      }
      // Si es cualquier otro le saco los _
      else {
        var columna = {
          name: c,
          label: (c.charAt(0).toUpperCase() + c.slice(1)).replace(/_/g, " "),
          options: {
            sort: false,
          },
        };
      }
      co.push(columna);
    }
    // Finalmente SETEO las columnas recién creadas
    // en el estado que usa la MUI Datatable
    setColumns(co);

    // Armo las filas (cada paciente)
    var fi = [];
    for (var p of pacientes) {
      // Armo la data del paciente
      var objetoPaciente = p.datos;
      objetoPaciente["obra_social"] = p.obra_social.nombre;
      objetoPaciente["tareas"] = "";

      // Me fijo si tiene el DNI en null, si es así pongo otra cosa
      if (objetoPaciente["dni"] === null) {
        objetoPaciente["dni"] = "No asignado";
      }

      // Lo agrego a la data general
      fi.push(objetoPaciente);
    }
    // Finalmente SETEO las filas recién creadas
    // en el estado que usa la MUI Datatable
    setRows(fi);
  }

  // Pasa una fecha de yyyy-mm-dd a dd/mm/yyyy
  function format(input) {
    var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
    if (!input || !input.match(pattern)) {
      return null;
    }
    return input.replace(pattern, "$3/$2/$1");
  }

  // Opciones para la tabla
  const options = {
    filter: false,
    search: false,
    selectableRows: false,
    download: false,
    print: false,
    serverSide: true,
    page: props.paginaActual - 1,
    onChangeRowsPerPage: cambioCantidadPorPagina,
    onChangePage: cambioPagina,
    rowsPerPageOptions: [5, 10, 15, 20, 30, 50, 100],
    count: props.cantPacientes,
    textLabels: {
      body: {
        noMatch: "No se encontró ningún paciente",
        toolTip: "Ordenar",
        columnHeaderTooltip: (column) => `Sort for ${column.label}`,
      },
      pagination: {
        next: "Siguiente página",
        previous: "Página anterior",
        rowsPerPage: "Pacientes por página:",
        displayRows: "de",
      },
      toolbar: {
        search: "Buscar paciente",
        downloadCsv: "Download CSV",
        print: "Print",
        viewColumns: "Elegir qué columnas ver",
        filterTable: "Filtrar tabla",
      },
      filter: {
        all: "All",
        title: "COLUMNAS",
        reset: "Reiniciar todos",
      },
      viewColumns: {
        title: "Columnas",
        titleAria: "Mostrar/Ocultar columnas",
      },
      selectedRows: {
        text: "fila/s seleccionada/s",
        delete: "Eliminar",
        deleteAria: "Eliminar filas seleccionadas",
      },
    },
  };

  /*
    Este método se ejecuta cada vez que
    tengo que actualizar los pacientes de la tabla
  */
  useEffect(() => {
    if (props.pacientes && props.pacientes.length) {
      armarTabla(props.pacientes);
      setIsLoaded(true);
      // Actualizo cant de estudios para el menú Tareas
      setActualizarMenu(!actualizarMenu);
    }
  }, [props.pacientes]);

  // Cuando cambian la cantidad de pacientes por página
  function cambioCantidadPorPagina(cant) {
    props.setPorPagina(cant);
    setMostrarSnackbar(true);
  }

  // Cuando se cambia la página
  function cambioPagina(pagina) {
    props.changePagina(pagina + 1);
  }

  /*
    Este método genera un <span> (si es necesario)
    al lado del título de la tabla, indicando el filtro
    aplicado (apellido, fecha de nac, o ambos).

    El DNI no lo indica porq si encuentra va a ser uno solo,
    y es al pedo
  */
  function textoSiHayFiltros() {
    if (props.apellidoFiltrado || props.fechaNacFiltrada) {
      var texto;
      if (props.apellidoFiltrado && props.fechaNacFiltrada) {
        texto = (
          <span>
            (con apellido{" "}
            <span style={{ color: "#2082f3", fonWeight: "800" }}>
              {props.apellidoFiltrado}
            </span>{" "}
            y fecha de nacimiento{" "}
            <span style={{ color: "#2082f3", fonWeight: "800" }}>
              {format(props.fechaNacFiltrada)}
            </span>
            )
          </span>
        );
      } else if (props.apellidoFiltrado) {
        texto = (
          <span>
            (con apellido{" "}
            <span style={{ color: "#2082f3", fonWeight: "800" }}>
              {props.apellidoFiltrado}
            </span>
            )
          </span>
        );
      } else {
        texto = (
          <span>
            (con fecha de nacimiento{" "}
            <span style={{ color: "#2082f3", fonWeight: "800" }}>
              {format(props.fechaNacFiltrada)}
            </span>
            )
          </span>
        );
      }
      return texto;
    } else return "";
  }

  return (
    <div>
      {isLoaded ? (
        <MUIDataTable
          title={
            <div>
              <h6
                className="MuiTypography-root MUIDataTableToolbar-titleText-42 MuiTypography-h6"
                style={{ display: "inline" }}
              >
                Listado de pacientes{" "}
              </h6>
              {textoSiHayFiltros()}
            </div>
          }
          data={rows}
          columns={columns}
          options={options}
        />
      ) : props.hayPacientes ? (
        <Grid
          container
          spacing={4}
          style={{ margin: "20px 5px", alignItems: "center" }}
        >
          <Grid item>
            <CircularProgress></CircularProgress>
          </Grid>
          <Grid item>
            <Typography>Cargando pacientes...</Typography>
          </Grid>
        </Grid>
      ) : (
        <Grid
          container
          spacing={4}
          style={{ margin: "20px 5px", alignItems: "center" }}
        >
          <Grid item>
            <Typography>
              No se han encontrado pacientes en el sistema.
            </Typography>
          </Grid>
        </Grid>
      )}

      <ModalPaciente
        abrirModal={abrirModal}
        cerrarModal={cerrarModal}
        mensaje={mensajeModal}
        cargandoAccion={cargandoAccion}
        icon={icon}
        iconColor={iconColor}
        botones={botonesModal}
        desactivarCruz={desactivarCruz}
      />
    </div>
  );
}
