import React from 'react';
import { Page, Text, View, Document, StyleSheet, PDFViewer, PDFDownloadLink } from '@react-pdf/renderer';
import { withStyles } from "@material-ui/core/styles";
import { Table, TableHeader, TableCell, TableBody, DataTableCell } from '@david.kucsai/react-pdf-table';

const fontSize = 12;
const weightingAncho = 0.8;
const weightingAngosto = 0.2;

// Estilos
const styles = StyleSheet.create({
    tituloDatoPaciente: {
        fontSize: fontSize - 2,
        fontWeight: '600'
    },
    valorDatoPaciente: {
        fontSize: fontSize - 3,
        color: '#444444'
    },
    sectionPaciente: {
        //margin: '6px 0 6px'
        margin: '0'
    },
    tituloPaciente: {
        fontSize: fontSize - 2,
        color: '#163b88',
        fontWeight: '600'
    },
    titulo: {
        fontSize: fontSize - 2,
        fontWeight: '600',
        marginBottom: 5
    },
    tituloDescripcion: {
        fontSize: fontSize - 2,
        fontWeight: '600',
        marginBottom: 1
    },
    valor: {
        fontSize: fontSize - 3,
        fontWeight: 800
    },

    sectionEstudio: {
        marginBottom: 5
    },

    contenedorAtributo: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    textoDefecto: {
        fontSize: fontSize - 3,
        marginBottom: 5,
        color: '#444444'
    },

    celda: {
        padding: '3px',
        fontSize: fontSize - 5
    },
    celdaTitulo: {
        padding: '3px',
        fontSize: fontSize - 5,
        color: '#333333'
    },
    tableHeader: {
        fontSize: fontSize - 4,
        padding: '3px'
    }
});

const dividerGrueso = (
    <View
        style={{
            marginTop: '5px',
            borderBottomColor: '#676767',
            borderBottomWidth: 1,
        }}
    />
);

const espaciado = (
    <View
        style={{
            margin: 5
        }}
    />
)

export default function PDFInfoEstudioCongenitas(props) {
    return (
        <View>
            {/* Info básica estudio */}
            <View style={styles.sectionPaciente}>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', fontSize: fontSize - 3, marginTop: 3 }}>
                    <Text style={styles.tituloDatoPaciente}>Situs: </Text>
                    <Text style={styles.valorDatoPaciente}>{props.estudio.atributos.situs} | </Text>
                    <Text style={styles.tituloDatoPaciente}>Conexión aurículo-ventricular: </Text>
                    <Text style={styles.valorDatoPaciente}>{props.estudio.atributos.conexion_auriculo_ventricular} | </Text>
                    <Text style={styles.tituloDatoPaciente}>Conexión ventrículo-arterial: </Text>
                    <Text style={styles.valorDatoPaciente}>{props.estudio.atributos.conexion_ventriculo_arterial}</Text>
                </View>
                {espaciado}
                <Text style={styles.titulo}>
                    Valores Bidimensional
                </Text>
                <Table data={[{ firstName: 'John' }]}>
                    <TableHeader>
                    </TableHeader>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>DDVI (mm)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_DDVI}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>AI área (cm2)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_Al_Area}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>DSVI (mm)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_DSVI}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>AI Vol/SC</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_Al_Vol_SC}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>SIV (mm)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_SIV}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Aorta (mm)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_Aorta}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>PP (mm)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_PP}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Ap VAo (mm)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_Ap_Vao}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>FEY (%)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_FEY}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>TSVI (mm)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vb_TSVI}</TableCell>
                    </TableBody>
                </Table>

                {espaciado}

                <Text style={styles.titulo}>
                    Valores Doppler
                </Text>
                <Table data={[{ firstName: 'John' }]}>
                    <TableHeader>
                        <TableCell weighting={weightingAncho} style={styles.tableHeader}>Válvula AÓRTICA</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.tableHeader}>Válvula MITRAL</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.tableHeader}>Válvula PULMONAR</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.tableHeader}>Válvula TRICÚSPIDE</TableCell>
                    </TableHeader>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Velocidad máxima Ao (m/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Velocidad_Maxima}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Velocidad onda E (m/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Velocidad_Onda_E}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Velocidad máxima A. pulmonar (m/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Velocidad_Maxima_A_Pulmonar}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Insuficiencia Tricuspidea</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Insuficiencia_Tricuspidea}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Gradiente máximo Ao (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Gradiente_Maximo_Ao}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Velocidad onda A (m/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Velocidad_Onda_A}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Gradiente máximo (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Gradiente_Maximo}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Velocidad Regurgitante TT (m/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_velocidad_Regurgitante_TT}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Gradiente medio Ao (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Gradiente_Medio_Ao}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Gradiente medio trasmitral (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Gradiente_Medio_Trasmitral}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Tiempo al pico (mseg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Tiempo_Al_Pico}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Gradiente pico TT (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Gradiente_Pico_TT}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Insuficiencia</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Insuficiencia_Aortica}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Insuficiencia</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Insuficiencia_Mitral}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Insuficiencia</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Insuficiencia_Pulmonar}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>PAP (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_PAP}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>THP (mseg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_THP}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>ORE</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_ORE}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Gradiente protosistólico (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Gradiente_Protosistolico}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>PmAP (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_PmAP}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Flujo reverso ADT</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Flujo_Reverso_ADT}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Volumen Regurgitante (ml)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Volumen_Regurgitante}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Gradiente telesistólico (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Gradiente_Telesistolico}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Pad (mmHg)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Pad}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}></TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}></TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Area VM THP (cm2)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Area_VM_THP}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}></TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}></TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}></TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}></TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Flujo reverso AA</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Flujo_Reverso_AA}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Dp/dt</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_Dp_dt}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Qp/qs</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.vd_QP_QS}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}></TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}></TableCell>
                    </TableBody>
                </Table>

                {espaciado}

                <Text style={styles.titulo}>
                    Valores Doppler Tisular
                </Text>
                <Table data={[{ firstName: 'John' }]}>
                    <TableHeader>
                    </TableHeader>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Onda S Septal (cm/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.dp_Onda_S_Septal}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Onda e´ (cm/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.dp_Onda_e}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Onda S lateral (cm/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.dp_Onda_S_Lateral}</TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Relación E/e´</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.dp_Relacion_E_e}</TableCell>
                    </TableBody>
                </Table>
                <Table data={[{ firstName: 'John' }]}>
                    <TableBody>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}></TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}></TableCell>
                        <TableCell weighting={weightingAncho} style={styles.celdaTitulo}>Onda S Vd (cm/s)</TableCell>
                        <TableCell weighting={weightingAngosto} style={styles.celda}>{props.estudio.atributos.dp_Onda_S_Vd}</TableCell>
                    </TableBody>
                </Table>

                {espaciado}

                <View style={{ marginTop: 2, marginBottom: 2 }}>
                    <Text style={styles.tituloDescripcion}>Descripción</Text>
                    <Text style={styles.textoDefecto}>
                        {props.estudio.atributos.descripcion}
                    </Text>
                </View>
                <View style={{ marginBottom: 3 }}>
                    <Text style={styles.tituloDatoPaciente}>Conclusión</Text>
                    <Text style={styles.valorDatoPaciente}>{props.estudio.atributos.conclusion}</Text>
                </View>

            </View>
        </View>
    )
}