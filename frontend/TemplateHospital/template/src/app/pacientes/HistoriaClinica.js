import React, { Component } from "react";
import ContenedorSeccion from "../otros/ContenedorSeccion";
import ListadoHistoriaClinica from "./ListadoHistoriaClinica";
import InfoPacienteSeleccionado from "./InfoPacienteSeleccionado";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import PersonIcon from "@material-ui/icons/Person";

export default class HistoriaClinica extends Component {
  constructor(props) {
    super(props);

    this.state = {
      breadcrumb: [
        {
          pathname: "/pacientes",
          state: "",
          texto: "Pacientes",
          icon: <PersonIcon />,
        },
        {
          pathname: "/pacientes/historia-clinica",
          state: { paciente: this.props.location.state.paciente },
          texto: "Historia clínica",
          icon: <LibraryBooksIcon />,
        },
      ],
    };
  }

  // Para pasarle el breadcrumb al header
  componentDidMount() {
    this.props.actualizarHeader({
      breadcrumb: this.state.breadcrumb,
    });
  }

  render() {
    // Paciente del cual voy a listar su historia clínica.
    const paciente = this.props.location.state.paciente;

    return (
      <ContenedorSeccion
        nombre="Historia clínica"
        descripcion="Podés consultar los estudios registrados y editar o eliminar alguno"
        preComponentes={[
          <InfoPacienteSeleccionado paciente={paciente} />
        ]}
        componentes={[
          <ListadoHistoriaClinica paciente={paciente}/>,
        ]}
      />
    );
  }
}
