import React, { Component } from 'react';
import FormularioCrearPaciente from './FormularioCrearPaciente';
import ContenedorSeccion from '../otros/ContenedorSeccion';
import Box from '@material-ui/core/Box';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import PersonIcon from '@material-ui/icons/Person';

export default class CrearPaciente extends Component {
    constructor(props) {
        super(props);

        this.state = {
            breadcrumb: [
                { pathname: '/pacientes', state: '', texto: 'Pacientes', icon: <PersonIcon /> },
                { pathname: '/pacientes/crear-paciente', state: '', texto: 'Crear paciente', icon: <PersonAddIcon />  }
            ]
        }
    };

    // Para pasarle el breadcrumb al header
    componentDidMount() {
        this.props.actualizarHeader({
            breadcrumb: this.state.breadcrumb
        })
    }

    render() {

        return (
            <ContenedorSeccion
                nombre="Crear paciente"
                descripcion="Podés crear un nuevo paciente"
                componentes={[
                    <FormularioCrearPaciente />
                ]}
            />
        )
    }
}