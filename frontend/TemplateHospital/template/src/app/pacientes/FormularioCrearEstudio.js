// React and core libraries
import React, { Component } from "react";
import axios from "axios";
// User made components
import ModalPaciente from "./ModalPaciente.js";
import EstudioBasico from "../estudios/FormulariosEstudios/EstudioBasico.jsx";
import EstudioDisincronia from "../estudios/FormulariosEstudios/EstudioDisincronia.jsx";
import EstudioSalinaAgitada from "../estudios/FormulariosEstudios/EstudioSalinaAgitada.jsx";
import Estudio2dDoppler from "../estudios/FormulariosEstudios/Estudio2dDoppler.jsx";
import EstudioCongenitas from "../estudios/FormulariosEstudios/EstudioCongenitas.jsx";
import EstudioTransesofagico from "../estudios/FormulariosEstudios/EstudioTransesofagico.jsx";
// Core Material-UI
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Alert from "@material-ui/lab/Alert";
import InputAdornment from "@material-ui/core/InputAdornment";
import Paper from "@material-ui/core/Paper";
import Collapse from "@material-ui/core/Collapse";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import HelpIcon from "@material-ui/icons/Help";

export default class FormularioCrearEstudio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // Datos para el estudio y selects
      tipo: {
        id: "",
        nombre: "",
      },
      fecha: this.formatDate(new Date()),
      conclusion: "",
      especialista: {
        id: "",
        nombre: "",
      },
      motivo: {
        id: null,
        nombre: "",
      },
      diagnostico_obligatorio: {
        id: null,
        nombre: "",
      },
      diagnostico_opcional_1: {
        id: null,
        nombre: "",
      },
      diagnostico_opcional_2: {
        id: null,
        nombre: "",
      },
      // Al paciente lo seteo en el componentDidMount
      paciente: null,

      // Datos especificos

      id_descripcion_predefinida: "",
      id_conclusion_predefinida: "",
      situs: "",
      auriculo_ventricular: "",
      ventriculo_arterial: "",

      // Todas las opciones predef para seleccionar
      tipos: [],
      especialistas: [],
      motivos: [],

      // Para modal
      abrirModal: false,
      huboError: false,
      mensaje: "¿Confirma la creación del estudio?",
      cargandoAccion: false,
      iconColor: "rgb(48, 63, 159)",
      icon: "far fa-question-circle",
      botonesModal: [],
      desactivarCruz: false,

      errores: {
        especialista: false,
        motivo: false,
        diagnostico_obligatorio: false,
        tipo_estudio: false,
        conclusion: false,
        escenario: false,
        situs: false,
        auriculo_ventricular: false,
        ventriculo_arterial: false,
        descripcion_congenitas: false,
        descripcion_transesofagico: false,
        descripcion_2dDoppler: false,
        errorAlertaGeneral: false,
        errorAlertaTipo: false,
        errorAlertaEspecifico: false,
      },
    };

    this.cambiarCampo = this.cambiarCampo.bind(this);
    this.manejarSubmit = this.manejarSubmit.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
    this.limpiarErrores = this.limpiarErrores.bind(this);
    this.limpiarForm = this.limpiarForm.bind(this);
    this.noFaltanDatosGenerales = this.noFaltanDatosGenerales.bind(this);
    this.noFaltanDatosEspecificos = this.noFaltanDatosEspecificos.bind(this);
    this.fetchTipos = this.fetchTipos.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.cambiarTipoEstudio = this.cambiarTipoEstudio.bind(this);
    this.cambiarOpcionPredefinida = this.cambiarOpcionPredefinida.bind(this);
    this.fetchOpcionesPredefinidas = this.fetchOpcionesPredefinidas.bind(this);
    this.cambiarMotivo = this.cambiarMotivo.bind(this);
    this.cambiarDiagObligatorio = this.cambiarDiagObligatorio.bind(this);
    this.cambiarDiagOpcional1 = this.cambiarDiagOpcional1.bind(this);
    this.cambiarDiagOpcional2 = this.cambiarDiagOpcional2.bind(this);
    this.inicializarPaciente = this.inicializarPaciente.bind(this);
  }

  componentDidMount() {
    // Inicializo al paciente
    this.inicializarPaciente();

    this.fetchTipos();
    this.fetchOpcionesPredefinidas();
  }

  inicializarPaciente() {
    var p;
    if (this.props.location && this.props.location.state.paciente) {
      p = this.props.location.state.paciente;
    } else p = this.props.paciente;

    this.setState({
      paciente: p,
    });
  }

  fetchTipos() {
    // Me traigo todos los tipos de estudio
    fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/tipo_estudio?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.status === 401) {
            localStorage.setItem("alert", "Expired/Invalid JWT Token");
            document.cookie =
              "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            window.location.href = "/user-pages/login-1";
          } else {
            this.setState({
              //isLoaded: true,
              //Estructura: id, nombre, nombre_mostrado
              tipos: result.body,
            });
          }
        },
        // Nota de la docu: es importante manejar errores aquí y no en
        // un bloque catch() para que no interceptemos errores
        // de errores reales en los componentes.
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }
  async fetchOpcionesPredefinidas() {
    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/especialista?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          this.setState(
            {
              especialistas: data.body,
            },
            () => {
              this.setState({
                desactivarCruz: false,
                botonesModal: [
                  {
                    texto: "Confirmar",
                    onClick: this.manejarSubmit,
                    color: "green",
                  },
                  {
                    link: "",
                    state: "",
                    texto: "Rechazar",
                    onClick: this.cerrarModal,
                    color: "crimson",
                  },
                ],
              });
            }
          );
        }
      });
    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/motivo?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          this.setState({
            motivos: data.body,
          });
        }
      });
    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/diagnostico?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          this.setState({
            diagnosticos: data.body,
          });
        }
      });
  }

  // Actualiza el campo que ni es select ni datepicker
  cambiarCampo(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }
  // Actualizo el tipo de estudio con el ID y el nombre
  cambiarTipoEstudio(event) {
    const { id } = event.currentTarget.dataset;
    this.setState({
      tipo: {
        id: id,
        nombre: event.target.value,
      },
    });
    this.setState((prevState) => ({
      errores: {
        ...prevState.errores,
        errorAlertaEspecifico: false,
      },
    }));
  }
  // Se basa en el nombre del select, y usa eso como índice para actualizar el estado.
  // Actualiza el id y el nombre (o texto)
  cambiarOpcionPredefinida(event) {
    const { id } = event.currentTarget.dataset;
    this.setState({
      [event.target.name]: {
        id: id,
        nombre: event.target.value,
      },
    });
  }

  // Actualiza el estado de la fecha
  cambiarFecha = (date) => {
    this.setState({
      fecha: this.formatDate(date),
    });
  };

  // Cambio de datos especificos en componentes hijos

  cambiarConclusionPredefinida(conclusion_id) {
    this.setState({ id_conclusion_predefinida: conclusion_id });
  }

  cambiarSitus(situs) {
    this.setState({ situs: situs });
  }

  cambiarAV(AV) {
    this.setState({ auriculo_ventricular: AV });
  }

  cambiarVA(VA) {
    this.setState({ ventriculo_arterial: VA });
  }

  limpiarErrores() {
    this.setState({
      errores: {
        especialista: false,
        motivo: false,
        diagnostico_obligatorio: false,
        tipo_estudio: false,
        conclusion: false,
        escenario: false,
        situs: false,
        auriculo_ventricular: false,
        descripcion_transesofagico: false,
        ventriculo_arterial: false,
        errorAlertaGeneral: false,
        errorAlertaTipo: false,
        errorAlertaEspecifico: false,
      },
    });
  }

  noFaltanDatosGenerales() {
    var hayError = false;
    if (this.state.especialista.id === "") {
      hayError = true;
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          especialista: true,
        },
      }));
    }
    if (this.state.motivo.id === null) {
      hayError = true;
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          motivo: true,
        },
      }));
    }
    if (this.state.diagnostico_obligatorio.id === null) {
      hayError = true;
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          diagnostico_obligatorio: true,
        },
      }));
    }
    if (this.state.tipo.nombre === "") {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          tipo_estudio: true,
        },
      }));
      if (hayError) {
        this.setState((prevState) => ({
          errores: {
            ...prevState.errores,
            errorAlertaGeneral: true,
          },
        }));
      }
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorAlertaTipo: true,
        },
      }));
      window.scrollTo({
        behavior: "smooth",
        top: 0,
      });
      return false;
    }
    if (hayError) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorAlertaGeneral: true,
        },
      }));
      window.scrollTo({
        behavior: "smooth",
        top: 0,
      });
      return false;
    } else {
      return true;
    }
  }

  cambiarMotivo(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        motivo: {
          ...prevState.motivo,
          id: newValue,
        },
      }));
    } else {
      this.setState({
        motivo: {
          id: null,
          nombre: "",
        },
      });
    }
  }

  cambiarDiagObligatorio(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        diagnostico_obligatorio: {
          ...prevState.diagnostico_obligatorio,
          id: newValue,
        },
      }));
    } else {
      this.setState({
        diagnostico_obligatorio: {
          id: null,
          nombre: "",
        },
      });
    }
  }

  cambiarDiagOpcional1(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        diagnostico_opcional_1: {
          ...prevState.diagnostico_opcional_1,
          id: newValue,
        },
      }));
    } else {
      this.setState({
        diagnostico_opcional_1: {
          id: null,
          nombre: "",
        },
      });
    }
  }

  cambiarDiagOpcional2(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        diagnostico_opcional_2: {
          ...prevState.diagnostico_opcional_2,
          id: newValue,
        },
      }));
    } else {
      this.setState({
        diagnostico_opcional_2: {
          id: null,
          nombre: "",
        },
      });
    }
  }

  noFaltanDatosEspecificos() {
    var hayError = false;
    if (this.state.tipo.nombre === "salina_agitada") {
      if (this.state.id_conclusion_predefinida === "") {
        hayError = true;
        this.setState((prevState) => ({
          errores: {
            ...prevState.errores,
            escenario: true,
          },
        }));
      }
    } else {
      if (this.state.tipo.nombre === "congenitas") {
        if (this.state.situs === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              situs: true,
            },
          }));
        }
        if (this.state.auriculo_ventricular === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              auriculo_ventricular: true,
            },
          }));
        }
        if (this.state.ventriculo_arterial === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              ventriculo_arterial: true,
            },
          }));
        }
        if (document.getElementById("descripcion").value === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              descripcion_congenitas: true,
            },
          }));
        }
      } else {
        if (this.state.tipo.nombre === "transesofagico") {
          if (document.getElementById("descripcion").value === "") {
            hayError = true;
            this.setState((prevState) => ({
              errores: {
                ...prevState.errores,
                descripcion_transesofagico: true,
              },
            }));
          }
        } else {
          if (this.state.tipo.nombre === "2d_doppler") {
            if (document.getElementById("descripcion").value === "") {
              hayError = true;
              this.setState((prevState) => ({
                errores: {
                  ...prevState.errores,
                  descripcion_2dDoppler: true,
                },
              }));
            }
          }
        }
      }
    }
    if (document.getElementById("conclusion").value === "") {
      hayError = true;
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          conclusion: true,
        },
      }));
    }
    if (hayError) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorAlertaEspecifico: true,
        },
      }));
      document
        .getElementById("select_tipo")
        .scrollIntoView({ behavior: "smooth" });
      return false;
    } else {
      return true;
    }
  }

  sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  limpiarForm() {
    this.setState(
      {
        cargandoAccion: true,
      },
      () => {
        this.setState(
          {
            tipo: {
              id: "",
              nombre: "",
            },
            fecha: this.formatDate(new Date()),
            conclusion: "",
            especialista: {
              id: "",
              nombre: "",
            },
            motivo: {
              id: null,
              nombre: "",
            },
            diagnostico_obligatorio: {
              id: null,
              nombre: "",
            },
            diagnostico_opcional_1: {
              id: null,
              nombre: "",
            },
            diagnostico_opcional_2: {
              id: null,
              nombre: "",
            },
            paciente: this.props.paciente,

            // Datos especificos

            id_descripcion_predefinida: "",
            id_conclusion_predefinida: "",
            situs: "",
            auriculo_ventricular: "",
            ventriculo_arterial: "",
          },
          () => {
            this.setState({ abrirModal: false }, async () => {
              window.scrollTo({
                behavior: "smooth",
                top: 0,
              });
              await this.sleep(500);
              this.setState({
                // Para modal
                abrirModal: false,
                huboError: false,
                mensaje: "¿Confirma la creación del estudio?",
                iconColor: "rgb(48, 63, 159)",
                icon: "far fa-question-circle",
                cargandoAccion: false,
                desactivarCruz: false,
                botonesModal: [
                  {
                    texto: "Confirmar",
                    onClick: this.manejarSubmit,
                    color: "green",
                  },
                  {
                    link: "",
                    state: "",
                    texto: "Rechazar",
                    onClick: this.cerrarModal,
                    color: "crimson",
                  },
                ],
              });
            });
          }
        );
      }
    );
  }

  manejarSubmit() {
    this.limpiarErrores();
    if (this.noFaltanDatosGenerales() && this.noFaltanDatosEspecificos()) {
      this.setState({ cargandoAccion: true });
      var formulario = document.getElementById("form_datos_especificos");
      var datosUnparsed = new FormData(formulario);
      var datosParsed = new FormData();
      for (var unparsedKey of datosUnparsed.keys()) {
        if (datosUnparsed.get(unparsedKey) != "") {
          datosParsed.append(unparsedKey, datosUnparsed.get(unparsedKey));
        }
      }
      datosParsed.append("nombre_tipo", this.state.tipo.nombre);
      datosParsed.append("fecha", this.state.fecha);
      datosParsed.append("id_especialista", this.state.especialista.id);
      datosParsed.append("id_motivo", this.state.motivo.id.id);
      datosParsed.append(
        "id_diagnostico_obligatorio",
        this.state.diagnostico_obligatorio.id.id
      );
      if (this.state.diagnostico_opcional_1.id != null) {
        datosParsed.append(
          "id_diagnostico_opcional_1",
          this.state.diagnostico_opcional_1.id.id
        );
      }
      if (this.state.diagnostico_opcional_2.id != null) {
        datosParsed.append(
          "id_diagnostico_opcional_2",
          this.state.diagnostico_opcional_2.id.id
        );
      }
      datosParsed.append("id_paciente", this.state.paciente.id);
      axios({
        method: "post",
        url: "http://" + process.env.REACT_APP_API_IP + "/api/estudio",
        data: datosParsed,
        headers: {
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
          "Content-Type": "multipart/form-data",
        },
      })
        .then((response) => {
          // Preparo el mensaje para el modal
          this.setState({
            abrirModal: true,
            mensaje: <span>El estudio se ha creado con éxito.</span>,
            icon: "fas fa-check-circle",
            iconColor: "green",
            cargandoAccion: false,
            desactivarCruz: true,
            botonesModal: [
              {
                texto: "Ver estudio completo",
                color: "#3f51b5",
                link: "/pacientes/historia-clinica/ver-estudio",
                state: [
                  { key: "paciente", data: this.props.paciente },
                  { key: "estudio", data: response.data.body.estudio },
                ],
              },
              {
                texto: "Crear otro estudio",
                onClick: this.limpiarForm,
                color: "#3f51b5",
              },
              {
                texto: "Ir a la historia clínica",
                color: "#3f51b5",
                link: "/pacientes/historia-clinica",
                state: { key: "paciente", data: this.props.paciente },
              },
            ],
          });
          // Reseteo los valores del form
          //this.limpiarForm();
        })
        .catch((error) => {
          // Armo un mensaje para mandarle al modal
          var mensajeError = "";
          switch (error.response.data.status) {
            case 400:
              mensajeError = (
                <span>Hubo un error de validación, intentá de nuevo</span>
              );
              break;
            case 401:
              localStorage.setItem("alert", "Expired/Invalid JWT Token");
              document.cookie =
                "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
              window.location.href = "/user-pages/login-1";
              break;
            case 500:
              mensajeError =
                "Ocurrió un error con el servidor, por favor intentá de nuevo.";
              break;
          }
          this.setState({
            mensaje: mensajeError,
            icon: "fas fa-times-circle",
            iconColor: "#dc004e",
            cargandoAccion: false,
            abrirModal: true,
            desactivarCruz: false,
            botonesModal: [
              {
                texto: "Reintentar",
                color: "green",
                onClick: this.manejarSubmit,
              },
              {
                texto: "Cancelar",
                color: "crimson",
                onClick: this.cerrarModal,
              },
            ],
          });
        });
    }
  }

  abrirModal() {
    this.limpiarErrores();
    if (this.noFaltanDatosGenerales() && this.noFaltanDatosEspecificos()) {
      this.setState({ abrirModal: true });
    }
  }

  // A este método llama el modal (component hijo) cuando necesita cerrarse
  cerrarModal() {
    this.setState({
      abrirModal: false,
    });
  }

  // Parse de fecha
  formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  render() {
    // Armar select de tipos de estudio
    const armarSelectTipos = () => {
      if (this.state.tipos) {
        return this.state.tipos.map((tipo) => (
          <MenuItem
            style={{ padding: "5px 15px 5px 15px" }}
            value={tipo.nombre}
            data-id={tipo.id}
          >
            {tipo.nombre_mostrado}
          </MenuItem>
        ));
      } else return "";
    };

    // Armar select de tipos de estudio
    const armarSelectOpcionPredefinida = (arregloDeLaOpcion) => {
      if (arregloDeLaOpcion) {
        return arregloDeLaOpcion.map((opcion) => (
          <MenuItem
            style={{ padding: "5px 15px 5px 15px" }}
            value={opcion.nombre}
            data-id={opcion.id}
          >
            {opcion.nombre}
          </MenuItem>
        ));
      } else return "";
    };

    const datosEstudio = {
      tipo: {
        id: this.state.tipo.id,
        nombre: this.state.tipo.nombre,
      },
      fecha: this.state.fecha,
      conclusion: this.state.conclusion,
      especialista: {
        id: this.state.especialista.id,
        nombre: this.state.especialista.nombre,
      },
      motivo: {
        id: this.state.motivo.id,
        nombre: this.state.motivo.nombre,
      },
      diagnostico_obligatorio: {
        id: this.state.diagnostico_obligatorio.id,
        nombre: this.state.diagnostico_obligatorio.nombre,
      },
      diagnostico_opcional_1: {
        id: this.state.diagnostico_opcional_1.id,
        nombre: this.state.diagnostico_opcional_1.nombre,
      },
      diagnostico_opcional_2: {
        id: this.state.diagnostico_opcional_2.id,
        nombre: this.state.diagnostico_opcional_2.nombre,
      },
      paciente: this.state.paciente,
    };

    return (
      <div>
        <Grid container spacing={3}>
          <Box p={1} flexGrow={1}>
            <Grid item xs={12}>
              <Paper
                style={{
                  padding: "5px",
                  textAlign: "center",
                  background:
                    "linear-gradient(120deg, #2f78e9, #128bfc, #18bef1)",
                }}
              >
                <b style={{ color: "white", fontSize: "20px" }}>
                  Por favor, completá los siguientes datos
                </b>
                <br></br>
                <Typography style={{ color: "white" }} variant="caption">
                  Los campos requeridos están marcados con un *
                </Typography>
              </Paper>
            </Grid>
          </Box>
          <Grid style={{ marginBottom: 0, paddingBottom: 0 }} item xs={12}>
            <Card variant="outlined">
              <Typography
                style={{
                  fontSize: 15,
                  paddingTop: "15px",
                  paddingLeft: "15px",
                }}
                color="textSecondary"
                gutterBottom
              >
                Datos generales
              </Typography>
              <Collapse in={this.state.errores.errorAlertaGeneral}>
                <Alert
                  style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}
                  variant="filled"
                  severity="error"
                >
                  Por favor, completá los campos marcados en rojo
                </Alert>
              </Collapse>
              <Box
                display="flex"
                flexDirection="row"
                flexWrap="wrap"
                p={2}
                m={1}
                bgcolor="background.paper"
              >
                <Box p={1}>
                  <Grid item xs={4}>
                    <Box display="flex">
                      <FormControl
                        style={{
                          display: "flex",
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                        error={this.state.errores.especialista}
                      >
                        <InputLabel id="especialista">
                          Médico especialista *
                        </InputLabel>
                        <Select
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-user-md"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          MenuProps={{ disableScrollLock: true }}
                          id="especialista"
                          name="especialista"
                          labelId="especialista"
                          value={datosEstudio.especialista.nombre}
                          onChange={this.cambiarOpcionPredefinida}
                          children={armarSelectOpcionPredefinida(
                            this.state.especialistas
                          )}
                        ></Select>
                      </FormControl>
                    </Box>
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <Autocomplete
                      id="motivo"
                      options={this.state.motivos}
                      getOptionLabel={(option) => option.nombre}
                      style={{ minWidth: 300, maxWidth: 300 }}
                      onChange={this.cambiarMotivo}
                      onInputChange={(event, newInputValue) => {
                        this.setState((prevState) => ({
                          motivo: {
                            ...prevState.motivo,
                            nombre: newInputValue,
                          },
                        }));
                      }}
                      value={this.state.motivo.id}
                      inputValue={this.state.motivo.nombre}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          error={this.state.errores.motivo}
                          style={{ marginTop: 4 }}
                          InputProps={{
                            ...params.InputProps,
                            startAdornment: (
                              <>
                                <InputAdornment position="start">
                                  <HelpIcon />
                                </InputAdornment>
                                {params.InputProps.startAdornment}
                              </>
                            ),
                          }}
                          label="Motivo de estudio *"
                        />
                      )}
                    />
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <Box display="flex">
                      <Autocomplete
                        id="diagnostico_obligatorio"
                        options={this.state.diagnosticos}
                        getOptionLabel={(option) => option.nombre}
                        style={{ minWidth: 300, maxWidth: 300 }}
                        value={this.state.diagnostico_obligatorio.id}
                        inputValue={this.state.diagnostico_obligatorio.nombre}
                        onChange={this.cambiarDiagObligatorio}
                        onInputChange={(event, newInputValue) => {
                          this.setState((prevState) => ({
                            diagnostico_obligatorio: {
                              ...prevState.diagnostico_obligatorio,
                              nombre: newInputValue,
                            },
                          }));
                        }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            error={this.state.errores.diagnostico_obligatorio}
                            name="diagnostico_obligatorio"
                            style={{ marginTop: 4 }}
                            InputProps={{
                              ...params.InputProps,
                              startAdornment: (
                                <>
                                  <InputAdornment position="start">
                                    <i
                                      className="fas fa-notes-medical"
                                      style={{ fontSize: "24px" }}
                                    ></i>
                                  </InputAdornment>
                                  {params.InputProps.startAdornment}
                                </>
                              ),
                            }}
                            label="Diagnóstico obligatorio *"
                          />
                        )}
                      />
                    </Box>
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <Box display="flex">
                      <Autocomplete
                        id="diagnostico_opcional_1"
                        options={this.state.diagnosticos}
                        getOptionLabel={(option) => option.nombre}
                        style={{ minWidth: 300, maxWidth: 300 }}
                        value={this.state.diagnostico_opcional_1.id}
                        inputValue={this.state.diagnostico_opcional_1.nombre}
                        onChange={this.cambiarDiagOpcional1}
                        onInputChange={(event, newInputValue) => {
                          this.setState((prevState) => ({
                            diagnostico_opcional_1: {
                              ...prevState.diagnostico_opcional_1,
                              nombre: newInputValue,
                            },
                          }));
                        }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            name="diagnostico_opcional_1"
                            style={{ marginTop: 4 }}
                            InputProps={{
                              ...params.InputProps,
                              startAdornment: (
                                <>
                                  <InputAdornment position="start">
                                    <i
                                      className="fas fa-notes-medical"
                                      style={{ fontSize: "24px" }}
                                    ></i>
                                  </InputAdornment>
                                  {params.InputProps.startAdornment}
                                </>
                              ),
                            }}
                            label="Diagnóstico opcional 1"
                          />
                        )}
                      />
                    </Box>
                  </Grid>
                </Box>
                <Box p={1}>
                  <Grid item xs={4}>
                    <Box display="flex">
                      <Autocomplete
                        id="diagnostico_opcional_2"
                        options={this.state.diagnosticos}
                        getOptionLabel={(option) => option.nombre}
                        style={{ minWidth: 300, maxWidth: 300 }}
                        value={this.state.diagnostico_opcional_2.id}
                        inputValue={this.state.diagnostico_opcional_2.nombre}
                        onChange={this.cambiarDiagOpcional2}
                        onInputChange={(event, newInputValue) => {
                          this.setState((prevState) => ({
                            diagnostico_opcional_2: {
                              ...prevState.diagnostico_opcional_2,
                              nombre: newInputValue,
                            },
                          }));
                        }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            name="diagnostico_opcional_2"
                            style={{ marginTop: 4 }}
                            InputProps={{
                              ...params.InputProps,
                              startAdornment: (
                                <>
                                  <InputAdornment position="start">
                                    <i
                                      className="fas fa-notes-medical"
                                      style={{ fontSize: "24px" }}
                                    ></i>
                                  </InputAdornment>
                                  {params.InputProps.startAdornment}
                                </>
                              ),
                            }}
                            label="Diagnóstico opcional 2"
                          />
                        )}
                      />
                    </Box>
                  </Grid>
                </Box>
              </Box>
            </Card>
          </Grid>
          <Grid style={{ marginBottom: 0, paddingBottom: 0 }} item xs={12}>
            <Card id="select_tipo" variant="outlined">
              <Alert
                style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}
                variant="filled"
                severity="info"
              >
                Elegí un{" "}
                <span style={{ fontWeight: "900" }}>tipo de estudio</span> para
                completar los datos correspondientes al mismo en la sección de
                abajo
              </Alert>
              <Collapse in={this.state.errores.errorAlertaTipo}>
                <Alert
                  style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}
                  variant="filled"
                  severity="error"
                >
                  Por favor, completá los campos marcados en rojo
                </Alert>
              </Collapse>
              <Typography
                style={{
                  fontSize: 15,
                  paddingTop: "15px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
                color="textSecondary"
                gutterBottom
              >
                Tipo
              </Typography>
              <Box
                display="flex"
                flexDirection="row"
                flexWrap="wrap"
                p={2}
                m={1}
                bgcolor="background.paper"
              >
                <Box>
                  <Grid item xs={4}>
                    <FormControl
                      style={{
                        minWidth: 300,
                        maxWidth: 300,
                      }}
                      error={this.state.errores.tipo_estudio}
                    >
                      <InputLabel id="">Tipo de estudio *</InputLabel>
                      <Select
                        startAdornment={
                          <InputAdornment position="start">
                            <i
                              className="fas fa-book-medical"
                              style={{ fontSize: "25px" }}
                            ></i>
                          </InputAdornment>
                        }
                        MenuProps={{ disableScrollLock: true }}
                        id="tipo"
                        name="tipo"
                        autoWidth={true}
                        labelId="tipo"
                        value={datosEstudio.tipo.nombre}
                        onChange={this.cambiarTipoEstudio}
                        children={armarSelectTipos()}
                      ></Select>
                    </FormControl>
                  </Grid>
                </Box>
              </Box>
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Card id="cardEspecificos" variant="outlined">
              <Typography
                style={{
                  fontSize: 15,
                  paddingTop: "15px",
                  paddingLeft: "15px",
                  paddingRight: "15px",
                }}
                color="textSecondary"
                gutterBottom
              >
                Datos específicos de estudio
              </Typography>
              <Collapse in={this.state.errores.errorAlertaEspecifico}>
                <Alert
                  style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}
                  variant="filled"
                  severity="error"
                >
                  Por favor, completá los campos marcados en rojo
                </Alert>
              </Collapse>

              {this.state.tipo.nombre === "" ? (
                <Alert style={{ margin: 15 }} severity="warning">
                  Aún no has seleccionado un <strong>tipo de estudio</strong>
                </Alert>
              ) : null}
              {this.state.tipo.nombre === "basico" ? (
                <EstudioBasico
                  errorConclusion={this.state.errores.conclusion}
                  data={"creando"}
                />
              ) : this.state.tipo.nombre === "disincronia" ? (
                <EstudioDisincronia
                  errorConclusion={this.state.errores.conclusion}
                  data={"creando"}
                />
              ) : this.state.tipo.nombre === "salina_agitada" ? (
                <EstudioSalinaAgitada
                  id_conclusion_predefinida={
                    this.state.id_conclusion_predefinida
                  }
                  cambiarConclusionPredefinida={this.cambiarConclusionPredefinida.bind(
                    this
                  )}
                  errorEscenario={this.state.errores.escenario}
                  data={"creando"}
                />
              ) : this.state.tipo.nombre === "2d_doppler" ? (
                <Estudio2dDoppler
                  data={"creando"}
                  errorDescripcion={this.state.errores.descripcion_2dDoppler}
                  errorConclusion={this.state.errores.conclusion}
                />
              ) : this.state.tipo.nombre === "congenitas" ? (
                <EstudioCongenitas
                  data={{
                    errorSitus: this.state.errores.situs,
                    errorAV: this.state.errores.auriculo_ventricular,
                    errorVA: this.state.errores.ventriculo_arterial,
                    errorConclusion: this.state.errores.conclusion,
                    errorDescripcion: this.state.errores.descripcion_congenitas,

                    datoSitus: this.state.situs,
                    datosAV: this.state.auriculo_ventricular,
                    datosVA: this.state.ventriculo_arterial,

                    funcSitus: this.cambiarSitus.bind(this),
                    funcAV: this.cambiarAV.bind(this),
                    funcVA: this.cambiarVA.bind(this),
                    creando: true,
                  }}
                />
              ) : this.state.tipo.nombre === "transesofagico" ? (
                <EstudioTransesofagico
                  errorDescripcion={
                    this.state.errores.descripcion_transesofagico
                  }
                  errorConclusion={this.state.errores.conclusion}
                  data={"creando"}
                ></EstudioTransesofagico>
              ) : this.state.tipo.nombre !== "" ? (
                <EstudioBasico
                  errorConclusion={this.state.errores.conclusion}
                  data={"creando"}
                />
              ) : null}
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Button
              style={{
                backgroundColor: "green",
                color: "white",
                fontSize: "15px",
              }}
              variant="contained"
              size="large"
              onClick={() => {
                this.abrirModal();
              }}
              endIcon={
                <i
                  style={{ fontSize: "18px" }}
                  className="fas fa-check-circle"
                ></i>
              }
            >
              Crear Estudio
            </Button>
          </Grid>
        </Grid>

        <ModalPaciente
          abrirModal={this.state.abrirModal}
          cerrarModal={this.cerrarModal}
          mensaje={this.state.mensaje}
          cargandoAccion={this.state.cargandoAccion}
          icon={this.state.icon}
          iconColor={this.state.iconColor}
          botones={this.state.botonesModal}
          desactivarCruz={this.state.desactivarCruz}
        />
      </div>
    );
  }
}
