import React, { Component } from "react";
import Box from "@material-ui/core/Box";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import InputAdornment from "@material-ui/core/InputAdornment";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import InfoPacienteCreandose from "./InfoPacienteCreandose";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import DateFnsUtils from "@date-io/date-fns";
import { es } from "date-fns/locale";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";
import "moment/locale/es";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import Alert from "@material-ui/lab/Alert";
import Collapse from "@material-ui/core/Collapse";

// Modals
import ModalPaciente from "./ModalPaciente";

// Un poco de estilo
const styles = (theme) => ({
  gridItem: {
    margin: "10px 0 10px 0",
  },
  formControl: {
    minWidth: 250,
  },
});

export class FormularioCrearPaciente extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nombre: "",
      apellido: "",
      dni: null,
      sexo: "",
      estado: "",
      fecha_nac: null,
      obra_social: {
        id: "",
        nombre: "",
      },
      localidad: "",

      isLoaded: false,
      obras_sociales: "",

      postId: "",
      abrirModal: false,
      mensaje: "¿Confirmar la creación del paciente?",
      cargandoAccion: false,
      iconColor: "rgb(48, 63, 159)",
      icon: "far fa-question-circle",
      botonesModal: [],

      errores: {
        errorNombre: false,
        errorApellido: false,
        errorSexo: false,
        errorEstado: false,
        errorObraSocial: false,
        errorFechaNac: false,
        errorLocalidad: false,
        abrirAlert: false,
      },
    };
    this.cambiarCampo = this.cambiarCampo.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.cambiarObraSocial = this.cambiarObraSocial.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
    this.limpiarForm = this.limpiarForm.bind(this);
    this.noFaltanDatos = this.noFaltanDatos.bind(this);
    this.preSubmit = this.preSubmit.bind(this);
    this.datosPaciente = this.datosPaciente.bind(this);
  }

  datosPaciente(id) {
    return {
      id: id,
      nombre: this.state.nombre,
      apellido: this.state.apellido,
      dni: this.state.dni,
      sexo: this.state.sexo,
      estado: this.state.estado,
      fecha_nac: this.formatDateFinal(this.state.fecha_nac),
      obra_social: {
        id: this.state.obra_social.id,
        nombre: this.state.obra_social.nombre,
      },
      localidad: this.state.localidad,
    };
  }

  // Actualiza el estado del campo que se cambió (excepto fecha de nacimiento)
  cambiarCampo(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }
  // Actualiza el estado de la fecha de nacimiento
  cambiarFecha = (date) => {
    this.setState(
      {
        fecha_nac: date,
      },
      () => {
        console.log(this.state.fecha_nac);
      }
    );
  };
  // Actualizo la obra social con el ID y el nombre
  cambiarObraSocial(event) {
    const { id } = event.currentTarget.dataset;
    this.setState({
      obra_social: {
        id: id,
        nombre: event.target.value,
      },
    });
  }

  // La paso al formato de la API
  formatDateFinal(date) {
    return moment(date).format("yyyy-MM-DD");
  }

  // Verifico si la fecha es válida
  fechaValida(date) {
    var d = this.formatDateFinal(date);
    return moment(d).isValid();
  }

  noFaltanDatos() {
    var hayError = false;

    if (!this.state.nombre) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorNombre: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.apellido) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorApellido: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.sexo) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorSexo: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.estado) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorEstado: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.fecha_nac || !this.fechaValida(this.state.fecha_nac)) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorFechaNac: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.obra_social.id) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorObraSocial: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!this.state.localidad) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorLocalidad: true,
          abrirAlert: true,
        },
      }));
      hayError = true;
    }

    if (!hayError) {
      return true;
    } else {
      return false;
    }
  }

  limpiarErrores() {
    this.setState({
      errores: {
        errorNombre: false,
        errorApellido: false,
        errorSexo: false,
        errorEstado: false,
        errorObraSocial: false,
        errorFechaNac: false,
        errorLocalidad: false,
        abrirAlert: false,
      },
    });
  }

  preSubmit() {
    this.limpiarErrores();
    if (this.noFaltanDatos()) {
      this.setState(
        {
          iconColor: "rgb(48, 63, 159)",
          icon: "far fa-question-circle",
          mensaje: "¿Confirmar la creación del paciente?",
          botonesModal: [
            {
              texto: "Confirmar",
              onClick: this.handleSubmit,
              color: "green",
            },
            {
              link: "",
              state: "",
              texto: "Rechazar",
              onClick: this.cerrarModal,
              color: "crimson",
            },
          ],
        },
        () => {
          this.setState({ abrirModal: true });
        }
      );
    } else {
      //Avisar que faltan datos
    }
  }

  // Submit del crear paciente
  handleSubmit(event) {
    if (this.noFaltanDatos()) {
      this.setState({ cargandoAccion: true }, () => {
        var datos = new FormData();

        datos.append("nombre", this.state.nombre);
        datos.append("apellido", this.state.apellido);
        console.log(this.state.dni === null);
        if (this.state.dni !== null && this.state.dni !== "") {
          datos.append("dni", this.state.dni);
        }
        datos.append("sexo", this.state.sexo);
        datos.append("estado", this.state.estado);
        datos.append("fecha_nac", this.formatDateFinal(this.state.fecha_nac));
        datos.append("obra_social_id", this.state.obra_social.id);
        datos.append("localidad", this.state.localidad);

        axios({
          method: "post",
          url: "http://" + process.env.REACT_APP_API_IP + "/api/paciente",
          data: datos,
          headers: {
            Authorization:
              "Bearer " +
              document.cookie.replace(
                /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
                "$1"
              ),
            "Content-Type": "multipart/form-data",
          },
        })
          .then((response) => {
            console.log(response);
            // Preparo el mensaje para el modal
            this.setState({
              cargandoAccion: false,
              icon: "fas fa-check-circle",
              iconColor: "green",
              botonesModal: [
                {
                  texto: "Crearle un estudio",
                  color: "#3f51b5",
                  link: "/pacientes/crear-estudio",
                  state: {
                    key: "paciente",
                    // le paso el ID también, que lo tengo acá
                    // del response. Lo necesito para el formCrearEstudio
                    data: this.datosPaciente(response.data.paciente.datos.id),
                  },
                },
                {
                  texto: "Volver al listado de pacientes",
                  color: "#3f51b5",
                  link: "/pacientes",
                  state: "",
                },
              ],
              mensaje: (
                <span>
                  El paciente{" "}
                  <strong>
                    {this.state.apellido}, {this.state.nombre}
                  </strong>{" "}
                  se ha creado con éxito.
                </span>
              ),
            });
            // Reseteo los valores del form
            this.limpiarForm();
          })
          .catch((error) => {
            console.log(error);
            // Armo un mensaje para mandarle al modal
            var botones = [];
            var mensajeError = "";
            switch (error.response.data.status) {
              case 400:
                mensajeError = (
                  <span
                    style={{
                      padding: "10px 20px",
                      display: "block",
                      fontSize: 17,
                    }}
                  >
                    El paciente{" "}
                    <strong>
                      {this.state.apellido}, {this.state.nombre}
                    </strong>{" "}
                    no se ha podido crear porque el DNI ingresado ya pertenece a
                    un paciente. Probablemente el paciente que estás creando ya
                    esté registrado en el sistema, verificalo en el listado de
                    pacientes.
                  </span>
                );
                botones.push(
                  {
                    texto: "Aceptar",
                    color: "green",
                    onClick: this.cerrarModal,
                  },
                  {
                    texto: "Ir al listado de pacientes",
                    color: "#3f51b5",
                    link: "/pacientes",
                    state: "",
                  }
                );
                break;
              case 401:
                localStorage.setItem("alert", "Expired/Invalid JWT Token");
                document.cookie =
                  "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                window.location.href = "/user-pages/login-1";
              case 500:
                mensajeError =
                  "Ocurrió un error con el servidor, por favor intentá de nuevo.";
                break;
            }
            if (botones.length == 0) {
              botones.push(
                {
                  texto: "Reintentar",
                  color: "green",
                  onClick: this.handleSubmit,
                },
                {
                  texto: "Cancelar",
                  color: "crimson",
                  onClick: this.cerrarModal,
                }
              );
            }
            this.setState({
              icon: "fas fa-times-circle",
              iconColor: "#dc004e",
              abrirModal: true,
              mensaje: mensajeError,
              cargandoAccion: false,
              botonesModal: botones,
            });
          });
      });
    } else {
      this.setState({
        abrirModal: true,
        huboError: true,
        mensaje: (
          <span>
            Hay campos obligatorios que faltan rellenarse, verificalo y volvé a
            intentar.
          </span>
        ),
      });
    }

    event.preventDefault();
  }

  limpiarForm() {
    this.setState({
      nombre: "",
      apellido: "",
      dni: null,
      sexo: "",
      estado: "",
      fecha_nac: null,
      obra_social: {
        id: "",
        nombre: "",
      },
      localidad: "",
    });
  }

  componentDidMount() {
    // Me traigo las OBRAS SOCIALES
    fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/obra_social?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.status === 401) {
            localStorage.setItem("alert", "Expired/Invalid JWT Token");
            document.cookie =
              "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            window.location.href = "/user-pages/login-1";
          } else {
            this.setState({
              isLoaded: true,
              obras_sociales: result.body,
            });
          }
        },
        // Nota de la docu: es importante manejar errores aquí y no en
        // un bloque catch() para que no interceptemos errores
        // de errores reales en los componentes.
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  // A este método llama el modal (component hijo) cuando necesita cerrarse
  cerrarModal() {
    this.setState({
      abrirModal: false,
    });
  }

  render() {
    // Obras sociales
    const armarSelectObrasSociales = () => {
      if (this.state.obras_sociales) {
        return this.state.obras_sociales.map((obra_social) => (
          <MenuItem
            style={{ padding: "5px 15px 5px 15px" }}
            value={obra_social.nombre}
            data-id={obra_social.id}
          >
            {obra_social.nombre}
          </MenuItem>
        ));
      } else return "";
    };

    // Localidades
    const armarSelectLocalidades = () => {
      return [
        <MenuItem style={{ padding: "5px 15px 5px 15px" }} value="La Plata">
          La Plata
        </MenuItem>,
        <MenuItem style={{ padding: "5px 15px 5px 15px" }} value="Otro">
          Otro
        </MenuItem>,
      ];
    };

    // Para el estilo
    const { classes } = this.props;

    // Me traigo sólo los campos del paciente del estado
    // Para más comodidad y para pasarle sólo esos datos al InfoPacienteCreandose.js
    const datosPaciente = {
      nombre: this.state.nombre,
      apellido: this.state.apellido,
      dni: this.state.dni,
      sexo: this.state.sexo,
      estado: this.state.estado,
      fecha_nac: this.state.fecha_nac,
      obra_social: {
        id: this.state.obra_social.id,
        nombre: this.state.obra_social.nombre,
      },
      localidad: this.state.localidad,
    };

    return (
      <div>
        <h3>{this.state.postId}</h3>
        <Grid container spacing={2}>
          <form onSubmit={this.handleSubmit}>
            <Box p={1} flexGrow={1}>
              <Grid item xs={12}>
                <Paper
                  style={{
                    padding: "5px",
                    textAlign: "center",
                    background:
                      "linear-gradient(120deg, #2f78e9, #128bfc, #18bef1)",
                  }}
                >
                  <b style={{ color: "white", fontSize: "20px" }}>
                    Por favor, completá los siguientes datos
                  </b>
                  <br></br>
                  <Typography style={{ color: "white" }} variant="caption">
                    Los campos requeridos están marcados con un *
                  </Typography>
                </Paper>
              </Grid>
            </Box>
            <Grid item xs={12} style={{ padding: 12 }}>
              <Card variant="outlined">
                <Collapse in={this.state.errores.abrirAlert}>
                  <Alert
                    style={{
                      marginTop: 10,
                      marginLeft: 20,
                      marginRight: 20,
                    }}
                    variant="filled"
                    severity="error"
                  >
                    Por favor, completá los campos obligatorios marcados en rojo
                  </Alert>
                </Collapse>
                <Typography
                  style={{
                    fontSize: 15,
                    paddingTop: "15px",
                    paddingLeft: "15px",
                  }}
                  color="textSecondary"
                  gutterBottom
                >
                  Datos del paciente
                </Typography>
                <Box
                  display="flex"
                  flexDirection="row"
                  flexWrap="wrap"
                  p={2}
                  m={1}
                  bgcolor="background.paper"
                >
                  <Box p={1}>
                    <Grid item xs={6}>
                      <TextField
                        style={{
                          display: "flex",
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                        inputProps={{
                          maxlength: 50,
                        }}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <i
                                style={{ fontSize: 20 }}
                                className="far fa-user-circle"
                              ></i>
                            </InputAdornment>
                          ),
                        }}
                        aria-describedby="my-helper-text"
                        required
                        id="nombre"
                        name="nombre"
                        value={datosPaciente.nombre}
                        onChange={this.cambiarCampo}
                        label="Nombre"
                        placeholder="Ej: Jorge"
                        helperText="Debe tener como máximo 50 caracteres"
                        error={this.state.errores.errorNombre}
                      />
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={6}>
                      <TextField
                        style={{
                          display: "flex",
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                        inputProps={{
                          maxlength: 50,
                        }}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <i
                                style={{ fontSize: 20 }}
                                className="far fa-user-circle"
                              ></i>
                            </InputAdornment>
                          ),
                        }}
                        required
                        id="apellido"
                        name="apellido"
                        value={datosPaciente.apellido}
                        onChange={this.cambiarCampo}
                        label="Apellido"
                        placeholder="Ej: Gutiérrez"
                        helperText="Debe tener como máximo 50 caracteres"
                        error={this.state.errores.errorApellido}
                      />
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={6}>
                      <TextField
                        style={{
                          display: "flex",
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <i
                                style={{ fontSize: 20 }}
                                className="far fa-id-card"
                              ></i>
                            </InputAdornment>
                          ),
                        }}
                        id="dni"
                        name="dni"
                        type="number"
                        value={datosPaciente.dni}
                        onChange={this.cambiarCampo}
                        label="DNI"
                        placeholder="Ej: 19489231"
                        helperText="No es obligatorio, pero es recomendable"
                        onInput={(e) => {
                          e.target.value = Math.max(0, parseInt(e.target.value))
                            .toString()
                            .slice(0, 10);
                        }}
                      />
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={6}>
                      <FormControl
                        style={{
                          display: "flex",
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                      >
                        <InputLabel
                          error={this.state.errores.errorSexo}
                          id="sexo"
                        >
                          Sexo *
                        </InputLabel>
                        <Select
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-venus-mars"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          MenuProps={{ disableScrollLock: true }}
                          error={this.state.errores.errorSexo}
                          id="sexo"
                          name="sexo"
                          labelId="sexo"
                          value={datosPaciente.sexo}
                          onChange={this.cambiarCampo}
                        >
                          <MenuItem value={"Masculino"}>Masculino</MenuItem>
                          <MenuItem value={"Femenino"}>Femenino</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={6}>
                      <FormControl
                        style={{
                          display: "flex",
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                      >
                        <InputLabel
                          error={this.state.errores.errorEstado}
                          id="estado"
                        >
                          Estado *
                        </InputLabel>
                        <Select
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-hospital-user"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          MenuProps={{ disableScrollLock: true }}
                          id="estado"
                          name="estado"
                          labelId="estado"
                          value={datosPaciente.estado}
                          onChange={this.cambiarCampo}
                          error={this.state.errores.errorEstado}
                        >
                          <MenuItem value={"Ambulatorio"}>Ambulatorio</MenuItem>
                          <MenuItem value={"Internado"}>Internado</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={6}>
                      <FormControl
                        style={{
                          display: "flex",
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                      >
                        <InputLabel
                          error={this.state.errores.errorObraSocial}
                          id="obra_social"
                        >
                          Obra social *
                        </InputLabel>
                        <Select
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-star-of-life"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          MenuProps={{ disableScrollLock: true }}
                          id="obra_social_id"
                          name="obra_social_id"
                          labelId="obra_social"
                          value={datosPaciente.obra_social.nombre}
                          onChange={this.cambiarObraSocial}
                          children={armarSelectObrasSociales()}
                          error={this.state.errores.errorObraSocial}
                        ></Select>
                      </FormControl>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={6}>
                      <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          autoOk
                          cancelLabel="Cancelar"
                          style={{
                            display: "flex",
                            minWidth: 300,
                            maxWidth: 300,
                          }}
                          InputProps={{
                            startAdornment: (
                              <InputAdornment position="start">
                                <i
                                  style={{ fontSize: 20 }}
                                  className="fas fa-calendar-day"
                                ></i>
                              </InputAdornment>
                            ),
                          }}
                          InputLabelProps={{ shrink: true }}
                          placeholder="Ej: 03/09/1994"
                          invalidDateMessage="La fecha no es válida. Recordá: día/mes/año"
                          maxDateMessage="La fecha no puede ser mayor al máximo permitido"
                          DialogProps={{ disableScrollLock: true }}
                          name="fecha_nac"
                          format="dd/MM/yyyy"
                          margin="normal"
                          id="fecha_nac"
                          required
                          label="Fecha de nacimiento"
                          value={datosPaciente.fecha_nac}
                          onChange={this.cambiarFecha}
                          KeyboardButtonProps={{
                            "aria-label": "Cambiar fecha",
                          }}
                          error={this.state.errores.errorFechaNac}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                  </Box>
                  <Box p={1}>
                    <Grid item xs={6}>
                      <FormControl
                        style={{
                          display: "flex",
                          marginTop: 15,
                          minWidth: 300,
                          maxWidth: 300,
                        }}
                      >
                        <InputLabel
                          error={this.state.errores.errorLocalidad}
                          id="localidad"
                        >
                          Localidad *
                        </InputLabel>
                        <Select
                          startAdornment={
                            <InputAdornment position="start">
                              <i
                                className="fas fa-map-marker-alt"
                                style={{ fontSize: "25px" }}
                              ></i>
                            </InputAdornment>
                          }
                          MenuProps={{ disableScrollLock: true }}
                          id="localidad"
                          name="localidad"
                          labelId="localidad"
                          value={datosPaciente.localidad}
                          onChange={this.cambiarCampo}
                          children={armarSelectLocalidades()}
                          error={this.state.errores.errorLocalidad}
                        ></Select>
                      </FormControl>
                    </Grid>
                  </Box>
                </Box>
              </Card>
            </Grid>
          </form>
        </Grid>
        <Grid item xs={12} style={{ padding: 4 }}>
          <Card variant="outlined">
            <InfoPacienteCreandose paciente={datosPaciente} tarea="crear" />
          </Card>
          <Button
            variant="contained"
            size="large"
            style={{ marginTop: 10, color: "white", backgroundColor: "green" }}
            endIcon={
              <AddCircleIcon style={{ marginBottom: 2 }}></AddCircleIcon>
            }
            onClick={this.preSubmit}
          >
            {" "}
            Crear Paciente
          </Button>
        </Grid>

        <ModalPaciente
          abrirModal={this.state.abrirModal}
          cerrarModal={this.cerrarModal}
          mensaje={this.state.mensaje}
          cargandoAccion={this.state.cargandoAccion}
          icon={this.state.icon}
          iconColor={this.state.iconColor}
          botones={this.state.botonesModal}
        />
      </div>
    );
  }
}

export default withStyles(styles)(FormularioCrearPaciente);
