import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import AddBoxIcon from "@material-ui/icons/AddBox";
import { Link } from "react-router-dom";

export default class BotonCrearEstudio extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={{ padding: "20px 10px" }}>
        <Link
          to={{
            pathname: "/pacientes/crear-estudio",
            state: { paciente: this.props.paciente },
          }}
        >
          <Button
            variant="contained"
            style={{
              padding: "10px 20px",
              color: "white",
              backgroundColor: "#297ced",
            }}
            endIcon={<AddBoxIcon />}
          >
            Nuevo Estudio
          </Button>
        </Link>
      </div>
    );
  }
}
