import React, { Component } from 'react';
import { Page, Text, View, Document, StyleSheet, PDFViewer, PDFDownloadLink, Image } from '@react-pdf/renderer';
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Divider from '@material-ui/core/Divider';
import PersonIcon from '@material-ui/icons/Person';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import CheckCircle from "@material-ui/icons/CheckCircle";
import InfoPacienteSeleccionado from './InfoPacienteSeleccionado';

import PDFInfoEstudioBasico from './PDFInfoEstudioBasico';

// Logos
import LogoFacu from "../../assets/images/Logo-completo-facu.png";
import LogoHospital from "../../assets/images/Logo-hospital.png";
import LogoLaboratorio from "../../assets/images/Logo-laboratorio.png";

import moment from "moment";
import "moment/locale/es";

const fontSize = 12;

// Create styles
const styles = StyleSheet.create({
    page: {
        flexDirection: 'column',
        backgroundColor: 'white',
        padding: '30px 50px 50px 50px',
    },
    /*
    sectionTitulo: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        marginBottom: 15,
        padding: '20px 20px 20px 20px',
        backgroundColor: '#f9f9f9',
        maxHeight: '40px',
    },
    */
    footer: {
        position: 'absolute',
        fontSize: fontSize - 3,
        bottom: 40,
        left: 0,
        right: 0,
        textAlign: 'center'
    },
    infoFacu: {
        position: 'absolute',
        fontSize: fontSize - 4,
        color: 'gray',
        bottom: 30,
        left: 50,
        right: 0,
        textAlign: 'left'
    },
    logoFacu: {
        width: '150px'
    },
    infoFecha: {
        position: 'absolute',
        fontSize: fontSize - 3,
        color: '#444444',
        bottom: 40,
        left: 0,
        right: 50,
        textAlign: 'right'
    },

    tituloDatoPaciente: {
        fontSize: fontSize - 2,
        fontWeight: '600'
    },
    valorDatoPaciente: {
        fontSize: fontSize - 3,
        color: '#444444'
    },

    // Header
    header: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 20 
    },
    logoHospitalContenedor: {
        display: 'flex',
        fontSize: fontSize - 2,
        color: 'gray',
        textAlign: 'left'
    },
    logoHospital: {
        width: '90px'
    },
    logoLaboratorioContenedor: {
        display: 'flex',
        fontSize: fontSize - 2,
        color: 'gray',
        textAlign: 'right'
    },
    logoLaboratorio: {
        width: '80px'
    },
    tituloContenedor: {
        display: 'flex',
        maxWidth: '50%'
    },
    titulo: {
        fontSize: fontSize+1,
        textAlign: 'center'
    },
});


// Create Document Component
class PDFEstudio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            estaListo: false,
            avisarDescarga: false
        }

        this.clickDescargar = this.clickDescargar.bind(this);
        this.format = this.format.bind(this);
        this.fechaHoy = this.fechaHoy.bind(this);
    }

    componentDidMount() {
        this.setState({
            estaListo: true
        })
    }

    clickDescargar() {
        this.setState({
            avisarDescarga: true
        })
    }

    format(input) {
        var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
        if (!input || !input.match(pattern)) {
            return null;
        }
        return input.replace(pattern, '$3/$2/$1');
    };

    fechaHoy() {
        return moment().format('L')
    }

    fechaFormateada() {
        return (
            moment(this.props.estudio.informacion_general.fecha)
                .format('L').replace('_', '-')
        )
    }

    render() {

        const estudio = this.props.estudio;
        const paciente = this.props.paciente;
        const infoEstudioComplejo = this.props.infoEstudioComplejo;

        /* La estructura de un estudio es:

        .informacion_general
        id, fecha, descripcion, especialista, tipo, motivo, diagnostico_obligatorio, diagnostico_opcional_1, diagnostico_opcional_2

        .atributos
        [varían]
      
        */

        const dividerGrueso = (
            <View
                style={{
                    marginTop: '5px',
                    borderBottomColor: '#676767',
                    borderBottomWidth: 1,
                }}
            />
        )
        const dividerFino = (
            <View
                style={{
                    margin: '5px 0px 10px',
                    borderBottomColor: '#b8b8b8',
                    borderBottomWidth: 1,
                }}
            />
        )

        const pdf = (
            <Document file="pdf-estudio.pdf">
                <Page size="A4" style={styles.page}>
                    {/* HEADER */}
                    <View style={styles.header}>
                        {/* Logo hospital */}
                        <View style={styles.logoHospitalContenedor}>
                            <Image
                                fixed
                                src={LogoHospital}
                                style={styles.logoHospital}
                            />
                        </View>
                        {/* Título */}
                        <View style={styles.tituloContenedor}>
                            <Text style={styles.titulo}>Informe del Laboratorio de Ecocardiografía 
                            del Hospital San Martín de La Plata</Text>
                        </View>
                        {/* Logo laboratorio */}
                        <View style={styles.logoLaboratorioContenedor}>
                            <Image
                                fixed
                                src={LogoLaboratorio}
                                style={styles.logoLaboratorio}
                            />
                        </View>
                    </View>

                    {/* Datos básicos (menos descripción y conclusión) */}
                    <PDFInfoEstudioBasico
                        paciente={paciente}
                        estudio={estudio}
                    />

                    {/* Si es un estudio básico muestro la conclusión,
                        sino llamo a su componente */}
                    {infoEstudioComplejo != '' ? infoEstudioComplejo :
                        <View style={{ marginBottom: 3 }}>
                            <Text style={styles.tituloDatoPaciente}>Conclusión</Text>
                            <Text style={styles.valorDatoPaciente}>{this.props.estudio.atributos.conclusion}</Text>
                        </View>
                    }

                    <View style={styles.infoFacu}>
                        <Image
                            fixed
                            src={LogoFacu}
                            style={styles.logoFacu}
                        />
                    </View>
                    <View style={styles.footer}>
                        <Text render={({ pageNumber, totalPages }) => (
                            `${pageNumber} / ${totalPages}`
                        )} fixed />
                    </View>
                    <View style={styles.infoFecha}>
                        <Text
                            fixed
                        >
                            Informe generado el {this.fechaHoy()}
                        </Text>
                    </View>

                </Page>
            </Document>
        )

        const fechaFormateada = this.fechaFormateada()

        return (
            <Box>
                <Box mt={1}>
                    <h5>Estudio <strong>{estudio.informacion_general.tipo.nombre_mostrado} </strong>
                        del {this.format(estudio.informacion_general.fecha)}</h5>
                </Box>
                {this.state.estaListo
                    ? (

                        !this.state.avisarDescarga ? (
                            <div>
                                <Box mt={3}><h5>¡Tu informe está listo!</h5></Box>
                                <Box my={2}>
                                    <PDFDownloadLink
                                        document={pdf}
                                        fileName={
                                            "Informe de estudio Ecocardiografía - " +
                                            paciente.nombre + " " + paciente.apellido + " " +
                                            "(" + fechaFormateada + ", " +
                                            estudio.informacion_general.tipo.nombre_mostrado + ")" +
                                            ".pdf"
                                        }
                                    >
                                        <Button
                                            color="primary"
                                            variant="contained"
                                            onClick={this.clickDescargar}
                                        >Descargar PDF
                                </Button>
                                    </PDFDownloadLink>
                                </Box>
                            </div>
                        )
                            : (
                                <Box container>
                                    <Box my={2} display='flex' item alignItems="center">
                                        <CheckCircle style={{ display: 'inline', color: 'green', fontSize: '40px' }} />
                                        <Divider></Divider>
                                        <h5 style={{ margin: '0px 0px 0px 5px' }}>¡Genial! El informe ya se descargó</h5>
                                    </Box>
                                    <Box my={2}>
                                        <PDFDownloadLink
                                            document={pdf} fileName="pdf-estudio.pdf">
                                            <Button
                                                color="primary"
                                                variant="contained"
                                                onClick={this.clickDescargar}
                                            >Descargar nuevamente
                                        </Button>
                                        </PDFDownloadLink>
                                    </Box>
                                </Box>
                            )

                    )
                    : <h4>Cargando...</h4>}
            </Box>
        )
    }

}

export default withStyles(styles)(PDFEstudio);
