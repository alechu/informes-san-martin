import React from "react";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import { withStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';
import { Button } from "react-bootstrap";

const defaultToolbarStyles = {
    iconButton: {
    },
};

class BotonCrearPacienteToolbar extends React.Component {

    handleClick = () => {
        console.log("¡Crear paciente!");
    }

    render() {

        return (
            <div style={{ padding: '20px 10px' }}>
                <Link to="/pacientes/crear-paciente">
                    <Button
                        color="primary"
                        size="large"
                        style={{ fontSize: '1.1em', padding: '10px 20px' }}
                    >
                        Crear paciente
                    </Button>
                </Link>
            </div>
        );
    }

}

export default BotonCrearPacienteToolbar;