import React, { Component } from 'react';
import FormularioEditarPaciente from './FormularioEditarPaciente';
import ContenedorSeccion from '../otros/ContenedorSeccion';
import Box from '@material-ui/core/Box';
import InfoPacienteSeleccionado from './InfoPacienteSeleccionado';
import EditIcon from '@material-ui/icons/Edit';
import PersonIcon from '@material-ui/icons/Person';

export default class EditarPaciente extends Component {
    constructor(props) {
        super(props);

        this.state = {
            breadcrumb: [
                { pathname: '/pacientes', state: '', texto: 'Pacientes', icon: <PersonIcon /> },
                { pathname: '/pacientes/editar-paciente', state: {paciente: this.props.location.state.paciente}, texto: 'Editar paciente', icon: <EditIcon />  }
            ]
        }
    };

    // Para pasarle el breadcrumb al header
    componentDidMount() {
        this.props.actualizarHeader({
            breadcrumb: this.state.breadcrumb
        })
    }

    render() {

        // Paciente al que le voy a agregar un estudio.
        const paciente = this.props.location.state.paciente;

        return (
            <ContenedorSeccion
                nombre="Editar paciente"
                descripcion="Podés editar cualquier atributo del paciente seleccionado"
                preComponentes={[
                    <InfoPacienteSeleccionado paciente={paciente} />
                ]}
                componentes={[
                    <FormularioEditarPaciente 
                        paciente={paciente}
                    />
                ]}
            />
        )
    }
}
