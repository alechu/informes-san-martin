import React, { Component } from "react";
import Box from "@material-ui/core/Box";
import PersonIcon from "@material-ui/icons/Person";
import RecentActorsIcon from "@material-ui/icons/RecentActors";

export default class InfoPacienteSeleccionado extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const paciente = this.props.paciente;
    const nombreCompleto = (
      paciente.apellido.toUpperCase() +
      ", " +
      paciente.nombre
    );

    return (
      <div style={{ width: "100%", background: "white", padding: "20px 30px" }}>
        <Box display="flex" flexDirection="column">
          <Box>
            <i className="fas fa-hospital-user"></i> Paciente:{" "}
            <strong>{nombreCompleto}</strong>
          </Box>
          <Box>
            {" "}
            <i className="fas fa-id-card"></i> DNI:{" "}
            <strong>{paciente.dni}</strong>
          </Box>
        </Box>
      </div>
    );
  }
}
