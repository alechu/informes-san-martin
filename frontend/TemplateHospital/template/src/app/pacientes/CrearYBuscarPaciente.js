import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import Alert from "@material-ui/lab/Alert";
import Box from "@material-ui/core/Box";
import { Link } from "react-router-dom";
import AddBoxIcon from "@material-ui/icons/AddBox";
import CircularProgress from "@material-ui/core/CircularProgress";
import ClearIcon from "@material-ui/icons/Clear";
import { Snackbar } from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import { es } from "date-fns/locale";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";
import "moment/locale/es";
import { withStyles } from "@material-ui/core/styles";

// Estilos
const styles = {
  boxItem: {
    margin: "0 10px",
    maxWidth: 150,
  },
};

const urlsAPI = {
  dni: "http://" + process.env.REACT_APP_API_IP + "/api/paciente?dni=",
  apellido:
    "http://" +
    process.env.REACT_APP_API_IP +
    "/api/paciente/all/search?apellido=",
  fecha_nac:
    "http://" +
    process.env.REACT_APP_API_IP +
    "/api/paciente/all/search?fecha_nac=",
};

class CrearYBuscarPaciente extends Component {
  constructor(props) {
    super(props);

    this.buscarPacienteDNI = this.buscarPacienteDNI.bind(this);
    this.buscarPacienteApellido = this.buscarPacienteApellido.bind(this);
    this.buscarPacienteFechaNacimiento = this.buscarPacienteFechaNacimiento.bind(
      this
    );
    this.buscarSubmit = this.buscarSubmit.bind(this);
    this.actualizarPacientes = this.actualizarPacientes.bind(this);
    this.setearPacientesBuscados = this.setearPacientesBuscados.bind(this);
    this.cambiarCampo = this.cambiarCampo.bind(this);
    this.cambiarCampoDNI = this.cambiarCampoDNI.bind(this);
    this.onChangeFiltros = this.onChangeFiltros.bind(this);
    this.reiniciarPacientes = this.reiniciarPacientes.bind(this);
    this.cerrarAlert = this.cerrarAlert.bind(this);

    this.state = {
      dni: "",
      apellido: "",
      nombre: "",
      fecha_nac: null,
      hayDatos: false,
      activarLoading: false,

      alertSeverity: "",
      mostrarAlert: false,
      alertTexto: "",

      urlAPI: "",

      totalEncontrados: null,
      hayErrorDNI: false,
    };
  }

  // Actualiza el estado de la fecha de nacimiento
  cambiarFecha = (date) => {
    this.setState(
      {
        fecha_nac: date,
      },
      () => {
        this.onChangeFiltros();
      }
    );
  };

  /*
    Reinicio inputs y le mando null al método
    que actualiza los pacientes, de esta forma
    se resetea la tabla
  */
  reiniciarPacientes() {
    this.setState({
      hayDatos: false,
      dni: "",
      apellido: "",
      nombre: "",
      fecha_nac: null,
      hayErrorDNI: false,
    });
    this.actualizarPacientes(null);
  }

  // Acá detecto si borré todos los filtros o no,
  // en el caso de que sí, reinicio la tabla
  onChangeFiltros() {
    if (this.state.dni || this.state.apellido || this.state.fecha_nac) {
      this.setState({
        hayDatos: true,
      });
    } else this.reiniciarPacientes();
  }

  /*
    Este método se ejecuta en el onChange de los filtros.
    Actualiza el estado correspondiente y llama al método
    de arriba
  */
  cambiarCampo(event) {
    this.setState(
      {
        [event.target.name]: event.target.value,
      },
      () => {
        this.onChangeFiltros();
      }
    );
  }

  cambiarCampoDNI(event) {
    if (isNaN(event.target.value)) {
      this.setState({
        hayErrorDNI: true,
      });
    } else {
      this.setState(
        {
          dni: event.target.value,
        },
        () => {
          this.onChangeFiltros();
          this.setState({
            hayErrorDNI: false,
          });
        }
      );
    }
  }

  // Cierro el alert
  cerrarAlert() {
    this.setState({
      mostrarAlert: false,
    });
  }

  // Recorro los que tenían el apellido ingresado
  // para ir viendo si coincide con el DNI ingresado
  compararPacienteDNIyApellido(pacientesApellido, pacienteDNI) {
    var listo = false;
    for (var p = 0; p < pacientesApellido.length; p++) {
      if (!listo && pacientesApellido[p].datos.id === pacienteDNI[0].datos.id) {
        var arr = [];
        arr.push(pacientesApellido[p]);
        return arr;
      }
    }
    return null;
  }

  // Recorro los que tenían la fecha de nac ingresada
  // para ir viendo si coincide con el paciente que me pasan
  compararPacienteFechaNac(paciente, pacientesFechaNac) {
    var listo = false;
    for (var p = 0; p < pacientesFechaNac.length; p++) {
      if (!listo && pacientesFechaNac[p].datos.id === paciente[0].datos.id) {
        var arr = [];
        arr.push(pacientesFechaNac[p]);
        return arr;
      }
    }
    return null;
  }

  /*
    Este método es el gran método que hace toda la lógica
    de la búsqueda.

    Con los valores ingresados llama a los fetchs correspondientes
    para después llamar al método setearPacientesBuscados con 
    los pacientes encontrados, o, si no encontró, undefined.
  */
  buscarSubmit(e) {
    this.setState({
      activarLoading: true,
    });
    // Chequeo si ingresó algún filtro
    if (this.state.dni || this.state.apellido || this.state.fecha_nac) {
      var pacienteDNI = "";
      var pacientesApellido = "";
      var pacientesFechaNac = "";
      var encontre = false;

      // Si ingresó los 3 campos
      if (this.state.dni && this.state.apellido && this.state.fecha_nac) {
        var hayDNI,
          hayApellido,
          hayFechaNac = false;
        this.buscarPacienteDNI().then((response) => {
          if (response && response.pacientes) {
            // Encontré con DNI
            pacienteDNI = response.pacientes;
            hayDNI = true;

            this.buscarPacienteApellido().then((response) => {
              if (response.pacientes) {
                // Encontré con APELLIDO
                pacientesApellido = response.pacientes;
                hayApellido = true;

                this.buscarPacienteFechaNacimiento()
                  .then((response) => {
                    if (response.pacientes) {
                      // Encontré con FECHA DE NACIMIENTO
                      pacientesFechaNac = response.pacientes;
                      hayFechaNac = true;

                      // Comparar
                      if (hayDNI && hayApellido && hayFechaNac) {
                        if (hayDNI && hayApellido) {
                          var paciente = this.compararPacienteDNIyApellido(
                            pacientesApellido,
                            pacienteDNI
                          );
                          if (paciente) {
                            if (hayFechaNac) {
                              var pacienteFN = this.compararPacienteFechaNac(
                                paciente,
                                pacientesFechaNac
                              );
                              if (pacienteFN) {
                                encontre = true;
                                this.setearPacientesBuscados({
                                  pacientes: pacienteFN,
                                  totalEncontrados: 1,
                                });
                              }
                            }
                          }
                        }
                      }
                    }
                  })
                  .then(() => {
                    if (!encontre) this.setearPacientesBuscados(undefined);
                  });
              } else this.setearPacientesBuscados(undefined);
            });
          } else this.setearPacientesBuscados(undefined);
        });
      } else {
        // Si ingresó DNI y APELLIDO
        if (this.state.dni && this.state.apellido) {
          this.buscarPacienteDNI().then((response) => {
            if (response && response.pacientes) {
              // Encontré con DNI
              pacienteDNI = response.pacientes;
              hayDNI = true;

              this.buscarPacienteApellido()
                .then((response) => {
                  if (response.pacientes) {
                    // Encontré con APELLIDO
                    pacientesApellido = response.pacientes;
                    hayApellido = true;

                    var paciente = this.compararPacienteDNIyApellido(
                      pacientesApellido,
                      pacienteDNI
                    );
                    console.log(paciente);
                    if (paciente) {
                      encontre = true;
                      this.setearPacientesBuscados({
                        pacientes: paciente,
                        totalEncontrados: 1,
                      });
                    }
                  }
                })
                .then(() => {
                  if (!encontre) this.setearPacientesBuscados(undefined);
                });
            } else this.setearPacientesBuscados(undefined);
          });
        } else {
          // Si ingresó DNI y FECHA NAC
          if (this.state.dni && this.state.fecha_nac) {
            this.buscarPacienteDNI().then((response) => {
              if (response && response.pacientes) {
                // Encontré con DNI
                pacienteDNI = response.pacientes;
                hayDNI = true;

                this.buscarPacienteFechaNacimiento()
                  .then((response) => {
                    if (response.pacientes) {
                      // Encontré con Fecha nac
                      pacientesFechaNac = response.pacientes;
                      hayFechaNac = true;

                      var paciente = this.compararPacienteFechaNac(
                        pacienteDNI,
                        pacientesFechaNac
                      );
                      if (paciente) {
                        encontre = true;
                        this.setearPacientesBuscados({
                          pacientes: paciente,
                          totalEncontrados: 1,
                        });
                      }
                    }
                  })
                  .then(() => {
                    if (!encontre) this.setearPacientesBuscados(undefined);
                  });
              } else this.setearPacientesBuscados(undefined);
            });
          } else {
            // Si ingresó APELLIDO y FECHA NAC
            if (this.state.apellido && this.state.fecha_nac) {
              this.buscarPacienteApellidoFechaNacimiento().then((response) => {
                if (response.pacientes) {
                  // Encontré con apellido y fecha de nac
                  encontre = true;
                  this.setearPacientesBuscados(response);
                } else this.setearPacientesBuscados(undefined);
              });
            } else {
              // Si ingresó uno solo de los tres campos
              if (this.state.dni) {
                this.buscarPacienteDNI().then((response) =>
                  this.setearPacientesBuscados(response)
                );
              } else if (this.state.apellido) {
                this.buscarPacienteApellido().then((response) =>
                  this.setearPacientesBuscados(response)
                );
              } else {
                this.buscarPacienteFechaNacimiento().then((response) =>
                  this.setearPacientesBuscados(response)
                );
              }
            }
          }
        }
      }
    }
    // Si no ingresó ningún filtro y apretó enter
    else {
      this.setState({
        activarLoading: false,
      });
      this.actualizarPacientes(null);
    }
    e.preventDefault();
  }

  /*
    Este método es llamado por el buscarSubmit,
    y el response puede ser un objeto
    {
      pacientes: [pacientes],
      totalEncontrados: [total encontrados],
      usarUrl: [si filtré por apellido, fecha, o ambos, la url]
      apellidoFiltrado: [si filtré apellido, el apellido],
      fechaNacFiltrada: [si filtré fecha, la fecha]
    }
    o también null o undefined.

    Este método chequea si se encontraron pacientes,
    y si es así llama al método actualizarPacientes del
    componente padre Pacientes, para que actualice la tabla
  */
  setearPacientesBuscados(response) {
    if (!response || !response.pacientes) {
      this.setState(
        {
          mostrarAlert: true,
          alertSeverity: "error",
          alertTexto: "No se encontraron pacientes",
        },
        () => {
          // Llamo a actualizar con undefined
          this.actualizarPacientes(undefined);
        }
      );
    } else {
      // Guardo los pacientes en el estado del padre
      this.actualizarPacientes(response);
    }
    this.setState({
      activarLoading: false,
    });
  }

  /*
    Este método hace la llamada a la API
    para buscar un paciente por DNI.
    Si encuentra arma un objeto con data
    para el padre
  */
  buscarPacienteDNI() {
    var url = urlsAPI.dni + this.state.dni;
    return fetch(url, {
      headers: new Headers({
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          if (data.body.paciente) {
            var arr = [];
            arr.push(data.body.paciente);
            var datosPacienteEncontrado = {
              pacientes: arr,
              totalEncontrados: 1,
              apellidoFiltrado: "",
              fechaNacFiltrada: "",
            };
            return datosPacienteEncontrado;
          }
        }
      });
  }

  /*
    Este método hace la llamada a la API
    para buscar pacientes por apellido.
    Si encuentra arma un objeto con data
    para el padre
  */
  buscarPacienteApellido() {
    // La urlSinPaginado es para pasarle al componente Pacientes
    // y que sepa que ante un cambio de página o cambio de cant
    // por página tiene que seguir buscando con este apellido
    var urlSinPaginado = urlsAPI.apellido + this.state.apellido;
    // La url posta es la que uso ahora para hacer el fetch,
    // que le agrega el page y perPage que ya están seteados de antes
    var url =
      urlSinPaginado + "&page=" + 1 + "&perPage=" + this.props.porPagina;

    return fetch(url, {
      headers: new Headers({
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          var datosPacientesEncontrados = {
            pacientes: data.body.pacientes,
            totalEncontrados: data.body.total,
            usarUrl: urlSinPaginado,
            apellidoFiltrado: this.state.apellido,
            fechaNacFiltrada: "",
          };
          return datosPacientesEncontrados;
        }
      });
  }

  /*
    Este método hace la llamada a la API
    para buscar pacientes por fecha de nacimiento.
    Si encuentra arma un objeto con data
    para el padre
  */
  buscarPacienteFechaNacimiento() {
    var fecha = moment(this.state.fecha_nac).format("yyyy-MM-DD");
    // La urlSinPaginado sin paginado es para pasarle al componente Pacientes
    // y que sepa que ante un cambio de página o cambio de cant
    // por página tiene que seguir buscando con esta fecha de nac
    var urlSinPaginado = urlsAPI.fecha_nac + fecha;
    // La url posta es la que uso ahora para hacer el fetch,
    // que le agrega el page y perPage que ya están seteados de antes
    var url =
      urlSinPaginado + "&page=" + 1 + "&perPage=" + this.props.porPagina;

    return fetch(url, {
      headers: new Headers({
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          var datosPacientesEncontrados = {
            pacientes: data.body.pacientes,
            totalEncontrados: data.body.total,
            usarUrl: urlSinPaginado,
            fechaNacFiltrada: fecha,
            apellidoFiltrado: "",
          };
          return datosPacientesEncontrados;
        }
      });
  }

  /*
    Este método hace la llamada a la API
    para buscar pacientes por apellido y fecha de nacimiento.
    Si encuentra arma un objeto con data
    para el padre
  */
  buscarPacienteApellidoFechaNacimiento() {
    var fecha = moment(this.state.fecha_nac).format("yyyy-MM-DD");
    // La urlSinPaginado sin paginado es para pasarle al componente Pacientes
    // y que sepa que ante un cambio de página o cambio de cant
    // por página tiene que seguir buscando con esta fecha de nac y apellido
    var urlSinPaginado = urlsAPI.apellido + this.state.apellido;
    // La url posta es la que uso ahora para hacer el fetch,
    // que le agrega el page y perPage que ya están seteados de antes
    var url =
      urlSinPaginado +
      "&fecha_nac=" +
      fecha +
      "&page=" +
      1 +
      "&perPage=" +
      this.props.porPagina;

    return fetch(url, {
      headers: new Headers({
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          var datosPacientesEncontrados = {
            pacientes: data.body.pacientes,
            totalEncontrados: data.body.total,
            usarUrl: urlSinPaginado,
            apellidoFiltrado: this.state.apellido,
            fechaNacFiltrada: fecha,
          };
          return datosPacientesEncontrados;
        }
      });
  }

  /*
    Acá es donde llamo al padre Pacientes
    y le paso los pacientes encontrados con los filtros.

    Si no encontré ninguno le mando null
  */
  actualizarPacientes(pacientes) {
    if (!pacientes) {
      this.props.actualizarPacientes(null);
    } else {
      this.props.actualizarPacientes(pacientes);
      var msg = "";
      if (pacientes.pacientes.length > 1) {
        msg = "¡Se encontraron pacientes!";
      } else msg = "¡Se encontró un paciente!";
      this.setState({
        mostrarAlert: true,
        alertSeverity: "success",
        alertTexto: msg,
      });
    }
  }

  render() {
    return (
      <div style={{ padding: "10px 10px 20px" }}>
        <Box
          display="flex"
          container
          flexDirection="row"
          alignItems="end"
          justifyContent="space-between"
        >
          <Box m={1}>
            <Link to="/pacientes/crear-paciente">
              <Button
                variant="contained"
                style={{
                  padding: "10px 20px",
                  color: "white",
                  backgroundColor: "#297ced",
                }}
                endIcon={<AddBoxIcon />}
              >
                Nuevo paciente
              </Button>
            </Link>
          </Box>
          <form onSubmit={this.buscarSubmit}>
            <Box
              m={1}
              display="flex"
              flexDirection="row"
              flexWrap="wrap"
              alignItems="end"
            >
              <Box>
                <TextField
                  label={<span style={{ fontSize: 15 }}>Buscar por DNI</span>}
                  id="dni"
                  name="dni"
                  onChange={this.cambiarCampoDNI}
                  value={this.state.dni}
                  style={styles.boxItem}
                  InputLabelProps={{
                    shrink: true,
                    style: {
                      width: "100%",
                      whiteSpace: "nowrap",
                    },
                  }}
                  placeholder="Ej: 16785982"
                  autoComplete="off"
                  error={this.state.hayErrorDNI}
                  helperText={
                    this.state.hayErrorDNI ? "Tienen que ser 8 dígitos" : ""
                  }
                  inputProps={{ maxLength: 8 }}
                />
              </Box>
              <Box item>
                <TextField
                  label={
                    <span style={{ fontSize: 15 }}>Buscar por apellido</span>
                  }
                  type="text"
                  id="apellido"
                  name="apellido"
                  onChange={this.cambiarCampo}
                  value={this.state.apellido}
                  style={styles.boxItem}
                  InputLabelProps={{
                    shrink: true,
                    style: {
                      width: "100%",
                      whiteSpace: "nowrap",
                    },
                  }}
                  placeholder="Ej: Sánchez"
                  autoComplete="off"
                />
              </Box>
              <Box item style={{ fontSize: 10 }}>
                <MuiPickersUtilsProvider locale={es} utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    autoOk
                    cancelLabel="Cancelar"
                    invalidDateMessage="La fecha no es válida. Recordá: día/mes/año"
                    maxDateMessage="La fecha no puede ser mayor al máximo permitido"
                    okLabel={""}
                    DialogProps={{ disableScrollLock: true }}
                    margin="normal"
                    style={{ minWidth: 200, maxWidth: 200, margin: "0 10px" }}
                    format="dd/MM/yyyy"
                    id="fecha_nac"
                    name="fecha_nac"
                    label={
                      <span
                        style={{
                          fontSize: 15,
                        }}
                      >
                        Buscar por fecha de nacimiento
                      </span>
                    }
                    InputLabelProps={{
                      shrink: true,
                      style: {
                        width: "100%",
                        whiteSpace: "nowrap",
                      },
                    }}
                    placeholder="Ej: 10/03/1997"
                    value={this.state.fecha_nac}
                    onChange={this.cambiarFecha}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Box>
              <Box mt={2}>
                {this.state.activarLoading ? (
                  <CircularProgress />
                ) : (
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    style={styles.boxItem}
                  >
                    Buscar
                  </Button>
                )}
              </Box>
              {this.state.hayDatos && (
                <Box item>
                  <IconButton
                    color="secondary"
                    size="small"
                    onClick={this.reiniciarPacientes}
                  >
                    <ClearIcon></ClearIcon>
                  </IconButton>
                </Box>
              )}
            </Box>
          </form>
        </Box>
        <Snackbar
          open={this.state.mostrarAlert}
          autoHideDuration={3000}
          onClose={this.cerrarAlert}
        >
          <Alert
            onClose={this.cerrarAlert}
            severity={this.state.alertSeverity}
            variant="filled"
          >
            {this.state.alertTexto}
          </Alert>
        </Snackbar>
      </div>
    );
  }
}

export default withStyles(styles)(CrearYBuscarPaciente);
