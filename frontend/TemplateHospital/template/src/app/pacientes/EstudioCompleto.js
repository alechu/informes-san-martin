import React, { Component } from 'react';
import PDFEstudio from './PDFEstudio';
import PersonIcon from '@material-ui/icons/Person';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import DescriptionIcon from '@material-ui/icons/Description';
import PDFInfoEstudioDisincronia from './PDFInfoEstudioDisincronia';
import PDFInfoEstudioSalinaAgitada from './PDFInfoEstudioSalinaAgitada';
import PDFInfoEstudio2DDoppler from './PDFInfoEstudio2DDoppler';
import PDFInfoEstudioCongenitas from './PDFInfoEstudioCongenitas';
import PDFInfoEstudioTransesofagico from './PDFInfoEstudioTransesofagico';
import InfoPacienteSeleccionado from './InfoPacienteSeleccionado';
import ContenedorSeccion from "../otros/ContenedorSeccion";

export default class EstudioCompleto extends Component {
    constructor(props) {
        super(props);

        this.state = {
            breadcrumb: [
                {
                    pathname: '/pacientes',
                    state: '',
                    texto: 'Pacientes',
                    icon: <PersonIcon />
                },
                {
                    pathname: '/pacientes/historia-clinica',
                    state: { paciente: this.props.location.state.paciente },
                    texto: 'Historia clínica',
                    icon: <LibraryBooksIcon />
                },
                {
                    pathname: '/pacientes/historia-clinica/ver-estudio',
                    state: { estudio: this.props.location.state.estudio, paciente: this.props.location.state.paciente },
                    texto: 'Ver estudio completo',
                    icon: <DescriptionIcon />
                }
            ],
            infoPDFDelTipoCorrespondiente: ''
        };
    }

    componentDidMount() {
        this.props.actualizarHeader({
            breadcrumb: this.state.breadcrumb
        });

        // Asociar la info con el tipo de estudio
        var estudio = this.props.location.state.estudio;
        var tipo = estudio.informacion_general.tipo.nombre_id;
        if (tipo === 'disincronia' ||
            tipo === 'salina_agitada' ||
            tipo === '2d_doppler' ||
            tipo === 'congenitas' ||
            tipo === 'transesofagico') {

            switch (tipo) {
                case 'disincronia':
                    this.setState({
                        infoPDFDelTipoCorrespondiente: (
                            <PDFInfoEstudioDisincronia
                                estudio={estudio}
                            />
                        )
                    });
                    break;
                case 'salina_agitada':
                    this.setState({
                        infoPDFDelTipoCorrespondiente: (
                            <PDFInfoEstudioSalinaAgitada
                                estudio={estudio}
                            />
                        )
                    });
                    break;
                case '2d_doppler':
                    this.setState({
                        infoPDFDelTipoCorrespondiente: (
                            <PDFInfoEstudio2DDoppler
                                estudio={estudio}
                            />
                        )
                    });
                    break;
                case 'congenitas':
                    this.setState({
                        infoPDFDelTipoCorrespondiente: (
                            <PDFInfoEstudioCongenitas
                                estudio={estudio}
                            />
                        )
                    });
                    break;
                case 'transesofagico':
                    this.setState({
                        infoPDFDelTipoCorrespondiente: (
                            <PDFInfoEstudioTransesofagico
                                estudio={estudio}
                            />
                        )
                    });
                    break;
            }
        }
    }

    render() {

        const estudio = this.props.location.state.estudio;
        const paciente = this.props.location.state.paciente;

        return (
            <div>
                <ContenedorSeccion
                    nombre="Ver estudio completo"
                    descripcion="Podés descargar el PDF con todos los datos del estudio"
                    preComponentes={[
                        <InfoPacienteSeleccionado paciente={paciente} />
                    ]}
                    componentes={[
                        <PDFEstudio
                            estudio={estudio}
                            paciente={paciente}
                            infoEstudioComplejo={this.state.infoPDFDelTipoCorrespondiente}
                        />
                    ]}
                />
            </div>

        )
    }
}