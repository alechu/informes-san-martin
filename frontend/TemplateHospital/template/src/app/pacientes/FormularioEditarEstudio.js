// React and core libraries
import React, { Component } from "react";
import axios from "axios";
// User made components
import ModalPaciente from "./ModalPaciente";
import EstudioBasico from "../estudios/FormulariosEstudios/EstudioBasico.jsx";
import EstudioDisincronia from "../estudios/FormulariosEstudios/EstudioDisincronia.jsx";
import EstudioSalinaAgitada from "../estudios/FormulariosEstudios/EstudioSalinaAgitada.jsx";
import Estudio2dDoppler from "../estudios/FormulariosEstudios/Estudio2dDoppler.jsx";
import EstudioCongenitas from "../estudios/FormulariosEstudios/EstudioCongenitas.jsx";
import EstudioTransesofagico from "../estudios/FormulariosEstudios/EstudioTransesofagico.jsx";
// Core Material-UI
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Alert from "@material-ui/lab/Alert";
import InputAdornment from "@material-ui/core/InputAdornment";
import Paper from "@material-ui/core/Paper";
import Collapse from "@material-ui/core/Collapse";
import LinearProgress from "@material-ui/core/LinearProgress";
import Grow from "@material-ui/core/Grow";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import HelpIcon from "@material-ui/icons/Help";
export default class FormularioEditarEstudio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // Datos para el estudio y selects
      tipo: {
        id: "",
        nombre: "",
      },
      fecha: "",
      conclusion: "",
      especialista: {
        id: "",
        nombre: "",
      },
      motivo: {
        id: null,
        nombre: "",
      },
      diagnostico_obligatorio: {
        id: null,
        nombre: "",
      },
      diagnostico_opcional_1: {
        id: null,
        nombre: "",
      },
      diagnostico_opcional_2: {
        id: null,
        nombre: "",
      },
      paciente: props.paciente,

      // Datos especificos

      id_descripcion_predefinida: "",
      id_conclusion_predefinida: "",
      situs: "",
      auriculo_ventricular: "",
      ventriculo_arterial: "",

      // Todas las opciones predef para seleccionar
      tipos: [],
      especialistas: [],
      motivos: [],

      // Para modal
      abrirModal: false,
      huboError: false,
      mensaje: "¿Confirma los cambios?",
      cargandoAccion: false,
      iconColor: "rgb(48, 63, 159)",
      icon: "far fa-question-circle",
      botonesModal: [],

      errores: {
        especialista: false,
        motivo: false,
        diagnostico_obligatorio: false,
        tipo_estudio: false,
        conclusion: false,
        escenario: false,
        situs: false,
        auriculo_ventricular: false,
        ventriculo_arterial: false,
        descripcion_congenitas: false,
        descripcion_transesofagico: false,
        descripcion_2dDoppler: false,
        errorAlertaGeneral: false,
        errorAlertaEspecifico: false,
      },

      loadingEstudio: true,
      datosEspecificosListos: false,
    };

    this.cambiarCampo = this.cambiarCampo.bind(this);
    this.manejarSubmit = this.manejarSubmit.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
    this.limpiarErrores = this.limpiarErrores.bind(this);
    this.noFaltanDatosGenerales = this.noFaltanDatosGenerales.bind(this);
    this.noFaltanDatosEspecificos = this.noFaltanDatosEspecificos.bind(this);
    this.fetchTipos = this.fetchTipos.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.cambiarTipoEstudio = this.cambiarTipoEstudio.bind(this);
    this.cambiarOpcionPredefinida = this.cambiarOpcionPredefinida.bind(this);
    this.fetchOpcionesPredefinidas = this.fetchOpcionesPredefinidas.bind(this);
    this.cambiarMotivo = this.cambiarMotivo.bind(this);
    this.cambiarDiagObligatorio = this.cambiarDiagObligatorio.bind(this);
    this.cambiarDiagOpcional1 = this.cambiarDiagOpcional1.bind(this);
    this.cambiarDiagOpcional2 = this.cambiarDiagOpcional2.bind(this);
  }

  componentDidMount() {
    this.fetchTipos();
    this.fetchOpcionesPredefinidas();
  }

  componentDidUpdate() {
    if (
      this.state.loadingEstudio &&
      this.camposGeneralesListos() &&
      this.camposEspecificosListosChild()
    ) {
      this.setState({ loadingEstudio: false });
    }
  }

  fetchTipos() {
    // Me traigo todos los tipos de estudio
    fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/tipo_estudio?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then(
        (result) => {
          if (result.status === 401) {
            localStorage.setItem("alert", "Expired/Invalid JWT Token");
            document.cookie =
              "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            window.location.href = "/user-pages/login-1";
          } else {
            this.setState(
              {
                //isLoaded: true,
                //Estructura: id, nombre, nombre_mostrado
                tipos: result.body,
              },
              () =>
                this.setState({
                  tipo: {
                    id: this.props.estudio.informacion_general.tipo.id,
                    nombre: this.props.estudio.informacion_general.tipo
                      .nombre_id,
                  },
                })
            );
          }
        },
        // Nota de la docu: es importante manejar errores aquí y no en
        // un bloque catch() para que no interceptemos errores
        // de errores reales en los componentes.
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  async fetchOpcionesPredefinidas() {
    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/especialista?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          this.setState(
            {
              especialistas: data.body,
            },
            () =>
              this.setState(
                {
                  especialista: {
                    id: this.props.estudio.informacion_general.especialista.id,
                    nombre: this.props.estudio.informacion_general.especialista
                      .nombre,
                  },
                },
                () => {
                  this.setState(
                    {
                      fecha: this.props.estudio.informacion_general.fecha,
                    },
                    () => {
                      this.setState({
                        botonesModal: [
                          {
                            texto: "Confirmar",
                            onClick: this.manejarSubmit,
                            color: "green",
                          },
                          {
                            link: "",
                            state: "",
                            texto: "Rechazar",
                            onClick: this.cerrarModal,
                            color: "crimson",
                          },
                        ],
                      });
                    }
                  );
                }
              )
          );
        }
      });
    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/motivo?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          this.setState(
            {
              motivos: data.body,
            },
            () =>
              this.setState({
                motivo: {
                  id: this.props.estudio.informacion_general.motivo.id,
                  nombre: this.props.estudio.informacion_general.motivo.nombre,
                },
              })
          );
        }
      });
    await fetch(
      "http://" + process.env.REACT_APP_API_IP + "/api/diagnostico?all=True",
      {
        headers: new Headers({
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else {
          this.setState(
            {
              diagnosticos: data.body,
            },
            () => {
              this.setState({
                diagnostico_obligatorio: {
                  id: this.props.estudio.informacion_general
                    .diagnostico_obligatorio,
                  nombre: this.props.estudio.informacion_general
                    .diagnostico_obligatorio.nombre,
                },
              });
              if (
                this.props.estudio.informacion_general.diagnostico_opcional_1 !=
                null
              ) {
                this.setState({
                  diagnostico_opcional_1: {
                    id: this.props.estudio.informacion_general
                      .diagnostico_opcional_1,
                    nombre: this.props.estudio.informacion_general
                      .diagnostico_opcional_1.nombre,
                  },
                });
              }
              if (
                this.props.estudio.informacion_general.diagnostico_opcional_2 !=
                null
              ) {
                this.setState({
                  diagnostico_opcional_2: {
                    id: this.props.estudio.informacion_general
                      .diagnostico_opcional_2,
                    nombre: this.props.estudio.informacion_general
                      .diagnostico_opcional_2.nombre,
                  },
                });
              }
            }
          );
        }
      });
  }

  // Updatea el State datosEspecificosListos para avisar que
  // el componente hijo esta listo

  camposGeneralesListos() {
    var ready = true;
    if (this.state.motivo === null) ready = false;
    if (this.state.especialista.nombre === "") ready = false;
    if (this.state.tipo.nombre === "") ready = false;
    if (this.state.fecha === "") ready = false;
    if (this.state.diagnostico_obligatorio.nombre === "") ready = false;
    if (this.props.estudio.informacion_general.diagnostico_opcional_1 != null) {
      if (this.state.diagnostico_opcional_1.nombre === "") ready = false;
    }
    if (this.props.estudio.informacion_general.diagnostico_opcional_2 != null) {
      if (this.state.diagnostico_opcional_2.nombre === "") ready = false;
    }
    return ready;
  }
  camposEspecificosListosChild() {
    if (this.state.datosEspecificosListos === true) {
      return true;
    } else {
      this.setState({ datosEspecificosListos: true }, () => {
        return true;
      });
    }
    return false;
  }

  // Actualiza el campo que ni es select ni datepicker
  cambiarCampo(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }
  // Actualizo el tipo de estudio con el ID y el nombre
  cambiarTipoEstudio(event) {
    const { id } = event.currentTarget.dataset;
    this.setState({
      tipo: {
        id: id,
        nombre: event.target.value,
      },
    });
    this.setState((prevState) => ({
      errores: {
        ...prevState.errores,
        errorAlertaEspecifico: false,
      },
    }));
  }
  // Se basa en el nombre del select, y usa eso como índice para actualizar el estado.
  // Actualiza el id y el nombre (o texto)
  cambiarOpcionPredefinida(event) {
    const { id } = event.currentTarget.dataset;
    this.setState({
      [event.target.name]: {
        id: id,
        nombre: event.target.value,
      },
    });
  }

  cambiarMotivo(event, newValue) {
    if (newValue !== null) {
      this.setState({ motivo: newValue });
    } else {
      this.setState({
        motivo: null,
      });
    }
  }

  cambiarDiagObligatorio(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        diagnostico_obligatorio: {
          ...prevState.diagnostico_obligatorio,
          id: newValue,
        },
      }));
    } else {
      this.setState({
        diagnostico_obligatorio: {
          id: null,
          nombre: "",
        },
      });
    }
  }

  cambiarDiagOpcional1(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        diagnostico_opcional_1: {
          ...prevState.diagnostico_opcional_1,
          id: newValue,
        },
      }));
    } else {
      this.setState({
        diagnostico_opcional_1: {
          id: null,
          nombre: "",
        },
      });
    }
  }

  cambiarDiagOpcional2(event, newValue) {
    if (newValue != null) {
      this.setState((prevState) => ({
        diagnostico_opcional_2: {
          ...prevState.diagnostico_opcional_2,
          id: newValue,
        },
      }));
    } else {
      this.setState({
        diagnostico_opcional_2: {
          id: null,
          nombre: "",
        },
      });
    }
  }

  // Actualiza el estado de la fecha
  cambiarFecha = (date) => {
    this.setState({
      fecha: this.formatDate(date),
    });
  };

  // Cambio de datos especificos en componentes hijos

  cambiarConclusionPredefinida(conclusion_id) {
    this.setState({ id_conclusion_predefinida: conclusion_id });
  }

  cambiarSitus(situs) {
    this.setState({ situs: situs });
  }

  cambiarAV(AV) {
    this.setState({ auriculo_ventricular: AV });
  }

  cambiarVA(VA) {
    this.setState({ ventriculo_arterial: VA });
  }

  limpiarErrores() {
    this.setState({
      errores: {
        especialista: false,
        motivo: false,
        diagnostico_obligatorio: false,
        tipo_estudio: false,
        conclusion: false,
        escenario: false,
        descripcion_congenitas: false,
        descripcion_transesofagico: false,
        situs: false,
        auriculo_ventricular: false,
        ventriculo_arterial: false,
        errorAlertaGeneral: false,
        errorAlertaEspecifico: false,
      },
    });
  }

  noFaltanDatosGenerales() {
    var hayError = false;
    if (this.state.especialista.id === "") {
      hayError = true;
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          especialista: true,
        },
      }));
    }
    if (this.state.motivo === null) {
      hayError = true;
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          motivo: true,
        },
      }));
    }
    if (this.state.diagnostico_obligatorio.id === null) {
      hayError = true;
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          diagnostico_obligatorio: true,
        },
      }));
    }
    if (this.state.tipo.nombre === "") {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          tipo_estudio: true,
        },
      }));
      if (hayError) {
        this.setState((prevState) => ({
          errores: {
            ...prevState.errores,
            errorAlertaGeneral: true,
          },
        }));
      }
      window.scrollTo({
        behavior: "smooth",
        top: 0,
      });
      return false;
    }
    if (hayError) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorAlertaGeneral: true,
        },
      }));
      window.scrollTo({
        behavior: "smooth",
        top: 0,
      });
      return false;
    } else {
      return true;
    }
  }

  noFaltanDatosEspecificos() {
    var hayError = false;
    if (this.state.tipo.nombre === "salina_agitada") {
      if (this.state.id_conclusion_predefinida === "") {
        hayError = true;
        this.setState((prevState) => ({
          errores: {
            ...prevState.errores,
            escenario: true,
          },
        }));
      }
    } else {
      if (this.state.tipo.nombre === "congenitas") {
        if (this.state.situs === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              situs: true,
            },
          }));
        }
        if (this.state.auriculo_ventricular === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              auriculo_ventricular: true,
            },
          }));
        }
        if (this.state.ventriculo_arterial === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              ventriculo_arterial: true,
            },
          }));
        }
        if (document.getElementById("descripcion").value === "") {
          hayError = true;
          this.setState((prevState) => ({
            errores: {
              ...prevState.errores,
              descripcion_congenitas: true,
            },
          }));
        }
      } else {
        if (this.state.tipo.nombre === "transesofagico") {
          if (document.getElementById("descripcion").value === "") {
            hayError = true;
            this.setState((prevState) => ({
              errores: {
                ...prevState.errores,
                descripcion_transesofagico: true,
              },
            }));
          }
        } else {
          if (this.state.tipo.nombre === "2d_doppler") {
            if (document.getElementById("descripcion").value === "") {
              hayError = true;
              this.setState((prevState) => ({
                errores: {
                  ...prevState.errores,
                  descripcion_2dDoppler: true,
                },
              }));
            }
          }
        }
      }
    }
    if (document.getElementById("conclusion").value === "") {
      hayError = true;
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          conclusion: true,
        },
      }));
    }
    if (hayError) {
      this.setState((prevState) => ({
        errores: {
          ...prevState.errores,
          errorAlertaEspecifico: true,
        },
      }));
      document
        .getElementById("select_tipo")
        .scrollIntoView({ behavior: "smooth" });
      return false;
    } else {
      return true;
    }
  }

  manejarSubmit() {
    this.limpiarErrores();
    if (this.noFaltanDatosGenerales() && this.noFaltanDatosEspecificos()) {
      this.setState({ cargandoAccion: true });
      var formulario = document.getElementById("form_datos_especificos");
      var datosUnparsed = new FormData(formulario);
      var datosParsed = new FormData();
      for (var unparsedKey of datosUnparsed.keys()) {
        if (datosUnparsed.get(unparsedKey) != "") {
          datosParsed.append(unparsedKey, datosUnparsed.get(unparsedKey));
        } else {
          datosParsed.append(unparsedKey, 0);
        }
      }
      datosParsed.append("nombre_tipo", this.state.tipo.nombre);
      datosParsed.append("fecha", this.state.fecha);
      datosParsed.append("id_especialista", this.state.especialista.id);
      datosParsed.append("id_motivo", this.state.motivo.id);
      datosParsed.append(
        "id_diagnostico_obligatorio",
        this.state.diagnostico_obligatorio.id.id
      );
      if (this.state.diagnostico_opcional_1.id !== null) {
        datosParsed.append(
          "id_diagnostico_opcional_1",
          this.state.diagnostico_opcional_1.id.id
        );
      } else {
        datosParsed.append("id_diagnostico_opcional_1", 0);
      }
      if (this.state.diagnostico_opcional_2.id !== null) {
        datosParsed.append(
          "id_diagnostico_opcional_2",
          this.state.diagnostico_opcional_2.id.id
        );
      } else {
        datosParsed.append("id_diagnostico_opcional_2", 0);
      }
      datosParsed.append("id_paciente", this.state.paciente.id);
      axios({
        method: "put",
        url:
          "http://" +
          process.env.REACT_APP_API_IP +
          "/api/estudio?id=" +
          this.props.estudio.informacion_general.id,
        data: datosParsed,
        headers: {
          Authorization:
            "Bearer " +
            document.cookie.replace(
              /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
              "$1"
            ),
          "Content-Type": "multipart/form-data",
        },
      })
        .then((response) => {
          // Preparo el mensaje para el modal
          this.setState({
            abrirModal: true,
            huboError: false,
            mensaje: <span>El estudio se ha actualizado con éxito.</span>,
            iconColor: "green",
            icon: "fas fa-check-circle",
            cargandoAccion: false,
            botonesModal: [
              {
                texto: "Seguir editando",
                onClick: this.cerrarModal,
                color: "#3f51b5",
              },
              {
                texto: "Volver a la historia clínica",
                color: "#3f51b5",
                link: "/pacientes/historia-clinica",
                state: { key: "paciente", data: this.props.paciente },
              },
            ],
          });
          // Reseteo los valores del form
          //this.limpiarForm();
        })
        .catch((error) => {
          console.log(error);
          // Armo un mensaje para mandarle al modal
          var mensajeError = "";
          switch (error.response.data.status) {
            case 400:
              mensajeError = <span>Hubo un error</span>;
              break;
            case 401:
              localStorage.setItem("alert", "Expired/Invalid JWT Token");
              document.cookie =
                "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
              window.location.href = "/user-pages/login-1";
              break;
            case 500:
              mensajeError =
                "Ocurrió un error con el servidor, por favor intentá de nuevo.";
              break;
          }
          this.setState({
            icon: "fas fa-times-circle",
            iconColor: "#dc004e",
            abrirModal: true,
            mensaje: mensajeError,
            cargandoAccion: false,
            botonesModal: [
              {
                texto: "Reintentar",
                color: "green",
                onClick: this.manejarSubmit,
              },
              {
                texto: "Cancelar",
                color: "crimson",
                onClick: this.cerrarModal,
              },
            ],
          });
        });
    }
  }

  // A este método llama el modal (component hijo) cuando necesita cerrarse
  cerrarModal() {
    this.setState({ abrirModal: false }, async () => {
      await this.sleep(500);
      this.resetModal();
    });
  }

  // Esta funcion esta por el simple hecho de que los estados
  // se actualizan muy rapido y se llega a ver cuando el modal
  // cambia sus botones al hacer click en "Aceptar"

  sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  resetModal() {
    this.setState({
      mensaje: "¿Confirma los cambios?",
      iconColor: "rgb(48, 63, 159)",
      icon: "far fa-question-circle",
      botonesModal: [
        {
          texto: "Confirmar",
          onClick: this.manejarSubmit,
          color: "green",
        },
        {
          link: "",
          state: "",
          texto: "Rechazar",
          onClick: this.cerrarModal,
          color: "crimson",
        },
      ],
    });
  }

  // Parse de fecha
  formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  // Parse fecha a dd/mm/yyyy
  format(input) {
    var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
    if (!input || !input.match(pattern)) {
      return null;
    }
    return input.replace(pattern, "$3/$2/$1");
  }

  render() {
    // Armar select de tipos de estudio
    const armarSelectTipos = () => {
      if (this.state.tipos) {
        return this.state.tipos.map((tipo) => (
          <MenuItem
            style={{ padding: "5px 15px 5px 15px" }}
            value={tipo.nombre}
            data-id={tipo.id}
          >
            {tipo.nombre_mostrado}
          </MenuItem>
        ));
      } else return "";
    };

    // Armar select de tipos de estudio
    const armarSelectOpcionPredefinida = (arregloDeLaOpcion) => {
      if (arregloDeLaOpcion) {
        return arregloDeLaOpcion.map((opcion) => (
          <MenuItem
            style={{ padding: "5px 15px 5px 15px" }}
            value={opcion.nombre}
            data-id={opcion.id}
          >
            {opcion.nombre}
          </MenuItem>
        ));
      } else return "";
    };

    const datosEstudio = {
      tipo: {
        id: this.state.tipo.id,
        nombre: this.state.tipo.nombre,
      },
      fecha: this.state.fecha,
      conclusion: this.state.conclusion,
      especialista: {
        id: this.state.especialista.id,
        nombre: this.state.especialista.nombre,
      },
      paciente: this.state.paciente,
    };

    if (this.state.loadingEstudio) {
      return (
        <div>
          <Grid container spacing={1}>
            <Box p={1} flexGrow={1}>
              <Grid item xs={12}>
                <Paper
                  style={{
                    padding: "15px",
                    textAlign: "center",
                    background:
                      "linear-gradient(120deg, #2f78e9, #128bfc, #18bef1)",
                  }}
                >
                  <b style={{ color: "white", fontSize: "20px" }}>
                    Cargando estudio . . .
                  </b>
                  <LinearProgress
                    style={{ backgroundColor: "white", marginTop: "10px" }}
                  ></LinearProgress>
                </Paper>
              </Grid>
            </Box>
          </Grid>
        </div>
      );
    } else {
      return (
        <div>
          <Grow
            in={!this.state.loadingEstudio}
            style={{ transformOrigin: "0 0 0" }}
            {...(!this.state.loadingEstudio ? { timeout: 1000 } : {})}
          >
            <Grid container spacing={3}>
              <Box p={1} flexGrow={1}>
                <Grid item xs={12}>
                  <Paper
                    style={{
                      padding: "5px",
                      textAlign: "center",
                      background:
                        "linear-gradient(120deg, #2f78e9, #128bfc, #18bef1)",
                    }}
                  >
                    <b style={{ color: "white", fontSize: "20px" }}>
                      Editando estudio -{" "}
                      {
                        this.props.estudio.informacion_general.tipo
                          .nombre_mostrado
                      }
                      {" (" +
                        this.format(
                          this.props.estudio.informacion_general.fecha
                        ) +
                        ")"}
                    </b>
                    <br></br>
                    <Typography style={{ color: "white" }} variant="caption">
                      Los campos requeridos están marcados con un *
                    </Typography>
                  </Paper>
                </Grid>
              </Box>
              <Grid style={{ marginBottom: 0, paddingBottom: 0 }} item xs={12}>
                <Card variant="outlined">
                  <Typography
                    style={{
                      fontSize: 15,
                      paddingTop: "15px",
                      paddingLeft: "15px",
                    }}
                    color="textSecondary"
                    gutterBottom
                  >
                    Datos generales
                  </Typography>
                  <Collapse in={this.state.errores.errorAlertaGeneral}>
                    <Alert
                      style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}
                      variant="filled"
                      severity="error"
                    >
                      Por favor, completá los campos marcados en rojo
                    </Alert>
                  </Collapse>
                  <Box
                    display="flex"
                    flexDirection="row"
                    flexWrap="wrap"
                    p={2}
                    m={1}
                    bgcolor="background.paper"
                  >
                    <Box p={1}>
                      <Grid item xs={4}>
                        <Box display="flex">
                          <FormControl
                            style={{
                              display: "flex",
                              minWidth: 300,
                              maxWidth: 300,
                            }}
                            error={this.state.errores.especialista}
                          >
                            <InputLabel id="especialista">
                              Médico especialista *
                            </InputLabel>
                            <Select
                              startAdornment={
                                <InputAdornment position="start">
                                  <i
                                    className="fas fa-user-md"
                                    style={{ fontSize: "25px" }}
                                  ></i>
                                </InputAdornment>
                              }
                              MenuProps={{ disableScrollLock: true }}
                              id="especialista"
                              name="especialista"
                              labelId="especialista"
                              value={datosEstudio.especialista.nombre}
                              onChange={this.cambiarOpcionPredefinida}
                              children={armarSelectOpcionPredefinida(
                                this.state.especialistas
                              )}
                            ></Select>
                          </FormControl>
                        </Box>
                      </Grid>
                    </Box>
                    <Box p={1}>
                      <Grid item xs={4}>
                        <Autocomplete
                          id="motivo"
                          options={this.state.motivos}
                          getOptionLabel={(option) => option.nombre}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          onChange={this.cambiarMotivo}
                          value={this.state.motivo}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              error={this.state.errores.motivo}
                              style={{ marginTop: 4 }}
                              InputProps={{
                                ...params.InputProps,
                                startAdornment: (
                                  <>
                                    <InputAdornment position="start">
                                      <HelpIcon />
                                    </InputAdornment>
                                    {params.InputProps.startAdornment}
                                  </>
                                ),
                              }}
                              label="Motivo de Estudio *"
                            />
                          )}
                        />
                      </Grid>
                    </Box>
                    <Box p={1}>
                      <Grid item xs={4}>
                        <Autocomplete
                          id="diagnostico_obligatorio"
                          options={this.state.diagnosticos}
                          getOptionLabel={(option) => option.nombre}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          value={this.state.diagnostico_obligatorio.id}
                          inputValue={this.state.diagnostico_obligatorio.nombre}
                          onChange={this.cambiarDiagObligatorio}
                          onInputChange={(event, newInputValue) => {
                            this.setState((prevState) => ({
                              diagnostico_obligatorio: {
                                ...prevState.diagnostico_obligatorio,
                                nombre: newInputValue,
                              },
                            }));
                          }}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              error={this.state.errores.diagnostico_obligatorio}
                              style={{ marginTop: 4 }}
                              InputProps={{
                                ...params.InputProps,
                                startAdornment: (
                                  <>
                                    <InputAdornment position="start">
                                      <i
                                        className="fas fa-notes-medical"
                                        style={{ fontSize: "25px" }}
                                      ></i>
                                    </InputAdornment>
                                    {params.InputProps.startAdornment}
                                  </>
                                ),
                              }}
                              label="Diagnóstico obligatorio *"
                            />
                          )}
                        />
                      </Grid>
                    </Box>
                    <Box p={1}>
                      <Grid item xs={4}>
                        <Autocomplete
                          id="diagnostico_opcional_1"
                          options={this.state.diagnosticos}
                          getOptionLabel={(option) => option.nombre}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          value={this.state.diagnostico_opcional_1.id}
                          inputValue={this.state.diagnostico_opcional_1.nombre}
                          onChange={this.cambiarDiagOpcional1}
                          onInputChange={(event, newInputValue) => {
                            this.setState((prevState) => ({
                              diagnostico_opcional_1: {
                                ...prevState.diagnostico_opcional_1,
                                nombre: newInputValue,
                              },
                            }));
                          }}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              style={{ marginTop: 4 }}
                              InputProps={{
                                ...params.InputProps,
                                startAdornment: (
                                  <>
                                    <InputAdornment position="start">
                                      <i
                                        className="fas fa-notes-medical"
                                        style={{ fontSize: "25px" }}
                                      ></i>
                                    </InputAdornment>
                                    {params.InputProps.startAdornment}
                                  </>
                                ),
                              }}
                              label="Diagnóstico opcional 1"
                            />
                          )}
                        />
                      </Grid>
                    </Box>
                    <Box p={1}>
                      <Grid item xs={4}>
                        <Autocomplete
                          id="diagnostico_opcional_2"
                          options={this.state.diagnosticos}
                          getOptionLabel={(option) => option.nombre}
                          style={{ minWidth: 300, maxWidth: 300 }}
                          value={this.state.diagnostico_opcional_2.id}
                          inputValue={this.state.diagnostico_opcional_2.nombre}
                          onChange={this.cambiarDiagOpcional2}
                          onInputChange={(event, newInputValue) => {
                            this.setState((prevState) => ({
                              diagnostico_opcional_2: {
                                ...prevState.diagnostico_opcional_2,
                                nombre: newInputValue,
                              },
                            }));
                          }}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              style={{ marginTop: 4 }}
                              InputProps={{
                                ...params.InputProps,
                                startAdornment: (
                                  <>
                                    <InputAdornment position="start">
                                      <i
                                        className="fas fa-notes-medical"
                                        style={{ fontSize: "25px" }}
                                      ></i>
                                    </InputAdornment>
                                    {params.InputProps.startAdornment}
                                  </>
                                ),
                              }}
                              label="Diagnóstico opcional 2"
                            />
                          )}
                        />
                      </Grid>
                    </Box>
                  </Box>
                </Card>
              </Grid>
              <Grid style={{ marginBottom: 0, paddingBottom: 0 }} item xs={12}>
                <Card id="select_tipo" variant="outlined">
                  <Alert
                    style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}
                    variant="filled"
                    severity="warning"
                  >
                    El <strong>tipo de estudio</strong> no se puede modificar
                  </Alert>
                  <Typography
                    style={{
                      fontSize: 15,
                      paddingTop: "15px",
                      paddingLeft: "15px",
                      paddingRight: "15px",
                    }}
                    color="textSecondary"
                    gutterBottom
                  >
                    Tipo
                  </Typography>
                  <Box
                    display="flex"
                    flexDirection="row"
                    flexWrap="wrap"
                    p={2}
                    m={1}
                    bgcolor="background.paper"
                  >
                    <Box flexGrow={1}>
                      <Grid item xs={4}>
                        <FormControl
                          style={{
                            minWidth: 300,
                            maxWidth: 300,
                          }}
                          error={this.state.errores.tipo_estudio}
                        >
                          <InputLabel id="">Tipo de estudio *</InputLabel>
                          <Select
                            startAdornment={
                              <InputAdornment position="start">
                                <i
                                  className="fas fa-book-medical"
                                  style={{ fontSize: "25px" }}
                                ></i>
                              </InputAdornment>
                            }
                            MenuProps={{ disableScrollLock: true }}
                            id="tipo"
                            name="tipo"
                            autoWidth={true}
                            labelId="tipo"
                            value={datosEstudio.tipo.nombre}
                            onChange={this.cambiarTipoEstudio}
                            children={armarSelectTipos()}
                            readOnly={true}
                            disabled={true}
                          ></Select>
                        </FormControl>
                      </Grid>
                    </Box>
                  </Box>
                </Card>
              </Grid>
              <Grid item xs={12}>
                <Card id="cardEspecificos" variant="outlined">
                  <Typography
                    style={{
                      fontSize: 15,
                      paddingTop: "15px",
                      paddingLeft: "15px",
                      paddingRight: "15px",
                    }}
                    color="textSecondary"
                    gutterBottom
                  >
                    Datos específicos del tipo de estudio
                  </Typography>
                  <Collapse in={this.state.errores.errorAlertaEspecifico}>
                    <Alert
                      style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}
                      variant="filled"
                      severity="error"
                    >
                      Por favor, completá los campos marcados en rojo
                    </Alert>
                  </Collapse>

                  {this.state.tipo.nombre === "" ? (
                    <Alert style={{ margin: 15 }} severity="warning">
                      Aún no has seleccionado un Tipo de Estudio
                    </Alert>
                  ) : null}
                  {this.state.tipo.nombre === "basico" ? (
                    <EstudioBasico
                      errorConclusion={this.state.errores.conclusion}
                      data={this.props.estudio.atributos}
                    />
                  ) : this.state.tipo.nombre === "disincronia" ? (
                    <EstudioDisincronia
                      errorConclusion={this.state.errores.conclusion}
                      data={this.props.estudio.atributos}
                    />
                  ) : this.state.tipo.nombre === "salina_agitada" ? (
                    <EstudioSalinaAgitada
                      id_conclusion_predefinida={
                        this.state.id_conclusion_predefinida
                      }
                      cambiarConclusionPredefinida={this.cambiarConclusionPredefinida.bind(
                        this
                      )}
                      errorEscenario={this.state.errores.escenario}
                      data={this.props.estudio.atributos}
                    />
                  ) : this.state.tipo.nombre === "2d_doppler" ? (
                    <Estudio2dDoppler
                      errorDescripcion={
                        this.state.errores.descripcion_2dDoppler
                      }
                      errorConclusion={this.state.errores.conclusion}
                      data={this.props.estudio.atributos}
                      funcListo={this.camposEspecificosListosChild.bind(this)}
                    />
                  ) : this.state.tipo.nombre === "congenitas" ? (
                    <EstudioCongenitas
                      data={{
                        errorSitus: this.state.errores.situs,
                        errorAV: this.state.errores.auriculo_ventricular,
                        errorVA: this.state.errores.ventriculo_arterial,
                        errorConclusion: this.state.errores.conclusion,
                        errorDescripcion: this.state.errores
                          .descripcion_congenitas,

                        datoSitus: this.state.situs,
                        datosAV: this.state.auriculo_ventricular,
                        datosVA: this.state.ventriculo_arterial,

                        funcSitus: this.cambiarSitus.bind(this),
                        funcAV: this.cambiarAV.bind(this),
                        funcVA: this.cambiarVA.bind(this),
                        creando: false,
                        datosEstudio: this.props.estudio.atributos,
                      }}
                    />
                  ) : this.state.tipo.nombre === "transesofagico" ? (
                    <EstudioTransesofagico
                      errorDescripcion={
                        this.state.errores.descripcion_transesofagico
                      }
                      errorConclusion={this.state.errores.conclusion}
                      data={this.props.estudio.atributos}
                    />
                  ) : this.state.tipo.nombre !== "" ? (
                    <EstudioBasico
                      errorConclusion={this.state.errores.conclusion}
                      data={this.props.estudio.atributos}
                    />
                  ) : null}
                </Card>
              </Grid>
              <Grid item xs={12}>
                <Button
                  style={{
                    backgroundColor: "dodgerblue",
                    color: "white",
                    fontSize: "15px",
                  }}
                  variant="contained"
                  size="large"
                  onClick={() => {
                    if (
                      this.noFaltanDatosEspecificos() &&
                      this.noFaltanDatosGenerales()
                    ) {
                      this.setState({ abrirModal: true });
                    }
                  }}
                  endIcon={
                    <i style={{ fontSize: "18px" }} className="fas fa-save"></i>
                  }
                >
                  Guardar Cambios
                </Button>
              </Grid>
            </Grid>
          </Grow>

          <ModalPaciente
            abrirModal={this.state.abrirModal}
            cerrarModal={this.cerrarModal}
            mensaje={this.state.mensaje}
            cargandoAccion={this.state.cargandoAccion}
            icon={this.state.icon}
            iconColor={this.state.iconColor}
            botones={this.state.botonesModal}
          />
        </div>
      );
    }
  }
}
