import React, { Component } from "react";
import ListadoPacientes from "./ListadoPacientes";
import ContenedorSeccion from "../otros/ContenedorSeccion";
import CrearYBuscarPaciente from "./CrearYBuscarPaciente";
import PersonIcon from "@material-ui/icons/Person";
import Divider from "@material-ui/core/Divider";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

class Pacientes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pacientes: [],
      cantPacientes: "",
      breadcrumb: [
        {
          pathname: "/pacientes",
          state: "",
          texto: "Pacientes",
          icon: <PersonIcon />,
        },
      ],
      porPagina: 10,
      paginaActual: 1,
      pacientesCargados: false,
      avisoPacientesPorPagina: false,
      usarUrl: "",
      apellidoFiltrado: "",
      fechaNacFiltrada: "",

      hayPacientes: true,
    };

    this.actualizarPacientes = this.actualizarPacientes.bind(this);
    this.setPorPagina = this.setPorPagina.bind(this);
    this.changePagina = this.changePagina.bind(this);
    this.cerrarAvisoPacientesPorPagina = this.cerrarAvisoPacientesPorPagina(
      this
    );
  }

  // Para pasarle el breadcrumb al header
  componentDidMount() {
    this.props.actualizarHeader({
      breadcrumb: this.state.breadcrumb,
    });

    // Me traigo los pacientes de la API
    this.fetchItems(this.state.paginaActual, this.state.porPagina);
  }

  // Cuando cambio la cant de entradas por página
  setPorPagina(cant) {
    this.setState(
      {
        pacientesCargados: false,
        avisoPacientesPorPagina: true,
        porPagina: cant,
        paginaActual: 1,
      },
      () => {
        // Vuelvo a hacer la llamada a la API con la cant especificada
        this.fetchItems(this.state.paginaActual, this.state.porPagina);
      }
    );
  }

  // Cuando cambio la página
  changePagina(pagina) {
    this.setState(
      {
        pacientesCargados: false,
        paginaActual: pagina,
      },
      () => {
        // Hago la llamada a la API
        this.fetchItems(this.state.paginaActual, this.state.porPagina);
      }
    );
  }

  // Este método hace la llamada a la API
  // y al final llama a un método que actualiza los pacientes

  // Recibe como parámetros el nro de página actual
  // y la cant por página
  fetchItems = async (pagina, porPagina) => {
    /*
            Si estoy en una búsqueda de apellidos
            o fechas de nacimiento o ambos,
            entonces no pido los pacientes en general,
            uso la URL que me pasa el componente
            CrearYBuscarPaciente y que la tengo en el estado
        */

    // La URL con filtros
    var url = this.state.usarUrl + "&page=" + pagina + "&perPage=" + porPagina;

    if (this.state.usarUrl === "") {
      // La URL normal, sin filtros
      url =
        "http://" +
        process.env.REACT_APP_API_IP +
        "/api/paciente/all?" +
        "page=" +
        pagina +
        "&perPage=" +
        porPagina;
    }
    await fetch(url, {
      headers: new Headers({
        Authorization:
          "Bearer " +
          document.cookie.replace(
            /(?:(?:^|.*;\s*)token\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          ),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 401) {
          localStorage.setItem("alert", "Expired/Invalid JWT Token");
          document.cookie =
            "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
          window.location.href = "/user-pages/login-1";
        } else if (data.status === 400) {
          this.setState({
            hayPacientes: false,
          });
        } else {
          // Llamo al método que actualiza los pacientes
          this.actualizarPacientes({
            pacientes: data.body.pacientes,
            totalEncontrados: data.body.total,
          });
          // Guardo la cant total de pacientes (con o sin filtro, depende)
          this.setState({
            cantPacientes: data.body.total,
          });
        }
      });
  };

  /*
       Este método actualiza el estado de pacientes,
       el cual es pasado al componente ListadoPacientes
       para que renderice la tabla.

       A este método le puede llegar un objeto
       {
           pacientes: [los pacientes],
           totalEncontrados: [total],
           usarUrl: [url con filtro],
           apellidoFiltrado: [si filtré apellido el apellido],
           fechaNacFiltrada: [si filtré fecha la fecha]
       }
       o también null o undefined

       Este método es llamado por este mismo componente
       y por CrearYBuscarPaciente cuando se aplicó algún filtro
    */
  actualizarPacientes(pacientes) {
    console.log(pacientes);
    if (pacientes && pacientes.pacientes) {
      var url = this.state.usarUrl;
      /*
                Chequeo si me vino una url, 
                o sea filtré por apellido o fecha de 
                nacimiento, o ambas
            */
      if (pacientes.usarUrl || url !== "") {
        if (pacientes.usarUrl) {
          url = pacientes.usarUrl;
          this.setState({
            paginaActual: 1,
          });
        }
        // seteo ese filtro para mostrarlo en el
        // title de la tabla
        if (pacientes.apellidoFiltrado || pacientes.fechaNacFiltrada) {
          this.setState({
            apellidoFiltrado: pacientes.apellidoFiltrado,
            fechaNacFiltrada: pacientes.fechaNacFiltrada,
          });
        }
      } else {
        this.setState({
          apellidoFiltrado: "",
          fechaNacFiltrada: "",
        });
      }
      /*
                Esta es la parte CLAVE, donde se actualizan
                realmente los pacientes que después le llegan al
                listado.
            */
      this.setState(
        {
          pacientes: pacientes.pacientes,
          cantPacientes: pacientes.totalEncontrados,
          usarUrl: url,
        },
        () => {
          this.setState({
            pacientesCargados: true,
          });
        }
      );
    } else {
      /*
            Si me vino null o undefined hago un fetch
            sin filtro con la página 1
        */
      this.setState(
        {
          usarUrl: "",
          paginaActual: 1,
        },
        () => {
          this.fetchItems(this.state.paginaActual, this.state.porPagina);
        }
      );
    }
  }

  // Snackbar
  cerrarAvisoPacientesPorPagina() {
    this.setState({
      avisoPacientesPorPagina: false,
    });
  }

  render() {
    return (
      <div>
        <ContenedorSeccion
          nombre="Listado de pacientes"
          descripcion="Podés crear, editar, eliminar un paciente, o incluso ver su historia clínica y agregarle un nuevo estudio"
          componentes={[
            <CrearYBuscarPaciente
              actualizarPacientes={this.actualizarPacientes}
              porPagina={this.state.porPagina}
            />,
            <Divider />,
            <ListadoPacientes
              pacientes={this.state.pacientes}
              cantPacientes={this.state.cantPacientes}
              setPorPagina={this.setPorPagina}
              porPagina={this.state.porPagina}
              paginaActual={this.state.paginaActual}
              changePagina={this.changePagina}
              apellidoFiltrado={this.state.apellidoFiltrado}
              fechaNacFiltrada={this.state.fechaNacFiltrada}
              hayPacientes={this.state.hayPacientes}
            />,
          ]}
        />
        {/* Dibujito de carga*/}
        <Snackbar
          open={!this.state.pacientesCargados && this.state.hayPacientes}
        >
          <Alert variant="filled" severity="info">
            Cargando...
          </Alert>
        </Snackbar>
        <Snackbar
          open={
            this.state.avisoPacientesPorPagina && this.state.pacientesCargados
          }
          autoHideDuration={3000}
          onClose={() => {
            this.setState({ avisoPacientesPorPagina: false });
          }}
        >
          <Alert variant="filled" severity="success">
            Ahora se muestran {this.state.porPagina} pacientes por página
          </Alert>
        </Snackbar>
      </div>
    );
  }
}

export default Pacientes;
