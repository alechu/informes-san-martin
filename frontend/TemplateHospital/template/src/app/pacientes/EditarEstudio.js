import React, { Component } from "react";
import FormularioEditarEstudio from "./FormularioEditarEstudio";
import ContenedorSeccion from "../otros/ContenedorSeccion";
import InfoPacienteSeleccionado from "./InfoPacienteSeleccionado";
import EditIcon from "@material-ui/icons/Edit";
import PersonIcon from "@material-ui/icons/Person";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";

export default class EditarEstudio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      breadcrumb: [
        {
          pathname: "/pacientes",
          state: "",
          texto: "Pacientes",
          icon: <PersonIcon />,
        },
        {
          pathname: "/pacientes/historia-clinica",
          state: { paciente: this.props.location.state.paciente },
          texto: "Historia clínica",
          icon: <LibraryBooksIcon />,
        },
        {
          pathname: "/pacientes/historia-clinica/editar-estudio",
          state: {
            estudio: this.props.location.state.estudio,
            paciente: this.props.location.state.paciente,
          },
          texto: "Editar estudio",
          icon: <EditIcon />,
        },
      ],
    };
  }

  // Para pasarle el breadcrumb al header
  componentDidMount() {
    this.props.actualizarHeader({
      breadcrumb: this.state.breadcrumb,
    });
  }

  render() {
    // Paciente al que le voy a agregar un estudio.
    const paciente = this.props.location.state.paciente;
    const estudio = this.props.location.state.estudio;

    return (
      <ContenedorSeccion
        nombre="Editar estudio"
        descripcion="Podés editar cualquier atributo del estudio seleccionado"
        preComponentes={[<InfoPacienteSeleccionado paciente={paciente} />]}
        componentes={[
          <FormularioEditarEstudio paciente={paciente} estudio={estudio} />,
        ]}
      />
    );
  }
}
