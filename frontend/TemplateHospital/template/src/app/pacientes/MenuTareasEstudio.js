import React, { Component } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Divider from "@material-ui/core/Divider";
import Box from "@material-ui/core/Box";
import { Link } from "react-router-dom";
import Fade from "@material-ui/core/Fade";
import { withStyles } from "@material-ui/core/styles";

//Icons
import DescriptionIcon from "@material-ui/icons/Description";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";


const styles = (theme) => ({
  menuItem: {
    "&:hover": {
      background: "#297ced",
      color: "white",
    },
  },
  menuItemBorrar: {
    color: "red",
    "&:hover": {
      background: "red",
      color: "white",
    },
  },
  menuItemLink: {
    "&:hover": {
      textDecoration: "none",
    },
  },
});

class MenuTareasEstudio extends Component {
  constructor(props) {
    super(props);
    this.abrirTareas = this.abrirTareas.bind(this);
    this.cerrarTareas = this.cerrarTareas.bind(this);

    this.state = {
      anchorEl: null,
    };
  }

  // Abro dropdown
  abrirTareas = (event) => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };
  // Cierro dropdown
  cerrarTareas = () => {
    this.setState({
      anchorEl: null,
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
        <button
          className="btn btn-success"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={this.abrirTareas}
        >
          Tareas
        </button>
        <Menu
          disableScrollLock={true}
          id="menu-tareas"
          anchorEl={this.state.anchorEl}
          keepMounted
          TransitionComponent={Fade}
          open={Boolean(this.state.anchorEl)}
          onClose={this.cerrarTareas}
        >
          <Link
            to={{
              pathname: "/pacientes/historia-clinica/ver-estudio",
              state: {
                estudio: this.props.estudio,
                paciente: this.props.paciente,
              },
            }}
            className={classes.menuItemLink}
          >
            <MenuItem className={classes.menuItem}>
              <Box>
                <DescriptionIcon />
              </Box>
              <Box mx={1}>
                <span>Ver estudio completo</span>
              </Box>
            </MenuItem>
          </Link>
          <Divider />
          <Link
            to={{
              pathname: "/pacientes/historia-clinica/editar-estudio",
              state: {
                estudio: this.props.estudio,
                paciente: this.props.paciente,
              },
            }}
            className={classes.menuItemLink}
          >
            <MenuItem className={classes.menuItem}>
              <Box>
                <EditIcon />
              </Box>
              <Box mx={1}>
                <span>Editar</span>
              </Box>
            </MenuItem>
          </Link>
          <MenuItem
            onClick={() => {
              this.props.funcEliminar(
                this.props.estudio.informacion_general.id
              );
            }}
            className={classes.menuItemBorrar}
          >
            <Box>
              <DeleteIcon />
            </Box>
            <Box mx={1}>
              <span>Eliminar</span>
            </Box>
          </MenuItem>
        </Menu>
      </div>
    );
  }
}
export default withStyles(styles)(MenuTareasEstudio);