import React, { Component } from 'react';
import Divider from '@material-ui/core/Divider';

// Iconos
import PersonIcon from "@material-ui/icons/Person";
import ListIcon from "@material-ui/icons/List";
import ShowChartIcon from "@material-ui/icons/ShowChart";
import HelpIcon from '@material-ui/icons/Help';
import HomeIcon from "@material-ui/icons/Home";

import { withStyles } from '@material-ui/core/styles';

const styles = {
  seccion: {
    margin: '20px 0px',
  },

  icon: {
    color: 'white'
  },

  contenedorTituloSeccion: {
    padding: '5px 25px',
    background: 'linear-gradient(90deg, #1d84f5, #2e79ea)',
    borderRadius: '15px 50px 30px'
  },
  tituloSeccion: {
    color: 'white',
    margin: 0,
    fontSize: 15,
    fontWeight: 500
  },

  contenidoSeccion: {
    padding: '15px 25px',
    fontSize: 14
  }
}

class Home extends Component {
  constructor(props) {
    super(props);
    this.statusChangedHandler = this.statusChangedHandler.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
    this.inputChangeHandler = this.inputChangeHandler.bind(this);

    this.state = {
      breadcrumb: [{
        pathname: '/',
        state: '',
        texto: 'Home',
        icon: <HomeIcon />
      }],
    }
  }

  statusChangedHandler(event, id) {
    const todo = { ...this.state.todos[id] };
    todo.isCompleted = event.target.checked;

    const todos = [...this.state.todos];
    todos[id] = todo;

    this.setState({
      todos: todos
    })
  }

  addTodo(event) {
    event.preventDefault();

    const todos = [...this.state.todos];
    todos.unshift({
      id: todos.length ? todos[todos.length - 1].id + 1 : 1,
      task: this.state.inputValue,
      isCompleted: false

    })

    this.setState({
      todos: todos,
      inputValue: ''
    })
  }

  removeTodo(index) {
    const todos = [...this.state.todos];
    todos.splice(index, 1);

    this.setState({
      todos: todos
    })
  }

  inputChangeHandler(event) {
    this.setState({
      inputValue: event.target.value
    });
  }
  toggleProBanner() {
    document.querySelector('.proBanner').classList.toggle("hide");
  }

  componentDidMount() {
    this.props.actualizarHeader({
      breadcrumb: this.state.breadcrumb
    });
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-sm-12 col-md-12 col-lg-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body" style={{ paddingBottom: "15px" }}>
                <div className="row">
                  <div className="col-md-12">
                    <span
                      style={{
                        fontSize: 17,
                        letterSpacing: 2,
                        fontWeight: '800'
                      }}
                    >
                      ¡Bienvenido!
                    </span>
                    <h3 className="font-weight-medium mb-3" style={{ fontFamily: "Source Sans Pro" }}>
                      <HelpIcon fontSize="large" color="black" />{" "}
                      ¿Cómo está <span style={{ color: '#247ff0', fontWeight: 'bold' }}>organizado</span> este sistema?
                    </h3>
                    <Divider style={{ marginBottom: '15px' }}></Divider>
                    <p style={{ marginTop: 10, fontSize: 15 }}>
                      Todas las tareas del sistema se dividen en tres grandes secciones:{" "}
                      <strong style={{ color: '#19d895' }}>Pacientes</strong>, {" "}
                      <strong style={{ color: '#19d895' }}>Opciones predefinidas</strong> y {" "}
                      <strong style={{ color: '#19d895' }}>Estadísticas</strong>.
                    </p>

                    <div style={styles.seccion}>
                      <div style={styles.contenedorTituloSeccion}>
                        <PersonIcon style={styles.icon} />
                        <span style={styles.tituloSeccion}>
                          {"  "}Pacientes
                        </span>
                      </div>
                      <div style={styles.contenidoSeccion}>
                        <span>En esta sección podrás crear, editar y eliminar tanto <strong>pacientes</strong> como <strong>estudios</strong>.
                        Además, podrás buscar un paciente y ver su <strong>historia clínica completa</strong>, con la
                        posibilidad de armar y descargar un <strong>informe</strong> de un estudio en formato PDF. </span>
                      </div>
                    </div>

                    <div style={styles.seccion}>
                      <div style={styles.contenedorTituloSeccion}>
                        <ListIcon style={styles.icon} />
                        <span style={styles.tituloSeccion}>
                          {"  "}Opciones predefinidas
                        </span>
                      </div>
                      <div style={styles.contenidoSeccion}>
                        <span>En esta sección podrás crear, editar y eliminar <strong>opciones seleccionables</strong> {" "}
                        para la creación de pacientes y estudios: obras sociales, motivos de estudio, tipos de estudio,
                        diagnósticos finales de estudio (tanto los obligatorios como los opcionales), médicos especialistas,
                        descripciones predefinidas y conclusiones predefinidas. </span>
                      </div>
                    </div>

                    <div style={styles.seccion}>
                      <div style={styles.contenedorTituloSeccion}>
                        <ShowChartIcon style={styles.icon} />
                        <span style={styles.tituloSeccion}>
                          {"  "}Estadísticas
                        </span>
                      </div>
                      <div style={styles.contenidoSeccion}>
                        <span>En esta sección podrás visualizar en un gráfico la <strong>cantidad de estudios</strong> registrados en un {" "}
                          <strong>rango de fechas</strong> establecido. Pero, además, tenés la posibilidad de filtrar los estudios en base a
                        distintos campos.</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(Home);