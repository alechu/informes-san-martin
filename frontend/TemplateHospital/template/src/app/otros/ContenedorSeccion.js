import React, { Component } from "react";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Divider, Paper } from "@material-ui/core";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

const styles = () => ({
  separator: {
    color: "white",
  },
});

class ContenedorSeccion extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    // Los componentes que tengo que renderizar
    const componentes = this.props.componentes.map((componente) => {
      return componente;
    });

    // Los componentes para renderizar antes de los otros
    const preComponentes =
      this.props.preComponentes &&
      this.props.preComponentes.map((preComp) => {
        return preComp;
      });

    return (
      <Paper>
        <Row>{preComponentes}</Row>
        {this.props.preComponentes && <Divider></Divider>}
        <Row id="contenedor-componente">
          <Container>
            {/* Renderizo los componentes que me pasaron */}
            {componentes}
          </Container>
        </Row>
      </Paper>
    );
  }
}

export default withStyles(styles)(ContenedorSeccion);
