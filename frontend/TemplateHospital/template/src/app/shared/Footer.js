import React, { Component } from 'react';

class Footer extends Component {
  render () {
    return (
      <footer className="footer">
        <div className="container-fluid">
          <div className="d-sm-flex justify-content-center justify-content-sm-between">
            <span className="text-muted text-center text-sm-left d-block d-sm-inline-block">
              Hospital San Martín de La Plata - Laboratorio de Ecocardiografía
            </span>
            <span className="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Desarrollado por la Facultad de Informática de la UNLP</span>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;