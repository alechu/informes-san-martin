import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { Divider } from "@material-ui/core"

//Icons
import PersonIcon from "@material-ui/icons/Person";
import ListIcon from "@material-ui/icons/List";
import ShowChartIcon from "@material-ui/icons/ShowChart";
import HomeIcon from "@material-ui/icons/Home";

class Sidebar extends Component {
  state = {};

  toggleMenuState(menuState) {
    if (this.state[menuState]) {
      this.setState({ [menuState]: false });
    } else if (Object.keys(this.state).length === 0) {
      this.setState({ [menuState]: true });
    } else {
      Object.keys(this.state).forEach((i) => {
        this.setState({ [i]: false });
      });
      this.setState({ [menuState]: true });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged();
    }
  }

  onRouteChanged() {
    document.querySelector("#sidebar").classList.remove("active");
    Object.keys(this.state).forEach((i) => {
      this.setState({ [i]: false });
    });

    const dropdownPaths = [
      { path: "/basic-ui", state: "basicUiMenuOpen" },
      { path: "/form-elements", state: "formElementsMenuOpen" },
      { path: "/pacientes", state: "pacientesMenuOpen" },
      { path: "/opciones-predefinidas", state: "opcionesPredefinidasMenuOpen" },
      { path: "/icons", state: "iconsMenuOpen" },
      { path: "/estadisticas", state: "estadisticasMenuOpen" },
      { path: "/user-pages", state: "userPagesMenuOpen" }
    ];

    dropdownPaths.forEach((obj) => {
      if (this.isPathActive(obj.path)) {
        this.setState({ [obj.state]: true });
      }
    });
  }
  render() {
    return (
      <nav className="sidebar sidebar-offcanvas" id="sidebar">
        <div className="text-center sidebar-brand-wrapper d-flex align-items-center mysidebar">
          <a className="sidebar-brand brand-logo" href="index.html">
            <img src={require("../../assets/images/logo.svg")} alt="logo" />
          </a>
          <a className="sidebar-brand brand-logo-mini pt-3"
            href="index.html">
            <img
              src={require("../../assets/images/logo-mini.svg")}
              alt="logo"
            />
          </a>
          <div className="sidebar-brand brand-logo"
            style={{
              textAlign: 'left',
              fontSize: 12
            }}>
            <p
              style={{
                lineHeight: 1.2,
                margin: '5px 0px 7px 0',
                fontWeight: 'bold',
                color: '#297ced'
              }}
            >
              Hospital San Martín
            </p>
            <span
              style={{
                display: 'block',
                lineHeight: 1.2,
                margin: '0px 0px 7px 0px'
              }}
            >
              Laboratorio de <strong>Ecocardiografía</strong>
            </span>
          </div>
        </div>

        <Divider style={{ margin: '0px 20px' }} />
        <ul className="nav">
          <li
            className={
              this.isPathActive("/") ? "nav-item active" : "nav-item"
            }
          >
            <Link className="nav-link" to="/dashboard">
              <HomeIcon style={{ marginRight: "7px" }} />
              <span className="menu-title">Pantalla principal</span>
            </Link>
          </li>
          <li
            className={
              this.isPathActive("pacientes") ? "nav-item active" : "nav-item"
            }
          >
            <Link className="nav-link" to="/pacientes">
              <PersonIcon style={{ marginRight: "7px" }} />
              <span className="menu-title">Pacientes</span>
            </Link>
          </li>
          <li
            className={
              this.isPathActive("opciones-predefinidas")
                ? "nav-item active"
                : "nav-item"
            }
          >
            <Link className="nav-link" to="/opciones-predefinidas">
              <ListIcon style={{ marginRight: "7px" }} />
              <span className="menu-title">Opciones predefinidas</span>
            </Link>
          </li>
          <li
            className={
              this.isPathActive("estadisticas")
                ? "nav-item active"
                : "nav-item"
            }
          >
            <Link className="nav-link" to="/estadisticas">
              <ShowChartIcon style={{ marginRight: "7px" }} />
              <span className="menu-title">Estadísticas</span>
            </Link>
          </li>
        </ul>
      </nav>
    );
  }

  isPathActive(path) {
    if (this.props.location.pathname === "/") {
      return this.props.location.pathname === path;
    }
    else {
      return this.props.location.pathname
      .slice(1, this.props.location.pathname.length)
      .startsWith(path)
    }
  }

  componentDidMount() {
    this.onRouteChanged();
    // add className 'hover-open' to sidebar navitem while hover in sidebar-icon-only menu
    const body = document.querySelector("body");
    document.querySelectorAll(".sidebar .nav-item").forEach((el) => {
      el.addEventListener("mouseover", function () {
        if (body.classList.contains("sidebar-icon-only")) {
          el.classList.add("hover-open");
        }
      });
      el.addEventListener("mouseout", function () {
        if (body.classList.contains("sidebar-icon-only")) {
          el.classList.remove("hover-open");
        }
      });
    });
  }
}

export default withRouter(Sidebar);
