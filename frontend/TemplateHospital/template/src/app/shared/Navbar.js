import React, { Component } from "react";
import { Dropdown } from "react-bootstrap";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Box from "@material-ui/core/Box";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

const styles = (theme) => ({
  separator: {
    color: "white",
  },
  link: {
    color: "white",
    fontWeight: "600",
    "&:hover": {
      color: "#1d2fa4",
      textDecoration: "none",
    },
  },
});

class Navbar extends Component {
  constructor(props) {
    super(props);
  }
  toggleOffcanvas() {
    document.querySelector(".sidebar-offcanvas").classList.toggle("active");
  }

  cerrarSesion() {
    localStorage.setItem("alert", "Sesión cerrada exitosamente");
    document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    window.location.href = "/user-pages/login-1";
  }

  render() {
    const { classes } = this.props;

    return (
      <nav className="navbar col-lg-12 col-12 p-lg-0 fixed-top d-flex flex-row">
        <div className="navbar-menu-wrapper ">
          <div>
            <Box
              display="flex"
              flexDirection="row"
              justifyContent="space-between"
            >
              <Box display="flex" justifyContent="flex-start">
                <a
                  className="navbar-brand brand-logo-mini align-self-center d-lg-none"
                  href="!#"
                  onClick={(evt) => evt.preventDefault()}
                >
                  <img
                    src={require("../../assets/images/logo-mini.svg")}
                    alt="logo"
                  />
                </a>

                <Box display="flex">
                  <button
                    className="navbar-toggler navbar-toggler align-self-center"
                    style={{ fontSize: 22 }}
                    type="button"
                    onClick={() => {
                      document.body.classList.toggle("sidebar-icon-only");
                      document
                        .getElementsByClassName("mysidebar")[0]
                        .classList.toggle("mysidebar-sin-margen");
                    }}
                  >
                    <i className="mdi mdi-menu"></i>
                  </button>
                </Box>

                <Box display="flex">
                  <div style={{ padding: "20px", display: "flex" }}>
                    <strong>¿Dónde estoy?</strong>
                    <Breadcrumbs
                      style={{ marginLeft: 20 }}
                      separator=">"
                      className={classes.separator}
                    >
                      {this.props.breadcrumb.map((pagina) => (
                        <Link
                          key={pagina.pathname}
                          className={classes.link}
                          to={{
                            pathname: pagina.pathname,
                            state: pagina.state,
                          }}
                        >
                          {pagina.icon} {pagina.texto}
                        </Link>
                      ))}
                    </Breadcrumbs>
                  </div>
                </Box>

                <Box display="flex">
                  <button
                    className="navbar-toggler navbar-toggler-right d-lg-none align-self-center"
                    type="button"
                    onClick={this.toggleOffcanvas}
                  >
                    <span className="mdi mdi-menu"></span>
                  </button>
                </Box>
              </Box>
              <Box display="flex" justifyContent="flex-end">
                <ul className="navbar-nav navbar-nav-right ml-lg-auto">
                  <li className="nav-item nav-profile border-0">
                    <Dropdown alignRight>
                      <Dropdown.Toggle className="nav-link count-indicator bg-transparent">
                        <span className="profile-text">
                          <strong style={{ fontSize: 15 }}>Operador</strong>
                        </span>
                        <i
                          style={{ fontSize: 25, color: "black" }}
                          className="fas fa-user-md"
                        ></i>
                      </Dropdown.Toggle>
                      <Dropdown.Menu className="preview-list navbar-dropdown pb-3">
                        <br></br>
                        <Dropdown.Item
                          className="dropdown-item preview-item d-flex align-items-center border-0"
                          onClick={() => this.cerrarSesion()}
                        >
                          Cerrar Sesión
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </li>
                </ul>
              </Box>
            </Box>
          </div>
        </div>
      </nav>
    );
  }
}

export default withStyles(styles)(Navbar);
